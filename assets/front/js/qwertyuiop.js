var halal = {
    dologin : function(){
        $.ajax({
            type: "POST",
            url: BASE_URL + "home/dologin",
            data: $("#login-form").serialize(),
            dataType: "TEXT",
            success: function(response)
            {
               if(response == 1){
                   $('#login_success').show(300);
                   $('#login_error').hide(300);
                   window.location.href = BASE_URL;
               }else{
                   $('#login_error').show(300);
               }
            }
        });
        return false;
    },
    checkout_login : function(){
        
        var email = $('#checkemail').val();
        var pass = $('#checkpass').val();
        
        $.ajax({
            type: "POST",
            url: BASE_URL + "home/dologin",
            data: {'email':email, 'password': pass},
            dataType: "TEXT",
            success: function(response)
            {
               if(response == 1){
                   $('#login_success').show(300);
                   $('#login_error').hide(300);
                   window.location.reload();
               }else{
                   $('#login_error').show(300);
               }
            }
        });
        return false;
    },
    signup : function(){
        //first validation of required field and other's
        var name = $('#signame').val();
        if(name ==''){$('#signame').css({"border-color": "red"}); return false;}else{$('#signame').css({"border-color": "green"});}
        
        var email = $('#sigemail').val();
        if(email ==''){$('#sigemail').css({"border-color": "red"}); return false;}else{$('#sigemail').css({"border-color": "green"});}
        if(!validateEmail(email)){$('#sigemail').css({"border-color": "red"}); return false;}else{$('#sigemail').css({"border-color": "green"});}
        var phone = $('#sigphone').val();
        if(phone ==''){$('#sigphone').css({"border-color": "red"}); return false;}else{$('#sigphone').css({"border-color": "green"});}
        
        var password = $('#sigpassword').val();
        var conpass  = $('#sigconpass').val();
        if(password != '' && password != conpass){
            $('#sigconpassalert').show(300); return false;
        }else{$('#sigconpassalert').hide(300);}
        
        var address  = $('#sigaddress').val();
        if(address ==''){$('#sigaddress').css({"border-color": "red"}); return false;}else{$('#sigaddress').css({"border-color": "green"});}
        
        $.ajax({
            type: "POST",
            url: BASE_URL + "home/signup_pro",
            data: $("#signup-form").serialize(),
            dataType: "TEXT",
            success: function(response)
            {
                if(response == 2){
                    $('#existemail').show(300);
                }
               if(response == 1){
                   window.location.href = BASE_URL;
               }else{
                  
               }
            }
        });
        return false;
    },
    addToCart : function(productId){
        $('#wait_'+productId).show(300);
        $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/addtocart",
            data: {'productId':productId},
            dataType: "TEXT",
            success: function(response)
            {
                var responseText = jQuery.parseJSON(response);
                $('#minimart_content').html(responseText.mini_html);
                $('.cart-number').html(responseText.cart_number);
                halal.mini_cart_content();
                $('#wait_'+productId).hide(300);
            }
        });
        return false;
    },
    remove_cart : function(rowid){
        $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/delete_from_cart/",
            data: {'rowid':rowid},
            dataType: "TEXT",
            success: function(response)
            {
                $('.cart-number').html(response);
                halal.view_cart_content();
                halal.mini_cart_content();
                halal.checkout_calculation();
            }
        });
        return false;
    },
    qtyplus: function(rowid){
        $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/qtyplus/",
            data: {'rowid':rowid},
            dataType: "TEXT",
            success: function(response)
            {
                $('.cart-number').html(response);
                halal.view_cart_content();
                halal.mini_cart_content();
                halal.checkout_calculation();
            }
        });
        return false;
    },
    qtyminus: function(rowid){
        $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/qtyminus/",
            data: {'rowid':rowid},
            dataType: "TEXT",
            success: function(response)
            {
                $('.cart-number').html(response);
                halal.view_cart_content();
                halal.mini_cart_content();
                halal.checkout_calculation();
            }
        });
        return false;
    },
   view_cart_content: function(){
    $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/view_cart_content/",
            dataType: "html",
            success: function(response)
            {
                $('#shop-cart-section').html(response);
            }
        });
},
mini_cart_content: function(){
    $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/mini_cart_content/",
            dataType: "html",
            success: function(response)
            {
                $('.minimart_content').html(response);
            }
        });
},
checkout_calculation: function(){
    $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/checkout_calculation/",
            dataType: "html",
            success: function(response)
            {
                $('.checkout_calculation').html(response);
            }
        });
},

qty_update: function(qty,rowid){
    $.ajax({
            type: "POST",
            url: BASE_URL + "cartprocess/update_cart_qty/",
            data: {'rowid':rowid,'qty':qty.value},
            dataType: "TEXT",
            success: function(response)
            {
                halal.view_cart_content();
                halal.mini_cart_content();
            }
        });
},
coupon_apply: function(){
    $('#coupon_wait').show('slow');
    var code = $('#coupon_val').val();
    if(code ==''){
        $('#invalid_coupon').show('slow');
        $('#coupon_wait').hide('slow');
        return false;
    }
    $.ajax({
        url: BASE_URL + "cartprocess/coupon_apply/" + code,
        success: function (data)
        {
            if (data == 0)
            {
                $('#invalid_coupon').show('slow');
                alert(data);
            } else {
                location.reload();
            }
            $('#coupon_wait').hide('slow');
        }
    });
}
}

function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

$(document).ready(function() {
    $('#same_as').change(function() {
        if (this.checked) {
            $('#shipping_address').fadeOut('slow');
        }
        else {
            $('#shipping_address').fadeIn('slow');
        }

    });

});