-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2017 at 04:15 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `halal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `login_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `roule` enum('admin','sales','operator','accounce') COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `last_visit` date NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `module_access` varchar(555) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `login_id`, `roule`, `type`, `phone`, `address`, `city`, `create_at`, `date`, `last_visit`, `email`, `password`, `module_access`, `status`) VALUES
(1, 'Halal Beef', '', 'admin', 1, 0, 0, 0, '2017-03-08 16:14:50', '0000-00-00', '0000-00-00', 'admin@halal.com', '37055fa4902360b7b618af6e91b2811f6eab9ece', '', 0),
(2, 'A K M Arifuzzman', '', 'admin', 2, 0, 0, 0, '2017-03-08 17:00:56', '0000-00-00', '0000-00-00', 'akmarifuzzaman@hotmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '', 0),
(3, 'asdf', '', 'admin', 1, 2147483647, 0, 0, '2017-03-08 18:40:01', '2017-03-08', '0000-00-00', '', '3ca4297f082595c480871657e1a7091b', '{"from":["category","coupon","dashboards","discount","Inventory","order","payment","product","site_configure","slideshow","supplier","user_management"],"to":null}', 0),
(4, 'hasan', '', 'admin', 1, 123456789, 0, 0, '2017-03-08 18:40:38', '2017-03-08', '0000-00-00', 'hasan@gmail.com', '96e79218965eb72c92a549dd5a330112', '{"from":null,"to":["category","coupon","dashboards","discount","Inventory","order","payment","product","site_configure","slideshow","supplier","user_management"]}', 0),
(5, 'hasan', '', 'admin', 1, 123456789, 0, 0, '2017-03-08 18:41:43', '2017-03-08', '0000-00-00', 'hasan@gmail.com', '96e79218965eb72c92a549dd5a330112', '{"from":null,"to":["category","coupon","dashboards","discount","Inventory","order","payment","product","site_configure","slideshow","supplier","user_management"]}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `delivery_cost` float NOT NULL,
  `delivery_time` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `default` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name`, `postcode`, `delivery_cost`, `delivery_time`, `status`, `default`) VALUES
(1, 'new jercy', '1234', 5, 'same_day', 1, 0),
(2, 'New work', 'e323', 1, 'same_day', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(555) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `slug`, `description`, `image`, `create_by`, `create_at`, `update_at`, `status`, `sort_order`) VALUES
(11, 0, 'Neck', '', 'necj', 'fb92fbc51d0d9b3b68b6fae85ee6da08.png', 0, '2017-03-20 11:25:36', '0000-00-00 00:00:00', '1', 10),
(12, 0, 'Fore Rib', '', 'Fore Rib', '950c539ac1e631a194c1acd9e6e8032d.png', 0, '2017-03-20 11:26:09', '0000-00-00 00:00:00', '1', 10),
(13, 0, 'rump', '', 'rump', '87433325d0fb37d62cb0e7cca3d2544a.png', 0, '2017-03-20 11:26:26', '0000-00-00 00:00:00', '1', 10),
(14, 0, 'Top Side-Sliver Side', '', 'Top Side / Sliver Side', 'b63f141b61e3054205f76b57269bf6e5.png', 0, '2017-03-20 11:26:59', '0000-00-00 00:00:00', '1', 10),
(15, 0, 'Thick Rib', '', 'Thick Rib', '09f3068d8a379d58effe6745cb20eb78.png', 0, '2017-03-20 11:27:36', '0000-00-00 00:00:00', '1', 10),
(16, 0, 'Thin Flank', '', 'Thin Flank', 'a3bfc8e93cc3abb4e41492988d12e5b9.png', 0, '2017-03-20 11:28:01', '0000-00-00 00:00:00', '1', 10),
(17, 0, 'Shin', '', 'Shin', 'f0f4b4bd818a1d39226eef949d9ad663.png', 0, '2017-03-20 11:28:22', '0000-00-00 00:00:00', '1', 10),
(18, 0, 'Chuck-Blade', '', 'Chuck / Blade', 'dad2384abbc5f160c953b5fba2736c1c.png', 0, '2017-03-20 11:28:54', '0000-00-00 00:00:00', '1', 10),
(19, 0, 'Sirlion', '', 'Sirlion', '83726fd08dae1160a0bc7daf3759175a.png', 0, '2017-03-20 11:29:16', '0000-00-00 00:00:00', '1', 10),
(20, 0, 'Fillet', '', 'Fillet', '5b947d7e0041702cbd776b55075449cb.png', 0, '2017-03-20 11:30:08', '0000-00-00 00:00:00', '1', 10),
(21, 0, 'Cold', '', 'Cold', '16d89905dfa14eec2037a11c9c0663ef.png', 0, '2017-03-20 11:31:35', '0000-00-00 00:00:00', '1', 10),
(22, 0, 'Thin Rib', '', 'Thin Rib', '1da4fcb3d47128b7e802a1953e6cbe7f.png', 0, '2017-03-20 11:31:57', '0000-00-00 00:00:00', '1', 10),
(23, 0, 'Thick Flannk', '', 'Thick Flannk', '52549b7c1131c09f49052f779757fc5e.png', 0, '2017-03-20 11:32:23', '0000-00-00 00:00:00', '1', 10),
(24, 0, 'Leg', '', 'Leg', '446132d78967b24815b26d9dbb194218.png', 0, '2017-03-20 11:32:38', '0000-00-00 00:00:00', '1', 10);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `code` varchar(512) NOT NULL,
  `type` enum('p','f') NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `total_amount` decimal(10,0) NOT NULL,
  `max_uses` int(11) NOT NULL,
  `max_user_uses` int(11) NOT NULL,
  `shipping` tinyint(4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `uses_total` int(11) NOT NULL,
  `uses_customer` int(11) NOT NULL,
  `apply_with_discount` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `name`, `code`, `type`, `discount`, `total_amount`, `max_uses`, `max_user_uses`, `shipping`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `apply_with_discount`, `status`, `date`, `modify_date`) VALUES
(3, '6% Discount', 'save6', 'p', '6', '1', 0, 0, 0, '2017-03-15', '2017-04-29', 0, 0, 0, 1, '2017-03-15 00:00:00', '2017-04-25 13:23:55'),
(4, 'xcvzxcv', 'werwer', 'p', '25', '250', 0, 0, 0, '2017-03-16', '2017-03-31', 0, 0, 0, 1, '2017-03-16 00:00:00', '2017-04-19 14:09:04');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_category`
--

CREATE TABLE `coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_history`
--

CREATE TABLE `coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `group` varchar(55) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `newsletter` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `region` varchar(100) NOT NULL,
  `company` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `place` varchar(500) NOT NULL,
  `postcode` varchar(100) NOT NULL,
  `ip` varchar(111) NOT NULL,
  `status` int(11) NOT NULL,
  `agree` int(11) NOT NULL,
  `approved` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `group`, `name`, `email`, `fax`, `image`, `telephone`, `username`, `password`, `newsletter`, `address`, `address_2`, `country`, `region`, `company`, `city`, `place`, `postcode`, `ip`, `status`, `agree`, `approved`, `date`, `modify_date`, `type`) VALUES
(35, '', 'nesar', 'nesar@halal.com', '', '', '01722098765', '', 'e10adc3949ba59abbe56e057f20f883e', 0, 'test', '', '', '', '', 'dhaka', '2', '', '', 1, 1, 0, '0000-00-00 00:00:00', '2017-05-03 05:06:11', 'registration');

-- --------------------------------------------------------

--
-- Table structure for table `customer_payment_info`
--

CREATE TABLE `customer_payment_info` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `card_no` varchar(30) NOT NULL,
  `cvv` varchar(10) NOT NULL,
  `token` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_shipping_address`
--

CREATE TABLE `customer_shipping_address` (
  `shipping_id` int(11) NOT NULL,
  `default` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(222) NOT NULL,
  `region` varchar(111) NOT NULL,
  `country` varchar(100) NOT NULL,
  `city` varchar(55) NOT NULL,
  `place` varchar(500) NOT NULL,
  `postcode` varchar(55) NOT NULL,
  `telephone` varchar(25) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_shipping_address`
--

INSERT INTO `customer_shipping_address` (`shipping_id`, `default`, `customer_id`, `name`, `email`, `region`, `country`, `city`, `place`, `postcode`, `telephone`, `address`, `address2`, `date`, `modify_date`, `status`) VALUES
(1, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:01:00', '0000-00-00', 0),
(2, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:19:01', '0000-00-00', 0),
(3, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:40:45', '0000-00-00', 0),
(4, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:42:55', '0000-00-00', 0),
(5, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:43:45', '0000-00-00', 0),
(6, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:44:31', '0000-00-00', 0),
(7, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:44:47', '0000-00-00', 0),
(8, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 16:45:47', '0000-00-00', 0),
(9, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 17:12:33', '0000-00-00', 0),
(10, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 17:12:56', '0000-00-00', 0),
(11, 0, 34, '', '', '', '', '', '2', '', '789876976', 'some are hereeee', '', '2017-04-28 17:14:31', '0000-00-00', 0),
(12, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-03 05:06:12', '0000-00-00', 0),
(13, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-03 10:03:40', '0000-00-00', 0),
(14, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-03 10:11:39', '0000-00-00', 0),
(15, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-03 10:12:45', '0000-00-00', 0),
(16, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-03 10:40:37', '0000-00-00', 0),
(17, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-03 10:44:57', '0000-00-00', 0),
(18, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-04 09:33:29', '0000-00-00', 0),
(19, 0, 35, '', '', '', '', '', '2', '', '01722098765', 'test', '', '2017-05-04 09:38:04', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_wishlist`
--

CREATE TABLE `customer_wishlist` (
  `id` int(11) NOT NULL COMMENT 'this is foreign key of customer table for track customer wishlist',
  `date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `type` enum('p','f') NOT NULL,
  `apply` varchar(20) NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `total_amount` decimal(10,0) NOT NULL,
  `shipping` tinyint(4) DEFAULT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `uses_total` int(11) DEFAULT NULL,
  `uses_customer` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `modify_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`id`, `name`, `type`, `apply`, `discount`, `total_amount`, `shipping`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date`, `modify_date`) VALUES
(1, 'save2', 'p', 'product', '2', '0', NULL, '2017-03-15', '2017-04-01', NULL, NULL, 1, '0000-00-00', '0000-00-00'),
(2, 'save4', 'p', 'product', '4', '0', NULL, '2017-03-07', '2017-03-30', NULL, NULL, 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `discount_category`
--

CREATE TABLE `discount_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `discount_product`
--

CREATE TABLE `discount_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discount_product`
--

INSERT INTO `discount_product` (`id`, `product_id`) VALUES
(1, 14),
(2, 10),
(2, 11),
(2, 14),
(3, 6),
(3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `email_setting`
--

CREATE TABLE `email_setting` (
  `id` int(11) NOT NULL,
  `protocol` varchar(222) NOT NULL,
  `smtp_host` varchar(222) NOT NULL,
  `smtp_port` varchar(222) NOT NULL,
  `smtp_user` varchar(222) NOT NULL,
  `smtp_pass` varchar(222) NOT NULL,
  `mailtype` varchar(222) NOT NULL,
  `charset` varchar(222) NOT NULL,
  `wordwrap` varchar(222) NOT NULL,
  `from_name` varchar(222) NOT NULL,
  `from_email` varchar(222) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_setting`
--

INSERT INTO `email_setting` (`id`, `protocol`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `mailtype`, `charset`, `wordwrap`, `from_name`, `from_email`, `status`) VALUES
(1, 'smtp', 'mail.halalic.com', '25', 'relay', 'info!@halal', 'html', 'iso-8859-1', 'TRUE', 'Halalic', 'info@halalic.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `phrase_id` int(11) NOT NULL,
  `phrase` longtext COLLATE utf8_unicode_ci NOT NULL,
  `english` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Bangla` longtext COLLATE utf8_unicode_ci,
  `Frence` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`phrase_id`, `phrase`, `english`, `Bangla`, `Frence`) VALUES
(1, 'admin', 'Admin', NULL, NULL),
(2, 'dashboard', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_id` varchar(60) NOT NULL,
  `customer_id` int(9) UNSIGNED DEFAULT NULL,
  `status` enum('','received','in_preparation','shipped','deleted') NOT NULL,
  `ordered_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_type` varchar(20) NOT NULL,
  `shipped_on` datetime NOT NULL,
  `tax` float(10,2) NOT NULL,
  `total` float(10,2) NOT NULL,
  `coupon_code` varchar(111) DEFAULT NULL,
  `coupon_value` decimal(10,0) DEFAULT NULL,
  `discount` decimal(10,0) NOT NULL,
  `item_total` decimal(10,0) NOT NULL,
  `subtotal` float(10,2) NOT NULL,
  `gift_card_discount` float(10,2) NOT NULL,
  `coupon_discount` float(10,2) NOT NULL,
  `shipping_cost` float(10,2) NOT NULL,
  `shipping_track_number` varchar(555) NOT NULL,
  `shipping_address` int(11) NOT NULL,
  `shipping_notes` text NOT NULL,
  `shipping_method` tinytext NOT NULL,
  `notes` text,
  `payment_info` text NOT NULL,
  `payment_type` varchar(10) NOT NULL,
  `payment_status` varchar(10) NOT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `transaction_id`, `customer_id`, `status`, `ordered_on`, `order_type`, `shipped_on`, `tax`, `total`, `coupon_code`, `coupon_value`, `discount`, `item_total`, `subtotal`, `gift_card_discount`, `coupon_discount`, `shipping_cost`, `shipping_track_number`, `shipping_address`, `shipping_notes`, `shipping_method`, `notes`, `payment_info`, `payment_type`, `payment_status`, `phone`, `email`) VALUES
(21, '', 35, 'shipped', '2017-05-03 05:42:18', 'delivery', '2017-05-03 00:00:00', 0.00, 1748.40, 'save6', NULL, '0', '1860', 1860.00, 0.00, 0.00, 0.00, '564567456', 12, '', '', 'test', '', '1', 'COD', NULL, NULL),
(22, '', 35, '', '2017-05-03 10:03:40', 'delivery', '0000-00-00 00:00:00', 0.00, 3158.40, 'save6', '202', '0', '3360', 3360.00, 0.00, 0.00, 0.00, '', 13, '', '', '', '', '1', 'COD', NULL, NULL),
(23, '', 35, '', '2017-05-03 10:11:39', 'delivery', '0000-00-00 00:00:00', 0.00, 338.40, 'save6', '22', '0', '360', 360.00, 0.00, 0.00, 0.00, '', 14, '', '', '', '', '1', 'COD', NULL, NULL),
(24, '', 35, '', '2017-05-03 10:12:46', 'delivery', '0000-00-00 00:00:00', 0.00, 2820.00, 'save6', '180', '0', '3000', 3000.00, 0.00, 0.00, 0.00, '', 15, '', '', '', '', '1', 'COD', NULL, NULL),
(25, '', 35, '', '0000-00-00 00:00:00', 'collection', '0000-00-00 00:00:00', 0.00, 3360.00, NULL, '0', '0', '3360', 3360.00, 0.00, 0.00, 0.00, '', 16, '', '', '', '', '2', 'not_paid', NULL, NULL),
(26, '', 35, 'deleted', '2017-05-03 10:47:05', 'collection', '0000-00-00 00:00:00', 0.00, 3360.00, NULL, '0', '0', '3360', 3360.00, 0.00, 0.00, 0.00, '', 17, '', '', '', '', '2', 'cancel', NULL, NULL),
(27, '', 35, '', '0000-00-00 00:00:00', 'delivery', '0000-00-00 00:00:00', 0.00, 1860.00, NULL, '0', '0', '1860', 1860.00, 0.00, 0.00, 0.00, '', 18, '', '', '', '', '1', 'not_paid', NULL, NULL),
(28, '', 35, '', '0000-00-00 00:00:00', 'delivery', '0000-00-00 00:00:00', 0.00, 1860.00, NULL, '0', '0', '1860', 1860.00, 0.00, 0.00, 0.00, '', 19, '', '', '', '', '1', 'not_paid', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(9) UNSIGNED NOT NULL,
  `rowid` varchar(555) NOT NULL,
  `order_id` int(9) UNSIGNED NOT NULL,
  `product_id` int(9) UNSIGNED NOT NULL,
  `product_name` varchar(555) NOT NULL,
  `product_image` varchar(555) NOT NULL,
  `quantity` int(11) NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `subtotal` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `contents` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `rowid`, `order_id`, `product_id`, `product_name`, `product_image`, `quantity`, `discount`, `subtotal`, `total`, `price`, `contents`) VALUES
(20, 'c51ce410c124a10e0db5e4b97fc2af39', 21, 13, 'No sea takimata', '8a02070a0d92fd322daf01b3a6bb8bb5.png', 1, '0', '1500', '1500', '0', ''),
(21, 'aab3238922bcc25a6f606eb525ffdc56', 22, 14, 'Head Meat', '80adca6d01f0936b380b8aa79118682a.png', 1, '0', '360', '360', '0', ''),
(22, 'aab3238922bcc25a6f606eb525ffdc56', 23, 14, 'Head Meat', '80adca6d01f0936b380b8aa79118682a.png', 1, '0', '360', '360', '0', ''),
(23, 'd3d9446802a44259755d38e6d163e820', 24, 10, 'Beef Meat', '33321168cc4631a44a65c8f2b115fa1c.png', 1, '0', '3000', '3000', '0', ''),
(24, 'aab3238922bcc25a6f606eb525ffdc56', 25, 14, 'Head Meat', '80adca6d01f0936b380b8aa79118682a.png', 1, '0', '360', '360', '0', ''),
(25, 'aab3238922bcc25a6f606eb525ffdc56', 26, 14, 'Head Meat', '80adca6d01f0936b380b8aa79118682a.png', 1, '0', '360', '360', '0', ''),
(26, 'aab3238922bcc25a6f606eb525ffdc56', 27, 14, 'Head Meat', '80adca6d01f0936b380b8aa79118682a.png', 1, '0', '360', '360', '0', ''),
(27, 'aab3238922bcc25a6f606eb525ffdc56', 28, 14, 'Head Meat', '80adca6d01f0936b380b8aa79118682a.png', 1, '0', '360', '360', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(5) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `default` varchar(1) DEFAULT '0',
  `status` varchar(1) NOT NULL DEFAULT '1',
  `configs` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `title`, `description`, `type`, `default`, `status`, `configs`, `date`) VALUES
(1, 'Cash on Delivery', 'COD', 'direct', '1', '1', '{"publish":"1","message":"test"}', '0000-00-00 00:00:00'),
(2, 'paypal Standard', 'paypal payment integration with support all credit card', 'paypal', '0', '1', '{"sandbox":"1","publish":"1","email":"rjs.jahid11@gmail.com","api_username":"jahid","password":"123456","signature":"aasdfasdfa","currency_code":"GB"}', '2017-04-20 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `feature_product` varchar(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `short_description` varchar(555) NOT NULL,
  `long_description` text NOT NULL,
  `sku` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `unit_per_weight` varchar(20) NOT NULL COMMENT '1= pound, 2= packet, 3=pcs',
  `unit_per_qty` int(11) NOT NULL,
  `stock_status` varchar(50) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `unit_price` float(10,2) NOT NULL,
  `sale_price` float(10,2) NOT NULL,
  `tax` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `subtract_stock` varchar(100) NOT NULL DEFAULT '1',
  `minimum_quantity` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `product_status` varchar(20) NOT NULL COMMENT 'like: sale, new, out of sell',
  `default` int(11) NOT NULL,
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `feature_product`, `name`, `slug`, `short_description`, `long_description`, `sku`, `quantity`, `unit_per_weight`, `unit_per_qty`, `stock_status`, `image`, `unit_price`, `sale_price`, `tax`, `date_available`, `weight`, `subtract_stock`, `minimum_quantity`, `sort_order`, `status`, `product_status`, `default`, `viewed`, `date`, `date_modified`) VALUES
(10, 0, '0', 'Beef Meat', '', 'This is Beef Meat short', 'This is Beef Meat long', '1111222333', 1, 'Pound', 1, '', '33321168cc4631a44a65c8f2b115fa1c.png', 5000.00, 3000.00, 6, '0000-00-00', '0.00000000', '1', 1, 5, 1, 'New', 0, 0, '2017-03-26 07:12:54', '0000-00-00 00:00:00'),
(11, 0, '1', 'Special Beef Meat', '', 'Special Beef Meat short', 'Special Beef Meat long', '4656564', 1, 'Pound', 2, '', '5124820882a60f6a8045792ff247ae67.png', 5000.00, 256.00, 6, '0000-00-00', '0.00000000', '1', 1, 0, 1, '', 0, 0, '2017-03-24 05:02:41', '0000-00-00 00:00:00'),
(12, 0, '1', 'Beef Leg meat ', '', 'Beef Leg meat short', 'Beef Leg meat long', '65468484', 1, 'Pound', 1, '', '0cec580a29d7a77714da8bcb4e16377c.png', 800.00, 360.00, 6, '0000-00-00', '0.00000000', '1', 1, 5, 1, '', 0, 0, '2017-03-24 05:02:54', '0000-00-00 00:00:00'),
(13, 0, '1', 'No sea takimata', '', 'No sea takimata short', 'No sea takimata long', '6546416', 1, 'Pound', 2, '', '8a02070a0d92fd322daf01b3a6bb8bb5.png', 3500.00, 1500.00, 6, '0000-00-00', '0.00000000', '1', 1, 6, 1, '', 0, 0, '2017-03-24 05:02:58', '0000-00-00 00:00:00'),
(14, 0, '1', 'Head Meat', '', 'Head Meat short', 'Head Meat long', '6564964', 1, 'Pound', 2, '', '80adca6d01f0936b380b8aa79118682a.png', 4510262.00, 360.00, 6, '0000-00-00', '0.00000000', '1', 1, 8, 1, '', 0, 0, '2017-03-24 05:03:03', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE `product_to_category` (
  `id` int(11) NOT NULL COMMENT 'this is foreign key of product table to relation with category table with category id',
  `category_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_category`
--

INSERT INTO `product_to_category` (`id`, `category_id`, `date`) VALUES
(11, 15, '0000-00-00 00:00:00'),
(11, 16, '0000-00-00 00:00:00'),
(12, 13, '0000-00-00 00:00:00'),
(12, 15, '0000-00-00 00:00:00'),
(12, 16, '0000-00-00 00:00:00'),
(12, 20, '0000-00-00 00:00:00'),
(12, 22, '0000-00-00 00:00:00'),
(12, 24, '0000-00-00 00:00:00'),
(13, 12, '0000-00-00 00:00:00'),
(13, 20, '0000-00-00 00:00:00'),
(13, 21, '0000-00-00 00:00:00'),
(13, 22, '0000-00-00 00:00:00'),
(14, 11, '0000-00-00 00:00:00'),
(14, 12, '0000-00-00 00:00:00'),
(14, 14, '0000-00-00 00:00:00'),
(10, 11, '0000-00-00 00:00:00'),
(10, 12, '0000-00-00 00:00:00'),
(10, 13, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `type`, `description`) VALUES
(1, 'system_name', 'Halal Organic Meat'),
(2, 'system_title', 'Halal Organic Meat'),
(3, 'address', 'Dhaka-1216'),
(4, 'phone', '0123456789'),
(6, 'currency', '$'),
(7, 'system_email', 'admin@gmail.com'),
(11, 'language', 'english'),
(12, 'text_align', 'left-to-right'),
(13, 'system_currency_id', '1'),
(17, 'skin_theme', 'green'),
(18, 'logo_url', './uploads/logo-orange.png');

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(555) NOT NULL,
  `type` enum('P','F') NOT NULL COMMENT 'two type of of rate . it''s fixed and percentange',
  `rate` decimal(10,0) NOT NULL COMMENT 'rate is tax value how percentange or fixed amount will add',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `default` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `name`, `title`, `description`, `type`, `rate`, `date`, `modify_date`, `status`, `default`) VALUES
(5, '1% vat', '1% vat', '1% vat', 'P', '1', '2017-03-19 05:03:51', '0000-00-00', 0, 0),
(6, '2% VAT', '2% VAT', '2% VAT', 'P', '2', '2017-03-19 05:03:51', '0000-00-00', 0, 1),
(7, '1% vat', '1% vat', '', 'P', '1', '2017-03-19 05:04:01', '0000-00-00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(150) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `phone`, `address`, `city`, `country`, `create_at`, `is_active`) VALUES
(1, 'Mahabubur Rahman', '', 'admin@halal.com', '01534710493', 'Mirpur, Dhaka', 'Dhaka', '', '2017-02-26 19:44:50', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(33) NOT NULL,
  `ip` varchar(22) NOT NULL,
  `date` date NOT NULL,
  `action` varchar(111) NOT NULL,
  `comment` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `user_id`, `user_name`, `ip`, `date`, `action`, `comment`) VALUES
(1, 1, 'Halal Beef', '::1', '2017-03-08', 'user', 'Add New Admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `permission` varchar(500) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `permission`, `date`, `status`) VALUES
(1, 'Super Admin', '', '2017-03-08 14:45:00', 1),
(2, 'Admin', '', '2017-03-06 13:41:09', 1),
(3, 'Delivery Man', '', '2017-03-08 14:45:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `web_about_us`
--

CREATE TABLE `web_about_us` (
  `id` int(2) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 NOT NULL,
  `desc` text CHARACTER SET utf8 NOT NULL,
  `img` varchar(15) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about_us`
--

INSERT INTO `web_about_us` (`id`, `title`, `desc`, `img`) VALUES
(1, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua elitr, sed diam nonumy eirmod tempor invidunt ut.</p>\r\n\r\n<blockquote>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</blockquote>\r\n\r\n<p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'about_us.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_faq`
--

CREATE TABLE `web_faq` (
  `id` int(3) NOT NULL,
  `ques` varchar(256) NOT NULL,
  `answ` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_faq`
--

INSERT INTO `web_faq` (`id`, `ques`, `answ`) VALUES
(1, 'Collapsible Group Item #1', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.'),
(2, 'Collapsible Group Item #2', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.'),
(3, 'Collapsible Group Item #3', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.');

-- --------------------------------------------------------

--
-- Table structure for table `web_farm_gallery`
--

CREATE TABLE `web_farm_gallery` (
  `id` int(5) NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `img` varchar(135) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_farm_gallery`
--

INSERT INTO `web_farm_gallery` (`id`, `title`, `img`) VALUES
(1, 'farm01', 'farm01.jpg'),
(2, 'farm2', 'farm2.jpg'),
(3, 'farm3', 'farm3.jpg'),
(4, 'farm4', 'farm4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_general_settings`
--

CREATE TABLE `web_general_settings` (
  `id` int(1) NOT NULL,
  `wlc_text` varchar(128) CHARACTER SET utf8 NOT NULL,
  `order_no` varchar(50) CHARACTER SET utf8 NOT NULL,
  `opera_desc` text CHARACTER SET utf8 NOT NULL,
  `fb` varchar(128) CHARACTER SET utf8 NOT NULL,
  `twt` varchar(128) CHARACTER SET utf8 NOT NULL,
  `y_tube` varchar(128) CHARACTER SET utf8 NOT NULL,
  `g_plus` varchar(128) CHARACTER SET utf8 NOT NULL,
  `logo_img` varchar(30) CHARACTER SET utf8 NOT NULL,
  `more_link` varchar(256) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_general_settings`
--

INSERT INTO `web_general_settings` (`id`, `wlc_text`, `order_no`, `opera_desc`, `fb`, `twt`, `y_tube`, `g_plus`, `logo_img`, `more_link`) VALUES
(1, 'WCOME TO HALAL ORGANIC MEAT SHOP', '0123756789', 'Monday to Thuesday 6:00 - 18:00\r\nFriday to Saturdays 12:00 - 22:00\r\nSundays closed', 'facebook', 'twitter', 'Youtube', 'Google Plus', 'logo.png', 'more_link');

-- --------------------------------------------------------

--
-- Table structure for table `web_offers`
--

CREATE TABLE `web_offers` (
  `id` int(3) NOT NULL,
  `title` varchar(128) NOT NULL,
  `desc` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_offers`
--

INSERT INTO `web_offers` (`id`, `title`, `desc`, `img`) VALUES
(1, 'call to order', '<p>Sed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urnaSed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urna</p>\r\n', 'call_to_order.jpg'),
(2, 'Fast Delivery', '<p>Sed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urnaSed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urna</p>\r\n', 'fast_delivery.jpg'),
(3, 'organic food', '<p>Sed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urnaSed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urna</p>\r\n', 'organic_food.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_our_farm`
--

CREATE TABLE `web_our_farm` (
  `id` int(2) NOT NULL,
  `title` varchar(128) NOT NULL,
  `desc` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_our_farm`
--

INSERT INTO `web_our_farm` (`id`, `title`, `desc`, `img`) VALUES
(1, 'Halal organic farm', '<p>We are certified HALAL facility. All Our animals are humanely treated and naturally fed and raised in a spacious feeding lot.</p>\r\n', 'halal_organic_farm.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_privacy_policy`
--

CREATE TABLE `web_privacy_policy` (
  `id` int(2) NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `desc` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_privacy_policy`
--

INSERT INTO `web_privacy_policy` (`id`, `title`, `desc`) VALUES
(1, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `web_slider`
--

CREATE TABLE `web_slider` (
  `id` int(2) NOT NULL,
  `cap_1` varchar(256) NOT NULL,
  `cap_2` varchar(256) NOT NULL,
  `img` varchar(260) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_slider`
--

INSERT INTO `web_slider` (`id`, `cap_1`, `cap_2`, `img`) VALUES
(1, 'Welcome to Organic 01', 'Halal Meat 01', 'welcome_to_organic_01.jpg'),
(2, 'Welcome to Organic 02', 'Halal Meat 02', 'welcome_to_organic_02.jpg'),
(3, 'Welcome to Organic 03', 'Halal Meat 03', 'welcome_to_organic_03.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_testimonial`
--

CREATE TABLE `web_testimonial` (
  `id` int(3) NOT NULL,
  `cli_name` varchar(128) NOT NULL,
  `cli_comment` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_testimonial`
--

INSERT INTO `web_testimonial` (`id`, `cli_name`, `cli_comment`, `img`) VALUES
(1, 'Josephine Walker 01', '<p>ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'josephine_walker_01.jpg'),
(2, 'Josephine Walker 02', '<p>ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'josephine_walker_02.jpg'),
(3, 'Josephine Walker 03', '<p>ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'josephine_walker_03.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_wlc`
--

CREATE TABLE `web_wlc` (
  `id` int(2) NOT NULL,
  `title1` varchar(128) NOT NULL,
  `title2` varchar(128) NOT NULL,
  `desc` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_wlc`
--

INSERT INTO `web_wlc` (`id`, `title1`, `title2`, `desc`, `img`) VALUES
(1, 'Welcome to', 'Halal organic meat', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. consetetur sadipscing elitr, magna aliquyam erat, sed diam voluptua.', 'wlc.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_category`
--
ALTER TABLE `coupon_category`
  ADD KEY `coupon_id` (`coupon_id`,`category_id`);

--
-- Indexes for table `coupon_history`
--
ALTER TABLE `coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group`);

--
-- Indexes for table `customer_payment_info`
--
ALTER TABLE `customer_payment_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_shipping_address`
--
ALTER TABLE `customer_shipping_address`
  ADD PRIMARY KEY (`shipping_id`);

--
-- Indexes for table `customer_wishlist`
--
ALTER TABLE `customer_wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`,`product_id`),
  ADD KEY `customer_id_2` (`customer_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id_3` (`customer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_category`
--
ALTER TABLE `discount_category`
  ADD KEY `discount_id` (`id`,`category_id`);

--
-- Indexes for table `discount_product`
--
ALTER TABLE `discount_product`
  ADD KEY `discount_id` (`id`,`product_id`);

--
-- Indexes for table `email_setting`
--
ALTER TABLE `email_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`phrase_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_about_us`
--
ALTER TABLE `web_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_faq`
--
ALTER TABLE `web_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_farm_gallery`
--
ALTER TABLE `web_farm_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_general_settings`
--
ALTER TABLE `web_general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_offers`
--
ALTER TABLE `web_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_our_farm`
--
ALTER TABLE `web_our_farm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_privacy_policy`
--
ALTER TABLE `web_privacy_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_slider`
--
ALTER TABLE `web_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_testimonial`
--
ALTER TABLE `web_testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_wlc`
--
ALTER TABLE `web_wlc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `coupon_history`
--
ALTER TABLE `coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `customer_payment_info`
--
ALTER TABLE `customer_payment_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_shipping_address`
--
ALTER TABLE `customer_shipping_address`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `email_setting`
--
ALTER TABLE `email_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `phrase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_about_us`
--
ALTER TABLE `web_about_us`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_faq`
--
ALTER TABLE `web_faq`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_farm_gallery`
--
ALTER TABLE `web_farm_gallery`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `web_general_settings`
--
ALTER TABLE `web_general_settings`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_offers`
--
ALTER TABLE `web_offers`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_our_farm`
--
ALTER TABLE `web_our_farm`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_privacy_policy`
--
ALTER TABLE `web_privacy_policy`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_slider`
--
ALTER TABLE `web_slider`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_testimonial`
--
ALTER TABLE `web_testimonial`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_wlc`
--
ALTER TABLE `web_wlc`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer_wishlist`
--
ALTER TABLE `customer_wishlist`
  ADD CONSTRAINT `customer_wishlist_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `customer_wishlist_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
