-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 07:14 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `halal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `login_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `roule` enum('admin','sales','operator','accounce') COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `last_visit` date NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `module_access` varchar(555) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `login_id`, `roule`, `type`, `phone`, `address`, `city`, `create_at`, `date`, `last_visit`, `email`, `password`, `module_access`, `status`) VALUES
(1, 'Halal Beef', '', 'admin', 1, 0, 0, 0, '2017-03-08 16:14:50', '0000-00-00', '0000-00-00', 'admin@halal.com', '37055fa4902360b7b618af6e91b2811f6eab9ece', '', 0),
(2, 'A K M Arifuzzman', '', 'admin', 2, 0, 0, 0, '2017-03-08 17:00:56', '0000-00-00', '0000-00-00', 'akmarifuzzaman@hotmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '', 0),
(3, 'asdf', '', 'admin', 1, 2147483647, 0, 0, '2017-03-08 18:40:01', '2017-03-08', '0000-00-00', '', '3ca4297f082595c480871657e1a7091b', '{"from":["category","coupon","dashboards","discount","Inventory","order","payment","product","site_configure","slideshow","supplier","user_management"],"to":null}', 0),
(4, 'hasan', '', 'admin', 1, 123456789, 0, 0, '2017-03-08 18:40:38', '2017-03-08', '0000-00-00', 'hasan@gmail.com', '96e79218965eb72c92a549dd5a330112', '{"from":null,"to":["category","coupon","dashboards","discount","Inventory","order","payment","product","site_configure","slideshow","supplier","user_management"]}', 0),
(5, 'hasan', '', 'admin', 1, 123456789, 0, 0, '2017-03-08 18:41:43', '2017-03-08', '0000-00-00', 'hasan@gmail.com', '96e79218965eb72c92a549dd5a330112', '{"from":null,"to":["category","coupon","dashboards","discount","Inventory","order","payment","product","site_configure","slideshow","supplier","user_management"]}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(555) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `slug`, `description`, `image`, `create_by`, `create_at`, `update_at`, `status`, `sort_order`) VALUES
(5, 0, 'jahid', '', 'ia asdf', 'ffc006995bc851c828c36455cb96546a.jpg', 1, '2017-02-25 00:38:13', '0000-00-00 00:00:00', '1', 0),
(8, 5, 'FARJANA', '', 'i am FARJANA', 'e69639ed29edfb5c55bac97cceb3c15c.jpg', 0, '2017-03-06 04:05:12', '0000-00-00 00:00:00', '1', 1),
(9, 8, 'AYAAN', '', 'I AM AYAAN', 'be7b1a8d1c3aa26e69026481fb640a99.jpg', 0, '2017-03-06 04:07:01', '0000-00-00 00:00:00', '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `code` varchar(512) NOT NULL,
  `type` enum('p','f') NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `total_amount` decimal(10,0) NOT NULL,
  `max_uses` int(11) NOT NULL,
  `max_user_uses` int(11) NOT NULL,
  `shipping` tinyint(4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `uses_total` int(11) NOT NULL,
  `uses_customer` int(11) NOT NULL,
  `apply_with_discount` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `name`, `code`, `type`, `discount`, `total_amount`, `max_uses`, `max_user_uses`, `shipping`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `apply_with_discount`, `status`, `date`, `modify_date`) VALUES
(3, 'save6', 'save6', 'p', '6', '1', 0, 0, 0, '2017-03-15', '2017-03-31', 0, 0, 0, 1, '2017-03-15 00:00:00', '2017-03-15 17:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_category`
--

CREATE TABLE IF NOT EXISTS `coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_history`
--

CREATE TABLE IF NOT EXISTS `coupon_history` (
`coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
`id` int(11) NOT NULL,
  `group` varchar(55) NOT NULL,
  `title` varchar(111) NOT NULL,
  `f_name` varchar(111) NOT NULL,
  `m_name` varchar(111) NOT NULL,
  `l_name` varchar(111) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `newsletter` int(11) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `region` varchar(100) NOT NULL,
  `company` varchar(50) NOT NULL,
  `vat_fiscal` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `place` varchar(500) NOT NULL,
  `postcode` varchar(100) NOT NULL,
  `ip` varchar(111) NOT NULL,
  `status` int(11) NOT NULL,
  `agree` int(11) NOT NULL,
  `approved` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_shipping_address`
--

CREATE TABLE IF NOT EXISTS `customer_shipping_address` (
`shipping_id` int(11) NOT NULL,
  `default` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `company` varchar(111) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(222) NOT NULL,
  `region` varchar(111) NOT NULL,
  `country` varchar(100) NOT NULL,
  `city` varchar(55) NOT NULL,
  `place` varchar(500) NOT NULL,
  `postcode` varchar(55) NOT NULL,
  `telephone` varchar(25) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_wishlist`
--

CREATE TABLE IF NOT EXISTS `customer_wishlist` (
  `id` int(11) NOT NULL COMMENT 'this is foreign key of customer table for track customer wishlist',
  `date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE IF NOT EXISTS `discount` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `type` enum('p','f') NOT NULL,
  `apply` varchar(20) NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `total_amount` decimal(10,0) NOT NULL,
  `shipping` tinyint(4) DEFAULT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `uses_total` int(11) DEFAULT NULL,
  `uses_customer` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `modify_date` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`id`, `name`, `type`, `apply`, `discount`, `total_amount`, `shipping`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date`, `modify_date`) VALUES
(1, 'save2', 'p', 'product', '2', '0', NULL, '2017-03-15', '2017-03-31', NULL, NULL, 1, '0000-00-00', '0000-00-00'),
(2, 'save4', 'p', 'product', '4', '0', NULL, '2017-03-07', '2017-03-30', NULL, NULL, 1, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `discount_category`
--

CREATE TABLE IF NOT EXISTS `discount_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `discount_product`
--

CREATE TABLE IF NOT EXISTS `discount_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discount_product`
--

INSERT INTO `discount_product` (`id`, `product_id`) VALUES
(1, 9),
(2, 0),
(2, 0),
(3, 6),
(3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
`phrase_id` int(11) NOT NULL,
  `phrase` longtext COLLATE utf8_unicode_ci NOT NULL,
  `english` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Bangla` longtext COLLATE utf8_unicode_ci,
  `Frence` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`phrase_id`, `phrase`, `english`, `Bangla`, `Frence`) VALUES
(1, 'admin', 'Admin', NULL, NULL),
(2, 'dashboard', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `feature_product` varchar(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `short_description` varchar(555) NOT NULL,
  `long_description` text NOT NULL,
  `sku` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `unit_per_weight` varchar(20) NOT NULL COMMENT '1= pound, 2= packet, 3=pcs',
  `unit_per_qty` int(11) NOT NULL,
  `stock_status` varchar(50) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `unit_price` float(10,2) NOT NULL,
  `sale_price` float(10,2) NOT NULL,
  `tax` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `subtract_stock` varchar(100) NOT NULL DEFAULT '1',
  `minimum_quantity` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `default` int(11) NOT NULL,
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `feature_product`, `name`, `slug`, `short_description`, `long_description`, `sku`, `quantity`, `unit_per_weight`, `unit_per_qty`, `stock_status`, `image`, `unit_price`, `sale_price`, `tax`, `date_available`, `weight`, `subtract_stock`, `minimum_quantity`, `sort_order`, `status`, `default`, `viewed`, `date`, `date_modified`) VALUES
(6, 0, '1', 'BD BEEF', '', 'NEW BEED IN BANGLADESH', 'SUPER TESTY BEEP EVER WORLD . SPECIALY WE DELIVER THIS.', 'S1234', 11, 'Pound', 0, '', 'd96298a732df811fd8c4704d1ee5fd38.jpg', 111.00, 222.00, 5, '0000-00-00', '0.00000000', '1', 1, 2, 1, 0, 0, '2017-03-08 04:01:52', '0000-00-00 00:00:00'),
(9, 0, '1', 'special beef', '', 'special beef', 'this is special beef with chep rate.', 'c444', 100, 'Pound', 1, '', 'a4443ebb4a137e856b5c695c188d10aa.jpg', 11.00, 15.00, 5, '0000-00-00', '0.00000000', '1', 1, 1, 1, 0, 0, '2017-03-07 06:30:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE IF NOT EXISTS `product_to_category` (
  `id` int(11) NOT NULL COMMENT 'this is foreign key of product table to relation with category table with category id',
  `category_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_category`
--

INSERT INTO `product_to_category` (`id`, `category_id`, `date`) VALUES
(9, 5, '0000-00-00 00:00:00'),
(9, 8, '0000-00-00 00:00:00'),
(9, 9, '0000-00-00 00:00:00'),
(6, 5, '0000-00-00 00:00:00'),
(6, 8, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`settings_id` int(11) NOT NULL,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `type`, `description`) VALUES
(1, 'system_name', 'Halal Organic Meat'),
(2, 'system_title', 'Halal Organic Meat'),
(3, 'address', 'Dhaka-1216'),
(4, 'phone', '0123456789'),
(6, 'currency', '$'),
(7, 'system_email', 'admin@gmail.com'),
(11, 'language', 'english'),
(12, 'text_align', 'left-to-right'),
(13, 'system_currency_id', '1'),
(17, 'skin_theme', 'green'),
(18, 'logo_url', './uploads/logo-orange.png');

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE IF NOT EXISTS `tax` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(555) NOT NULL,
  `type` enum('P','F') NOT NULL COMMENT 'two type of of rate . it''s fixed and percentange',
  `rate` decimal(10,0) NOT NULL COMMENT 'rate is tax value how percentange or fixed amount will add',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `default` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `name`, `title`, `description`, `type`, `rate`, `date`, `modify_date`, `status`, `default`) VALUES
(5, '1% vat', '1% vat', '1% vat', 'P', '1', '2015-05-11 15:36:48', '0000-00-00', 0, 0),
(6, '2% VAT', '2% VAT', '2% VAT', 'P', '2', '2015-05-11 15:36:49', '0000-00-00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(150) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `phone`, `address`, `city`, `country`, `create_at`, `is_active`) VALUES
(1, 'Mahabubur Rahman', '', 'admin@halal.com', '01534710493', 'Mirpur, Dhaka', 'Dhaka', '', '2017-02-26 19:44:50', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(33) NOT NULL,
  `ip` varchar(22) NOT NULL,
  `date` date NOT NULL,
  `action` varchar(111) NOT NULL,
  `comment` varchar(222) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `user_id`, `user_name`, `ip`, `date`, `action`, `comment`) VALUES
(1, 1, 'Halal Beef', '::1', '2017-03-08', 'user', 'Add New Admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `permission` varchar(500) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `permission`, `date`, `status`) VALUES
(1, 'Super Admin', '', '2017-03-08 14:45:00', 1),
(2, 'Admin', '', '2017-03-06 13:41:09', 1),
(3, 'Delivery Man', '', '2017-03-08 14:45:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `web_about_us`
--

CREATE TABLE IF NOT EXISTS `web_about_us` (
`id` int(2) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 NOT NULL,
  `desc` text CHARACTER SET utf8 NOT NULL,
  `img` varchar(15) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_about_us`
--

INSERT INTO `web_about_us` (`id`, `title`, `desc`, `img`) VALUES
(1, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua elitr, sed diam nonumy eirmod tempor invidunt ut.\r\n\r\nAt vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\r\nAt vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', 'about_us.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_faq`
--

CREATE TABLE IF NOT EXISTS `web_faq` (
`id` int(3) NOT NULL,
  `ques` varchar(256) NOT NULL,
  `answ` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `web_faq`
--

INSERT INTO `web_faq` (`id`, `ques`, `answ`) VALUES
(1, 'Collapsible Group Item #1', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.'),
(2, 'Collapsible Group Item #2', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.'),
(3, 'Collapsible Group Item #3', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.');

-- --------------------------------------------------------

--
-- Table structure for table `web_general_settings`
--

CREATE TABLE IF NOT EXISTS `web_general_settings` (
`id` int(1) NOT NULL,
  `wlc_text` varchar(128) CHARACTER SET utf8 NOT NULL,
  `order_no` varchar(50) CHARACTER SET utf8 NOT NULL,
  `opera_desc` text CHARACTER SET utf8 NOT NULL,
  `fb` varchar(128) CHARACTER SET utf8 NOT NULL,
  `twt` varchar(128) CHARACTER SET utf8 NOT NULL,
  `y_tube` varchar(128) CHARACTER SET utf8 NOT NULL,
  `g_plus` varchar(128) CHARACTER SET utf8 NOT NULL,
  `logo_img` varchar(30) CHARACTER SET utf8 NOT NULL,
  `more_link` varchar(256) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_general_settings`
--

INSERT INTO `web_general_settings` (`id`, `wlc_text`, `order_no`, `opera_desc`, `fb`, `twt`, `y_tube`, `g_plus`, `logo_img`, `more_link`) VALUES
(1, 'WCOME TO HALAL ORGANIC MEAT SHOP', '0123756789', 'Monday to Thuesday 6:00 - 18:00\r\nFriday to Saturdays 12:00 - 22:00\r\nSundays closed', 'facebook', 'twitter', 'Youtube', 'Google Plus', 'logo.png', 'test linkasdf');

-- --------------------------------------------------------

--
-- Table structure for table `web_slider`
--

CREATE TABLE IF NOT EXISTS `web_slider` (
`id` int(2) NOT NULL,
  `cap_1` varchar(256) NOT NULL,
  `cap_2` varchar(256) NOT NULL,
  `img` varchar(260) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `web_slider`
--

INSERT INTO `web_slider` (`id`, `cap_1`, `cap_2`, `img`) VALUES
(2, 'slider 02 cap 01', 'slider 02 cap 02', 'slider_02_cap_01.jpg'),
(4, 'Caption 01', 'Caption 02', 'caption_01.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_category`
--
ALTER TABLE `coupon_category`
 ADD KEY `coupon_id` (`coupon_id`,`category_id`);

--
-- Indexes for table `coupon_history`
--
ALTER TABLE `coupon_history`
 ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`id`), ADD KEY `group_id` (`group`);

--
-- Indexes for table `customer_shipping_address`
--
ALTER TABLE `customer_shipping_address`
 ADD PRIMARY KEY (`shipping_id`);

--
-- Indexes for table `customer_wishlist`
--
ALTER TABLE `customer_wishlist`
 ADD PRIMARY KEY (`id`), ADD KEY `customer_id` (`customer_id`,`product_id`), ADD KEY `customer_id_2` (`customer_id`), ADD KEY `product_id` (`product_id`), ADD KEY `customer_id_3` (`customer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_category`
--
ALTER TABLE `discount_category`
 ADD KEY `discount_id` (`id`,`category_id`);

--
-- Indexes for table `discount_product`
--
ALTER TABLE `discount_product`
 ADD KEY `discount_id` (`id`,`product_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
 ADD PRIMARY KEY (`phrase_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`), ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_about_us`
--
ALTER TABLE `web_about_us`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_faq`
--
ALTER TABLE `web_faq`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_general_settings`
--
ALTER TABLE `web_general_settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_slider`
--
ALTER TABLE `web_slider`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `coupon_history`
--
ALTER TABLE `coupon_history`
MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_shipping_address`
--
ALTER TABLE `customer_shipping_address`
MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
MODIFY `phrase_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_about_us`
--
ALTER TABLE `web_about_us`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_faq`
--
ALTER TABLE `web_faq`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_general_settings`
--
ALTER TABLE `web_general_settings`
MODIFY `id` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_slider`
--
ALTER TABLE `web_slider`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer_wishlist`
--
ALTER TABLE `customer_wishlist`
ADD CONSTRAINT `customer_wishlist_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
ADD CONSTRAINT `customer_wishlist_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
