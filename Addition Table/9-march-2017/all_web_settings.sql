-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2017 at 01:50 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `halal`
--

-- --------------------------------------------------------

--
-- Table structure for table `web_about_us`
--

CREATE TABLE IF NOT EXISTS `web_about_us` (
`id` int(2) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 NOT NULL,
  `desc` text CHARACTER SET utf8 NOT NULL,
  `img` varchar(15) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_about_us`
--

INSERT INTO `web_about_us` (`id`, `title`, `desc`, `img`) VALUES
(1, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua elitr, sed diam nonumy eirmod tempor invidunt ut.</p>\r\n\r\n<blockquote>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</blockquote>\r\n\r\n<p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'about_us.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_faq`
--

CREATE TABLE IF NOT EXISTS `web_faq` (
`id` int(3) NOT NULL,
  `ques` varchar(256) NOT NULL,
  `answ` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `web_faq`
--

INSERT INTO `web_faq` (`id`, `ques`, `answ`) VALUES
(1, 'Collapsible Group Item #1', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.'),
(2, 'Collapsible Group Item #2', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.'),
(3, 'Collapsible Group Item #3', 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.');

-- --------------------------------------------------------

--
-- Table structure for table `web_farm_gallery`
--

CREATE TABLE IF NOT EXISTS `web_farm_gallery` (
`id` int(5) NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `img` varchar(135) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `web_farm_gallery`
--

INSERT INTO `web_farm_gallery` (`id`, `title`, `img`) VALUES
(1, 'farm01', 'farm01.jpg'),
(2, 'farm2', 'farm2.jpg'),
(3, 'farm3', 'farm3.jpg'),
(4, 'farm4', 'farm4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_general_settings`
--

CREATE TABLE IF NOT EXISTS `web_general_settings` (
`id` int(1) NOT NULL,
  `wlc_text` varchar(128) CHARACTER SET utf8 NOT NULL,
  `order_no` varchar(50) CHARACTER SET utf8 NOT NULL,
  `opera_desc` text CHARACTER SET utf8 NOT NULL,
  `fb` varchar(128) CHARACTER SET utf8 NOT NULL,
  `twt` varchar(128) CHARACTER SET utf8 NOT NULL,
  `y_tube` varchar(128) CHARACTER SET utf8 NOT NULL,
  `g_plus` varchar(128) CHARACTER SET utf8 NOT NULL,
  `logo_img` varchar(30) CHARACTER SET utf8 NOT NULL,
  `more_link` varchar(256) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_general_settings`
--

INSERT INTO `web_general_settings` (`id`, `wlc_text`, `order_no`, `opera_desc`, `fb`, `twt`, `y_tube`, `g_plus`, `logo_img`, `more_link`) VALUES
(1, 'WCOME TO HALAL ORGANIC MEAT SHOP', '0123756789', 'Monday to Thuesday 6:00 - 18:00\r\nFriday to Saturdays 12:00 - 22:00\r\nSundays closed', 'facebook', 'twitter', 'Youtube', 'Google Plus', 'logo.png', 'more_link');

-- --------------------------------------------------------

--
-- Table structure for table `web_offers`
--

CREATE TABLE IF NOT EXISTS `web_offers` (
`id` int(3) NOT NULL,
  `title` varchar(128) NOT NULL,
  `desc` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `web_offers`
--

INSERT INTO `web_offers` (`id`, `title`, `desc`, `img`) VALUES
(1, 'call to order', '<p>Sed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urnaSed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urna</p>\r\n', 'call_to_order.jpg'),
(2, 'Fast Delivery', '<p>Sed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urnaSed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urna</p>\r\n', 'fast_delivery.jpg'),
(3, 'organic food', '<p>Sed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urnaSed cursus lectus ut lorem vehicula finibus. Maecenas vehicula sem at urna</p>\r\n', 'organic_food.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_our_farm`
--

CREATE TABLE IF NOT EXISTS `web_our_farm` (
`id` int(2) NOT NULL,
  `title` varchar(128) NOT NULL,
  `desc` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_our_farm`
--

INSERT INTO `web_our_farm` (`id`, `title`, `desc`, `img`) VALUES
(1, 'Halal organic farm', '<p>We are certified HALAL facility. All Our animals are humanely treated and naturally fed and raised in a spacious feeding lot.</p>\r\n', 'halal_organic_farm.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_privacy_policy`
--

CREATE TABLE IF NOT EXISTS `web_privacy_policy` (
`id` int(2) NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `desc` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_privacy_policy`
--

INSERT INTO `web_privacy_policy` (`id`, `title`, `desc`) VALUES
(1, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquy', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `web_slider`
--

CREATE TABLE IF NOT EXISTS `web_slider` (
`id` int(2) NOT NULL,
  `cap_1` varchar(256) NOT NULL,
  `cap_2` varchar(256) NOT NULL,
  `img` varchar(260) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `web_slider`
--

INSERT INTO `web_slider` (`id`, `cap_1`, `cap_2`, `img`) VALUES
(1, 'Welcome to Organic 01', 'Halal Meat 01', 'welcome_to_organic_01.jpg'),
(2, 'Welcome to Organic 02', 'Halal Meat 02', 'welcome_to_organic_02.jpg'),
(3, 'Welcome to Organic 03', 'Halal Meat 03', 'welcome_to_organic_03.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_testimonial`
--

CREATE TABLE IF NOT EXISTS `web_testimonial` (
`id` int(3) NOT NULL,
  `cli_name` varchar(128) NOT NULL,
  `cli_comment` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `web_testimonial`
--

INSERT INTO `web_testimonial` (`id`, `cli_name`, `cli_comment`, `img`) VALUES
(1, 'Josephine Walker 01', '<p>ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'josephine_walker_01.jpg'),
(2, 'Josephine Walker 02', '<p>ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'josephine_walker_02.jpg'),
(3, 'Josephine Walker 03', '<p>ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>\r\n', 'josephine_walker_03.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `web_wlc`
--

CREATE TABLE IF NOT EXISTS `web_wlc` (
`id` int(2) NOT NULL,
  `title1` varchar(128) NOT NULL,
  `title2` varchar(128) NOT NULL,
  `desc` text NOT NULL,
  `img` varchar(135) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_wlc`
--

INSERT INTO `web_wlc` (`id`, `title1`, `title2`, `desc`, `img`) VALUES
(1, 'Welcome to', 'Halal organic meat', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. consetetur sadipscing elitr, magna aliquyam erat, sed diam voluptua.', 'wlc.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `web_about_us`
--
ALTER TABLE `web_about_us`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_faq`
--
ALTER TABLE `web_faq`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_farm_gallery`
--
ALTER TABLE `web_farm_gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_general_settings`
--
ALTER TABLE `web_general_settings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_offers`
--
ALTER TABLE `web_offers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_our_farm`
--
ALTER TABLE `web_our_farm`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_privacy_policy`
--
ALTER TABLE `web_privacy_policy`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_slider`
--
ALTER TABLE `web_slider`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_testimonial`
--
ALTER TABLE `web_testimonial`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_wlc`
--
ALTER TABLE `web_wlc`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `web_about_us`
--
ALTER TABLE `web_about_us`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_faq`
--
ALTER TABLE `web_faq`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_farm_gallery`
--
ALTER TABLE `web_farm_gallery`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `web_general_settings`
--
ALTER TABLE `web_general_settings`
MODIFY `id` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_offers`
--
ALTER TABLE `web_offers`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_our_farm`
--
ALTER TABLE `web_our_farm`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_privacy_policy`
--
ALTER TABLE `web_privacy_policy`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_slider`
--
ALTER TABLE `web_slider`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_testimonial`
--
ALTER TABLE `web_testimonial`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_wlc`
--
ALTER TABLE `web_wlc`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
