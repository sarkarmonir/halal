<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
class Taxs extends CI_Model
{
        /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'tax';
        parent :: __construct();
    }
    /**
     * Add New tax in database table tax
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add()
    {
        $value = array(
            'name'=>          $this->input->post('name'),
            'title'=>         $this->input->post('title'),
            'type'=>          $this->input->post('type'),
            'rate'=>          $this->input->post('rate'),
            'status'=>        $this->input->post('status')
        );
        
        /**
         * call data model for save basic data in customer_group table and return customer_group id
         * @author rjs
         */
         $tax = $this->data->save($this->table,$value);
         if($tax)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
    }
    public function edit($id)
    { 
            $value = array(
            'name'=>          $this->input->post('name'),
            'title'=>         $this->input->post('title'),
            'type'=>          $this->input->post('type'),
            'rate'=>          $this->input->post('rate'),
            'status'=>        $this->input->post('status')
        );  
       
        /**
         * call data model for save basic data in tax table and return tax id
         * @author rjs
         */
         $tax = $this->data->update($this->table,$id,$value);
         if($tax)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
   
}

