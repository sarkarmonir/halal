<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Model used in product models for category/brand/product type/product/
 * @package product
 * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
 */
class Product extends CI_Model {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'product';
        $this->table3 = 'product_to_category';
        parent :: __construct();
    }

    /**
     * this method return all active product
     * @return type
     */
    public function getall() {
        $this->db->select('*');
        $this->db->from('product');
        return $this->db->get()->result();
    }

    /**
     * 
     * @return type
     * this method return feature product
     * @author Jahid Al Mamun
     */
    public function feature() {
//        $this->db->where('status',1);
        $this->db->where('feature_product', '1');
        $this->db->from('product');
        return $result = $this->db->get()->result();
    }

    /**
     * 
     * @return type
     * this method use for show promo product and call from common helper to show promo product in every page
     * @author Jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function promo() {
        $cdate = date('Y-m-d');
        $this->db->select('discount.*,discount.id as discount_id,product.*');
        $this->db->order_by('discount.id', 'RANDOM');
        $this->db->join('discount_product', 'discount_product.id=discount.id', 'left');
        $this->db->join('product', 'product.id=discount_product.product_id', 'left');
        $this->db->where('discount.apply', 'product');
        $this->db->where('discount.date_end >', $cdate);
        $this->db->where('product.status', 1);
        $this->db->from('discount');
        return $this->db->get()->result();
    }

    /**
     * 
     * @return type
     * this method use for get individual product discount and call from common helper
     * @author Jahid al mamun<rjs.jahid11@gmail.com>
     * 
     */
    public function product_discount($id) {
        $cdate = date('Y-m-d');
        $this->db->select('*');
        $this->db->limit(1);
        $this->db->join('discount', 'discount.id = discount_product.id');
        $this->db->where('discount_product.product_id', $id);
        $this->db->where('discount.date_end >', $cdate);
        $this->db->from('discount_product');
        return $this->db->get()->result();
    }


    /**
     * this method use for get any specific product tax
     * @param type $id
     * @return type
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function product_tax($id) {
        $query = $this->db->query("select tax.name,tax.id,tax.title from product left join tax on product.tax=tax.id where product.id = $id");
        return $query->result();
    }



    /*     * get instance price vat calculation 
     * return without vat price from include vat price
     * 
     */

    public function vat_price($id, $price) {

        $this->db->select('type,rate');
        $this->db->where('id', $id);
        $this->db->from('tax');
        $result = $this->db->get()->result();
        $type = $result[0]->type;
        $rate = $result[0]->rate;
        if ($type == 'P') {
            $vat_value = $price * $rate / 100;
            $exclude_vat = $price - $vat_value;
            return $exclude_vat;
        } else if ($type == 'F') {
            return $exclude_vat = $price - $rate;
        }
    }

    /**
     * 
     * @param type $id
     * @param type $price
     * @return type
     * include vat price with vat id and price
     * @author jahid al mamun
     */
    public function include_vat_price($id, $price) {
        $this->db->select('type,rate');
        $this->db->where('id', $id);
        $this->db->from('tax');
        $result = $this->db->get()->result();
        $type = $result[0]->type;
        $rate = $result[0]->rate;
        if ($type == 'P') {
            $vat_value = $price * $rate / 100;
            $exclude_vat = $price + $vat_value;
            return $exclude_vat;
        } else if ($type == 'F') {
            return $exclude_vat = $price + $rate;
        }
    }

    /**
     * Add New product in database table product
     * @author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add($image) {

        /**
         * prepare product table value for insert in product table.
         */
       
            $value = array(
                'name' => $this->input->post('name'),
                'short_description' => $this->input->post('short_description'),
                'long_description' => $this->input->post('description'),
                'sku' => $this->input->post('sku'),
                'image' => $image,
                'unit_price' => $this->input->post('unit_price'),
                'sale_price' => $this->input->post('sale_price'),
                'quantity' => $this->input->post('quantity'),
                'minimum_quantity' => $this->input->post('min_quantity'),
           
                'tax' => $this->input->post('tax'),
                'feature_product' => $this->input->post('feature_product'),
                'unit_per_weight' => $this->input->post('unit_per_weight'),
                'unit_per_qty' => $this->input->post('unit_per_qty'),
                'sort_order' => $this->input->post('sort_order'),
                'status' => $this->input->post('status'),
                'product_status' => $this->input->post('product_status'),
                'date'=>  date('Y-m-d h:i:s a', time())
            );
        


        /**
         * call data model for update basic data in product table and return product id
         * @author rjs
         */
        $product_id = $this->data->save($this->table, $value);
        if ($product_id) {
            //product category entry in product_to_category table
            $product_category = $this->input->post('category');

            $delete_exist_category = $this->data->delete($this->table3, $product_id);

            foreach ($product_category as $product_category) {
                $value = array(
                    'id' => $product_id,
                    'category_id' => $product_category
                );
                $this->data->save($this->table3, $value);
            }
            $status = TRUE;
        } else {

            $status = FALSE;
        }

        if ($status == TRUE) {
            return $product_id;
        } else {
            return $product_id;
        }
    }

    

 

    /**
     * this function use to get product quantity
     * @author jahid al mamun
     */
    public function product_quantity_count($id) {
        $this->db->select('quantity');
        $this->db->where('id', $id);
        $this->db->from('product');
        $result = $this->db->get()->result();
        if (count($result) > 0) {
            return $result[0]->quantity;
        }
    }

    public function edit($id, $image = NULL) {

        /**
         * prepare product table value for insert in product table.
         */
        $value = array(
                'name' => $this->input->post('name'),
                'short_description' => $this->input->post('short_description'),
                'long_description' => $this->input->post('description'),
                'sku' => $this->input->post('sku'),
                'image' => $image,
                'unit_price' => $this->input->post('unit_price'),
                'sale_price' => $this->input->post('sale_price'),
                'quantity' => $this->input->post('quantity'),
                'minimum_quantity' => $this->input->post('min_quantity'),
                'unit_per_weight' => $this->input->post('unit_per_weight'),
                'unit_per_qty' => $this->input->post('unit_per_qty'),
           
                'tax' => $this->input->post('tax'),
                'feature_product' => $this->input->post('feature_product'),
                
                'sort_order' => $this->input->post('sort_order'),
                'status' => $this->input->post('status'),
                'product_status' => $this->input->post('product_status'),
                'date'=>  date('Y-m-d h:i:s a', time())
            );

//      
//        
        /**
         * call data model for update basic data in product table and return product id
         * @author rjs
         */
        $product_id = $this->data->update($this->table, $id, $value);
        if ($product_id) {
            //product category entry in product_to_category table
            $product_category = $this->input->post('category');

            $delete_exist_category = $this->data->delete($this->table3, $id);

            foreach ($product_category as $product_category) {
                $value = array(
                    'id' => $id,
                    'category_id' => $product_category
                );
                $this->data->save($this->table3, $value);
            }
           
            
            $status = TRUE;
        } else {

            $status = FALSE;
        }

        if ($status == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function category($id) {
        $query = $this->db->query("select *from product_to_category left join category ON category.id = product_to_category.category_id where product_to_category.id = $id");
        $product_category = $query->result();
        return $product_category;
    }

    /**
     * update table value with table name / id / and array value
     * @author Jahid All Mamun
     */
    function update($table, $id, $data) {
        $this->db->where('uniq_id', $id);
        $result = $this->db->update($table, $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function lot_update($table, $id, $data) {
        $this->db->where('lot_id', $id);
        $result = $this->db->update($table, $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * 
     * This method use for get all data with id with status = 1
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     * @param type $table
     * @param type $id
     * @return type
     */
    public function getallbyid_with_status($table, $id) {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    /**
     * 
     * @param type $id
     * @return type
     * This method use for get category product with category id and use for related product
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function product_category($id) {
        $this->db->select('product_to_category.category_id');
        $this->db->join('product_to_category', 'product_to_category.id=product.id', 'left');
        $this->db->where('product.id', $id);
        $this->db->from('product');
        return $this->db->get()->result();
    }

    public function brand_product($product_brand_id, $lang_id) {
        $this->db->select('product.id,product.name,product.product_type,product.category_id,product.lot_type,product.feature_product,product.brand,product.model,product.sku,product.ean,product.quantity,product.price,product_lng.meta_tag_title,product_lng.description,product_lng.meta_tag_description,product_lng.meta_tag_keyword,product.image');
        $this->db->join($this->table2, "product_lng.id=product.id and product_lng.lng_id = $lang_id", 'left');
        $this->db->where('product.brand', $product_brand_id);
        $this->db->from('product');
        return $this->db->get()->result();
    }

    /**
     * 
     * @param type $id
     * @return type
     * this method use to get product vat with product id
     * @author jahid al mamun
     */
    public function product_vat($id) {
        $this->db->select('product.tax,tax.id,tax.type as tax_type,tax.rate');
        $this->db->join('tax', 'product.tax = tax.id');
        $this->db->where('product.id', $id);
        $this->db->from('product');
        return $this->db->get()->result();
    }

    /**
     * 
     * @param type $id
     * this method use to get product rating value
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function rating_value($id) {
        $this->db->select('AVG(rating_value) as rating');
        $this->db->where('product_id', $id);
        $this->db->from('product_rating');
        return $this->db->get()->result();
    }

    /**
     * 
     * @param type $id
     * @return type
     * this method use for check product avalability on add to cart process and call from products controller
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function check_availability($id) {
        $this->db->select('quantity,minimum_quantity');
        $this->db->where('id', $id);
        $this->db->from('product');
        return $this->db->get()->result();
    }

    /**
     * this function use to get all product list with id and name
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function related_product() {
        $this->db->select('id,name');
        $this->db->where('status', 1);
        $this->db->from('product');
        return $this->db->get()->result();
    }

    public function product_related_product($id = NULL, $lang_id) {
        $query = $this->db->query("select product.id,product.name,product.product_type,product.category_id,product.lot_type,"
                . "product.feature_product,product.brand,product.model,product.sku,product.ean,product.quantity,"
                . "product.price,product_lng.meta_tag_title,product_lng.description,product_lng.meta_tag_description,"
                . "product_lng.meta_tag_keyword,product.image,product.font_color,product.background_color from product"
                . " left join product_related ON product_related.related_id = product.id"
                . " left join product_lng  ON product_lng.id = product.id AND product_lng.lng_id = $lang_id"
                . " where product_related.id = $id");

        return $query->result();
    }

}
