<?php

class Order_model extends CI_Model
{
        /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = '';
        parent :: __construct();
    }
   //get customer shipping address
    public function customer_shipping_address($id)
    {
        $this->db->select('*');
        $this->db->where('customer_id',$id);
        $this->db->where('status',1);
        $this->db->where('default',1);
        $this->db->order_by('shipping_id','desc');
        $this->db->from('customer_shipping_address');
        return $this->db->get()->result();
    }
    /**
     * 
     * @return type
     * this method use to show all orders in order list
     * @author Jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function all_orders()
    {
        $this->db->select('orders.*,orders.id as oid,orders.status as ostatus,customers.*,customers.id as cid');
        $this->db->join('customers','orders.customer_id=customers.id','left')->order_by('orders.id','desc');
        $this->db->from('orders');
        
        return $this->db->get()->result();
        
    }
    
    public function all_orders_stock()
    {
        $this->db->select('orders.*,orders.id as oid,orders.status as ostatus,customers.*,customers.id as cid');
        $this->db->join('customers','orders.customer_id=customers.id','left');
        $this->db->where('orders.status !=','received');
        $this->db->from('orders');
        return $this->db->get()->result();
        
    }
    /**
     * This method use for get last 5 order and show in dashboard
     * @author Jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function last_orders()
    {
        $this->db->select('orders.*,orders.id as oid,orders.status as ostatus,customers.*,customers.id as cid');
        $this->db->join('customers','orders.customer_id=customers.id','left');
        $this->db->limit(5);
        $this->db->order_by('orders.id','desc');
        $this->db->from('orders');
        return $this->db->get()->result();
        
    }
    /**
     * 
     * @param type $oid
     * @param type $cid
     * @return type
     * this method use for show all message under admin/customer message box and call from order controller
     * @author Jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function old_message($oid,$cid)
    {
        $this->db->select('*');
        $this->db->where('order_id',$oid);
        $this->db->where('customer_id',$cid);
        $this->db->from('order_communication');
        return $this->db->get()->result();
    }
    /**
     * 
     * @param type $id
     * @return type
     * this method use to get communication message under customer backend with customer id
     * and call from order controller communication_msg function
     * @author jahid al mamun<rjs.jahid11@gmail.com>
     */
    public function communication_msg($id)
    {
        $this->db->select('customers.f_name,customers.l_name,customers.id as customer_id,order_communication.*');
        $this->db->join('customers','customers.id=order_communication.customer_id');
        $this->db->where('order_communication.customer_id',$id);
        $this->db->group_by('order_communication.order_id');
        $this->db->from('order_communication');
        return $this->db->get()->result();
    }
    /**
     * this method return unread message count under customer id and call from common helper unread_msg function
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function unread_order_msg($id)
    {
        $this->db->select('count(id) as unread');
        $this->db->where('read_status',0);
        $this->db->where('order_communication.customer_id',$id);
        $this->db->from('order_communication');
        return $this->db->get()->result();
        
    }
    /**
     * 
     * @param type $oid
     * @param type $cid
     * @param type $read_type
     * this method use for update read status on order_communcation table and call from order controller read_msg function
     * @author jahid al mamun
     */
    public function update_msg($oid,$cid,$read_type)
    {
        if($read_type == 'customer')
        {
            $value = array(
                'read_status'=>1
            );
        }
        $this->db->where('order_id', $oid);
        $this->db->where('customer_id', $cid);
        $this->db->update('order_communication', $value);
        return TRUE;
    }
    
    public function total_income()
    {
        $this->db->select('SUM(total) as total');
        $this->db->where('status',1);
        $this->db->from('transections');
        return $this->db->get()->result();
    }
    
    public function total_order()
    {
        $this->db->select('count(id) as ototal');
        $this->db->where('status',1);
        $this->db->from('orders');
        return $this->db->get()->result();
    }
    
    public function total_product()
    {
        $this->db->select('count(id) as ptotal');
        $this->db->where('status',1);
        $this->db->from('product');
        return $this->db->get()->result();
    }
}
