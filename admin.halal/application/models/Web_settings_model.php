<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Web_settings_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function clear_cache() {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

// ##############################################################################################
//  General settings section
// ##############################################################################################

    // get_general_settings --------------+-------------------------------------
    public function get_general_settings() {
        $r = $this->db->select('*')->from('web_general_settings')->where('id', '1')->get();
        return $result = $r->row();
    }

    // update_general_settings --------------+----------------------------------
    public function update_general_settings($data) {
        $this->db->where('id', '1')->update('web_general_settings', $data);
    }


// ##############################################################################################
//  Slider section
// ##############################################################################################

    // add slider --------------------------------------------------------------
    public function add_slider($data) {
        $this->db->insert('web_slider', $data);
    }

    // get slider --------------------------------------------------------------
    public function get_slider() {
        $r = $this->db->select('*')->from('web_slider')->get();
        return  $result = $r->result();
    }

    // update slider -----------------------------------------------------------
    public function update_slider($data, $id) {
        $this->db->where("id", $id)->update("web_slider", $data);
    }

    // replace slide image -----------------------------------------------------
    public function rep_slider_image($data, $id) {
        $this->db->where("id", $id)->update("web_slider", $data);
    }

    // slider_delete -----------------------------------------------------------
    public function slider_delete($id) {
        $this->db->where("id", $id)->delete("web_slider");
    }

    
// ##############################################################################################
//  About us section
// ##############################################################################################

    // get_about_us_ -----------------------------------------------------------
    public function get_about_us() {
        $r = $this->db->select('*')->from('web_about_us')->where('id', '1')->get();
        return $result = $r->row();
    }

    // update_about_us ---------------------------------------------------------
    public function update_about_us($data) {
        $this->db->where('id', '1')->update('web_about_us', $data);
    }


// ##############################################################################################
//  FAQ section
// ##############################################################################################

    // add faq -----------------------------------------------------------------
    public function add_faq($data) {
        $this->db->insert('web_faq', $data);
    }

    // get faq -----------------------------------------------------------------
    public function get_faq() {
        $r = $this->db->select('*')->from('web_faq')->get();
        return  $result = $r->result();
    }

    // update faq --------------------------------------------------------------
    public function update_faq($data, $id) {
        $this->db->where("id", $id)->update("web_faq", $data);
    }

    // faq delete --------------------------------------------------------------
    public function faq_delete($id) {
        $this->db->where("id", $id)->delete("web_faq");
    }

// ##############################################################################################
//  testimonial section
// ##############################################################################################

    // add testimonial --------------------------------------------------------------
    public function add_testimonial($data) {
        $this->db->insert('web_testimonial', $data);
    }

    // get testimonial --------------------------------------------------------------
    public function get_testimonial() {
        $r = $this->db->select('*')->from('web_testimonial')->get();
        return  $result = $r->result();
    }

    // update testimonial -----------------------------------------------------------
    public function update_testimonial($data, $id) {
        $this->db->where("id", $id)->update("web_testimonial", $data);
    }

    // replace testimonial image -----------------------------------------------------
    public function rep_testimonial_image($data, $id) {
        $this->db->where("id", $id)->update("web_testimonial", $data);
    }

    // testimonial delete -----------------------------------------------------------
    public function testimonial_delete($id) {
        $this->db->where("id", $id)->delete("web_testimonial");
    }


// ##############################################################################################
//  Offers section
// ##############################################################################################

    // add offer ---------------------------------------------------------------
    public function add_offer($data) {
        $this->db->insert('web_offers', $data);
    }

    // get testimonial --------------------------------------------------------------
    public function get_offer() {
        $r = $this->db->select('*')->from('web_offers')->get();
        return  $result = $r->result();
    }

    // update testimonial -----------------------------------------------------------
    public function update_offer($data, $id) {
        $this->db->where("id", $id)->update("web_offers", $data);
    }

    // replace testimonial image -----------------------------------------------------
    public function rep_offer_image($data, $id) {
        $this->db->where("id", $id)->update("web_offers", $data);
    }

    // testimonial delete -----------------------------------------------------------
    public function offer_delete($id) {
        $this->db->where("id", $id)->delete("web_offers");
    }    

// ##############################################################################################
//  welcome message on home section
// ##############################################################################################

    // get_wlc -----------------------------------------------------------------
    public function get_wlc() {
        $r = $this->db->select('*')->from('web_wlc')->where('id', '1')->get();
        return $result = $r->row();
    }

    // update_about_us ---------------------------------------------------------
    public function update_wlc($data) {
        $this->db->where('id', '1')->update('web_wlc', $data);
    }

// ##############################################################################################
//  privacy policy section
// ##############################################################################################

    // get privacy -------------------------------------------------------------
    public function get_privacy() {
        $r = $this->db->select('*')->from('web_privacy_policy')->where('id', '1')->get();
        return $result = $r->row();
    }

    // update privacy ----------------------------------------------------------
    public function update_privacy($data) {
        $this->db->where('id', '1')->update('web_privacy_policy', $data);
    }



// ##############################################################################################
//  our farm section
// ##############################################################################################

    // add farm ----------------------------------------------------------------
    public function add_farm($data) {
        $this->db->insert('web_our_farm', $data);
    }

    // get farm ----------------------------------------------------------------
    public function get_farm() {
        $r = $this->db->select('*')->from('web_our_farm')->get();
        return  $result = $r->result();
    }

    // update farm -------------------------------------------------------------
    public function update_farm($data, $id) {
        $this->db->where("id", $id)->update("web_our_farm", $data);
    }

    // replace farm image ------------------------------------------------------
    public function rep_farm_image($data, $id) {
        $this->db->where("id", $id)->update("web_our_farm", $data);
    }

    // farm delete -------------------------------------------------------------
    public function farm_delete($id) {
        $this->db->where("id", $id)->delete("web_our_farm");
    }


// ##############################################################################################
// farm gallery section
// ##############################################################################################

    // add farm gallery --------------------------------------------------------
    public function add_farm_gallery($data) {
        $this->db->insert('web_farm_gallery', $data);
    }

    // get farm gallery --------------------------------------------------------
    public function get_farm_gallery() {
        $r = $this->db->select('*')->from('web_farm_gallery')->get();
        return  $result = $r->result();
    }

    // update farm gallery -----------------------------------------------------
    public function update_farm_gallery($data, $id) {
        $this->db->where("id", $id)->update("web_farm_gallery", $data);
    }

    // replace farm gallery image ----------------------------------------------
    public function rep_farm_gallery_image($data, $id) {
        $this->db->where("id", $id)->update("web_farm_gallery", $data);
    }

    // farm gallery delete -----------------------------------------------------
    public function farm_gallery_delete($id) {
        $this->db->where("id", $id)->delete("web_farm_gallery");
    }

// ##############################################################################################
// for front end 
// ##############################################################################################
    
    // home --------------------------------------------------------------------










}