<?php

/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * @date: 2015-9-10
 * 
 * @copyright  Copyright (C) 2015 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
class Category extends CI_Model {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'category';
        parent :: __construct();
    }

    /**
     * Add New category in database table category
     * @author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add($image) {
        $value = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'parent_id' => $this->input->post('parent'),
            'image' => $image,
            'sort_order' => $this->input->post('sort_order'),
            'status' => $this->input->post('status'),
            'create_at' => date('Y-m-d h:i:s a', time())
        );

        /**
         * call data model for save basic data in category table and return category id
         * @author rjs
         */
        $category_id = $this->data->save($this->table, $value);
        if($category_id){
                        return TRUE;}else{return false;}
        
    }

    public function edit($id, $image) {

        $value = array(
            'name' => $this->input->post('name'),
            'parent_id' => $this->input->post('parent'),
            'description' => $this->input->post('description'),
            'sort_order' => $this->input->post('sort_order'),
            'image' => $image,

            'status' => $this->input->post('status')
        );



        /**
         * call data model for save basic data in category table and return category id
         * @author rjs
         */
        $category_id = $this->data->update($this->table, $id, $value);
        if ($category_id) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function all_by_option() {
        $result = '';
        $query = $this->db->query("select * from (
  select c.*,
  coalesce(nullif(c.parent_id, 0), c.id) as groupID,
  coalesce(nullif(c.name, 0), c.name) as pname,
  case when c.parent_id = 0 then 1 else 0 end as isparent,
  case when p.`sort_order` = 0 then c.id end as orderbyint
  from category c
  left join category p on p.id = c.parent_id
) c order by groupID, isparent desc, orderbyint, name");
        $category = $query->result();
        return $category;
    }

    public function category_list() {
        $query = $this->db->query("select node.name as root_name 
     ,node.id as id,node.image as image,node.sort_order as sort_order
     , down1.name as down1_name , down1.sort_order as down1_sort_order , down1.name as down1_image 
     , down1.id as down1_id 
     , down2.name as down2_name, down2.sort_order as down2_sort_order , down2.name as down2_image  
     , down2.id as down2_id
     , down3.name as down3_name , down3.sort_order as down3_sort_order , down3.name as down3_image 
     , down3.id as down3_id
  from category as node
left outer 
  join category as down1 
    on down1.id = node.parent_id  
left outer 
  join category as down2
    on down2.id = down1.parent_id  
left outer 
  join category as down3
    on down3.id = down2.parent_id
order
    by node.sort_order
    ,down1.sort_order
    ,down2.sort_order
    ,down3.sort_order");
        return $result = $query->result();
    }

}
