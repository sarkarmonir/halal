<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2015 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
class Coupons extends CI_Model
{
        /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'coupon';
        $this->table1 = 'coupon_product';
        $this->table2 = 'coupon_category';
        parent :: __construct();
    }
    /**
     * Add New coupon in database table coupon id
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add()
    {
        $current_date = date('Y-m-d');
        $value = array(
            'name'=>             $this->input->post('name'),
            'code'=>             $this->input->post('code'),
            'discount'=>         $this->input->post('discount'),
            'type'=>             $this->input->post('type'),
            'total_amount'=>     $this->input->post('total_amount'),
            'date'=>             $current_date,
            'date_start'=>       $this->input->post('start_date'),
            'date_end'=>         $this->input->post('end_date'),
            'status'=>           $this->input->post('status')
//            'apply_with_discount'=> $this->input->post('apply_with_discount')
        );
        //rint_r($value); die();
        /**
         * call data model for save basic data in coupon table and return coupon id
         * @author rjs
         */
         $coupon = $this->data->save($this->table,$value);
         if($coupon)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
    /**
     * 
     * @param type $id
     * @return boolean
     */
    public function edit($id)
    { 
         $value = array(
            'name'=>             $this->input->post('name'),
            'discount'=>         $this->input->post('discount'),
            'code'=>             $this->input->post('code'),
            'type'=>             $this->input->post('type'),
            'total_amount'=>     $this->input->post('total_amount'),
            'max_uses'=>         $this->input->post('max_uses'),
            'max_user_uses'=>    $this->input->post('max_user_uses'),
            'shipping'=>         $this->input->post('shipping'),
            'date_start'=>       $this->input->post('start_date'),
            'date_end'=>         $this->input->post('end_date'),
            'status'=>           $this->input->post('status')
//            'apply_with_discount'=> $this->input->post('apply_with_discount')
        );
        
        /**
         * call data model for save basic data in discount table and return coupon
         * @author rjs
         */
         $coupon = $this->data->update($this->table,$id,$value);
         if($coupon)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
   public function product($id)
   {
       $query = $this->db->query('select product.id,product.name from product left join coupon_product ON coupon_product.product_id = product.id');
       $product = $query->result();
       return $product;
   }
   
      public function category($id)
   {
       $query = $this->db->query('select category.name, category.id from category left join coupon_category ON coupon_category.category_id = category.id');
       $category = $query->result();
       return $category;
   }
}
