<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
class Payment_method_model extends CI_Model
{
        /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'payments';
        parent :: __construct();
    }
    /**
     * Add New payment Method in database table payments 
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add()
    {
        
        $type = $this->input->post('type');
        
        if($type == 'paypal')
        {
            $config = array(
            'sandbox'=>       $this->input->post('sandbox'),
            'publish'=>       $this->input->post('status'),
            'email'=>         $this->input->post('email'),
            'api_username'=>  $this->input->post('api_username'),
            'password'=>      $this->input->post('api_password'),
            'signature'=>     $this->input->post('api_signature'),
            'currency_code'=> $this->input->post('currency_code'),    
            );
            $config = json_encode($config);
            
            $value = array(
            'title'=>         $this->input->post('title'),
            'description'=>   $this->input->post('description'),
            'type'=>          $this->input->post('type'),
            'default'=>       0,
            'configs'=>        $config,
            'date'=>          date('Y-m-d'),
            'status'=>        $this->input->post('status')
        );
        }else if($type =='direct')
        {
            $config = array(
            'publish'=>       $this->input->post('status'),
            'message'=>       $this->input->post('message'),  
            );
            $config = json_encode($config);
            $value = array(
            'title'=>         $this->input->post('title'),
            'description'=>   $this->input->post('description'),
            'configs'=>        $config,
            'type'=>          $this->input->post('type'),
            'default'=>       $this->input->post('default'),    
            'status'=>        $this->input->post('status')
        ); 
        }else if($type == 'authorize')
        {
            $config = array(
            'sandbox'=>       $this->input->post('sandbox'),
            'publish'=>       $this->input->post('status'),
            'api_login_id'=> $this->input->post('api_login_id'),
            'transaction_key'=> $this->input->post('transaction_key')
            );
            $config = json_encode($config);
            
            $value = array(
            'title'=>         $this->input->post('title'),
            'description'=>   $this->input->post('description'),
            'type'=>         $this->input->post('type'),
            'default'=>       $this->input->post('default'), 
            'configs'=>        $config,
            'status'=>        $this->input->post('status')
        );
        }
        
        /**
         * call data model for save basic data in payment method model table and return customer_group id
         * @author rjs
         */
         $payment = $this->data->save($this->table,$value);
         if($payment)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
    }
    public function edit($id)
    { 
         $type = $this->input->post('type');
        
        if($type == 'paypal')
        {
            $config = array(
            'sandbox'=>       $this->input->post('sandbox'),
            'publish'=>       $this->input->post('status'),
            'email'=>         $this->input->post('email'),
            'api_username'=>  $this->input->post('api_username'),
            'password'=>      $this->input->post('api_password'),
            'signature'=>     $this->input->post('api_signature'),
            'currency_code'=> $this->input->post('currency_code'),    
            );
            $config = json_encode($config);
            
            $value = array(
            'title'=>         $this->input->post('title'),
            'description'=>   $this->input->post('description'),
            'type'=>          $this->input->post('type'),
            'configs'=>        $config,
            'date'=>          date('Y-m-d'),
            'status'=>        $this->input->post('status')
        );
        }else if($type =='direct')
        {
            $config = array(
            'publish'=>       $this->input->post('status'),
            'message'=>       $this->input->post('message'),  
            );
            $config = json_encode($config);
            $value = array(
            'title'=>         $this->input->post('title'),
            'description'=>   $this->input->post('description'),
            'configs'=>        $config,
            'type'=>          $this->input->post('type'),   
            'status'=>        $this->input->post('status')
        ); 
        }else if($type == 'authorize')
        {
            $config = array(
            'sandbox'=>       $this->input->post('sandbox'),
            'publish'=>       $this->input->post('status'),
            'api_login_id'=> $this->input->post('api_login_id'),
            'transaction_key'=> $this->input->post('transaction_key')
            );
            $config = json_encode($config);
            
            $value = array(
            'title'=>         $this->input->post('title'),
            'description'=>   $this->input->post('description'),
            'type'=>         $this->input->post('type'), 
            'configs'=>        $config,
            'status'=>        $this->input->post('status')
        );
        } 
       
        /**
         * call data model for save basic data in payment Method table and return payment Method id
         * @author rjs
         */
         $payment = $this->data->update($this->table,$id,$value);
         if($payment)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
   
}

