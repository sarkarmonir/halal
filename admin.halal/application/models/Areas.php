<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
class Areas extends CI_Model
{
        /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'area';
        parent :: __construct();
    }
    /**
     * Add New area in database table area
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add()
    {
        $value = array(
            'name'=>          $this->input->post('name'),
            'postcode'=>          $this->input->post('postcode'),
            'delivery_cost'=>         $this->input->post('delivery_cost'),
            'delivery_time'=>          $this->input->post('delivery_time'),
            'status'=>        $this->input->post('status')
        );
        
        /**
         * call data model for save basic data in customer_group table and return customer_group id
         * @author rjs
         */
         $area = $this->data->save($this->table,$value);
         if($area)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
    }
    public function edit($id)
    { 
            $value = array(
            'name'=>          $this->input->post('name'),
            'postcode'=>          $this->input->post('postcode'),
            'delivery_cost'=>         $this->input->post('delivery_cost'),
            'delivery_time'=>          $this->input->post('delivery_time'),
            'status'=>        $this->input->post('status')
        );
       
        /**
         * call data model for save basic data in area table and return area id
         * @author rjs
         */
         $area = $this->data->update($this->table,$id,$value);
         if($area)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
   
}

