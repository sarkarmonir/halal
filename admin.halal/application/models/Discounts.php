<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * @date: 2015-9-10
 * 
 * @copyright  Copyright (C) 2015 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
class Discounts extends CI_Model
{
        /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'discount';
        $this->table1 = 'discount_product';
        $this->table2 = 'discount_category';
        parent :: __construct();
    }
    /**
     * Add New discount in database table discount id
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add()
    {
        $value = array(
            'name'=>             $this->input->post('name'),
            'discount'=>         $this->input->post('discount'),
            'type'=>             $this->input->post('type'),
            'apply'=>            $this->input->post('apply'),
            'shipping'=>         $this->input->post('shipping'),
            'date_start'=>       $this->input->post('start_date'),
            'date_end'=>         $this->input->post('end_date'),
            'status'=>           $this->input->post('status')
        );
    
        /**
         * call data model for save basic data in discount table and return discount id
         * @author rjs
         */
         $discount = $this->data->save($this->table,$value);
         if($discount)
         {
             $apply = $this->input->post('apply');
             if($apply == 'product')
                 {
                  $product = $this->input->post('product');
                  foreach ($product as $product)
                    {
                       $value = array(
                           'product_id'  =>$product,
                           'id'  =>$discount
                       );
                       $this->data->save($this->table1,$value);
                    }
                  
                 }else if($apply == 'category')
                 {
                   $category = $this->input->post('category');
                   foreach ($category as $category)
                    {
                       $value = array(
                           'category_id'  =>$category,
                           'id'  =>$discount
                       );
                       $this->data->save($this->table2,$value);
                    }
                   
                 }
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
    /**
     * 
     * @param type $id
     * @return boolean
     */
    public function edit($id)
    { 
         $value = array(
            'name'=>             $this->input->post('name'),
            'discount'=>         $this->input->post('discount'),
            'type'=>             $this->input->post('type'),
            'apply'=>            $this->input->post('apply'),
            'shipping'=>         $this->input->post('shipping'),
            'date_start'=>       $this->input->post('start_date'),
            'date_end'=>         $this->input->post('end_date'),
            'status'=>           $this->input->post('status')
        );

        /**
         * call data model for save basic data in discount table and return coupon
         * @author rjs
         */
         $discount = $this->data->update($this->table,$id,$value);
         if($discount)
         {
             
              $this->data->delete($this->table2,$id);
              $this->data->delete($this->table1,$id);
              
              $apply = $this->input->post('apply');
              if($apply == 'product')
                 {
                   $product = $this->input->post('product');
                    if(!empty($product))
                    {
                    foreach ($product as $product)
                    {
                       $value = array(
                           'product_id' =>$product,
                           'id'  =>$id
                       );
                       $this->data->save($this->table1,$value);
                       $i++;
                    }
                    }
                 }else if($apply == 'category')
                 {
                        $category = $this->input->post('category');
                        if(!empty($category))
                        { 
                        foreach ($category as $cat)
                        {
                           $value = array(
                               'category_id'  =>$cat,
                               'id'  =>$id
                           );
                           $this->data->save($this->table2,$value);
                           $i++;
                        }
                        }
                 }
            
             
             
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
   public function product($id)
   {
       $query = $this->db->query("select product.id,product.name from discount_product left join product ON product.id = discount_product.product_id where discount_product.id=$id");
       $product = $query->result();
       return $product;
   }
   
      public function category($id)
   {
       $query = $this->db->query("select category.name, category.id from discount_category left join category ON category.id = discount_category.category_id where discount_category.id = $id");
       $category = $query->result();
       return $category;
   }
}
