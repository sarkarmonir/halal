
<!-- jQuery 2.2.3 -->
<script src="<?=site_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?=site_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>


<!-- DataTables -->
<script src="<?=site_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?=site_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>

<!-- FastClick -->

<script src="<?=site_url('assets/plugins/fastclick/fastclick.js');?>"></script>

<!-- AdminLTE App -->
<script src="<?=site_url('assets/dist/js/app.min.js');?>"></script>
<!-- Sparkline -->
<script src="<?=site_url('assets/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
<!-- jvectormap -->
<script src="<?=site_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
<script src="<?=site_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>

<script src="<?=site_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>

<!-- SlimScroll 1.3.0 -->
<script src="<?=site_url('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?=site_url('assets/plugins/chartjs/Chart.min.js');?>"></script>
<script src="<?=site_url('assets/plugins/select2/select2.full.min.js');?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="<?=site_url('assets/dist/js/pages/dashboard2.js');?>"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="<?=site_url('assets/dist/js/demo.js');?>"></script>

<!-- CK Editor -->
<script src="<?=site_url('assets/plugins/ckeditor/ckeditor.js');?>"></script>
<script src="<?=site_url('assets/js/common.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/multiselect.min.js"></script>

<?php if ($this->session->flashdata('message') != ""){ ?>
    <script>
        toastr.info('<?php echo $this->session->flashdata('message');?>');
    </script>>
<?php } ?> 

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
  });
  // ck_editor
  $(function () {
    CKEDITOR.replace('ck_editor');
  });

  // global msg
  $(document).ready (function(){
    $(".alert-dismissable").fadeTo(2000, 500).fadeOut(500, function(){
      $(".alert-dismissable").alert('close');
    });
  });

</script>