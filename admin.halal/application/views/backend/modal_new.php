    <script type="text/javascript">
    function showAjaxNewModal(url)
    {
        // SHOWING AJAX PRELOADER IMAGE
        jQuery('#modal_ajax_new .modal-body').html('<div style="text-align:center;margin-top:200px;"><img src="<?php echo base_url();?>assets/images/preloader.gif" style="height:25px;" /></div>');
        
        // LOADING THE AJAX MODAL
        jQuery('#modal_ajax_new').modal('show', {backdrop: 'true'});
        
        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
            url: url,
            success: function(response)
            {
                jQuery('#modal_ajax_new .modal-body').html(response);
            }
        });

        $('#modal_ajax_new,#modal_ajax_new .modal-dialog,#modal_ajax_new .modal-content').height($(window).height());
        $('#modal_ajax_new .modal-body').height($(window).height()-(52+71));
        console.log($('body').height());
        console.log($(window).height());
        console.log($(document).height());
    }
    </script>
    <style type="text/css">
        #modal_ajax_new, #modal_ajax_new .modal-dialog{
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            background-color: rgba(52, 152, 219, 0.35);
            overflow-y: auto;
            z-index: 9999;
            opacity: 1;
            animation-duration: 0.6s;   
            padding:0;         
        }
        #modal_ajax_new .modal-body{padding: 0!important;}
        body.modal-open{width: 100%!important;}
    </style>
<!--     <link rel="stylesheet" type="text/css" href="http://localhost/testserver/animatedModal.js-master/demo/css/normalize.min.css">
<link rel="stylesheet" type="text/css" href="http://localhost/testserver/animatedModal.js-master/demo/css/animate.min.css"> -->
    <!-- (Ajax Modal)-->
    <div class="modal fade animated lightSpeedIn" id="modal_ajax_new" style="display:none;">
        <div class="modal-dialog" >
            <div class="modal-content">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $system_name;?></h4>
                </div>                
                <div class="modal-body" style="height:auto;overflow:auto;">
                
                    
                    
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    <script type="text/javascript">
    function confirm_modal(delete_url , post_refresh_url)
    {
        $('#preloader-delete').html('');
        jQuery('#modal_delete').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link').setAttribute("onClick" , "delete_data('" + delete_url + "' , '" + post_refresh_url + "')" );
        document.getElementById('delete_link').focus();
    }
    </script>
    
    <!-- (Normal Modal)-->
    <div class="modal fade" id="modal_delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>
                
                
                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <span id="preloader-delete"></span>
                    </br>
                      <button type="button" class="btn btn-danger" id="delete_link" onClick=""><?php echo get_phrase('delete');?></button>
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="delete_cancel_link"><?php echo get_phrase('cancel');?></button>
                    
                </div>
            </div>
        </div>
    </div>
<!-- 
    <script type="text/javascript" src="http://localhost/testserver/animatedModal.js-master/demo/js/animatedModal.min.js"></script> -->