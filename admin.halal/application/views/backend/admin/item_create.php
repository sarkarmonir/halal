

<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo get_phrase('create_item'); ?></h3>
            </div>

            <div class="box-body">


                <!-- <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>admin/doctor/create" method="post" enctype="multipart/form-data"> -->
                <form action="<?php echo base_url(); ?>customer_group/doadd" method="post" class="form-horizontal form-groups-bordered" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Group name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Group Name" value="<?php echo set_value('name'); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Short Description</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-body">
                                    <input type="submit" class="btn btn-primary" value="Add Admin Type">                                    
                                </div>

                            
                            
                        </div>

                    </div>
                </form>

            </div>

        </div>

    </div>
</div>


<div class="row">
    <div class="col-md-12">

              <!-- Input addon -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo get_phrase('create_item'); ?></h3>
            </div>

            <div class="box-body">


                <!-- <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>admin/doctor/create" method="post" enctype="multipart/form-data"> -->
                <?php echo form_open('product/create',array('class'=>'form-horizontal form-groups-bordered','role'=>'form')); ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Product name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="title" class="form-control" id="title" placeholder="Item Name" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Short Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="short_description" placeholder="Product short description.."></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="details" placeholder="Product description.."></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label"><?php echo get_phrase('regular_price'); ?></label>

                                <div class="col-sm-9">
                                    <input type="text" name="regular_price" class="form-control" id="regular_price" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="sale_price" class="col-sm-3 control-label"><?php echo get_phrase('sale_price'); ?></label>

                                <div class="col-sm-9">
                                    <input type="text" name="sale_price" class="form-control" id="sale_price" >
                                </div>
                            </div>                 
                            <div class="form-group">
                                <label for="sku" class="col-sm-3 control-label"><?php echo get_phrase('sku'); ?></label>

                                <div class="col-sm-9">
                                    <input type="text" name="sku" class="form-control" id="sku" >
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="stock_quantity" class="col-sm-3 control-label"><?php echo get_phrase('stock_quantity'); ?></label>

                                <div class="col-sm-9">
                                    <input type="text" name="quantity" class="form-control" id="stock_quantity" >
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-4">
                                  <!-- Input addon -->
                              <div class="box box-primary">
                                <div class="box-header with-border">
                                  <h3 class="box-title">Product Image</h3>
                                </div>

                                <div class="box-body">
                                    <div class="" ng-app="main-App" ng-controller="AdminController">
                                            <input ng-model="form.image" type="file" class="form-control input-lg" accept="image/*" onchange="angular.element(this).scope().uploadedFile(this)" style="width:400px; display: none;" id="fileInput" >
                                            <img ng-src="{{image_source}}" style="width:300px;" ng-click="triggerUpload()">
                                            <input type="hidden" value="{{logo_url}}" id="logo_url" name="featured_image"/>
                                    </div>                                       
                                </div>
                            </div>

                              <div class="box box-primary">
                                <div class="box-header with-border">
                                  <h3 class="box-title">Product Category</h3>
                                </div>

                                <div class="box-body">
                           
                                    <select name="categories" class="form-control">
                                        <option value="">--</option>
                                    </select>                                     
                                </div>
                            </div>
                            
                              <div class="box box-primary">
                                <div class="box-header with-border">
                                  <h3 class="box-title">Product Status</h3>
                                </div>

                                <div class="box-body">
                                    <input type="submit" class="btn btn-primary" value="Publish">                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script type="text/javascript">

        var app =  angular.module('main-App',[]);

        app.controller('AdminController', function($scope, $http) {

          $scope.form = [];
          $scope.files = [];

          $scope.image_source="<?=base_url();?><?= $this->db->get_where('settings', array('type' => 'logo_url'))->row()->description; ?>";
          $scope.logo_url="<?= $this->db->get_where('settings', array('type' => 'logo_url'))->row()->description; ?>";

          $scope.submit = function(image) {
            // $scope.form.image = $scope.files[0];
            $scope.form.image = image;

            $http({
              method  : 'POST',
              url     : "<?=base_url('admin/image_upload')?>",
              processData: false,
              transformRequest: function (data) {
                  var formData = new FormData();
                  formData.append("userfile", $scope.form.image);  
                  return formData;  
              },  
              data : $scope.form,
              headers: {
                     'Content-Type': undefined
              }
           }).success(function(data){
                console.log(data);
                $scope.logo_url = data.image_upload_path+data.file_name;
                console.log(data.image_upload_path);
           });

          };

          $scope.triggerUpload=function()
            {
             var fileuploader = angular.element("#fileInput");
                fileuploader.on('click',function(){
                    console.log("File upload triggered programatically");
                })
                fileuploader.trigger('click')
            }

          $scope.uploadedFile = function(element) {
            $scope.currentFile = element.files[0];
            var reader = new FileReader();
            // console.log(reader);
            $scope.submit($scope.currentFile);
            reader.onload = function(event) {
              $scope.image_source = event.target.result
              $scope.$apply(function($scope) {
                $scope.files = element.files;
              });
            }
                    reader.readAsDataURL(element.files[0]);
          }

        });
    </script>