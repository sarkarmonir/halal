
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">+Add New Tax</h3>
            </div>

            <div class="box-body">
                <div id="errorMsg" style="color:#CC0000;"><?php
                    if (validation_errors()) {
                        echo '<div id="validation_errors" title="Error:">';
                        echo '<div class="response-msgs errors ui-corner-all"><span></span><br /><ul>';
                        echo validation_errors();
                        echo '</ul></div>';
                        echo '</div>';
                    }
                    ?></div>
                            <form id="discont_form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>tax/doadd" method="post" enctype="multipart/form-data">
<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Tax Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Tax Name" value="<?php echo set_value('name', $tax[0]->name); ?>" />
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Tax Title<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="title" required class="form-control" id="title" placeholder="Tax Title" value="<?php echo set_value('title', $tax[0]->title); ?>" />
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Type<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="type" class="form-control">
                                                    <option <?php
                                                    if ($tax[0]->type == 'P') {
                                                        echo "selected='selected'";
                                                    }
                                                    ?> value="P">Percentage</option>
                                                    <option <?php
                                                    if ($tax[0]->type == 'F') {
                                                        echo "selected='selected'";
                                                    }
                                                    ?> value="F">Fixed</option>
                                                </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Tax Rate<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="rate" required="required" value="<?php echo set_value('rate', $tax[0]->rate); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                                    <option <?php
                                                    if ($tax[0]->status == 1) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?> value="1" selected="selected">Enable</option>
                                                    <option <?php
                                                    if ($tax[0]->status == 0) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?> value="0">Disable</option>
                                                </select>
                                </div>

                            </div>


                            <div class="box-body">
                                <input type="submit" class="btn btn-primary" value="Update Tax">                                    
                            </div>

                        </div>

                    </div>
                                
                        </div>
                        
                        </form>
                    </div>

    </div>

</div>
</div>

<script>
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

    });
    
        $('#datepicker').datepicker({
      autoclose: true
    });
</script>

