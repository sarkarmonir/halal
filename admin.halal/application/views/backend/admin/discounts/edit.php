
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Update Discount</h3>
            </div>

            <div class="box-body">
                <div id="errorMsg" style="color:#CC0000;"><?php
                    if (validation_errors()) {
                        echo '<div id="validation_errors" title="Error:">';
                        echo '<div class="response-msgs errors ui-corner-all"><span></span><br /><ul>';
                        echo validation_errors();
                        echo '</ul></div>';
                        echo '</div>';
                    }
                    ?></div>
                            <form id="discont_form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>discount/doedit" method="post" enctype="multipart/form-data">
<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Discount Name" value="<?php echo set_value('name', $discount[0]->name); ?>" />
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Type<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="type" class="form-control">
                                                    <option <?php
                                                    if ($discount[0]->type == 'p') {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?> value="p">Percentage</option>
                                                    <option <?php
                                                    if ($discount[0]->type == 'f') {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?> value="f">Fixed</option>
                                                </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Discount<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="discount" required="required" value="<?php echo set_value('discount', $discount[0]->discount); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label"> Start Date<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input required type="date" name="start_date"  placeholder="Select coupon start date" class="form-control"  required id="datepicker" value="<?php echo set_value('start_date', $discount[0]->date_start); ?>">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label"> End Date<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input required type="date" name="end_date" placeholder="Select coupon end date" class="form-control" id="datepicker"  required value="<?php echo set_value('end_date', $discount[0]->date_end); ?>">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Discount Apply On<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="apply" class="form-control" id="discount_apply">
<!--                                                    <option <?php
                                                        //if ($discount[0]->apply == 'all') {
                                                            //echo "selected='selected'";
                                                       // }
                                                        ?> value="all">All Order</option>-->
                                                    <option <?php
                                                        if ($discount[0]->apply == 'product') {
                                                            echo "selected='selected'";
                                                        }
                                                        ?> value="product">Product</option>

                                                </select>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Products<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select multiple style="width:100%" class="select2" name="product[]" required>
                                                    <?php foreach ($discount_product as $product) { ?>
                                                        <option value="<?php echo $product->id; ?>"selected='selected' ><?php echo $product->name; ?></option>
                                                    <?php } ?>
                                                    <?php foreach ($products as $product) { ?>
                                                        <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
<?php } ?>
                                                </select>
                                </div>

                            </div>
                            

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                                    <option <?php
                                                    if ($discount[0]->status == 1) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?> value="1" selected="selected">Enable</option>
                                                    <option <?php
                                                    if ($discount[0]->status == 0) {
                                                        echo "selected='selected'";
                                                    }
                                                    ?> value="0">Disable</option>
                                                </select>
                                </div>

                            </div>


                            <div class="box-body">
                                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                <input type="submit" class="btn btn-primary" value="UPdate Discount">                                    
                            </div>



                        </div>


                    </div>
                                
                        </div>
                        
                        </form>
                    </div>

    </div>

</div>
</div>

<script>
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

    });
    
        $('#datepicker').datepicker({
      autoclose: true
    });
</script>

