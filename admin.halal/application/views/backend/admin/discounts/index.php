<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Discounts</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="<?php echo base_url(); ?>discount/add" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                    <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                    <script>
                                     var table =['discount'];
                                    </script>
                                    <a href="#" class="btn btn-danger" onclick="return deleteall(table,image);"><i class="fa fa-trash-o"></i></a>

                                </div>
                            </div><br>
                             <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
                                <thead>			                
                                    <tr>
                                        <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Discount Name</th>
                                        <th data-hide="phone"></i> Type</th>
                                        <th data-hide="phone"> Discount</th>
                                        <!--<th data-hide="phone"> Shipping</th>-->
                                        <th data-hide="phone"> Date Start</th>
                                        <th data-hide="phone"> Date End</th>
<!--                                        <th data-hide="phone"> Uses Total</th>
                                        <th data-hide="phone"> Uses Customer</th>-->
                                        <th data-hide="phone"> Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($discount as $discount) { ?>
                                        <tr id="row_<?php echo $discount->id; ?>">
                                            <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $discount->id; ?>" value="<?php echo $discount->id; ?>-img/" /></td>
                                            <td><?php echo $discount->name; ?></td>
                                            <td><?php if($discount->type=='p'){echo "Percentage";}else{echo "Fixed";} ?></td>
                                            <td><?php echo $discount->discount; ?></td>
                                            <!--<td><?php if($discount->shipping ==1){echo "Free Shipping";}else{echo "Not Shipping";} ?></td>-->
                                            <td><?php echo mydate($discount->date_start,'/'); ?></td>
                                            <td><?php echo mydate($discount->date_end,'/'); ?></td>
<!--                                            <td><?php echo $discount->uses_total; ?></td>
                                            <td><?php echo $discount->uses_customer; ?></td>-->
                                            <script>var table='discount';</script>
                                            <td><?php if($discount->status == 1){echo "<span id='status_".$discount->id."'><a href='#' class = 'btn btn-success' onclick='status_change(table,".$discount->id.",".$discount->status.");'>Enable</a></span>";}else{echo "<span id='status_".$discount->id."'><a href='#' class = 'btn btn-danger'  onclick='status_change(table,".$discount->id.",".$discount->status.");'>Disable</a></span>";} ?></td>
                                            <td><div class="btn-group">
                                                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="<?php echo base_url() ?>discount/edit/<?php echo $discount->id; ?>">Edit</a>
                                                        </li>
                                                        <li>
                                                            <script>
                                                                var table = ["discount"];
                                                                var image = "img";
                                                            </script>
                                                            <a href="#" onclick="return dodelete(table,<?php echo $discount->id; ?>,image);">Delete</a>
                                                        </li>

                                                    </ul>
                                                </div></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
</div>
<script type="text/javascript">

                                $(function() {
// $("#example1").DataTable();
                                    $('#table-2').DataTable();
                                });
</script>

