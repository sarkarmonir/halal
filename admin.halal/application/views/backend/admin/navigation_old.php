<div class="sidebar-menu">
    <header class="logo-env" >
        <?php $logo = base_url().$this->db->get_where('settings', array('type' => 'logo_url'))->row()->description; ?>
        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="<?=$logo;?>"  style="max-height:60px;"/>
            </a>
        </div>

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>
    <div class="sidebar-user-info">

        <div class="sui-normal">
            <a href="#" class="user-link">
                <img src="<?php echo $this->crud_model->get_image_url($this->session->userdata('login_type'), $this->session->userdata('login_user_id'));?>" alt="" class="img-circle" style="height:44px;">

                <span><?php echo get_phrase('welcome'); ?>,</span>
                <strong><?php
                    echo $this->db->get_where($this->session->userdata('login_type'), array($this->session->userdata('login_type') . '_id' =>
                        $this->session->userdata('login_user_id')))->row()->name;
                    ?>
                </strong>
            </a>
        </div>

        <div class="sui-hover inline-links animate-in"><!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile -->				
            <a href="<?php echo base_url(); ?><?php echo $account_type; ?>/profile">
                <i class="entypo-pencil"></i>
                <?php echo get_phrase('edit_profile'); ?>
            </a>

            <a href="<?php echo base_url(); ?><?php echo $account_type; ?>/profile">
                <i class="entypo-lock"></i>
                <?php echo get_phrase('change_password'); ?>
            </a>

            <span class="close-sui-popup">×</span><!-- this is mandatory -->			
        </div>
    </div>


    <div style="border-top:1px solid rgba(69, 74, 84, 0.7);"></div>	
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->


        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>admin/dashboard">
                <i class="fa fa-desktop"></i>
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>        
 
        <!-- Products -->
        <li class="<?php if(in_array($page_name, array('category_manage','category_create','item_manage','item_create'))) echo "opened active"; ?>">
            <a href="#">
                <i class="fa fa-product"></i>
                <span><?=get_phrase('Product'); ?></span>
            </a>
            <ul>
                <li class="<?php if($page_name=='item_manage') echo "active"; ?>">
                    <a href="<?=base_url('product') ?>">
                        <span><i class="fa fa-list"></i> <?=get_phrase('product_list'); ?></span>
                    </a>
                </li>
                <li class="<?php if($page_name=='item_create') echo "active"; ?>">
                    <a href="<?=base_url('product/create') ?>">
                        <span><i class="fa fa-list"></i> <?=get_phrase('create_product'); ?></span>
                    </a>
                </li>
                <li class="<?php if(in_array($page_name,array('category_manage','category_create'))) echo "active"; ?>">
                    <a href="<?=base_url('category') ?>">
                        <span><i class="fa fa-list"></i> <?=get_phrase('category'); ?></span>
                    </a>
                </li>

            </ul>
        </li>

        <!-- SETTINGS -->
        <li class="<?php if(in_array($page_name, array('system_settings','manage_language'))) echo "opened active";?> ">
            <a href="#">
                <i class="fa fa-wrench"></i>
                <span><?php echo get_phrase('settings'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>admin/system_settings">
                        <span><i class="fa fa-h-square"></i> <?php echo get_phrase('system_settings'); ?></span>
                    </a>
                </li>                             
            </ul>
        </li>

        <!-- ACCOUNT -->
        <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>admin/manage_profile">
                <i class="entypo-lock"></i>
                <span><?php echo get_phrase('profile'); ?></span>
            </a>
        </li>



    </ul>

</div>