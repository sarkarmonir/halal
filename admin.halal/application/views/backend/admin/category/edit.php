
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Category Update</h3>
            </div>

            <div class="box-body">


                <!-- <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>admin/doctor/create" method="post" enctype="multipart/form-data"> -->
                <form action="<?php echo base_url(); ?>categories/doedit" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Category Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Category Name" value="<?php echo set_value('name', $category[0]->name); ?>" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Category Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <textarea row="3" name="description" required class="form-control" placeholder="Description"  ><?php echo set_value('description', $category[0]->description); ?></textarea>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="parent" class="col-sm-3 control-label">Parent</label>
                                <div class="col-sm-8">
                                    <select name="parent" class="form-control" >
                                        <option value="0">-- None --</option>
                                        <?php foreach ($allcategory as $cat) { ?>
                                            <?php
                                            if ($category[0]->parent_id == $cat->id) {
                                                echo "<option value='" . $cat->id . "' selected='selected'>" . $cat->name . "</option>";
                                            } else {
                                                ?>
                                                <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Category Image<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <div class="col col-sm-10"><label class="input">Image
                                                            <input type="file" name="image" value="<?php echo set_value('image'); ?>" />
                                                            <input type="hidden" name="img" value="<?php echo $category[0]->image; ?>" />
                                                        </label></div>
                                                    <div class="col col-sm-2"><img src="<?php echo base_url(); ?>img/category/<?php echo $category[0]->image; ?>" height="50px" width="50px"/></div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Sort Order<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="sort_order" class="form-control" value="<?php echo set_value('sort_order', $category[0]->sort_order); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">

                                                        <option <?php
                                                        if ($category[0]->status == 1) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?> value="1" >Enable</option>

                                                        <option <?php
                                                        if ($category[0]->status == 0) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?> value="0">Disable</option>

                                                    </select>
                                </div>
                            </div>
                            <div class="box-body">
                                <input type="hidden" name="id" value="<?php echo $category[0]->id; ?>" />
                                <input type="submit" class="btn btn-primary" value="Update Category">                                    
                            </div>



                        </div>


                    </div>
                </form>

            </div>

        </div>

    </div>
</div>