
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo get_phrase('create_item'); ?></h3>
            </div>

            <div class="box-body">


                <!-- <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>admin/doctor/create" method="post" enctype="multipart/form-data"> -->
                <form action="<?php echo base_url(); ?>categories/doadd" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Category Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Category Name" value="<?php echo set_value('name'); ?>" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Category Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <textarea row="3" name="description" required class="form-control" placeholder="Description" value="<?php echo set_value('description'); ?>" ></textarea>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="parent" class="col-sm-3 control-label">Parent</label>
                                <div class="col-sm-8">
                                    <select name="parent" class="form-control" >
                                        <option value="0" selected="selected">----</option>
                                        <?php foreach ($allcategory as $category) { ?>
                                            <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Category Image<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="file" name="image" class="form-control" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Sort Order<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="sort_order" class="form-control" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Short Description</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-body">
                                    <input type="submit" class="btn btn-primary" value="Add Category">                                    
                                </div>

                            
                            
                        </div>
                        

                    </div>
                </form>

            </div>

        </div>

    </div>
</div>