
<div style="clear:both;"></div>

<br>
<!-- Input addon -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Product Category</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <a href="<?php echo base_url(); ?>categories/add" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                <script>
                    var table = ['category'];
                </script>
                <a href="#" class="btn btn-danger" onclick="return deleteall(table, image);"><i class="fa fa-trash-o"></i></a>
            </div>
        </div><br>
        <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
            <thead>			                
                <tr>
                    <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Name</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Image</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Sort Order</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($allcategory as $category) {
                    //if(!empty($category->down3_id)){$category->id = $category->down3_id;}elseif (!empty($category->down3_id)) { $category->id = $category->down2_id;}elseif (!empty($category->down1_id)) { $category->id = $category->down1_id; }else{$category->id = $category->id;}
                    //if(!empty($category->down3_sort_order)){$category->sort_order = $category->down3_sort_order;}elseif (!empty($category->down3_sort_order)) { $category->sort_order = $category->down2_sort_order;}elseif (!empty($category->down1_sort_order)) { $category->sort_order = $category->down1_sort_order; }else{$category->sort_order = $category->sort_order;}
                    //if(!empty($category->down3_image)){$category->sort_image = $category->down3_image;}elseif (!empty($category->down3_image)) { $category->image = $category->down2_image;}elseif (!empty($category->down1_image)) { $category->image = $category->down1_image; }
                    ?>

                    <tr id="row_<?php echo $category->id; ?>">
                        <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $category->id; ?>" value="<?php echo $category->id; ?>-img/category/<?php //echo $category->image;  ?>" /></td>
                        <td><?php if (!empty($category->down3_name)) {
                        echo $category->down3_name . " <span style='color:red;'> > </span> ";
                    }if (!empty($category->down2_name)) {
                        echo $category->down2_name . " <span style='color:red;'> > </span> ";
                    }if (!empty($category->down1_name)) {
                        echo $category->down1_name . " <span style='color:red;'>></span> ";
                    } echo $category->root_name; ?></td>
                        <td><img src="<?php echo base_url() ?>img/<?php if ($category->image != '') {
                        echo 'category/' . $category->image;
                    } else {
                        echo "/no_image.jpg";
                    } ?>" width="40px" /></td>
                        <td><?php echo $category->sort_order; ?></td>
                        <td><div class="btn-group">
                                <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="categories/edit/<?php echo $category->id; ?>">Edit</a>
                                    </li>
                                    <li>
                                        <script>
                        var table = ["category", "category_lng"];
                        var image = "img/category/<?php echo $category->image; ?>";
                                        </script>
                                        <a href="#" onclick="return dodelete(table,<?php echo $category->id; ?>, image);">Delete</a>
                                    </li>

                                </ul>
                            </div></td>
                    </tr>
<?php } ?>

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">

                    $(function() {
// $("#example1").DataTable();
                        $('#table-2').DataTable();
                    });
</script>
