
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">+Add New Coupon</h3>
            </div>

            <div class="box-body">
                <div id="errorMsg" style="color:#CC0000;"><?php
                    if (validation_errors()) {
                        echo '<div id="validation_errors" title="Error:">';
                        echo '<div class="response-msgs errors ui-corner-all"><span></span><br /><ul>';
                        echo validation_errors();
                        echo '</ul></div>';
                        echo '</div>';
                    }
                    ?></div>
                <form id="coupon_form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>coupon/doadd" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Coupon Name" value="<?php echo set_value('name'); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Code<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="code" required class="form-control" id="title" placeholder="Code" value="<?php echo set_value('code'); ?>" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Type<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="type" class="form-control">
                                        <option value="p">Percentage</option>
                                        <option value="f">Fixed</option>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Discount<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="discount" required="required" value="<?php echo set_value('discount'); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Minimum Apply Order Amount<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="total_amount" required="required" value="<?php echo set_value('total_amount'); ?>" />

                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label"> Start Date<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input required type="date" name="start_date"  placeholder="Select coupon start date" class="form-control"  required id="datepicker">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label"> End Date<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input required type="date" name="end_date" placeholder="Select coupon end date" class="form-control" id="datepicker"  required>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>

                            </div>


                            <div class="box-body">
                                <input type="submit" class="btn btn-primary" value="Add Coupon">                                    
                            </div>



                        </div>


                    </div>

            </div>
            
            </form>
        </div>

    </div>

</div>
</div>

<script>
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

    });
    
        $('#datepicker').datepicker({
      autoclose: true
    });
</script>