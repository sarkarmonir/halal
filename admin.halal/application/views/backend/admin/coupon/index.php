
<!-- Input addon -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Coupon</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="<?php echo base_url(); ?>coupon/add" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                    <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                    <script>
                                     var table =['coupon'];
                                    </script>
                                    <a href="#" class="btn btn-danger" onclick="return deleteall(table,image);"><i class="fa fa-trash-o"></i></a>

                                </div>
                            </div> </br>
                            <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
                                <thead>			                
                                    <tr>
                                        <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Coupon Name</th>
                                        <th data-hide="phone"> Coupon Code</th>
                                        <th data-hide="phone"></i> Type</th>
                                        <th data-hide="phone"> Discount</th>
                                        <th data-hide="phone"> Date Start</th>
                                        <th data-hide="phone"> Date End</th>
                                        <th data-hide="phone"> Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($coupon as $coupon) { ?>
                                        <tr id="row_<?php echo $coupon->id; ?>">
                                            <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $coupon->id; ?>" value="<?php echo $coupon->id; ?>-img/" /></td>
                                            <td><?php echo $coupon->name; ?></td>
                                            <td><?php echo $coupon->code; ?></td>
                                            <td><?php echo $coupon->type; ?></td>
                                            <td><?php echo $coupon->discount; ?></td>
                                            <td><?php echo $coupon->date_start; ?></td>
                                            <td><?php echo $coupon->date_end; ?></td>
                                            <script>var table='coupon';</script>
                                            <td><?php if($coupon->status == 1){echo "<span id='status_".$coupon->id."'><a href='#' class = 'btn btn-success' onclick='status_change(table,".$coupon->id.",".$coupon->status.");'>Enable</a></span>";}else{echo "<span id='status_".$coupon->id."'><a href='#' class = 'btn btn-danger'  onclick='status_change(table,".$coupon->id.",".$coupon->status.");'>Disable</a></span>";} ?></td>
                                           
                                            <td><div class="btn-group">
                                                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="coupon/edit/<?php echo $coupon->id; ?>">Edit</a>
                                                        </li>
                                                        <li>
                                                            <script>
                                                                var table = ["coupon"];
                                                                var image = "img";
                                                            </script>
                                                            <a href="#" onclick="return dodelete(table,<?php echo $coupon->id; ?>,image);">Delete</a>
                                                        </li>

                                                    </ul>
                                                </div></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
</div>
<script type="text/javascript">

                                $(function() {
// $("#example1").DataTable();
                                    $('#table-2').DataTable();
                                });
</script>
