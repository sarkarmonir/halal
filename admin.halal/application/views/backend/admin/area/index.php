<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Area Class</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="<?php echo base_url(); ?>area/add" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                    <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                    <script>
                                     var table =['area'];
                                     var image = '';
                                    </script>
                                    <a href="#" class="btn btn-danger" onclick="return deleteall(table,image);"><i class="fa fa-trash-o"></i></a>

                                </div>
                            </div><br>
                            <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
                                <thead>			                
                                    <tr>
                                        <th><input type="checkbox" name="selectall" id="selectall"/></th>
                                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Area Name</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Area Code</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Delivery Cost</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Delivery Time</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Default</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($area as $area) { ?>
                                            <tr id="row_<?php echo $area->id; ?>">
                                            <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $area->id; ?>" value="<?php echo $area->id; ?>-img/<?php //echo $country->image; ?>" /></td>
                                            <td><?php echo $area->name; ?></td>
                                            <td><?php echo $area->postcode; ?></td>
                                            <td><?php echo $area->delivery_cost; ?></td>
                                            <td><?php echo $area->delivery_time; ?></td>
                                            <script>var table='area';</script>
                                            <td><?php if($area->status == 1){echo "<span id='status_".$area->id."'><a href='#' class = 'btn btn-success' onclick='status_change(table,".$area->id.",".$area->status.");'>Enable</a></span>";}else{echo "<span id='status_".$area->id."'><a href='#' class = 'btn btn-danger'  onclick='status_change(table,".$area->id.",".$area->status.");'>Disable</a></span>";} ?></td>
                                            <td><?php $table = "area";$url = "area"; if($area->default == 1){echo "<a href='#' class = 'btn btn-success'><i class='fa fa-check'></i> </a>";}else{echo "<a href='".base_url()."common/default_change/$table/$area->id/$url"."' class = 'btn'><i class='fa fa-square-o'></i></a>";} ?></td>
                                            <td><div class="btn-group">
                                                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="area/edit/<?php echo $area->id; ?>">Edit</a>
                                                        </li>
                                                        <li>
                                                            <script>
                                                                var table = ["area"];
                                                                var image = "";
                                                            </script>
                                                            <a href="#" onclick="return dodelete(table,<?php echo $area->id; ?>,image);">Delete</a>
                                                        </li>

                                                    </ul>
                                                </div></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
</div>
</div>
<script type="text/javascript">

                                $(function() {
// $("#example1").DataTable();
                                    $('#table-2').DataTable();
                                });
</script>

