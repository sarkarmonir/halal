
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">+Add New Area</h3>
            </div>

            <div class="box-body">
                <div id="errorMsg" style="color:#CC0000;"><?php
                    if (validation_errors()) {
                        echo '<div id="validation_errors" title="Error:">';
                        echo '<div class="response-msgs errors ui-corner-all"><span></span><br /><ul>';
                        echo validation_errors();
                        echo '</ul></div>';
                        echo '</div>';
                    }
                    ?></div>
                            <form id="discont_form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>area/doadd" method="post" enctype="multipart/form-data">
<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Area Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Area Name" value="<?php echo set_value('name'); ?>" />
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Area Postcode<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="postcode" required class="form-control" id="title" placeholder="Area Postcode" value="<?php echo set_value('postcode'); ?>" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Delivery Cost<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="delivery_cost" required class="form-control" id="title" placeholder="Delivery Cost" value="<?php echo set_value('delivery_cost'); ?>" />
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Delivery Time<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="delivery_time" class="form-control">
                                        <option value="same_day">Same Day Delivery</option>
                                        <option value="next_day">Next Day Delivery</option>
                                        <option value="weekly">Weekly Delivery</option>
                                    </select>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>

                            </div>


                            <div class="box-body">
                                <input type="submit" class="btn btn-primary" value="Add Area">                                    
                            </div>

                        </div>

                    </div>
                                
                        </div>
                        
                        </form>
                    </div>

    </div>

</div>
</div>

<script>
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

    });
    
        $('#datepicker').datepicker({
      autoclose: true
    });
</script>
