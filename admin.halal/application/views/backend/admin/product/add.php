
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">+Add New Product</h3>
            </div>

            <div class="box-body">


                <!-- <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>admin/doctor/create" method="post" enctype="multipart/form-data"> -->
                <form action="<?php echo base_url(); ?>products/doadd" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Product Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Product Name" value="<?php echo set_value('name'); ?>" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Short Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <textarea row="3" name="short_description" required class="form-control" placeholder="Short Description" value="<?php echo set_value('short_description'); ?>" ></textarea>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <textarea row="3" name="description" required class="form-control" placeholder="Description" value="<?php echo set_value('description'); ?>" ></textarea>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Category</label>
                                <div class="col-sm-8">
                                    <select name="category[]" class="form-control select2" multiple="multiple" data-placeholder="You Can select Multiple Category" style="width: 100%;" >

                                        <?php foreach ($allcategory as $category) { ?>
                                            <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Image<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <div class="row" >
                                        <div class="col col-lg-6">
                                            <input type="file"  name="image"   id="myFile" onchange="imageshow(this);" required />
                                        </div>
                                        <div class="col col-lg-6 " >
                                            <img id="previewImg1" width="80px" src="<?php echo base_url(); ?>img/no_image.jpg" />
                                            <script>
                                                function imageshow(input) {

                                                    if (input.files && input.files[0]) {
                                                        var reader = new FileReader();

                                                        reader.onload = function(e) {
                                                            $('#previewImg1')
                                                                    .attr('src', e.target.result)
                                                                    .width(130);
                                                        }
                                                        reader.readAsDataURL(input.files[0]);

                                                    } else {
                                                        var filename = "";
                                                        filename = "file:\/\/" + input.value;
                                                        document.form2.previewImg1.src = filename;
                                                        document.form2.previewImg1.style.width = "80px";

                                                    }
                                                }
                                            </script>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">SKU<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="sku" class="form-control" required />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Unit Cost<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="unit_price" class="form-control" required />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Sale Price<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="sale_price" class="form-control" required />
                                </div>

                            </div>
                            <div class="form-group">
                                
                                        <label for="" class="col-sm-3 control-label">Unit Per Weight<span style="color:red;">*</span></label>
                                        <div class="col-sm-8">
                                            <select name="unit_per_weight" class="form-control">
                                                <option value="1">Pound</option>
                                                <option value="2">Packet</option>
                                                <option value="1" >PCS</option>
                                            </select>
                                        </div>

                                    
                                   
                                        <label for="" class="col-sm-3 control-label">Unit Per Quantity<span style="color:red;">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="unit_per_qty" class="form-control" required placeholder="e.g. : 2 pound, 2 packet" />
                                        </div>
                                    
                             


                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Quantity<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="quantity" class="form-control" required />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Minimum Quantity<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="min_quantity" class="form-control" required value="1" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tax Classs<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="tax" class="form-control" required>
                                        <?php foreach ($tax as $tax) { ?>
                                            <option <?php if ($tax->default == '1') {
                                            echo "selected='selected'";
                                        } ?> value="<?php echo $tax->id; ?>"><?php echo $tax->name; ?></option>
<?php } ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Feature Product</label>
                                <div class="col-sm-8">
                                    <select name="feature_product" class="form-control" required>
                                        <option value="1" >Yes</option>
                                        <option value="0" selected >No</option>
                                    </select>
                                </div>

                            </div>


                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Sort Order</label>
                                <div class="col-sm-8">
                                    <input type="text" name="sort_order" class="form-control" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Product Status</label>
                                <div class="col-sm-8">
                                    <select name="product_status" class="form-control">
                                        <option value="" selected="selected">No Status</option>
                                        <option value="New" >New</option>
                                        <option value="Sale">Sale</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="regular_price" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-body">
                                <input type="submit" class="btn btn-primary" value="Add Product">                                    
                            </div>



                        </div>


                    </div>
                </form>

            </div>

        </div>

    </div>
</div>

<script>
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

    });
</script>