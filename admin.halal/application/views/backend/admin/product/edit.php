
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">!Update Product</h3>
            </div>

            <div class="box-body">


                <!-- <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>admin/doctor/create" method="post" enctype="multipart/form-data"> -->
                <form action="<?php echo base_url(); ?>products/doedit" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Product Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" required class="form-control" id="title" placeholder="Category Name" value="<?php echo set_value('name', $product[0]->name); ?>" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Short Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <textarea row="3" name="short_description" required class="form-control" placeholder="Short Description" ><?php echo set_value('short_description', $product[0]->short_description); ?></textarea>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <textarea row="3" name="description" required class="form-control" placeholder="Description" ><?php echo set_value('long_description', $product[0]->long_description); ?></textarea>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Category</label>
                                <div class="col-sm-8">
                                    <select name="category[]" class="form-control select2" multiple="multiple" data-placeholder="You Can select Multiple Category" style="width: 100%;" >

                                        <?php
                                        $i = 0;
                                        foreach ($category as $category) {

                                            if ($product_category[$i]->category_id == $category->id) {
                                                $i++;
                                                ?>
                                                <option selected="selected" value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>


                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Image<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col col-lg-8">
                                            <input type="file"  name="image"  value=""   id="myFile" onchange="imageshow(this);" />
                                            <input type="hidden" name="img" value="<?php echo $product[0]->image; ?>" />
                                        </div>
                                        <div class="col col-lg-4">
                                            <img id="previewImg1" src="<?php echo base_url() ?>img/products/<?php echo $product[0]->image; ?>" width="130px" />
                                            <script>
                                                function imageshow(input) {

                                                    if (input.files && input.files[0]) {
                                                        var reader = new FileReader();

                                                        reader.onload = function(e) {
                                                            $('#previewImg1')
                                                                    .attr('src', e.target.result)
                                                                    .width(130);
                                                        }
                                                        reader.readAsDataURL(input.files[0]);

                                                    } else {
                                                        var filename = "";
                                                        filename = "file:\/\/" + input.value;
                                                        document.form2.previewImg1.src = filename;
                                                        document.form2.previewImg1.style.width = "80px";

                                                    }
                                                }
                                            </script>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">SKU<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="sku" class="form-control" required value="<?php echo set_value('name', $product[0]->sku); ?>"  />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Unit Cost<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="unit_price" class="form-control" required value="<?php echo set_value('name', $product[0]->unit_price); ?>"  />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Sale Price<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="sale_price" class="form-control" required value="<?php echo set_value('name', $product[0]->sale_price); ?>"  />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Unit Per Weight<span style="color:red;">*</span></label>
                                        
                                           
                                    <div class="col-sm-8">
                                         <select name="unit_per_weight" class="form-control">
                                        <option value="Pound" <?php
                                        if ($product[0]->unit_per_weight == 'Pound') {
                                            echo 'selected="selected"';
                                        }
                                        ?> >Pound</option>
                                        <option value="Packet" <?php
                                        if ($product[0]->unit_per_weight == 'Packet') {
                                            echo 'selected="selected"';
                                        }
                                        ?> >Packet</option>
                                        <option value="pcs" <?php
                                        if ($product[0]->unit_per_weight == 'pcs') {
                                            echo 'selected="selected"';
                                        }
                                        ?> >PCS</option>
                                    </select>
                                       

                                    </div>
                                    
                                


                            </div>
                            <div class="form-group">
                                
                                    <label for="" class="col-sm-3 control-label">Unit Per Quantity<span style="color:red;">*</span></label>
                                        
                                            
                                    <div class="col-sm-8">
                                        <input type="text" name="unit_per_qty" class="form-control" required value="<?php echo set_value('unit_per_qty', $product[0]->unit_per_qty); ?>"  />
                                        
                                    </div>
                                


                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Quantity<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="quantity" class="form-control" required value="<?php echo set_value('name', $product[0]->quantity); ?>"  />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Minimum Quantity<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="min_quantity" class="form-control" required  value="<?php echo set_value('name', $product[0]->minimum_quantity); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Tax Class<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="tax" class="form-control">
                                        <?php
                                        foreach ($tax as $tax) {
                                            if ($product_select_tax[0]->id == $tax->id) {
                                                ?>
                                                <option selected="selected" value="<?php echo $tax->id; ?>"><?php echo $tax->title; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $tax->id; ?>"><?php echo $tax->title; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Feature Product</label>
                                <div class="col-sm-8">
                                    <select name="feature_product" class="form-control">
                                        <option value="1" <?php
                                        if ($product[0]->feature_product == 1) {
                                            echo 'selected="selected"';
                                        }
                                        ?> >Yes</option>
                                        <option value="0" <?php
                                        if ($product[0]->feature_product == 0) {
                                            echo 'selected="selected"';
                                        }
                                        ?>>NO</option>
                                    </select>
                                </div>

                            </div>


                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Sort Order</label>
                                <div class="col-sm-8">
                                    <input type="text" name="sort_order" class="form-control"  value="<?php echo set_value('name', $product[0]->sort_order); ?>" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Product Status</label>
                                <div class="col-sm-8">
                                    <select name="product_status" class="form-control">
                                        <option value="" <?php
                                        if ($product[0]->product_status == '') {
                                            echo 'selected="selected"';
                                        }
                                        ?>>No Status</option>
                                        <option value="New" <?php
                                        if ($product[0]->product_status == 'New') {
                                            echo 'selected="selected"';
                                        }
                                        ?> >New</option>
                                        <option value="Sale" <?php
                                        if ($product[0]->product_status == 'Sale') {
                                            echo 'selected="selected"';
                                        }
                                        ?>>Sale</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        if ($product[0]->status == 1) {
                                            echo 'selected="selected"';
                                        }
                                        ?> >Enable</option>
                                        <option value="0" <?php
                                        if ($product[0]->status == 0) {
                                            echo 'selected="selected"';
                                        }
                                        ?>>Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-body">
                                <input type="hidden" name="id" value="<?php echo $product[0]->id; ?>" />
                                <input type="submit" class="btn btn-primary" value="Update Product">                                    
                            </div>



                        </div>


                    </div>
                </form>

            </div>

        </div>

    </div>
</div>

<script>
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

    });
</script>

