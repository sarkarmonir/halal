
<!-- Input addon -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Product</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>

        <div class="row">
            <div class="col-lg-12">
                <a href="<?php echo base_url(); ?>products/add" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <a href="javascript:void(0);" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                <script>
                    var table = ['product', 'product_to_category'];
                    var image = '';
                </script>
                <a href="javascript:void(0);" class="btn btn-danger" onclick="return deleteall(table, image);"><i class="fa fa-trash-o"></i></a>
                <a href="javascript:void(0);" class="btn btn-success" onclick="return product_clone();"><i class="fa fa-copy"></i></a>
                <div style="display:none;" id="waitingprocess"><img src="<?php echo base_url(); ?>img/select2-spinner.gif" />Please Wait.......</div>
            </div>
        </div><br>

        <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
            <thead>	

                <tr>
                    <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Name</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Image</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> DP/TP</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i>MRP</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Quantity</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Sort Order</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Feature</th>
                    <!--<th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Gallery</th>-->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($all_products as $product) { ?>
                    <tr id="row_<?php echo $product->id; ?>">
                        <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $product->id; ?>" value="<?php echo $product->id; ?>-img/products/<?php echo $product->image; ?>" /></td>
                        <td><?php echo $product->name ?></td>
                        <td> <img src="<?php echo base_url() ?>img/products/<?php if ($product->image != '') {
                    echo $product->image;
                } else {
                    echo "no_image.jpg";
                } ?>" width="40px" /></td>
                        <td>$<?php echo $product->unit_price ?></td>
                        <td>$<?php echo $product->sale_price ?></td>
                        <!--<td><span id="quantity_row_<?php echo $product->id; ?>"><?php echo $product->quantity ?></span><a style="float:right;" href="javascript:void();" onclick="quantity_update(<?php echo $product->id; ?>);" ><i class="fa fa-edit"></i></a></td>-->
                        <td><span><?php echo $product->quantity ?></span></td>
                        
                        <td><?php echo $product->sort_order; ?></td>
                <script>var table1 = 'product';</script>
                <td><?php if ($product->status == 1) {
                    echo "<span id='status_" . $product->id . "'><a href='#' class = 'btn btn-success' onclick='p_status_change(table1," . $product->id . "," . $product->status . ");'>Enable</a></span>";
                } else {
                    echo "<span id='status_" . $product->id . "'><a href='#' class = 'btn btn-danger'  onclick='p_status_change(table1," . $product->id . "," . $product->status . ");'>Disable</a></span>";
                } ?></td>
                <script>var f_table = 'product';</script>
                <td><?php if ($product->feature_product == 1) {
                    echo "<span id='feature_" . $product->id . "'><a href='#' class = 'btn btn-warning' onclick='feature_change(f_table," . $product->id . "," . $product->feature_product . ");'><i class='fa fa-circle-thin'></i></a></span>";
                } else {
                    echo "<span id='feature_" . $product->id . "'><a href='#' class = 'btn btn-default'  onclick='feature_change(f_table," . $product->id . "," . $product->feature_product . ");'><i class='fa fa-circle-thin'></i></a></span>";
                } ?></td>
                
                <!--<td><a href="<?php echo base_url(); ?>products/gallery/<?php echo $product->id; ?>"><i class="fa fa-image"></i></a></td>-->
                <td><div class="btn-group">
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="products/edit/<?php echo $product->id; ?>">Edit</a>
                            </li>
                            <li>
                                <script>
                                    var table = ["product", "product_to_category"];
                                    var image = "img/product/<?php echo $product->image; ?>";
                                </script>
                                <a href="#" onclick="return dodelete(table,<?php echo $product->id; ?>, image);">Delete</a>
                            </li>

                        </ul>
                    </div></td>
                </tr>
<?php } ?>

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">

                                $(function() {
// $("#example1").DataTable();
                                    $('#table-2').DataTable();
                                });
</script>

<script type="text/javascript">
    function quantity_update(id)
    {
        var new_quantity = prompt('Enter New Product Quantity');
        if (new_quantity) {
            $.ajax({
                url: BASE_URL + 'products/quantity_update/' + id + '/' + new_quantity,
                success: function(data)
                {
                    if (data == 1)
                    {
                        $('#quantity_row_' + id).html(new_quantity);
                    }
                }
            });
        }
    }
</script>