
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">+Add New Payment Method</h3>
            </div>

            <div class="box-body">
                <div id="errorMsg" style="color:#CC0000;"><?php
                    if (validation_errors()) {
                        echo '<div id="validation_errors" title="Error:">';
                        echo '<div class="response-msgs errors ui-corner-all"><span></span><br /><ul>';
                        echo validation_errors();
                        echo '</ul></div>';
                        echo '</div>';
                    }
                    ?></div>
                <form id="payments_form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>payment_method/doadd" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Payment Method Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="title" required class="form-control" id="title" placeholder="payment method Name" value="<?php echo set_value('title'); ?>" />
                                    
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="description" required class="form-control" id="title" placeholder="Description" value="<?php echo set_value('description'); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Type<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="type" id="method_type_select" class="form-control">
                                        <option value="direct" selected="selected">Direct payments</option>
                                        <option value="paypal" >Paypal</option>
                                    </select>
                                </div>

                            </div>
                            
                            <div  id="payment_method_form">

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Message<span style="color:red;">*</span></label>
                                <div class="col-sm-8" id="method_direct">
                                    <textarea class="form-control" name="message" rows="4"><?php echo set_value('message'); ?></textarea>
                                </div>

                            </div>
                                
                            </div>
                            
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="1" selected="selected">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                </div>

                            </div>
                            


                            <div class="box-body">
                                <input type="submit" class="btn btn-primary" value="Add Payment Method">                                    
                            </div>

                        </div>

                    </div>

                    
      
            
            </form>
        </div>

    </div>

</div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        /*
         jahid al mamun <rjs.jahid11@gmail.com>
         */
        $("#method_type_select").change(function() {
            $("#method_type_select option:selected").each(function() {
                if ($(this).attr("value") == "direct") {
                    $('#payment_method_form').html('<div id="method_direct"><label for="name" class="col-sm-3 control-label">Message<span style="color:red;">*</span></label><div class="col-sm-8" id="method_direct"><textarea class="form-control" name="message" rows="4"><?php echo set_value('message'); ?></textarea></div></div>').show('slow');
                }
                if ($(this).attr("value") == "paypal") {
                    $('#payment_method_form').html('<div id="method_paypal"><div class="form-group"><label for="name" class="col-sm-3 control-label">Sanbox Account<span style="color:red;">*</span></label><div class="col-sm-8"><select name="sandbox" class="form-control"><option value="1" >Yes</option><option value="0" >No</option></select></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">Email<span style="color:red;">*</span></label><div class="col-sm-8"><input type="email" required="" class="form-control" name="email" /></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">API Username<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" required="" class="form-control" name="api_username" /></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">API Password<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" class="form-control" name="api_password" /></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">API Signature<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" class="form-control" name="api_signature" /></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">Currency Code<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" class="form-control" name="currency_code" /></div></div></div>').show('slow');
                }
               
            });
        }).change();
    });

</script>
