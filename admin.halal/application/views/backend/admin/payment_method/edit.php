
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">!Edit Payment Method</h3>
            </div>

            <div class="box-body">
                <div id="errorMsg" style="color:#CC0000;"><?php
                    if (validation_errors()) {
                        echo '<div id="validation_errors" title="Error:">';
                        echo '<div class="response-msgs errors ui-corner-all"><span></span><br /><ul>';
                        echo validation_errors();
                        echo '</ul></div>';
                        echo '</div>';
                    }
                    ?></div>
                <form id="payments_form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>payment_method/doedit" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Payment Method Name<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="title" required class="form-control" id="title" placeholder="payment method Name" value="<?php echo set_value('title', $methods->title); ?>" />

                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Description<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" name="description" required class="form-control" id="title" placeholder="Description" value="<?php echo set_value('description', $methods->description); ?>" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Type<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="type" id="method_type_select" class="form-control">
                                        <option <?php
                                        if ($methods->type == 'paypal') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="paypal" >Paypal</option>

                                        <option <?php
                                        if ($methods->type == 'direct') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="direct">Direct payments</option>
                                    </select>
                                </div>

                            </div>

                            <div id="payment_method_form">
                                <?php
                                $config = json_decode($methods->configs);

                                if ($methods->type == 'paypal') {
                                    ?>
                                    <div id="method_paypal"><div class="form-group"><label for="name" class="col-sm-3 control-label">Sanbox Account<span style="color:red;">*</span></label><div class="col-sm-8"><select name="sandbox" class="form-control"><option <?php
                                                    if ($config->sandbox == 1) {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?> value="1" >Yes</option><option value="0" <?php
                                                        if ($config->sandbox == 0) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?> >No</option></select></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">Email<span style="color:red;">*</span></label><div class="col-sm-8"><input type="email" required="" class="form-control" name="email" value="<?php echo set_value('email', $config->email); ?>" /></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">API Username<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" required="" class="form-control" name="api_username" value="<?php echo set_value('api_username', $config->api_username); ?>" /></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">API Password<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" class="form-control" name="api_password" value="<?php echo set_value('api_password', $config->password); ?>"/></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">API Signature<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" class="form-control" name="api_signature" value="<?php echo set_value('api_signature', $config->signature); ?>" /></div></div><div class="form-group"><label for="name" class="col-sm-3 control-label">Currency Code<span style="color:red;">*</span></label><div class="col-sm-8"><input type="text" class="form-control" name="currency_code" value="<?php echo set_value('currency_code', $config->currency_code); ?>" /></div></div></div>
                                                    <?php } else if ($methods->type == 'direct') { ?>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label">Message<span style="color:red;">*</span></label>
                                        <div class="col-sm-8" id="method_direct">
                                            <textarea class="form-control" name="message" rows="4"><?php echo set_value('message', $config->message); ?></textarea>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option <?php
                                        if ($methods->status == 1) {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="1" >Enable</option>
                                        <option <?php
                                        if ($methods->status == 0) {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="0">Disable</option>
                                    </select>
                                </div>

                            </div>



                            <div class="box-body">
                                <input type="submit" class="btn btn-primary" value="Update Payment Method">                                    
                            </div>

                        </div>

                    </div>
                </form>
            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>
    <!-- Widget ID (each widget will need unique ID)-->

    <!-- end widget -->

</article>
<!-- WIDGET END -->

</div>

<!-- end row -->
<!-- end row -->

</section>
<!-- end widget grid -->

</div>

<!--<script type="text/javascript">

    $(document).ready(function () {
        /*
         jahid al mamun <rjs.jahid11@gmail.com>
         */
        $("#method_type_select").change(function () {

            $("#method_type_select option:selected").each(function () {
                if ($(this).attr("value") == "direct") {
                    $('#payment_method_form').html('<div id="method_direct"><section><label class="input">Message<textarea class="form-control" name="message" rows="4"><?php echo set_value('message'); ?></textarea></label></section></div>').show('slow');
                }
                if ($(this).attr("value") == "paypal") {
                    $('#payment_method_form').html('<div id="method_paypal"><section><label class="input">Sanbox Account<select name="sandbox" class="form-control"><option value="1" >Yes</option><option value="0" >No</option></select></label></section><section><label class="input">email<input type="email" required="" class="form-control" name="email" /></label></section><section><label class="input">API Username<input type="text" required="" class="form-control" name="api_username" /></label></section><section><label class="input">API Password<input type="text" class="form-control" name="api_password" /></label></section><section><label class="input">API Signature<input type="text" class="form-control" name="api_signature" /></label></section><section><label class="input">Currency Code<input type="text" class="form-control" name="currency_code" /></label></section></div>').show('slow');
                }
                if ($(this).attr("value") == "authorize") {
                    $('#payment_method_form').html('<div id="method_authorize"><section><label class="input">Sanbox Account<select name="sandbox" class="form-control"><option value="1" >Yes</option><option value="0" >No</option></select></label></section><section><label class="input">Api login id<input type="text" required="" class="form-control" name="api_login_id" /></label></section><section><label class="input">Transaction key<input type="text" required="" class="form-control" name="transaction_key" /></label></section></div>').show('slow');
                }
            });
        }).change();
    });

</script>-->
