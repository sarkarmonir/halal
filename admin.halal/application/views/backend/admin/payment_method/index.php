<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Payment method</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="<?php echo base_url(); ?>payment_method/add" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                    <a href="#" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                    <script>
                                     var table =['payments'];
                                     var image = '';
                                    </script>
                                    <a href="#" class="btn btn-danger" onclick="return deleteall(table,image);"><i class="fa fa-trash-o"></i></a>

                                </div>
                            </div><br>
                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>			                
                                    <tr>
                                        <th><input type="checkbox" name="selectall" id="selectall"/></th>
                                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Shipping Name</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Description</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Type</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Default</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($payment_method as $method) { ?>
                                            <tr id="row_<?php echo $method->id; ?>">
                                            <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $method->id; ?>" value="<?php echo $method->id; ?>-img/<?php //echo $country->image; ?>" /></td>
                                            <td><?php echo $method->title; ?></td>
                                            <td><?php echo $method->description; ?></td>
                                            <td><?php echo $method->type; ?></td>
                                            <script>var table='payments';</script>
                                            <td><?php if($method->status == 1){echo "<span id='status_".$method->id."'><a href='#' class = 'btn btn-success' onclick='status_change(table,".$method->id.",".$method->status.");'>Enable</a></span>";}else{echo "<span id='status_".$method->id."'><a href='#' class = 'btn btn-danger'  onclick='status_change(table,".$method->id.",".$method->status.");'>Disable</a></span>";} ?></td>
                                            <td><?php $table = "payments";$url = "payment_method"; if($method->default == 1){echo "<a href='#' class = 'btn btn-success'><i class='fa fa-check'></i> </a>";}else{echo "<a href='".base_url()."common/default_change/$table/$method->id/$url"."' class = 'btn'><i class='fa fa-square-o'></i></a>";} ?></td>
                                            <td><div class="btn-group">
                                                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Action <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="payment_method/edit/<?php echo $method->id; ?>">Edit</a>
                                                        </li>
                                                        <li>
                                                            <script>
                                                                var table = ["payments"];
                                                                var image = "";
                                                            </script>
                                                            <a href="#" onclick="return dodelete(table,<?php echo $method->id; ?>,image);">Delete</a>
                                                        </li>

                                                    </ul>
                                                </div></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                       </div>
</div>
<script type="text/javascript">

                                $(function() {
// $("#example1").DataTable();
                                    $('#table-2').DataTable();
                                });
</script>


