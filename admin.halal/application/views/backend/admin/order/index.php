<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Order List</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
                            <div class="row">
                                
                            </div><br>
                                <form action="" name="order_filter" id="order_filter">
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <input type="text" name="start_date"  placeholder="From:" class="form-control datepicker" data-dateformat="dd/mm/yy">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <input type="text" name="start_date"  placeholder="To" class="form-control datepicker" data-dateformat="dd/mm/yy">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <select name="status"  class="form-control" >
                                            <option style="background-color:white; color: black;" value="all">---------</option>
                                            <option style="background-color:green; color: white;" value="received">Received</option>
                                            <option style="background-color:palevioletred; color: white;" value="in_preparation">In preparation</option>
                                            <option style="background-color:paleturquoise; color: white;" value="shipped">Shipped</option>
                                            <option style="background-color:red; color: white;" value="deleted">Deleted</option>
                                        </select> 
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="submit" value="Filter" class="btn btn-primary" /> 
                                    </div>
                                </form>

                            </div>
                            <div class="row">
                                <div class="col col-sm-12">
                            <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
                                <thead>			                
                                    <tr>
                                        <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Order ID</th>
                                        <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Customer Name</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Date</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Total</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Payment Method</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Payment Status</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Delivery Type</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Details</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($all_orders as $order) { ?>
                                        <tr id="row_<?php echo $order->oid; ?>">
                                            <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $order->oid; ?>" value="<?php echo $order->oid; ?>-img/" /></td>
                                            <td><a href="<?php echo base_url('order/order_view') . '/' . $order->oid; ?>" > <?php echo 'H'.$order->oid; ?></a></td>
                                            <td><?php echo $order->name; ?></td>
                                            <td><?php echo $order->ordered_on; ?></td>
                                            <td><?php echo $order->total; ?></td>
                                            <td><?php echo payment_show($order->payment_type); ?></td>
                                            <td><?php echo $order->payment_status; ?></td>
                                            <td><?php echo $order->order_type; ?></td>
                                            <td><a href="<?php echo base_url('order/order_view') . "/" . $order->oid; ?>" ><i class="fa fa-eye"></i></a></td>
                                            
                                            <td>
                                                <select name="status" id="order_status_<?php echo $order->oid; ?>" onchange="order_status(<?php echo $order->oid; ?>);" <?php if ($order->ostatus == 'in_preparation') {
                                        echo 'style="background-color:palevioletred;"';
                                    }if ($order->ostatus == 'shipped') {
                                        echo 'style="background-color:paleturquoise;"';
                                    }if ($order->ostatus == 'deleted') {
                                        echo 'style="background-color:red;"';
                                    } if ($order->ostatus == 'received') {
                                        echo 'style="background-color:green;"';
                                    } ?>  >
                                                    <option <?php if ($order->ostatus == '') {
                                        echo 'selected ="selected"';
                                    } ?> style="background-color:white; color: black;" value="---">---------</option>
                                                    <option <?php if ($order->ostatus == 'received') {
                                        echo 'selected ="selected"';
                                    } ?> style="background-color:green; color: white;" value="received">Received</option>
                                                    <option <?php if ($order->ostatus == 'in_preparation') {
                                        echo 'selected ="selected"';
                                    } ?> style="background-color:palevioletred; color: white;" value="in_preparation">In preparation</option>
                                                    <option <?php if ($order->ostatus == 'shipped') {
                                        echo 'selected ="selected"';
                                    } ?> style="background-color:paleturquoise; color: white;" value="shipped">Shipped</option>
                                                    <option <?php if ($order->ostatus == 'deleted') {
                                        echo 'selected ="selected"';
                                    } ?> style="background-color:red; color: white;" value="deleted">Deleted</option>
                                                </select> 
                                            </td>
                                        </tr>
<?php } ?>

                                </tbody>
                            </table>
              </div>
</div>
<script type="text/javascript">

                                $(function() {
// $("#example1").DataTable();
                                    $('#table-2').DataTable();
                                });
</script>

