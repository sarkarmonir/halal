<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Order Details</h3>
    </div>

    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
                            <div class="row">
                                
                            </div><br>

                            <a href="<?php echo base_url('order'); ?>" ><input type="button" class="btn btn-default" value="<<Back" /></a>
                            <div class="row">

                                <!-- a blank row to get started -->
                                

                            </div>
                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand"><i class="fa fa-fw fa-adn text-muted hidden-md hidden-sm hidden-xs"></i>Product</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i>Price</th>
                                        <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i>Quantity</th>
                                        <th  data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="jahid"></tr>
                                    <?php
                                    $item_total = 0;
//                echo "<pre>";
//                print_r($order_items);
                                    foreach ($order_items as $item) {
                                        ?>
                                        <tr>
                                            <td data-th="Product">
                                                <div class="row">
                                                    <div class="col-sm-2"><img src="<?php echo base_url(); ?>img/products/<?php echo $item->product_image; ?>" alt="..." width="50px"/></div>
                                                    <div class="col-sm-10">
                                                        <h4 class="nomargin"><a target="blank" href="<?php echo base_url(); ?>products/details/<?php echo $item->product_id; ?>/<?php echo $item->product_name; ?>" ><?php echo $item->product_name; ?></a></h4>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-th="Price">€<?php echo $item->subtotal; ?></td>
                                            <td>
                                                <div class="quantity-adder quantity-adder2"  style="width: 77px; ">
                                                    <input type="text" value="<?php echo $item->quantity; ?>" size="2" name="quantity"  class="form-control" disabled="">
                                                </div>
                                            </td>

                                            <td data-th="Subtotal" class="text-center">€<?php echo $item->total; ?></td>
                                        </tr>
                                        <?php
                                        $item_total += $item->total;
                                    }
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td ><strong>Item Total</strong></td>
                                        <td class="text-right"><strong>€<?php echo $item_total; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td> <strong>
                                                <span class="fa fa-shopping-cart"></span>
                                                Coupon Discount
                                            </strong> </td>
                                        <td></td>
                                        <td ></td>

                                        <td class="text-right"><strong>-&nbsp;€<?php echo $order_details[0]->coupon_value; ?></strong></td>
                                    </tr>
                                    
                                    <tr>
                                        <td><span class="addCart">
                                                <span class="fa fa-shopping-cart"></span>
                                                Shipping cost
                                            </span>
                                        </td>
                                        <td colspan="2"><h5></h5></td>
                                        <td class="text-right"><strong>+ &nbsp;€<?php echo $order_details[0]->shipping_cost; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td ><h3>Total</h3></td>
                                        <td class="text-right"><h3><strong>€<?php echo $order_details[0]->total; ?></strong></h3></td>
                                    </tr>

                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2>Billing Information</h2>
                                    <table class="table table-striped table-bordered table-hover" width="100%">
                                        <tbody>
                                            
                                            <tr>
                                                <td>Name</td>
                                                <td><?php echo $customer_details[0]->name; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php echo $customer_details[0]->email; ?></td>
                                            </tr>
                                            
                                            <tr>
                                                <td>City</td>
                                                <td><?php echo $customer_details[0]->city; ?></td>
                                            </tr>
                                           
                                            <tr>
                                                <td>Telephone</td>
                                                <td><?php echo $customer_details[0]->telephone; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td><?php echo $customer_details[0]->address; ?><br><?php echo $customer_details[0]->address_2; ?></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <h2>Shipping Information</h2>
                                    <?php if ($order_details[0]->shipping_address == 0) { ?>

                                        <h3 class="heading_title" style="background-color: cadetblue;"><span> Shipping Address Same as Billing address </span></h3>

                                    <?php } else { ?>

                                        <table class="table table-striped table-bordered table-hover" width="100%">
                                            <tbody>
                                                
                                                <tr>
                                                    <td>Place</td>
                                                    <td><?php echo show_place($shipping_address[0]->place); ?></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Telephone</td>
                                                    <td><?php echo $shipping_address[0]->telephone; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td><?php echo $shipping_address[0]->address; ?><br><?php echo $shipping_address[0]->address2; ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col col-lg-4"><br>
                                    <strong>Payment Method</strong>

                                </div>
                                <div class="col col-lg-4">

                                </div>
                                <?php if ($order_details[0]->payment_type == '2') { ?>
                                <h2>Paypal</h2>
                                <?php } else if($order_details[0]->payment_type == '1'){
                                    ?>
                                    <h2>Cash On Delivery</h2>
                                <?php } ?>
                            </div>
                            
                            <div class="row">
                                <div class="col col-lg-4"><br>
                                    <strong>Payment Status</strong>

                                </div>
                                
                                <div class="col col-lg-4">
                                    <h2><?php echo $order_details[0]->payment_status; ?></h2>
                                </div>
                                
                            </div>


                            <div class="row" style="background-color: darkgrey;">
                                <div class="col col-lg-4">
                                    <strong>Order Status</strong>
                                </div>
                                <div class="col col-lg-4">

                                </div>
                                <div class="col col-lg-4">
                                    <h2 style="color:#ffffff" ><?php echo $order_details[0]->status; ?></h2>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col col-lg-12">
                                    <strong>Notes</strong>
                                    <textarea class="form-control" name="note" rows="3" placeholder="Your Comment."><?php echo $order_details[0]->notes; ?></textarea>
                                </div>
                            </div>


                            <!-- end widget content -->


                            <!-- end widget div -->

                        </div>
</div>




               