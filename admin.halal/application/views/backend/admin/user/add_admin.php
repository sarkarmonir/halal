
<div class="row">
    <div class="col-md-12">

        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">+Add New Product</h3>
            </div>

            <div class="box-body">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="/#home">User Informmation</a></li>
            <li><a data-toggle="tab" href="/#menu1">User Role Assign</a></li>
        </ul>
        <form class="form-horizontal" method="post" id="add_user" action="<?php echo base_url(); ?>user/add_admin_save" >
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                
                    <div class="form-group required">
                        <label class="control-label col-sm-2">User Type:</label>
                        <div class="col-sm-8" id="select_user_type">
                            <select class="form-control" id="sel1" name="type">
                                <?php foreach ($user_type as $type){ ?>
                                <option value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
                               <?php } ?>
                                
                            </select>
                        </div>
<!--                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user_group">
                            <i class="fa fa-plus"></i>
                        </button>-->
                        
                    </div>
                    <div class="form-group required">
                        <label class="control-label col-sm-2" for="email">Name:</label>
                        <div class="col-sm-8">
                            <input required type="text" class="form-control" id="name" name="name" placeholder="Enter your Name">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="control-label col-sm-2" for="email">Email:</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="control-label col-sm-2" for="phn">Mobile:</label>
                        <div class="col-sm-8">
                            <input required type="tel" class="form-control" id="phn" name="phone" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="control-label col-sm-2" for="phn">Password:</label>
                        <div class="col-sm-8">
                            <input required type="password" class="form-control" id="phn" name="password" placeholder="Password">
                        </div>
                    </div>

                <div class="form-group required del_zone"  style="display:none;">
                    <label class="control-label col-sm-2" for="phn">Zone:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                            <select id="lunch" class="form-control" data-live-search="true" title="Please select a Zone..." name="deliver_zone">
                                <option value="">--Select--</option>
                                <?php foreach ($zone as $z) { ?>
                                    <option value="<?php echo $z->id; ?>"><?php echo $z->zone_name; ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                </div>

                <!--<div class="form-group required del_zone" style="display:none;">
                    <label class="control-label col-sm-2" for="phn">Change Password:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key fa-10x"></i></span>
                            <input type="password" class="form-control" id="changePass" placeholder="Change Password" required="required">
                        </div>
                    </div>
                </div>-->



            </div><!----end tab--->
            <div class="form-group tab-pane fade" id="menu1">
                <div class="form-group">
                            <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <select name="from[]" id="multiselect" class="form-control" size="8" multiple>
                                                <option value="dashboards">Dashboards</option>
                                                <option value="user_management">User Management</option>

                                                <option value="product">Product</option>
                                                <option value="category">Category</option>
                                                <option value="coupon">Coupon Management</option>
                                                <option value="discount">Discount Management</option>
                                                <option value="slideshow">Slideshow</option>
                                                <option value="supplier">Supplier</option>
                                                <option value="site_configure">Site Configuration</option>
                                                <option value="payment">Payment</option>
                                                <option value="order">Order</option>
                                                <option value="Inventory">Inventory</option>
                                            </select>
                                        </div>

                                        <div class="col-xs-2">
                                            <button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="fa fa-forward"></i></button>
                                            <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-circle-right"></i></button>
                                            <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-circle-left"></i></button>
                                            <button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="fa fa-backward"></i></button>
                                        </div>

                                        <div class="col-xs-5">
                                            <select name="to[]" id="multiselect_to" class="form-control" size="8" multiple>
                                                
                                            </select>
                                        </div>
                                    </div>
                            </div>
                    
                        </div>
            </div>
            <br>
            <div class="form-group">
                <div class="pull-left">
                    <button type="submit" class="btn btn-primary" style="margin-top: 22%;">Add User</button>
                </div>
            </div>
            

        </div>
</form>
            </div>

        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
$('#multiselect').multiselect();

    })

</script>