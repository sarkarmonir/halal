<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="/#home">User Informmation</a></li>
            <li><a data-toggle="tab" href="/#menu1">User Role Assign</a></li>
        </ul>
        <form autocomplete="off" class="form-horizontal" method="post" id="add_user" action="<?php echo base_url(); ?>user/edit_admin_save" >
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                    <div class="form-group">
                        <label class="control-label col-sm-2">User Type:</label>
                        <div class="col-sm-8" id="select_user_type">
                            <select class="form-control" name="type">
                                <?php foreach ($user_type as $type){ ?>
                                <option <?php if ($type->id == $user[0]->type){echo 'selected';} ?> value="<?php echo $type->id; ?>"><?php echo $type->name ?></option>
                               <?php } ?>
                                
                                
                            </select>
                        </div>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user_group">
                            <i class="fa fa-plus"></i>
                        </button>
                        
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">*Name:</label>
                        <div class="col-sm-8">
                            <input required type="text" class="form-control" id="name" value="<?php echo $user[0]->name; ?>" name="name" placeholder="Enter your Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email:</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" value="<?php echo $user[0]->email; ?>" id="email" name="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phn">*Mobile:</label>
                        <div class="col-sm-8">
                            <input required type="text" class="form-control" value="<?php echo $user[0]->phone; ?>"  name="phone" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phn">*Change Password:</label>
                        <div class="col-sm-8">
                            <input type="password" value="" class="form-control"  name="password" placeholder="If you want to change then Enter New password">
                        </div>
                    </div>

                <?php if($user[0]->type=="7"){  ?>

                    <div class="form-group required del_zone">
                        <label class="control-label col-sm-2" for="phn">Zone:</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                                <select id="lunch" class="form-control" data-live-search="true" title="Please select a Zone..." name="deliver_zone">
                                    <?php foreach ($zone as $z){ ?>
                                        <option <?php if($user[0]->delivery_zone==$z->id){ echo "selected";}?> value="<?php echo $z->id; ?>"><?php echo $z->zone_name; ?></option>
                                    <?php  } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                <?php } ?>

            </div><!----end tab--->
            <div class="form-group tab-pane fade" id="menu1">
                <div class="form-group">
                            
                            <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <select name="from[]" id="multiselect" class="form-control" size="8" multiple="multiple">
                                                <?php $allmodel = $user[0]->module_access;

                                                if(!empty($allmodel) && $allmodel != ''){
                                                      $allmodel = json_decode($allmodel);
                                                      foreach ($allmodel->from as $from){
                                                          ?>
                                                <option value="<?php echo $from; ?>"><?php echo $from; ?></option>
                                                <?php   }}else{
                                                    ?>
                                                    <option value="dashboards">Dashboards</option>
                                                    <option value="user_management">User Management</option>
                                                    <option value="product">Product</option>
                                                    <option value="category">Category</option>
                                                    <option value="brand">Brand Management</option>
                                                    <option value="banner">Banner Management</option>
                                                    <option value="color">product color</option>
                                                    <option value="size">Product size</option>
                                                    <option value="coupon">Coupon Management</option>
                                                    <option value="discount">Discount Management</option>
                                                    <option value="slideshow">Slideshow</option>
                                                    <option value="supplier">Supplier</option>
                                                    <option value="site_configure">Site Configuration</option>
                                                    <option value="payment">Payment</option>
                                                    <option value="language">Language</option>
                                                    <option value="order">Order</option>
                                                    <option value="delivery">Delivery</option>
                                                    <option value="zone">Zone</option>
                                                    <option value="area">Area</option>
                                                    <option value="import">Product Import</option>
                                                    <option value="shipping">Shipping</option>
                                                    <option value="discounted">Discounted Product</option>
													<option value="page">PAGE Management</option>
                                                    <option value="Inventory">Inventory</option>
                                                    <option value="Task">Delivery Man Task</option>
                                                    <option value="Account">Password Change</option>
                                                <?php
                                                }
                                                ?>
                                               
                                          
                                            </select>
                                        </div>

                                        <div class="col-xs-2">
                                            <button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="fa fa-forward"></i></button>
                                            <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-circle-right"></i></button>
                                            <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-circle-left"></i></button>
                                            <button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="fa fa-backward"></i></button>
                                        </div>

                                        <div class="col-xs-5">
                                            <select name="to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">
                                                <?php
                                                if($allmodel !=''){
                                                    foreach ($allmodel->to as $to){
                                                          ?>
                                                <option value="<?php echo $to; ?>"><?php echo $to; ?></option>
                                                   <?php   }}
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                            </div>
                        </div>
            </div>
            <br>
            <div class="form-group">
                <div class="pull-left">
                    <input type="hidden" name="id" value="<?php echo $user[0]->id; ?>" />
                    <button type="submit" class="btn btn-primary" style="margin-top: 22%;">Update Admin</button>
                </div>
            </div>
            

        </div>
</form>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->