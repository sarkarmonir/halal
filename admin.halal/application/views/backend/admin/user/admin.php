<div class="page-header">
    <small>
        
        <h3>Admin List</h3>
    </small>

</div><!-- /.page-header -->
<script>
var table ='admin';
var image = "";
</script>
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <a href="user/add_admin"  class="btn btn-success"><i class="fa fa-plus"></i></a>
        <a href="javascript:void(0)" onclick="refresh();" class="btn btn-primary"><i class="fa fa-refresh"></i></a>
        <a href="javascript:void(0)" onclick="deleteall(table,image)" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
<!-- -->
        <div class="row">
            <div class="col-xs-12">
                <div id="add_edit_form" style="display:none;">
                    
                </div>
                
                <div>
                    
                   <div id="listing">
                    <table id="admin" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="center">
                                    <input type="checkbox" class="checkbox" id="selectall" />
                                   
                            </th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th class="hidden-480">Email</th>
                            <th class="hidden-480">Type</th>
                            <th>
                                <i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
                                Last Visit
                            </th>
                            <th class="hidden-480">Status</th>

                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                            <?php 
                            foreach ($admin_users as $user){
                                ?>
                           <tr id="row_<?php echo $user->id; ?>">
                            <td class="center">
                                
                                    <input class="checkbox" type="checkbox" name="selected[]" id="del_<?php echo $user->id; ?>" value="<?php echo $user->id; ?>" />
                                
                            </td>

                            <td>
                                <a href=""><?php echo $user->name; ?></a>
                            </td>
                            <td><?php echo $user->phone; ?></td>
                            <td class="hidden-480"><?php echo $user->email; ?></td>
                            <td class="hidden-480"><?php echo user_type($user->type); ?></td>
                            <td><?php echo $user->last_visit; ?></td>

                            <td class="hidden-480">
                                <?php  
                                    if($this->session->userdata('type')==1){  
                                    ?>
                                    
                                       
                                                    
                                <?php //if($user->status == 0){echo "<span id='status_".$user->id."'><a href='' class = 'btn btn-success' onclick='ekid.status_change(table,".$user->id.",".$user->status.");'>Enable</a></span>";}else{echo "<span id='status_".$user->id."'><a href='' class = 'btn btn-danger'  onclick='ekid.status_change(table,".$user->id.",".$user->status.");'>Disable</a></span>";} ?>
                                       <?php if($user->type==1){  }else{ ?>
                                <?php if($user->status == 0){echo "<span id='status_".$user->id."'><a href='' class = 'btn btn-success btn-sm' onclick='status_change(table,".$user->id.",".$user->status.");'>Enable</a></span>";}else{echo "<span id='status_".$user->id."'><a href='' class = 'btn btn-danger'  onclick='status_change(table,".$user->id.",".$user->status.");'>Disable</a></span>";} ?>
                                    <?php }} ?>
                            </td>

                            <td>
                              
                            <?php  
                                    if($this->session->userdata('type')==1){  
                                    ?>          
                                <?php //if($user->status == 0){echo "<span id='status_".$user->id."'><a href='' class = 'btn btn-success' onclick='ekid.status_change(table,".$user->id.",".$user->status.");'>Enable</a></span>";}else{echo "<span id='status_".$user->id."'><a href='' class = 'btn btn-danger'  onclick='ekid.status_change(table,".$user->id.",".$user->status.");'>Disable</a></span>";} ?>
                                       <?php if($user->id==$this->session->userdata('id')){ ?> 
                                    <a class="green" href="user/edit_admin/<?php echo $user->id; ?>" >
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>
                                <?php }else if($user->type!=1){ ?>
                                <a class="green" href="user/edit_admin/<?php echo $user->id; ?>">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                <a class="red" href="javascript:void(0)" onclick="dodelete(<?php echo $user->id; ?>,image)">
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>
                                    <?php }} ?>
                                
                                
                                

                            </td>
                        </tr>
                                        <?php
                            } ?>
 
                        


                        </tbody>
                    </table>
                </div>
					<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="user_group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New User Group</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
                        <label class="control-label col-sm-2" for="email">User Type:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="user_type_val" placeholder="Enter User Type">
                        </div>
        </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="ekid.add_user_group()">Add User Group</button>
      </div>
    </div>
  </div>
</div>
                </div>
            </div>
        </div><!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->

</div><!-- /.row -->

<script type="text/javascript">
    $(document).ready(function() {
        //initiate dataTables plugin
        var myTable =
            $('#admin').DataTable({
                    "searching": true,
                    "ordering": true,
                    "pagingType": "full_numbers",

                });
               
   });
</script>