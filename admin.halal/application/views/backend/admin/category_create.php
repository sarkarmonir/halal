
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('create_category'); ?></h3>
                </div>
            </div>

            <div class="panel-body">

                <!-- <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>admin/doctor/create" method="post" enctype="multipart/form-data"> -->
                <?php echo form_open('category/create',array('class'=>'form-horizontal form-groups-bordered','role'=>'form')); ?>

                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" name="name" class="form-control" id="name" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="slug" class="col-sm-3 control-label"><?php echo get_phrase('slug'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" name="slug" class="form-control" id="slug" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="parent" class="col-sm-3 control-label"><?php echo get_phrase('parent'); ?></label>

                        <div class="col-sm-5">
                            <select class="form-control" id="parent" name="p_id">
                                <option value="">-</option>
                            <?php 
                            if($categories): 
                                foreach ($categories as $key => $category):
                            ?>
                                <option value="<?= $category->id;?>"><?=$category->name; ?></option>
                            <?php 
                                endforeach;
                            endif; 
                            ?>
                            </select>
                        </div>
                    </div>                   

                    <div class="col-sm-3 control-label col-sm-offset-2">
                        <input type="submit" class="btn btn-success" value="Submit">
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>