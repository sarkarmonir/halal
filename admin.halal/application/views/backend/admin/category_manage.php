<!-- <button onclick="showAjaxModal('<?php echo base_url();?>modal/popup/category_create/');" class="btn btn-primary pull-right">
        <i class="fa fa-plus">
            <?php echo get_phrase('add_category'); ?>            
        </i>
    </button> -->
    <a type="button" class="btn btn-primary pull-right" href="<?=base_url('category/create'); ?>">
        <i class="fa fa-plus">
            <?php echo get_phrase('add_category'); ?>            
        </i>
    </a>

    <div style="clear:both;"></div>

    <br>
    <!-- Input addon -->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">All Categories</h3>
      </div>

      <div class="box-body">

        <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2">

            <thead>

                <tr>

                    <th><?php echo get_phrase('name');?></th>

                    <th><?php echo get_phrase('slug');?></th>

                    <th><?php echo get_phrase('parent');?></th>

                    <th><?php echo get_phrase('actions');?></th>

                </tr>

            </thead>



            <tbody>

                <?php foreach ($categories as $key => $category) { ?>   

                <tr>

                    <td><?php echo $category->name;?></td>

                    <td><?php echo $category->slug;?></td>

                    <td><?php echo $this->crud_model->category_name_by_id($category->p_id);?></td>

                    <td>

                        <a  onclick="showAjaxNewModal('<?php echo base_url();?>modal/popup/category_edit/<?php echo $category->id;?>');" 

                            class="btn btn-default btn-sm btn-icon icon-left">

                            <i class="entypo-pencil"></i>

                            Edit

                        </a>

                        <a href="<?php echo base_url('category/delete/'.$category->id);?>" class="btn btn-danger btn-sm btn-icon icon-left" onclick="return checkDelete();">
                            <i class="entypo-cancel"></i>Delete
                        </a>
                    </td>

                </tr>

                <?php } ?>

            </tbody>

        </table>
    </div>
</div>
        <script type="text/javascript">

          $(function () {
    // $("#example1").DataTable();
    $('#table-2').DataTable();
});
</script>

<script type="text/javascript">

    jQuery(window).load(function ()

    {

        var $ = jQuery;



        $("#table-2").dataTable({

            "sPaginationType": "bootstrap",

            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"

        });



        $(".dataTables_wrapper select").select2({

            minimumResultsForSearch: -1

        });



        // Highlighted rows

        $("#table-2 tbody input[type=checkbox]").each(function (i, el)

        {

            var $this = $(el),

            $p = $this.closest('tr');



            $(el).on('change', function ()

            {

                var is_checked = $this.is(':checked');



                $p[is_checked ? 'addClass' : 'removeClass']('highlight');

            });

        });



        // Replace Checboxes

        $(".pagination a").click(function (ev)

        {

            replaceCheckboxes();

        });

    });

</script>