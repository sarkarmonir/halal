<!-- <button onclick="showAjaxModal('<?php echo base_url();?>modal/popup/category_create/');" class="btn btn-primary pull-right">
        <i class="fa fa-plus">
            <?php echo get_phrase('add_category'); ?>            
        </i>
    </button> -->
    <a type="button" class="btn btn-primary pull-right" href="<?=base_url('product/create'); ?>">
        <i class="fa fa-plus">
            <?php echo get_phrase('add_item'); ?>            
        </i>
    </a>

    <div style="clear:both;"></div>

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>

                    <th><?php echo get_phrase('image');?></th>

                    <th><?php echo get_phrase('name');?></th>

                    <th><?php echo get_phrase('sku');?></th>

                    <th><?php echo get_phrase('stock');?></th>

                    <th><?php echo get_phrase('category');?></th>

                    <th><?php echo get_phrase('actions');?></th>

                </tr>

            </thead>



            <tbody>

                <?php foreach ($items as $key => $item) { ?>   

                <tr>

                    <td> <img src="<?php echo $item->featured_image;?>" style="width:100px;"></td>

                    <td><?php echo $item->title;?></td>

                    <td><?php echo $item->sku;?></td>

                    <td><?php echo $item->quantity;?></td>

                    <td><?php echo $item->categories;?></td>

                    <td>

                        <a  onclick="showAjaxNewModal('<?php echo base_url();?>modal/popup/category_edit/<?php echo $item->id;?>');" 

                            class="btn btn-default btn-sm btn-icon icon-left">

                            <i class="entypo-pencil"></i>

                            Edit

                        </a>
                        
                        <a href="<?php echo base_url('item/delete/'.$item->id);?>" class="btn btn-danger btn-sm btn-icon icon-left" onclick="return checkDelete();">
                            <i class="entypo-cancel"></i>Delete
                        </a>
                    </td>

                </tr>

                <?php } ?>

            </tbody>

        </table>



        <script type="text/javascript">

            jQuery(window).load(function ()

            {

                var $ = jQuery;



                $("#table-2").dataTable({

                    "sPaginationType": "bootstrap",

                    "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"

                });



                $(".dataTables_wrapper select").select2({

                    minimumResultsForSearch: -1

                });



        // Highlighted rows

        $("#table-2 tbody input[type=checkbox]").each(function (i, el)

        {

            var $this = $(el),

            $p = $this.closest('tr');



            $(el).on('change', function ()

            {

                var is_checked = $this.is(':checked');



                $p[is_checked ? 'addClass' : 'removeClass']('highlight');

            });

        });



        // Replace Checboxes

        $(".pagination a").click(function (ev)

        {

            replaceCheckboxes();

        });

    });

</script>