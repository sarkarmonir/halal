<div class="row">
    <div class="col-md-12">
        <!-- box -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add offer form</h3>
            </div><!-- /.box-header -->
            <!-- form -->
            <?php echo form_open_multipart(site_url("web_settings/offers"), array("class" => "form-horizontal")) ?>
            <div class="box-body">
                <!-- offer title -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Offer title<sup>*</sup></label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Offer title" name="title" required>
                    </div>
                </div>
                <!-- offer details -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Offer Details<sup>*</sup></label>
                    <div class="col-sm-8">
                        <textarea id="ck_editor" class="form-control" name="desc" rows="15" placeholder="Write offer details here ...." required></textarea>
                    </div>
                </div>
                <!-- image -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"> Offer Image<sup>*</sup></label>
                    <div class="col-sm-8">
                        <input type="file" name="offer_img" required>
                        <p class="help-block">Image type: jpg or png.</p> 
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Add Offer</button>
                        <button type="reset" value="Reset" class="btn btn-danger">Reset</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- notice -->
            <?php $sm = $this->session->flashdata('succ_msg');
            if (!empty($sm)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-check"></i><?=$this->session->flashdata('succ_msg')?></h5>
                </div>
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- box header -->
            <?php if(empty($lists)) : ?>
                <div class="box-header">
                    <h3 class="box-title">No Offer Found!</h3>
                </div>
            <?php else: ?>
                <div class="box-header">
                    <h3 class="box-title">Offer List</h3>
                </div>
                <!-- box content -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>Sl.</th>
                                <th>Offer Title</th>
                                <th>Offer Details</th>
                                <th>OfferImage</th>
                                <th>Actions</th>
                            </tr>
                            <?php $x = 0; foreach($lists as $list) : $x++; ?>
                            <tr>
                                <td><?php echo $x; ?></td>
                                <td>
                                    <?php 
                                    $a = $list->title;
                                    if (strlen($a) > 20) {
                                        $stringCut = substr($a, 0, 20);
                                        echo $stringCut . '...';
                                    }else{
                                        echo $a;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                    $a = $list->desc;
                                    if (strlen($a) > 20) {
                                        $stringCut = substr($a, 0, 20);
                                        echo $stringCut . '...';
                                    }else{
                                        echo $a;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <img src="<?php echo base_url(); ?>uploads/offers/<?=$list->img?>" alt="<?=$list->title?>" width="100px" height="100px">
                                </td>
                                <td>
                                <a data-toggle="modal" href="#update<?=$list->id?>" title="update" data-original-title="update">
                                        <i class="fa fa-pencil-square-o text-blue" data-toggle="tooltip" title="" data-original-title="update"></i>
                                    </a> |
                                    <a data-toggle="modal" href="#replace<?=$list->id?>" title="Replace Image" data-original-title="Replace Image">
                                        <i class="fa fa-file-image-o text-green" data-toggle="tooltip" title="" data-original-title="Replace Image"></i>
                                    </a> |
                                    <a data-toggle="modal" href="#delete<?=$list->id?>" title="Delete" data-original-title="Delete">
                                        <i class="fa fa-trash text-red" data-toggle="tooltip" title="" data-original-title="Delete"></i>
                                    </a>
                                </td>
                            </tr>
                            <!-- update offers -->
                            <div class="modal fade" id="update<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Update Information</h4>
                                        </div>
                                        <?php echo form_open(site_url("web_settings/update_offer")); ?>
                                        <div class="modal-body">
                                            <!-- client name -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Offer title<sup>*</sup></label>
                                                        <input type="text" class="form-control" name="title" value="<?=$list->title?>" placeholder="Offer title" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- client comment -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label> Offer Details<sup>*</sup></label>
                                                        <textarea id="ck_editor<?=$list->id?>" class="form-control" rows="15" name="desc" placeholder="Write offer details here ...." required><?=$list->desc?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input name="id" type="hidden" value="<?=$list->id?>" required />
                                            <input name="old_img" type="hidden" value="<?=$list->img?>" required />
                                            <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Update</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                                <!-- page script -->
                                <script>
                                    $(function () {
                                        CKEDITOR.replace('ck_editor<?=$list->id?>');
                                    });
                                </script>
                            </div>
                            <!-- replace image -->
                            <div class="modal fade" id="replace<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Replace Image</h4>
                                        </div>
                                        <?php echo form_open_multipart(site_url("web_settings/rep_offer_image")); ?>
                                        <div class="modal-body">
                                            <!-- replace image -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="inputbr_name"> Replace Image <sup>*</sup></label>
                                                        <input type="file" name="rep_offer_img" required>
                                                        <p class="help-block">Image type: jpg or png.</p> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input name="img" type="hidden" value="<?=$list->img?>" required>
                                            <input name="id" type="hidden" value="<?=$list->id?>" required />
                                            <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Replace Image</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                            <!-- delete modal -->
                            <div class="modal fade" id="delete<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Are you sure want to delete this?</h4>
                                        </div>
                                        <?php echo form_open(site_url("web_settings/offer_delete")); ?>
                                        <div class="modal-footer">
                                            <input name="id" type="hidden" value="<?=$list->id?>" required/>
                                            <input name="img" type="hidden" value="<?=$list->img?>" required/>
                                            <button type="submit" class="btn btn-danger btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Delete</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>  
</div>
</div>