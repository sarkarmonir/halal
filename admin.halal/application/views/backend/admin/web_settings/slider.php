<div class="row">
    <div class="col-md-12">
        <!-- box -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add slider form</h3>
            </div><!-- /.box-header -->
            <!-- form -->
            <?php echo form_open_multipart(site_url("web_settings/slider"), array("class" => "form-horizontal")) ?>
            <div class="box-body">
                <!-- caption 01 -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Caption 01<sup>*</sup></label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="caption 01" name="cap_1"  required>
                    </div>
                </div>
                <!-- caption 02 -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Caption 02<sup>*</sup></label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="caption 02" name="cap_2" required>
                    </div>
                </div>
                <!-- image -->
                <div class="form-group">
                    <label for="Inputprod_img" class="col-sm-2 control-label"> Slider Image <sup>*</sup></label>
                    <div class="col-sm-8">
                        <input type="file" name="slider_img" required>
                        <p class="help-block">Image type: jpg or png.</p> 
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Add Slider</button>
                        <button type="reset" value="Reset" class="btn btn-danger">Reset</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- notice -->
            <?php $sm = $this->session->flashdata('succ_msg');
            if (!empty($sm)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-check"></i><?=$this->session->flashdata('succ_msg')?></h5>
                </div>
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- box header -->
            <?php if(empty($lists)) : ?>
                <div class="box-header">
                    <h3 class="box-title">No Slider Found!</h3>
                </div>
            <?php else: ?>
                <div class="box-header">
                    <h3 class="box-title">Slider List</h3>
                </div>
                <!-- box content -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>Sl.</th>
                                <th>Caption 01</th>
                                <th>Caption 02</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            <?php $x = 0; foreach($lists as $list) : $x++; ?>
                            <tr>
                                <td><?php echo $x; ?></td>
                                <td>
                                    <?php 
                                    $a = $list->cap_1;
                                    if (strlen($a) > 20) {
                                        $stringCut = substr($a, 0, 20);
                                        echo $stringCut . '...';
                                    }else{
                                        echo $a;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                    $a = $list->cap_2;
                                    if (strlen($a) > 20) {
                                        $stringCut = substr($a, 0, 20);
                                        echo $stringCut . '...';
                                    }else{
                                        echo $a;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <img src="<?php echo base_url(); ?>uploads/slider/<?=$list->img?>" alt="<?=$list->cap_1?>" width="120px" height="60px">
                                </td>
                                <td>
                                <a data-toggle="modal" href="#update<?=$list->id?>" title="update" data-original-title="update">
                                        <i class="fa fa-pencil-square-o text-blue" data-toggle="tooltip" title="" data-original-title="update"></i>
                                    </a> |
                                    <a data-toggle="modal" href="#replace<?=$list->id?>" title="Replace Image" data-original-title="Replace Image">
                                        <i class="fa fa-file-image-o text-green" data-toggle="tooltip" title="" data-original-title="Replace Image"></i>
                                    </a> |
                                    <a data-toggle="modal" href="#delete<?=$list->id?>" title="Delete" data-original-title="Delete">
                                        <i class="fa fa-trash text-red" data-toggle="tooltip" title="" data-original-title="Delete"></i>
                                    </a>
                                </td>
                            </tr>
                            <!-- update slider -->
                            <div class="modal fade" id="update<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Update Information</h4>
                                        </div>
                                        <?php echo form_open(site_url("web_settings/update_slider")); ?>
                                        <div class="modal-body">
                                            <!-- main caption -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="inputbr_name"> Caption 01<sup>*</sup></label>
                                                        <input type="text" class="form-control" name="cap_1" value="<?=$list->cap_1?>" placeholder="Caption 01" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- sub caption -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="inputbr_name"> Caption 02<sup>*</sup></label>
                                                        <input type="text" class="form-control" name="cap_2" value="<?=$list->cap_2?>" placeholder="Caption 01" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input name="id" type="hidden" value="<?=$list->id?>" required />
                                            <input name="old_img" type="hidden" value="<?=$list->img?>" required />
                                            <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Update</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                            <!-- replace image -->
                            <div class="modal fade" id="replace<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Replace Image</h4>
                                        </div>
                                        <?php echo form_open_multipart(site_url("web_settings/rep_slider_image")); ?>
                                        <div class="modal-body">
                                            <!-- replace image -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="inputbr_name"> Replace Image <sup>*</sup></label>
                                                        <input type="file" name="rep_slider_img" required>
                                                        <p class="help-block">Image type: jpg or png.</p> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input name="img" type="hidden" value="<?=$list->img?>" required>
                                            <input name="id" type="hidden" value="<?=$list->id?>" required />
                                            <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Replace Image</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                            <!-- delete modal -->
                            <div class="modal fade" id="delete<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Are you sure want to delete this?</h4>
                                        </div>
                                        <?php echo form_open(site_url("web_settings/slider_delete")); ?>
                                        <div class="modal-footer">
                                            <input name="id" type="hidden" value="<?=$list->id?>" required/>
                                            <input name="img" type="hidden" value="<?=$list->img?>" required>
                                            <button type="submit" class="btn btn-danger btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Delete</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>  
</div>
</div>