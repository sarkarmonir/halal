<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Privacy & policy page</h3>
            </div>
            <?php echo form_open(site_url("web_settings/privacy_policy"), array("class" => "form-horizontal")) ?>
            <div class="box-body">
                <!-- tilte -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Title <sup>*</sup></label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" name="title" value="<?=$get_info->title?>" placeholder="Title" required>
                    </div>
                </div>
                <!-- descr -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Description <sup>*</sup></label>
                    <div class="col-sm-8">
                        <textarea id="ck_editor" class="form-control" name="desc" rows="15" placeholder="Write page content here ...." required><?=$get_info->desc?></textarea>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- notice -->
            <?php $sm = $this->session->flashdata('succ_msg');
            if (!empty($sm)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-check"></i><?=$this->session->flashdata('succ_msg')?></h5>
                </div>
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div>