<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
            <h3 class="box-title">FAQ page</h3>
            </div>
            <?php echo form_open_multipart(site_url("web_settings/faq"), array("class" => "form-horizontal")) ?>
            <div class="box-body">
                <!-- tilte -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Question <sup>*</sup></label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" name="ques" placeholder="Question" required>
                    </div>
                </div>
                <!-- desc -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Answer <sup>*</sup></label>
                    <div class="col-sm-8">
                        <textarea id="ck_editor" class="form-control" name="answ" rows="15" placeholder="Write your description here ...." required></textarea>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Add FAQ</button>
                        <button type="reset" value="Reset" class="btn btn-danger">Reset</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- notice -->
            <?php $sm = $this->session->flashdata('succ_msg');
            if (!empty($sm)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-check"></i><?=$this->session->flashdata('succ_msg')?></h5>
                </div>
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div>
<!-- slider list row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <!-- box header -->
            <?php if(empty($lists)) : ?>
                <div class="box-header">
                    <h3 class="box-title">No FAQ Found!</h3>
                </div>
            <?php else: ?>
                <div class="box-header">
                    <h3 class="box-title">FAQ List</h3>
                </div>
                <!-- box content -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>Sl.</th>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Actions</th>
                            </tr>
                            <?php $x = 0; foreach($lists as $list) : $x++; ?>
                            <tr>
                                <td><?php echo $x; ?></td>
                                <td>
                                    <?php 
                                    $a = $list->ques;
                                    if (strlen($a) > 20) {
                                        $stringCut = substr($a, 0, 20);
                                        echo $stringCut . '...';
                                    }else{
                                        echo $a;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                    $a = $list->answ;
                                    if (strlen($a) > 20) {
                                        $stringCut = substr($a, 0, 20);
                                        echo $stringCut . '...';
                                    }else{
                                        echo $a;
                                    }
                                    ?>
                                </td>
                                
                                <td>
                                    <a data-toggle="modal" href="#update<?=$list->id?>" title="update" data-original-title="update">
                                        <i class="fa fa-pencil-square-o text-blue" data-toggle="tooltip" title="" data-original-title="update"></i>
                                    </a> |
                                    <a data-toggle="modal" href="#delete<?=$list->id?>" title="Delete" data-original-title="Delete">
                                        <i class="fa fa-trash text-red" data-toggle="tooltip" title="" data-original-title="Delete"></i>
                                    </a>
                                </td>
                            </tr>
                            <!-- update -->
                            <div class="modal fade" id="update<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Update Information</h4>
                                        </div>
                                        <?php echo form_open(site_url("web_settings/update_faq")); ?>
                                        <div class="modal-body">
                                            <!-- question -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="inputbr_name"> Question<sup>*</sup></label>
                                                        <input type="text" class="form-control" name="ques" value="<?=$list->ques?>" placeholder="Question" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- answer -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="inputbr_name"> Answer<sup>*</sup></label>
                                                        <textarea id="ck_editor<?=$list->id?>" class="form-control" name="answ" rows="15" placeholder="Write your description here ...." required><?=$list->answ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input name="id" type="hidden" value="<?=$list->id?>" required />
                                            <button type="submit" class="btn btn-primary btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Update</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                                <!-- page script -->
                                <script>
                                    $(function () {
                                        CKEDITOR.replace('ck_editor<?=$list->id?>');
                                    });
                                </script>
                            </div>
                            <!-- delete modal -->
                            <div class="modal fade" id="delete<?=$list->id?>" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Are you sure want to delete this?</h4>
                                        </div>
                                        <?php echo form_open(site_url("web_settings/faq_delete")); ?>
                                        <div class="modal-footer">
                                            <input name="id" type="hidden" value="<?=$list->id?>" required/>
                                            <button type="submit" class="btn btn-danger btn-icon"><i class="fa fa-fw fa-check-square-o"></i> Delete</button>
                                            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancel</button>
                                        </div>
                                        <?php echo form_close(); ?> 
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>  
</div>
</div>