<div class="row">
  <div class="col-md-12">
    <!-- Input addon -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"> <?php echo get_phrase('genaral_settings'); ?></h3>
      </div>


      
      <!-- form -->
      <?php echo form_open_multipart(site_url("web_settings/general_settings"), array('class'=>'form-horizontal form-groups-bordered','role'=>'form')) ?>
      <div class="row">
        <!-- main settings column -->
        <div class="col-md-6">
          <div class="box-body">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Main settings</h3>
              </div>
              <div class="box-body">
                <!-- logo -->
                <div class="form-group">
                  <label for="image-in" class="col-sm-2 control-label">Logo</label>
                  <div class="col-sm-10">
                    <p><img src='<?=base_url()?>uploads/logo/<?=$gen_settings->logo_img?>'></p>
                    <input type="file" name="logo_img" />
                    <p class="help-block">Image type: jpg or png. </p> 
                  </div>
                </div>
                <!-- welcome text -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Welcome Text</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="wlc_text" placeholder="Welcome text" value="<?=$gen_settings->wlc_text?>" required>
                  </div>
                </div>
                <!-- order no -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Order No</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="order_no" placeholder="0123 456 789" value="<?=$gen_settings->order_no?>" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- operation time column -->
        <div class="col-md-6">
          <div class="box-body">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Operation Time</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Details</label>
                  <div class="col-sm-10">
                    <textarea name="opera_desc" rows="5" style="width:100%" required ><?=$gen_settings->opera_desc?></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- second row -->
      <div class="row">
      <!-- social column -->
        <div class="col-md-6">
          <div class="box-body">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Social Link</h3>
              </div>
              <div class="box-body">
                <!-- facebook -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Facebook</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="fb" placeholder="link" value="<?=$gen_settings->fb?>" required>
                  </div>
                </div>
                <!-- twitter -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Twitter</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="twt" placeholder="link" value="<?=$gen_settings->twt?>" required>
                  </div>
                </div>
                <!-- youtube -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Youtube</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="y_tube" placeholder="link" value="<?=$gen_settings->y_tube?>" required>
                  </div>
                </div>
                <!-- google plus -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Google+</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="g_plus" placeholder="link" value="<?=$gen_settings->g_plus?>" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- more link column -->
        <div class="col-md-6">
          <div class="box-body">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">More Link</h3>
              </div>
              <div class="box-body">
                <!-- more link -->
                <div class="form-group">
                  <label class="col-sm-2 control-label"> Link</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="more_link" placeholder="Link" value="<?=$gen_settings->more_link?>" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="form-group">
          <div class="col-sm-2"></div>
          <div class="col-sm-10">
            <input name="logo_img" type="hidden" value="<?=$gen_settings->logo_img?>" required/>
            <button type="submit" class="btn btn-primary">Update Settings</button>
          </div>
        </div>
      </div>
      <?php echo form_close() ?>
      <!-- notice -->
      <?php $sm = $this->session->flashdata('succ_msg');
      if (!empty($sm)) { ?>
          <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h5><i class="icon fa fa-check"></i><?=$this->session->flashdata('succ_msg')?></h5>
          </div>
      <?php } ?>
    </div><!-- /.box -->
  </div>
</div>