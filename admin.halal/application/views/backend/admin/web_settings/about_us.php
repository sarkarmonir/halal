<!-- slider form row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">About us page</h3>
            </div>
            <?php echo form_open_multipart(site_url("web_settings/about_us"), array("class" => "form-horizontal")) ?>
            <div class="box-body">
                <!-- tilte -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Title <sup>*</sup></label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" name="title" value="<?=$get_info->title?>" placeholder="Title" required>
                    </div>
                </div>
                <!-- desc -->
                <div class="form-group">
                    <label for="inputphn" class="col-sm-2 control-label">Description <sup>*</sup></label>
                    <div class="col-sm-8">
                        <textarea id="ck_editor" class="form-control" name="desc" rows="15" placeholder="Write your description here ...." required><?=$get_info->desc?></textarea>
                    </div>
                </div>
                <!-- image -->
                <div class="form-group">
                    <label for="Inputprod_img" class="col-sm-2 control-label"> Image <sup>*</sup></label>
                    <div class="col-sm-8">
                        <p><img src='<?=base_url()?>uploads/about_us/<?=$get_info->img?>' width="100px" height="100px"></p>
                        <input type="file" name="about_us_img" />
                        <p class="help-block">Image type: jpg or png. </p> 
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-6">
                        <input name="img" type="hidden" value="<?=$get_info->img?>" required/>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- notice -->
            <?php $sm = $this->session->flashdata('succ_msg');
            if (!empty($sm)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-check"></i><?=$this->session->flashdata('succ_msg')?></h5>
                </div>
            <?php } ?>
        </div><!-- /.box -->
    </div>
</div>