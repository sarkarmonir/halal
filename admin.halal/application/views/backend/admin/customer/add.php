<div class="row">
    <div class="col-md-12">
        <!-- Input addon -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">+Add New Customer</h3>
            </div>
            <?=form_open_multipart(site_url("customers/doadd"), array("id" => "customer_form", "class" => "form-horizontal"))?>
                <div class="box-body">
                    <div id="errorMsg" style="color:#CC0000;">
                    <?php
                        if (validation_errors()) {
                            echo '<div id="validation_errors" title="Error:">';
                            echo '<div class="response-msgs errors ui-corner-all"><span></span><br /><ul>';
                            echo validation_errors();
                            echo '</ul></div>';
                            echo '</div>';
                    } ?>
                    </div>
                    <div class="tab-pane active"  id="hr3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name<span style="color:red;">*</span></label>   
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="name" placeholder="Name" value="<?php echo set_value('name'); ?>" required />
                                   </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label">Email<span style="color:red;">*</span></label>   
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo set_value('email'); ?>" required />
                                   </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Password<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>" required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Confirm Password<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="passconf" placeholder="Confirm Password" value="<?php echo set_value('passconf'); ?>" required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Telephone<span style="color:red;">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="telephone" placeholder="Telephone" value="<?php echo set_value('telephone'); ?>" required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Fax</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="fax" placeholder="Fax" value="<?php echo set_value('fax'); ?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Newsletter<span style="color:red;">*</span></label>
                                    <div class="col-sm-2">
                                        <select name="newsletter" class="form-control">
                                            <option value="1">Enable</option>
                                            <option value="0">Disable</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Status<span style="color:red;">*</span></label>
                                    <div class="col-sm-2">
                                        <select name="status" class="form-control">
                                            <option value="1">Enable</option>
                                            <option value="0">Disable</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                            
                    <div class="box-footer">
                        <div class="form-group">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary">Add New Customer</button>
                            </div>
                        </div>
                    </div>
                </div>    
            <?=form_close()?>
        </div>
    </div>
</div>