<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Customer List</h3>
    </div>
    <div class="box-body">
        <?php
        if (validation_errors()) {
            echo '<div id="validation_errors" title="Error:" ';
            echo '<div style="color:red;" class="response-msgs errors ui-corner-all"><ul>';
            echo validation_errors();
            echo '</ul></div>';
            echo '</div>';
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <a href="<?php echo base_url(); ?>customers/add" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <a href="javascript:void(0)" onclick="return refresh();" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                <script>
                    var table =['customers'];
                </script>
                <a href="#" class="btn btn-danger" onclick="return deleteall(table, image);"><i class="fa fa-trash-o"></i></a>
            </div>
        </div>
        <table class="table table-bordered table-striped datatable dataTables_wrapper form-inline dt-bootstrap" id="table-2" width="100%">
            <thead>			                
                <tr>
                    <th data-hide="phone"><input type="checkbox" name="selectall" id="selectall"/></th>
                    <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Customer Name</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Email</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> IP</th>
                    <th data-hide="phone"><i class="fa fa-fw fa-sort text-muted hidden-md hidden-sm hidden-xs"></i> Date Added</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($customers as $customer) { ?>
                <tr id="row_<?php echo $customer->id; ?>">
                    <td><input class="checkbox1" type="checkbox" name="selected[]" id="del_<?php echo $customer->id; ?>" value="<?php echo $customer->id; ?>-img/customer/<?php echo $customer->image; ?>" /></td>
                    <td><?php echo $customer->name; ?></td>
                    <td><?php echo $customer->email; ?></td>
                    <script>var table='customer';</script>
                    <td><?php if($customer->status == 1){echo "<span id='status_".$customer->id."'><a href='#' class = 'btn btn-success' onclick='status_change(table,".$customer->id.",".$customer->status.");'>Enable</a></span>";}else{echo "<span id='status_".$customer->id."'><a href='#' class = 'btn btn-danger'  onclick='status_change(table,".$customer->id.",".$customer->status.");'>Disable</a></span>";} ?></td>
                    <td><?php echo $customer->ip; ?></td>
                    <td><?php echo $customer->date; ?></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>customers/edit/<?php echo $customer->id; ?>">Edit</a>
                                </li>
                                <li>
                                    <script>
                                        var table = ["customers"];
                                        var image = "img/customer/<?php echo $customer->image; ?>";
                                    </script>
                                    <a href="#" onclick="return dodelete(table,<?php echo $customer->id; ?>,image);">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(function() {
    //  $("#example1").DataTable();
        $('#table-2').DataTable();
    });
</script>