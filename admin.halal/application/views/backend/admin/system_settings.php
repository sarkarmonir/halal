<?php echo form_open('admin/system_settings/do_update', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" >

            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('system_settings'); ?>
                </div>
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('system_name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="system_name" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('system_title'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="system_title" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'system_title'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'address'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('phone'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="phone" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('currency'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="currency" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'currency'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('system_email'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="system_email" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'system_email'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('language'); ?></label>
                    <div class="col-sm-5">
                        <select name="language" class="selectboxit">
                            <?php
                            $fields = $this->db->list_fields('language');
                            foreach ($fields as $field) {
                                if ($field == 'phrase_id' || $field == 'phrase')
                                    continue;

                                $current_default_language = $this->db->get_where('settings', array('type' => 'language'))->row()->description;
                                ?>
                                <option value="<?php echo $field; ?>"
                                        <?php if ($current_default_language == $field) echo 'selected'; ?>> <?php echo $field; ?> </option>
                                        <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('skin_theme'); ?></label>
                    <div class="col-sm-5">
                        <select name="skin_theme" class="selectboxit">
                            <?php
                            $fields = array('black','blue','cafe','green','purple','red','white','yellow'); 
                            foreach ($fields as $field) {
                                if ($field == 'phrase_id' || $field == 'phrase')
                                    continue;

                                $current_default_language = $this->db->get_where('settings', array('type' => 'skin_theme'))->row()->description;
                                ?>
                                <option value="<?php echo $field; ?>"
                                        <?php if ($current_default_language == $field) echo 'selected'; ?>> <?php echo $field; ?> </option>
                                        <?php
                                    }
                                    ?>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">Image</label>
                    <div class="col-sm-5" ng-app="main-App" ng-controller="AdminController">
                        <input ng-model="form.image" type="file" class="form-control input-lg" accept="image/*" onchange="angular.element(this).scope().uploadedFile(this)" style="width:400px; display: none;" id="fileInput" >
                        <img ng-src="{{image_source}}" style="width:300px;" ng-click="triggerUpload()">
                        <input type="hidden" value="{{logo_url}}" id="logo_url" name="logo_url"/>
                    </div>
                </div>


                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('text_align'); ?></label>
                    <div class="col-sm-5">
                        <select name="text_align" class="selectboxit">
                            <?php $text_align = $this->db->get_where('settings', array('type' => 'text_align'))->row()->description; ?>
                            <option value="left-to-right" <?php if ($text_align == 'left-to-right') echo 'selected'; ?>> left-to-right (LTR)</option>
                            <option value="right-to-left" <?php if ($text_align == 'right-to-left') echo 'selected'; ?>> right-to-left (RTL)</option>
                        </select>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('save'); ?></button>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
<?php echo form_close(); ?>

<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script type="text/javascript">

        var app =  angular.module('main-App',[]);

        app.controller('AdminController', function($scope, $http) {

          $scope.form = [];
          $scope.files = [];

          $scope.image_source="<?=base_url();?><?= $this->db->get_where('settings', array('type' => 'logo_url'))->row()->description; ?>";
          $scope.logo_url="<?= $this->db->get_where('settings', array('type' => 'logo_url'))->row()->description; ?>";

          $scope.submit = function(image) {
            // $scope.form.image = $scope.files[0];
            $scope.form.image = image;

            $http({
              method  : 'POST',
              url     : "<?=base_url('admin/image_upload')?>",
              processData: false,
              transformRequest: function (data) {
                  var formData = new FormData();
                  formData.append("userfile", $scope.form.image);  
                  return formData;  
              },  
              data : $scope.form,
              headers: {
                     'Content-Type': undefined
              }
           }).success(function(data){
                console.log(data);
                $scope.logo_url = data.image_upload_path+data.file_name;
                console.log(data.image_upload_path);
           });

          };

          $scope.triggerUpload=function()
            {
             var fileuploader = angular.element("#fileInput");
                fileuploader.on('click',function(){
                    console.log("File upload triggered programatically");
                })
                fileuploader.trigger('click')
            }

          $scope.uploadedFile = function(element) {
            $scope.currentFile = element.files[0];
            var reader = new FileReader();
            // console.log(reader);
            $scope.submit($scope.currentFile);
            reader.onload = function(event) {
              $scope.image_source = event.target.result
              $scope.$apply(function($scope) {
                $scope.files = element.files;
              });
            }
                    reader.readAsDataURL(element.files[0]);
          }

        });
    </script>