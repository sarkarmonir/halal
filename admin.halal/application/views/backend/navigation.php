<header class="main-header">
    <!-- Logo -->
    <a href="<?= site_url(); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>H</b>OM</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Halal</b> ADMIN</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 4 messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="<?= site_url('assets/dist/img/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <!-- end message -->
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="<?= site_url('assets/dist/img/user3-128x128.jpg'); ?>" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            AdminLTE Design Team
                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="<?= site_url('assets/dist/img/user4-128x128.jpg'); ?>" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Developers
                                            <small><i class="fa fa-clock-o"></i> Today</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="<?= site_url('assets/dist/img/user3-128x128.jpg'); ?>" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Sales Department
                                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="<?= site_url('assets/dist/img/user4-128x128.jpg'); ?>" class="img-circle" alt="User Image">
                                        </div>
                                        <h4>
                                            Reviewers
                                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 10 notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                                        page and may cause design problems
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-red"></i> 5 new members joined
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 9 tasks</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Design some buttons
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Create a nice theme
                                            <small class="pull-right">40%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">40% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Some task I need to do
                                            <small class="pull-right">60%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">60% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li><!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Make beautiful transitions
                                            <small class="pull-right">80%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">80% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= site_url('assets/dist/img/user2-160x160.jpg'); ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs">Halal Admin</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= site_url('assets/dist/img/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image">

                            <p>
                                Alexander Pierce - Super Admin
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= site_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>

    </nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= site_url('assets/dist/img/user2-160x160.jpg'); ?>" class="img-circle" style="width: 28px;" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Super Admin</p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?>">
                <a href="<?=site_url('admin')?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            
            <!-- user management -->
            <li class="treeview <?php if (in_array($page_name, array(
                                    'user/admin',
                                    'customer/index',
                                    'customer/index'
                                ))) echo "opened active"; ?>" >
                <a href="#">
                    <i class="fa fa-folder"></i> <span>User Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <!-- sub drop down  -->
                <ul class="treeview-menu">
                    <li class="<?php if ($page_name == 'next_time/next_time') echo "active"; ?>">
                        <a href="<?php echo base_url('customer_group'); ?>">
                            <span><i class="fa fa-circle-o"></i> Admin Type</span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'user/admin') echo "active"; ?>">
                        <a href="<?php echo base_url('admin'); ?>">
                            <span><i class="fa fa-circle-o"></i> Admin Management</span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'user/admin') echo "active"; ?>">
                        <a href="<?php echo base_url('customer'); ?>">
                            <span><i class="fa fa-circle-o"></i> Customer Management</span>
                        </a>
                    </li>
                </ul>
            </li>

            

            <li class="treeview <?php if (in_array($page_name, array('category_manage', 'category_create', 'item_manage', 'item_create','category_manage', 'category_create', 'item_manage', 'item_create'))) echo "opened active"; ?>"">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Product Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('categories'); ?>"><i class="fa fa-circle-o"></i> Product Category</a></li>
                    <li class="<?php if ($page_name == 'item_manage') echo "active"; ?>" ><a href="<?php echo base_url('products'); ?>"><i class="fa fa-circle-o"></i> Product</a></li>
                    
                </ul>
            </li>
            <li class="treeview <?php if (in_array($page_name, array('promotion_manage', 'coupon', 'discount'))) echo "opened active"; ?>"">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Promotion Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($page_name == 'coupon') echo "active"; ?>" ><a href="<?php echo base_url('coupon'); ?>"><i class="fa fa-circle-o"></i> Coupon Management</a></li>
                    <li><a href="<?php echo base_url('discount'); ?>"><i class="fa fa-circle-o"></i> Discount Management</a></li>
                </ul>
            </li>

            

            <li class="treeview <?php if (in_array($page_name, array('category_manage', 'category_create', 'item_manage', 'item_create'))) echo "opened active"; ?>">
                <a href="<?= base_url() ?>">
                    <i class="fa fa-money"></i>
                    <span>Payment Settings</span>
                </a>
            </li>

            <li class="treeview <?php if (in_array($page_name, array('category_manage', 'category_create', 'item_manage', 'item_create'))) echo "opened active"; ?>">
                <a href="<?= base_url() ?>">
                    <i class="fa  fa-file-text"></i>
                    <span>Stock Management</span>
                </a>
            </li>
            <li class="treeview <?php if (in_array($page_name, array('category_manage', 'category_create', 'item_manage', 'item_create'))) echo "opened active"; ?>">
                <a href="<?= base_url() ?>">
                    <i class="fa  fa-file-text"></i>
                    <span>Inventory</span>
                </a>
            </li>

            <li class="treeview <?php if (in_array($page_name, array('category_manage', 'category_create', 'item_manage', 'item_create'))) echo "opened active"; ?>">
                <a href="<?= base_url('order') ?>">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Order Management</span>
                </a>
            </li>

            <!-- SETTINGS -->
            <li class="treeview <?php if (in_array($page_name, array('system_settings', 'manage_language'))) echo "opened active"; ?> ">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Admin <?php echo get_phrase('settings'); ?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>admin/system_settings">
                            <span><i class="fa fa-h-square"></i> <?php echo get_phrase('system_settings'); ?></span>
                        </a>
                    </li>                             
                </ul>
            </li>
            <!-- SETTINGS --> 
            <li class="treeview <?php if (in_array($page_name, array(
                                    'web_settings/general_settings',
                                    'web_settings/slider',
                                    'web_settings/wlc',
                                    'web_settings/offers',
                                    'web_settings/testimonial',
                                    'web_settings/about_us',
                                    'web_settings/faq',
                                    'web_settings/our_farm',
                                    'web_settings/farm_gallery',
                                    'web_settings/privacy_policy'
                                ))) echo "opened active"; ?> ">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>Web Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($page_name == 'web_settings/general_settings') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/general_settings')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Genaral_Settings')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/slider') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/slider')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Slider')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/wlc') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/wlc_msg_on_home')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Welcome_Msg_on_home')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/offers') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/offers')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Offers')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/testimonial') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/testimonial')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Testimonial')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/about_us') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/about_us')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('About_page')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/faq') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/faq')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('faq_page')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/our_farm') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/our_farm')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Our_farm')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/farm_gallery') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/farm_gallery')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Farm_gallery')?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'web_settings/privacy_policy') echo "active"; ?>">
                        <a href="<?=base_url('web_settings/privacy_policy')?>">
                            <span><i class="fa fa-square"></i> <?=get_phrase('Privacy_&_policy_page')?></span>
                        </a>
                    </li>
                    <li><a href="#"><span><i class="fa fa-square"></i> Conatact details</span></a></li>                      
                    <li><a href="#"><span><i class="fa fa-square"></i> Static menu</span></a></li> 
                </ul>
            </li>

            <!-- ACCOUNT -->
            <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>admin/manage_profile">
                    <i class="fa fa-user"></i>
                    <span><?php echo get_phrase('profile'); ?></span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>