  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=site_url('assets/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?=site_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
  
  <link rel="stylesheet" href="<?=site_url('assets/plugins/datepicker/datepicker3.css');?>">
  <!-- Datatable csss -->
  <link rel="stylesheet" href="<?=site_url('assets/plugins/datatables/dataTables.bootstrap.css');?>">
  <link rel="stylesheet" href="<?=site_url('assets/plugins/select2/select2.min.css');?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=site_url('assets/dist/css/AdminLTE.min.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=site_url('assets/dist/css/skins/_all-skins.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-multiselect.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery 2.2.3 -->
<script src="<?=site_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>

<?php if ($text_align == 'right-to-left') : ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-rtl.css">
<?php endif; ?>

<script>
    var BASE_URL = '<?php echo base_url(); ?>';
    
    function checkDelete()
    {
        var chk=confirm("Are You Sure To Delete This !");
        if(chk)
        {
          return true;  
        }
        else{
            return false;
        }
    }
</script>

<style>
    .tile-stats .icon
    {
        bottom: 30px;
    }
</style>