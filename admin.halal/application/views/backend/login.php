<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo get_phrase('login'); ?> | <?php echo $this->system_title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=site_url('assets/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=site_url('assets/dist/css/AdminLTE.min.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=site_url('assets/plugins/iCheck/square/blue.css');?>">

  <link rel="stylesheet" href="<?=site_url('assets/css/custom.css');?>">

  <link rel="stylesheet" href="<?=site_url('assets/css/neon-forms.css');?>">

  <link rel="stylesheet" href="<?=site_url('assets/css/font-icons/entypo/css/entypo.css');?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery 2.2.3 -->
  <script src="<?=site_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
</head>
<body class="hold-transition page-body login-page login-form-fall loaded login-form-fall-init">
    <!-- This is needed when you send requests via Ajax -->
    <script type="text/javascript">
        var baseurl = '<?php echo base_url(); ?>';
    </script>
<div class="login-container">
    <div class="login-header login-caret">
        <div class="login-content" style="width:100%">
            <!-- <a href="<?=site_url();?>"><b>Admin</b>LTE</a> -->
            <a href="<?php echo base_url(); ?>" class="logo">
                <img src="<?=$this->logo;?>" height="60" alt="" />
            </a>

            <p class="description">
                <h2 style="color:#cacaca; font-weight:100;">
                    <?php echo $this->system_name; ?>
                </h2>
            </p>

            <!-- progress bar indicator -->
            <div class="login-progressbar-indicator">
                <h3>43%</h3>
                <span>logging in...</span>
            </div>
        </div>        
    </div>
    <div class="login-progressbar">
        <div style="width: 0px;"></div>
    </div>
  <!-- /.login-logo -->
    <div class="login-form">
        <div class="login-content">
            <div class="form-login-error">
        <h3>Invalid login</h3>
        <p>Please enter correct email and password!</p>
            </div>

            <form method="post" role="form" id="form_login">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" data-mask="email" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                    </div>
                </div>
                <div class="form-group">
                <!-- /.col -->        
                    <button type="submit" class="btn btn-primary btn-block btn-login"><i class="entypo-login"></i>Login</button>        
                <!-- /.col -->
                </div>
            </form>

    <!-- /.social-auth-links -->

         </div>
    </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<script src="<?=site_url('assets/js/gsap/main-gsap.js');?>"></script>

<script src="<?=site_url('assets/plugins/jQueryUI/jquery-ui.min.js');?>"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?=site_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- iCheck -->
<script src="<?=site_url('assets/plugins/iCheck/icheck.min.js');?>"></script>

<script src="<?=site_url('assets/js/joinable.js');?>"></script>

<script src="<?=site_url('assets/js/resizeable.js');?>"></script>

<script src="<?=site_url('assets/js/neon-api.js');?>"></script>

<script src="<?=site_url('assets/js/jquery.validate.min.js');?>"></script>

<script src="<?=site_url('assets/js/neon-login.js');?>"></script>

<script src="<?=site_url('assets/js/neon-custom.js');?>"></script>

<script src="<?=site_url('assets/js/neon-demo.js');?>"></script>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
