<?php 
/**
* 
*/
class Auth extends CI_Controller
{

	public $logo = " ";
    public $system_name = " ";
    public $system_title = " ";

	function __construct()
	{
		parent::__construct();
		$this->logo = base_url().$this->db->get_where('settings', array('type' => 'logo_url'))->row()->description;
    	$this->system_name = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
    	$this->system_title = $this->db->get_where('settings', array('type' => 'system_title'))->row()->description;
	}
}