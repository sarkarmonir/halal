<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for customer group maintains
 * @package customer
 * @author Jahid Al Mamun
 */
class Tax extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'tax';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('taxs');
    }

    /**
     * This method display all tax  with tree based 
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @package customer
     * 
     * 
     */
    public function index() {
        
        $data['tax'] = $this->data->getall($this->table);
        
        $data['page_name'] = 'tax/index';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new tax
     * @author Jahid al mamun
     * 
     */
    public function add() {
        $data['page_name'] = 'tax/add';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }
/**
     * this method use for add new tax action process
     * @author Jahid al mamun
     * 
     */
    public function doadd() {
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            redirect('tax/add',$data);
        } else {
             
                if ($this->taxs->add()) {
                    $this->session->set_flashdata('success','tax Added Successfully.');
                    redirect('tax');
                    
                }else
                {
                    
                }
            
        }
//        echo json_encode($response);
    }

    /**
     * This method use for edit tax
     * @param int $id edit with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function edit($id) {
        
      
        $data['id'] = $id;
        $data['tax'] = $this->data->getone($this->table,$id);
        $data['page_name'] = 'tax/edit';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
       
    }

    /**
     * this function use for update edit data process
     * @author rjs
     */
    public function doedit()
    {
        $id = $this->input->post('id');
        
        
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('type', 'Tax Type', 'required');
        $this->form_validation->set_rules('rate', 'Rate', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            redirect("tax/edit/$id",$data);
        } else {
                if ($this->taxs->edit($id)) {
                    $this->session->set_flashdata('success','Customer tax Successfully.');
                    redirect('tax',$data);
                }
        }
    }
    /**
     * use for delete this tax with id
     * @param int $id delete tax with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function delete($id) {
        
    }

}
