<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for customer group maintains
 * @package customer
 * @author Jahid Al Mamun
 */
class Payment_method extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'payments';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('payment_method_model');
    }

    /**
     * This method display all shipping  with tree based 
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @package customer
     * 
     * 
     */
    public function index() {
        $data['payment_method'] = $this->data->getall($this->table);
        
        $data['page_name'] = 'payment_method/index';

        $data['page_title'] = 'Payment methods';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new shipping
     * @author Jahid al mamun
     * 
     */
    public function add() {
        
        $data['page_name'] = 'payment_method/add';

        $data['page_title'] = 'Payment methods';

        $this->load->view('backend/index', $data);
    }
/**
     * this method use for add new shipping action process
     * @author Jahid al mamun
     * 
     */
    public function doadd() {
       
        $this->form_validation->set_rules('title', 'Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            redirect('payment_method/add',$data);
        } else {
                if ($this->payment_method_model->add()) {
                    $this->session->set_flashdata('success','payment method Added Successfully.');
                    redirect('payment_method');
                    
                }else
                {
                    
                }
            
        }
//        echo json_encode($response);
    }

    /**
     * This method use for edit shipping
     * @param int $id edit with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function edit($id) {
      
        $data['id'] = $id;
        $methods = $this->data->getone($this->table,$id);
        $data['methods'] = $methods[0]; 
        
        $data['page_name'] = 'payment_method/edit';

        $data['page_title'] = 'Payment methods';

        $this->load->view('backend/index', $data);
       
    }

    /**
     * this function use for update edit data process
     * @author rjs
     */
    public function doedit()
    {
        $id = $this->input->post('id');
        $this->form_validation->set_rules('title', 'Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            redirect("payment_method/edit/$id",$data);
        } else {
                if ($this->payment_method_model->edit($id)) {
                    $this->session->set_flashdata('success','payment method Update Successfully.');
                    redirect('payment_method',$data);
                }
        }
    }
    /**
     * use for delete this shipping with id
     * @param int $id delete shipping with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function delete($id) {
        
    }

}
