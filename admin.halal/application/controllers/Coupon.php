<?php

/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for product coupon maintains
 * @package product
 * @author Jahid Al Mamun
 */
class Coupon extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'coupon';
        $this->table1 = 'coupon_product';
        $this->table2 = 'coupon_category';
        $this->table3 = 'product';
        $this->table4 = 'category';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('coupons');

        /* cache control */

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        $this->output->set_header('Pragma: no-cache');

        if ($this->session->userdata('roule') != 'admin')
            redirect(base_url() . 'login', 'refresh');
    }

    /**
     * This method display all coupon 
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @package coupon
     * 
     * 
     */
    public function index() {

        $data['coupon'] = $this->data->getall($this->table);
        
        $data['page_name'] = 'coupon/index';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new coupon
     * @author Jahid al mamun
     * 
     */
    public function add() {
        $data['products'] = $this->data->getall($this->table3);
        $data['categories'] = $this->data->getall($this->table4);
        
        $data['page_name'] = 'coupon/add';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new coupon action process
     * @author Jahid al mamun
     * 
     */
    public function doadd() {
//        print_r($_POST); die();

        $response = array();
        $response['error'] = 1;
        $this->form_validation->set_rules('name', 'Coupon Name', 'required');
        $this->form_validation->set_rules('code', 'Coupon Code', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('discount', 'Discount', 'required');


        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
            if ($this->coupons->add()) {
                $this->session->set_flashdata('success', 'Coupon Added Successfully.');
                redirect('coupon');
            } else {
                
            }
        }
//        echo json_encode($response);
    }

    /**
     * This method use for edit customer
     * @param int $id edit with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function edit($id) {
        $data['id'] = $id;
        $data['coupon'] = $this->data->getone($this->table, $id);
        
        $data['page_name'] = 'coupon/edit';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }

    /**
     * this function use for update edit data process
     * @author rjs
     */
    public function doedit() {
        $id = $this->input->post('id');
        $this->form_validation->set_rules('name', 'Coupon Name', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('discount', 'Discount', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger', 'Validation error');
            redirect("coupon/edit/$id", $data);
        } else {
            if ($this->coupons->edit($id)) {
                $this->session->set_flashdata('success', 'Coupon Update Successfully.');
                redirect('coupon', $data);
            }
        }
    }

    /**
     * use for delete this customer with id
     * @param int $id delete customer with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function delete($id) {
        
    }

}
