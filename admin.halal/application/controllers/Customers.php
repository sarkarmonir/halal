<?php

/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for customer maintains
 * @package customer
 * @author Jahid Al Mamun
 */
class Customers extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'customers';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('customer');
    }

    /**
     * This method display all customers with tree based 
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @package product
     * 
     * 
     */
    public function index() {
        
        $data['customers'] = $this->data->getall($this->table);
        
        $data['page_name'] = 'customer/index';

        $data['page_title'] = 'Customer Management';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new customer
     * @author Jahid al mamun
     * 
     */
    public function add() {

        $data['page_name'] = 'customer/add';

        $data['page_title'] = 'Add Customer';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new customer action process
     * @author Jahid al mamun
     * 
     */
    public function doadd() {

        $response = array();
        $response['error'] = 1;

        $this->form_validation->set_rules('f_name', 'First Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[customers.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('telephone', 'Telephone', 'required');

        if ($this->form_validation->run() == FALSE) {
            
            $data['page_name'] = 'customer/add';

            $data['page_title'] = 'Add Customer';

            $this->load->view('backend/index', $data);

        } else {

            if ($this->customer->add()) {

                $this->session->set_flashdata('success', 'Customer Added Successfully.');
                redirect('customers');

            } else {

                $this->session->set_flashdata('danger', 'Customer Not Added Successfully');
                redirect('customers');
            }
        }
    }

    /**
     * This method use for customer edit or update and view comming from product/edit
     * @author Jahid al mamun
     * @package customer
     * @return varchar product Description
     */
    public function edit($id) {

        $data['customer'] = $this->data->getone($this->table, $id);

        $data['id'] = $id;

        $data['page_name'] = 'customer/edit';

        $data['page_title'] = 'Customer Edit';

        $this->load->view('backend/index', $data);
    }

    /**
     * this function use for update edit data process
     * @author Jahid al mamun
     */
    public function doedit() {
      
        $id = $this->input->post('id');

        $this->form_validation->set_rules('name', 'First Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('telephone', 'Telephone', 'required');

        if ($this->form_validation->run() == FALSE) {
            
            $data['page_name'] = 'customer/edit';

            $data['page_title'] = 'Customer Edit';

            $this->load->view('backend/index', $data);

        } else {

            if ($this->customer->edit($id)) {

                $this->session->set_flashdata('success', 'Customer Edit Successfully.');
                redirect('customers', 'refresh');

            } else {

                $this->session->set_flashdata('danger', 'Customer Not Edit Successfully');
                redirect('customers', 'refresh');
            }
        }
    }
    

    /**
     * use for delete this customer with id
     * @param int $id delete customer with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function delete($id) {
        
    }


}