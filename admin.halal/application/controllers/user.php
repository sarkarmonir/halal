<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('adminuser');
    }

    public function index() {
        $data['admin_users'] = $this->data->getall('admin');
        
        $data['page_name'] = 'user/admin';

        $data['page_title'] = 'Admin Management';

        $this->load->view('backend/index', $data);
    }

    public function admin() {
        //First retrive all admin user
        $data['admin_users'] = $this->data->getall('admin');
        
        $data['page_name'] = 'user/admin';

        $data['page_title'] = 'Admin User';

        $this->load->view('backend/index', $data);
    }

    public function add_admin() {
        //retrive user types
        $data['user_type'] = $this->data->getall_with_status('user_types');
        
        $data['page_name'] = '/user/add_admin';

        $data['page_title'] = 'Admin User';

        $this->load->view('backend/index', $data);
    }

    public function edit_admin($id) {
        //retrive user types
        $data['user_type'] = $this->common_model->getall_with_status('user_types');
        //retrive user info
        $data['user'] = $this->common_model->getone('users', $id);
        $data['zone'] = $this->adminuser->get_delivery_zone();

        $this->load->view('edit_admin', $data);
    }

    public function add_admin_save() {
        //print_r($_POST); die();

        if ($_POST) {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['password'] = md5($this->input->post('password'));
            $data['phone'] = $this->input->post('phone');
            $data['type'] = $this->input->post('type');


            $service = array();
            $service['from'] = $this->input->post('from');
            $service['to'] = $this->input->post('to');
            $access_data = json_encode($service);
            $data['module_access'] = $access_data;


            $data['date'] = date('Y-m-d');
            //echo "<pre>"; print_r($data); die();
            if ($this->data->save('admin', $data)) {
                //now store user log
                $last = date('Y-m-d H:i:s');
                $user_log = array(
                    'user_id' => $this->session->userdata('login_user_id'),
                    'user_name' => $this->session->userdata('name'),
                    'ip' => get_user_ip(),
                    'date' => $last,
                    'action' => 'user',
                    'comment' => 'Add New Admin'
                );
                $this->data->save('user_log', $user_log);
                redirect('user');
            } else {
                redirect();
            }
        }
    }

    public function edit_admin_save() {
        //print_r($_POST); die();
        if ($_POST) {
            $id = $this->input->post('id');
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['delivery_zone'] = $this->input->post('deliver_zone');
            if (!empty($this->input->post('password'))) {
                $data['password'] = md5($this->input->post('password'));
            }

            $data['phone'] = $this->input->post('phone');
            $data['type'] = $this->input->post('type');
            $service = array();
            $service['from'] = $this->input->post('from');
            $service['to'] = $this->input->post('to');
            $access_data = json_encode($service);
            $data['module_access'] = $access_data;
            $data['date'] = date('Y-m-d');
            //echo "<pre>"; print_r($data); die();
            if ($this->common_model->update('users', $id, $data)) {
                redirect('#admin');
            } else {
                redirect();
            }
        }
    }

    public function add_user_group() {
        $user_type = array(
            'name' => $this->input->post('name')
        );
        if ($this->common_model->save('user_types', $user_type)) {
            //retrive all user types
            $user_types = $this->common_model->getall('user_types');
            //print_r($user_types);
            $html = '';
            foreach ($user_types as $type) {
                $html .= '<option value="' . $type->id . '">' . $type->name . '</option>';
            }
            echo $html;
        }
    }

}

/* End of file Projects.php */
/* Location: ./application/controllers/Projects.php */