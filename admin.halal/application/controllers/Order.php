<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for shopping cart payment process maintain
 * @package shopping cart
 * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
 */

class Order extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    
    function __construct() {
        $this->table = '';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('order_model');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        $this->output->set_header('Pragma: no-cache');

        if ($this->session->userdata('roule') != 'admin')

            redirect(base_url() . 'login', 'refresh');
    }
  

    
    /**
     * This method use for show all customer order with filter
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     */
    public function index()
    {
        $data['all_orders'] = $this->order_model->all_orders();
       
       
        $data['page_name'] = 'order/index';

        $data['page_title'] = 'Order Management';

        $this->load->view('backend/index', $data);
    }
    
    public function stock_view()
    {
        $data['parent'] = 'order';
        $data['child'] = 'order';
        if (!$this->auth->is_admin()) {
            $this->session->set_userdata('back_url','order');
            redirect('admin');
        }
        $data['all_orders'] = $this->order_model->all_orders_stock();
       
        $data['content'] = "order/stock_view";
        $this->load->view('admin_template', $data);
    }
    
    
    /**
     * 
     * @param type $id
     * This method use for show single order view with order item
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     */
    public function order_view($id)
    {
        $this->load->model('customer');
        $data['order_details'] = $this->data->getone('orders',$id);
        $data['order_items'] = $this->customer->order_items($id);
        $shipping_id = $data['order_details'][0]->shipping_address;
        $customer_id = $data['order_details'][0]->customer_id;
        $data['customer_details'] = $this->data->getone('customers',$customer_id);
        
        
        
        if($shipping_id == 0)
        {
         $data['shipping_address'] = '';  
        }else
        {
        $data['shipping_address'] = $this->customer->shipping_address($shipping_id);
        }
        $this->session->set_userdata('back_url','');
        
        
        $data['page_name'] = 'order/details';

        $data['page_title'] = 'Order Management';

        $this->load->view('backend/index', $data);
    }
  
}
