<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



/*  

 *  @author : Mahabubur Rahman

 *  date    : 1 August, 2016

 *  http://mahabub.me

 *  http://nextdot.com.au

 */



class Category extends CI_Controller {



    function __construct() {

        parent::__construct();

        $this->load->database();



        /* cache control */

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        $this->output->set_header('Pragma: no-cache');

        if ($this->session->userdata('roule') != 'admin')

            redirect(base_url() . 'login', 'refresh');

        

    }



    /*     * *default function, redirects to login page if no admin logged in yet** */



    public function index() {
        $page_data['categories'] = $this->crud_model->get_caretories();
        $page_data['page_name'] = 'category_manage';

        $page_data['page_title'] = get_phrase('manage_category');

        $this->load->view('backend/index', $page_data);
    }

    public function create()
    {
        if($this->input->post()){
            if($this->crud_model->create_category()){
                $this->session->set_flashdata('flash_message', get_phrase('cateogry_create_successfully'));
                redirect('category','refresh');
            }
        }

        
        $page_data['categories'] = $this->crud_model->get_active_caretories();
        $page_data['page_name'] = 'category_create';

        $page_data['page_title'] = get_phrase('admin_dashboard');

        $this->load->view('backend/index', $page_data);
    }

    public function delete($id='')
    {
        if($id){
            if($this->crud_model->category_delete_by_id($id)){
                $this->session->set_flashdata('flash_message', get_phrase('cateogry_delete_successfully'));
                redirect('category','refresh');
            }
        }
    }



}