<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for customer group maintains
 * @package customer
 * @author Jahid Al Mamun
 */
class Area extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'area';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('areas');
    }

    /**
     * This method display all area  with tree based 
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @package customer
     * 
     * 
     */
    public function index() {
        
        $data['area'] = $this->data->getall($this->table);
        
        $data['page_name'] = 'area/index';

        $data['page_title'] = 'Area Management';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new area
     * @author Jahid al mamun
     * 
     */
    public function add() {
        $data['page_name'] = 'area/add';

        $data['page_title'] = 'Area';

        $this->load->view('backend/index', $data);
    }
/**
     * this method use for add new area action process
     * @author Jahid al mamun
     * 
     */
    public function doadd() {
        $this->form_validation->set_rules('delivery_cost', 'Delivery Cost', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            redirect('area/add',$data);
        } else {
             
                if ($this->areas->add()) {
                    $this->session->set_flashdata('success','area Added Successfully.');
                    redirect('area');
                    
                }else
                {
                    
                }
            
        }
//        echo json_encode($response);
    }

    /**
     * This method use for edit area
     * @param int $id edit with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function edit($id) {
        
      
        $data['id'] = $id;
        $data['area'] = $this->data->getone($this->table,$id);
        $data['page_name'] = 'area/edit';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
       
    }

    /**
     * this function use for update edit data process
     * @author rjs
     */
    public function doedit()
    {
        $id = $this->input->post('id');
        
        
        $this->form_validation->set_rules('postcode', 'Postcode', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('delivery_time', 'Delivery Time', 'required');
        $this->form_validation->set_rules('delivery_cost', 'Delivery Cost', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            redirect("area/edit/$id",$data);
        } else {
                if ($this->areas->edit($id)) {
                    $this->session->set_flashdata('success','area update Successfully.');
                    redirect('area',$data);
                }
        }
    }
    /**
     * use for delete this area with id
     * @param int $id delete area with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function delete($id) {
        
    }

}
