<?php

/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for product discount maintains
 * @package product
 * @author Jahid Al Mamun
 */
class Discount extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'discount';
        $this->table1 = 'discount_product';
        $this->table2 = 'discount_category';
        $this->table3 = 'product';
        $this->table4 = 'category';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('discounts');
    }

    /**
     * This method display all coupon 
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @package coupon
     * 
     * 
     */
    public function index() {
        $data['discount'] = $this->data->getall($this->table);

        $data['page_name'] = 'discounts/index';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new coupon
     * @author Jahid al mamun
     * 
     */
    public function add() {
        $this->load->model('category');
        $data['category'] = $this->category->all_by_option();
        $data['products'] = $this->data->getall($this->table3);

        $data['page_name'] = 'discounts/add';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for add new discount action process
     * @author Jahid al mamun
     * 
     */
    public function doadd() {
        $response = array();
        $response['error'] = 1;
        $this->form_validation->set_rules('name', 'Discount Name', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('discount', 'Discount', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger', 'Validation error');


            $this->load->model('category');
            $data['category'] = $this->category->all_by_option();
            $data['products'] = $this->data->getall($this->table3);
            $data['page_name'] = 'discounts/add';

            $data['page_title'] = 'Coupon';

            $this->load->view('backend/index', $data);
        } else {
            if ($this->input->post('end_date') < $this->input->post('start_date')) {

                $this->session->set_flashdata('danger', 'Start Date and End Date is not valid');

                $this->load->model('category');
                $data['category'] = $this->category->all_by_option();
                $data['products'] = $this->data->getall($this->table3);
                $data['page_name'] = 'discounts/add';

                $data['page_title'] = 'Coupon';

                $this->load->view('backend/index', $data);
            } else {

                if ($this->discounts->add()) {
                    $this->session->set_flashdata('success', 'Discount Added Successfully.');
                    redirect('discount');
                } else {

                    $this->load->model('category');
                    $data['category'] = $this->category->all_by_option();
                    $data['products'] = $this->data->getall($this->table3);
                    $data['page_name'] = 'discounts/add';

                    $data['page_title'] = 'Coupon';

                    $this->load->view('backend/index', $data);
                }
            }
        }
    }

    /**
     * This method use for edit customer
     * @param int $id edit with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function edit($id) {
        $data['id'] = $id;
        $data['discount'] = $this->data->getone($this->table, $id);
        $data['products'] = $this->data->getall($this->table3);
        $data['categories'] = $this->data->getall($this->table4);
        $data['discount_product'] = $this->discounts->product($id);
        //$data['discount_category'] = $this->discounts->category($id);
        //print_r($data['discount_product']); die();
        $data['page_name'] = 'discounts/edit';

        $data['page_title'] = 'Coupon';

        $this->load->view('backend/index', $data);
    }

    /**
     * this function use for update edit data process
     * @author rjs
     */
    public function doedit() {
        $this->form_validation->set_rules('name', 'Discount Name', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('discount', 'Discount', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger', 'Validation error');
            redirect("discount/edit/$id", $data);
        } else {
            $id = $this->input->post('id');
            if ($this->discounts->edit($id)) {
                $this->session->set_flashdata('success', 'Discount Update Successfully.');
                redirect('discount', $data);
            }
        }
    }

    /**
     * use for delete this Discount with id
     * @param int $id delete Discount with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function delete($id) {
        
    }

}
