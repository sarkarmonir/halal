<?php

if (!defined('BASEPATH'))

    exit('No direct script access allowed');

class Admin extends CI_Controller {


    function __construct() {

        parent::__construct();

        $this->load->database();



        /* cache control */

        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

        $this->output->set_header('Pragma: no-cache');

        if ($this->session->userdata('roule') != 'admin')

            redirect(base_url() . 'login', 'refresh');

        

    }



    /*     * *default function, redirects to login page if no admin logged in yet** */



    public function index() {
            redirect(base_url() . 'admin/dashboard', 'refresh');
    }



    /*     * *ADMIN DASHBOARD** */



    function dashboard() {

        $page_data['page_name'] = 'dashboard';

        $page_data['page_title'] = get_phrase('admin_dashboard');

        $this->load->view('backend/index', $page_data);

    }



    /*     * ***LANGUAGE SETTINGS******** */



    function manage_language($param1 = '', $param2 = '', $param3 = '') {


        if ($param1 == 'edit_phrase') {

            $page_data['edit_profile'] = $param2;

        }

        if ($param1 == 'update_phrase') {

            $language = $param2;

            $total_phrase = $this->input->post('total_phrase');

            for ($i = 1; $i < $total_phrase; $i++) {

                //$data[$language]  =   $this->input->post('phrase').$i;

                $this->db->where('phrase_id', $i);

                $this->db->update('language', array($language => $this->input->post('phrase' . $i)));

            }

            redirect(base_url() . 'admin/manage_language/edit_phrase/' . $language, 'refresh');

        }

        if ($param1 == 'do_update') {

            $language = $this->input->post('language');

            $data[$language] = $this->input->post('phrase');

            $this->db->where('phrase_id', $param2);

            $this->db->update('language', $data);

            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'admin/manage_language/', 'refresh');

        }

        if ($param1 == 'add_phrase') {

            $data['phrase'] = $this->input->post('phrase');

            $this->db->insert('language', $data);

            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'admin/manage_language/', 'refresh');

        }

        if ($param1 == 'add_language') {

            $language = $this->input->post('language');

            $this->load->dbforge();

            $fields = array(

                $language => array(

                    'type' => 'LONGTEXT'

                )

            );

            $this->dbforge->add_column('language', $fields);



            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'admin/manage_language/', 'refresh');

        }

        if ($param1 == 'delete_language') {

            $language = $param2;

            $this->load->dbforge();

            $this->dbforge->drop_column('language', $language);

            $this->session->set_flashdata('message', get_phrase('settings_updated'));



            redirect(base_url() . 'admin/manage_language/', 'refresh');

        }

        $page_data['page_name'] = 'manage_language';

        $page_data['page_title'] = get_phrase('manage_language');

        //$page_data['language_phrases'] = $this->db->get('language')->result_array();

        $this->load->view('backend/index', $page_data);

    }



    /*     * ***SITE/SYSTEM SETTINGS******** */



    function system_settings($param1 = '', $param2 = '', $param3 = '') {

        if ($param1 == 'do_update') {

            $this->crud_model->update_system_settings();
            
            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'admin/system_settings/', 'refresh');

        }

        if ($param1 == 'upload_logo') {

            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');

            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'admin/system_settings/', 'refresh');

        }

        $page_data['page_name'] = 'system_settings';

        $page_data['page_title'] = get_phrase('system_settings');

        $page_data['settings'] = $this->db->get('settings')->result_array();

        $this->load->view('backend/index', $page_data);

    }



    // SMS settings.

    function sms_settings($param1 = '') {



        if ($param1 == 'do_update') {

            $this->crud_model->update_sms_settings();

            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'admin/sms_settings/', 'refresh');

        }



        $page_data['page_name'] = 'sms_settings';

        $page_data['page_title'] = get_phrase('sms_settings');

        $this->load->view('backend/index', $page_data);

    }



    /*     * ****MANAGE OWN PROFILE AND CHANGE PASSWORD** */



    function manage_profile($param1 = '', $param2 = '', $param3 = '') {


        if ($param1 == 'update_profile_info') {

            $data['name'] = $this->input->post('name');

            $data['email'] = $this->input->post('email');



            $this->db->where('id', $this->session->userdata('login_user_id'));

            $this->db->update('admin', $data);



            $this->session->set_flashdata('message', get_phrase('profile_info_updated_successfuly'));

            redirect('admin/manage_profile');

        }

        if ($param1 == 'change_password') {

            $current_password_input = sha1($this->input->post('password'));

            $new_password = sha1($this->input->post('new_password'));

            $confirm_new_password = sha1($this->input->post('confirm_new_password'));



            $current_password_db = $this->db->get_where('admin', array('id' =>

                        $this->session->userdata('login_user_id')))->row()->password;



            if ($current_password_db == $current_password_input && $new_password == $confirm_new_password) {

                $this->db->where('id', $this->session->userdata('login_user_id'));

                $this->db->update('admin', array('password' => $new_password));



                $this->session->set_flashdata('message', get_phrase('password_info_updated_successfuly'));

                redirect('admin/manage_profile');

            } else {

                $this->session->set_flashdata('message', get_phrase('password_update_failed'));

                redirect('admin/manage_profile');

            }

        }

        $page_data['page_name'] = 'manage_profile';

        $page_data['page_title'] = get_phrase('manage_profile');

        $page_data['edit_data'] = $this->db->get_where('admin', array('id' => $this->session->userdata('login_user_id')))->result_array();

        $this->load->view('backend/index', $page_data);

    }



    function department($task = "", $department_id = "") {


        if ($task == "create") {

            $this->crud_model->save_department_info();

            $this->session->set_flashdata('message', get_phrase('department_info_saved_successfuly'));

            redirect('admin/department');

        }



        if ($task == "update") {

            $this->crud_model->update_department_info($department_id);

            $this->session->set_flashdata('message', get_phrase('department_info_updated_successfuly'));

            redirect('admin/department');

        }



        if ($task == "delete") {

            $this->crud_model->delete_department_info($department_id);

            redirect('admin/department');

        }



        $data['department_info'] = $this->crud_model->select_department_info();

        $data['page_name'] = 'manage_department';

        $data['page_title'] = get_phrase('department');

        $this->load->view('backend/index', $data);

    }



    function payment_history($task = "") {


        $data['invoice_info'] = $this->crud_model->select_invoice_info();

        $data['page_name'] = 'show_payment_history';

        $data['page_title'] = get_phrase('payment_history');

        $this->load->view('backend/index', $data);

    }

    function notice($task = "", $notice_id = "") {

        if ($this->session->userdata('admins_login') != 1) {

            $this->session->set_userdata('last_page', current_url());

            redirect(base_url(), 'refresh');

        }



        if ($task == "create") {

            $this->crud_model->save_notice_info();

            $this->session->set_flashdata('message', get_phrase('notice_info_saved_successfuly'));

            redirect('admin/notice');

        }



        if ($task == "update") {

            $this->crud_model->update_notice_info($notice_id);

            $this->session->set_flashdata('message', get_phrase('notice_info_updated_successfuly'));

            redirect('admin/notice');

        }



        if ($task == "delete") {

            $this->crud_model->delete_notice_info($notice_id);

            redirect('admin/notice');

        }



        $data['notice_info'] = $this->crud_model->select_notice_info();

        $data['page_name'] = 'manage_notice';

        $data['page_title'] = get_phrase('noticeboard');

        $this->load->view('backend/index', $data);

    }




    public function table_options($param1 = '', $param2 = '', $param3 = '')

    {

        $data['param1']=$param1;

        if($this->input->post('add_item')){

            if($param1=='treatment_record'){
                $this->crud_model->add_new_option_item_tr($param1); 
            }else{
                $this->crud_model->add_new_option_item($param1);                
            }

        }

        if($param2=='delete'){
            if($param1=='treatment_record'){
                // $table_option_details=$this->db->get_where('options', array('name' => $param1))->row_array();
                // $table_option_details=json_decode($table_option_details['details'],True);
                // var_dump(FindElementWithValue($table_option_details,'name',$param3));
                // exit;
                $this->crud_model->delete_option_item_tr($param1,$param3);
            }else{
                $this->crud_model->delete_option_item($param1,$param3);
            }

            // exit;

            redirect('admin/table_options/'.$param1);

        }

        $data['table_option_list']=$this->db->get_where('options', array('name' => $param1))->row_array();

        $data['table_options']   = $this->crud_model->select_option_table_name();

        $data['page_name']          = 'manage_options';

        $data['page_title']         = get_phrase('options');

        

        // var_dump($data);

        $this->load->view('backend/index', $data);

    }



    public function expense($page='',$type='',$action='',$id='')
    {
        $data['type']=$type;
        $data['page']=$page;
        $data['action']=$action;

        $data['id']=$id;
        if($action=='delete'){
            // echo $action;
            // exit;
            // $this->db->query("DELETE FROM lab_expense WHERE patient_id= ".$id);
            if($this->db->delete($page, array('id' => $id))){
                $this->session->set_flashdata('message' , get_phrase('lab_expense_delete_successfuly'));
            }else{
                $this->session->set_flashdata('message' , get_phrase('unable_to_delete_lab_expense'));
            }

            redirect('admin/expense/'.$page.'/'.$type);
        }
        // if($action=='edit'){
        //     // $this->db->query("DELETE FROM lab_expense WHERE patient_id= ".$id);
        //     $data = array(
        //         'title' => $title,
        //         'name' => $name,
        //         'date' => $date
        // );
        //  $this->db->update('lab_expense', $data, array('id' => $id));

        //     redirect('admin/expense/lab_expense/'.$type);
        // }
        if($page=='lab_expense'){
            $data['page_name']          = 'lab_report';
            $data['page_title']         = get_phrase('lab_expense');
            if($this->input->post()){
                // var_dump($this->input->post());

                // exit;
                if($this->crud_model->add_lab_expense()){
                    $this->session->set_flashdata('message' , get_phrase('lab_expense_saved_successfuly'));
                }
                redirect('admin/expense/lab_expense');
            }
        }elseif($page=='material_expense'){


                   if($type=='edit'){
                if($this->input->post()){
                    $update_data=$this->input->post();
                    $update_data['date']=date('Y-m-d',strtotime($update_data['date']));
                    if($this->db->update('material_expense',$update_data,array('id'=>$action))){
                        $this->session->set_flashdata('message' , get_phrase('material_expense_update_successfuly'));
                    }else{
                        $this->session->set_flashdata('message' , get_phrase(' unable_to_update_material_expense'));
                    }
                    redirect('admin/expense/material_expense');
                }
                $data['single_material_expense']=$this->db->get_where('material_expense',array('id'=>$action))->row();
                $data['page_name']          = 'edit_material_expense';
                $data['page_title']         = get_phrase('edit_material_expense');
            }else{
                if($this->input->post()){
                    if($this->crud_model->add_material_expense()){
                        $this->session->set_flashdata('message' , get_phrase('material_expense_saved_successfuly'));
                    }
                    redirect('admin/expense/material_expense');
                }
                $data['page_name']          = 'material_expense';
                $data['page_title']         = get_phrase('material_expense');                
            }


            // if($this->input->post()){
            //     if($this->crud_model->add_material_expense()){
            //         $this->session->set_flashdata('message' , get_phrase('material_expense_saved_successfuly'));
            //     }
            //     redirect('admin/expense/material_expense');
            // }
            // $data['page_name']          = 'material_expense';
            // $data['page_title']         = get_phrase('material_expense');


        }elseif($page=='utility_expense'){ 
            if($type=='edit'){
                if($this->input->post()){
                    $update_data=$this->input->post();
                    $update_data['date']=date('Y-m-d',strtotime($update_data['date']));
                    if($this->db->update('utility_expense',$update_data,array('id'=>$action))){
                        $this->session->set_flashdata('message' , get_phrase('utility_expense_update_successfuly'));
                    }else{
                        $this->session->set_flashdata('message' , get_phrase(' unable_to_update_utility_expense'));
                    }
                    redirect('admin/expense/utility_expense');
                }
                $data['single_expense']=$this->db->get_where('utility_expense',array('id'=>$action))->row();
                $data['page_name']          = 'edit_utility_expense';
                $data['page_title']         = get_phrase('edit_utility_expense');
            }else{
                if($this->input->post()){
                    if($this->crud_model->add_utility_expense()){
                        $this->session->set_flashdata('message' , get_phrase('utility_expense_saved_successfuly'));
                    }
                    redirect('admin/expense/utility_expense');
                }
                $data['page_name']          = 'utility_expense';
                $data['page_title']         = get_phrase('utility_expense');                
            }


        }else{
            $data['page_name']          = 'expense';
            $data['page_title']         = get_phrase('expense'); 
        }
               
        
        $this->load->view('backend/index', $data);
    }

    public function income($type='')
    {
        $data['type']=$type;
        $data['page_name']='income';
        $data['page_title']='income';
        $this->load->view('backend/index', $data);
    }

    public function due($type='')
    {
         $data['type']=$type;
        $data['page_name']='due';
        $data['page_title']='due';
        $this->load->view('backend/index', $data);
    }




    public function invoice($type='',$id='')
    {
        if($type=='update'){
            $update_data=$this->input->post();
            $invoice_details=array();
            foreach ($update_data['date'] as $key => $date) {
                $invoice_details[]=array(
                    'description'=>$update_data['name'][$key],
                    'amount'=>$update_data['amount'][$key],
                    'date'=>$update_data['date'][$key]
                    );
            }

            // $invoice_info=$this->db->get_where('invoice',array('invoice_id'=>$id))->row();
            if($this->db->update('invoice',array('invoice_details'=>json_encode($invoice_details)),array('invoice_id'=>$id))){
                foreach ($invoice_details as $key => $value) {
                    $this->db->update('income',array('income_type'=>$value['description'],'amount'=>$value['amount']),array('invoice_id'=>$id));
                }
                // exit;
                $referred_from = $this->session->userdata('referred_from');
                redirect($referred_from, 'refresh');
            }
        }

        if($type=='delete'){
            if($this->db->delete('invoice', array('invoice_id' => $id))){
                if($this->db->delete('income',array('invoice_id'=>$id))){
                    $referred_from = $this->session->userdata('referred_from');
                    redirect($referred_from, 'refresh');                    
                }
            }
        }
    }




    
    public function image_upload($image_upload_path=null)
    {	
    	$config['upload_path']          = './uploads/';
    	if($image_upload_path){
        	$config['upload_path']          = './uploads/'.$image_upload_path.'/';
        }
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5120;
        // $config['max_width']            = 2048;
        // $config['max_height']           = 1536;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
                $error = array('error' => $this->upload->display_errors());

                // $this->load->view('upload_form', $error);
        }
        else
        {
                // $data = array('upload_data' => $this->upload->data());
                $data = $this->upload->data();

                // $this->load->view('upload_success', $data);
                $data['image_upload_path']=$config['upload_path'];
                echo json_encode($data);
        }
    }

    public function category(){
        
    }

}