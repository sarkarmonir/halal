<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Web_settings extends CI_Controller {

	function __construct() {
		parent::__construct();
		// $this->load->database(); 
		$this->load->model("web_settings_model");
        // cache control
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if ($this->session->userdata('roule') != 'admin')
			redirect(base_url() . 'login', 'refresh');
	}

	// default action, redirects to login page if no admin logged in yet
	public function index() {
        $page_data['items'] = $this->crud_model->get_items();
        $page_data['page_name'] = 'item_manage';
		$page_data['page_title'] = get_phrase('manage_items');
		$this->load->view('backend/index', $page_data);
    }

// ##############################################################################################
//  General settings section
// ##############################################################################################

	// general settings function -----------------------------------------------
	public function general_settings() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['wlc_text'] = $this->input->post("wlc_text", true);
            $data['order_no'] = $this->input->post("order_no", true);
            $data['opera_desc'] = $this->input->post("opera_desc", true);
            $data['fb'] = $this->input->post("fb", true);
            $data['twt'] = $this->input->post("twt", true);
            $data['y_tube'] = $this->input->post("y_tube", true);
            $data['g_plus'] = $this->input->post("g_plus", true);
            $data['more_link'] = $this->input->post("more_link", true);
           
            // logo img uploading ----------------------------------------------
            if(!empty($_FILES['logo_img']['name'])){
                // delete file -------------------------------------------------
                $logo_img = $this->input->post("logo_img", true);
                unlink(FCPATH.'/uploads/logo/' . $logo_img);
                
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/logo/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = "logo";
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('logo_img')){
                    // get file extension with name ----------------------------
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $divide = explode('.', $file_name);
                    $ext = end($divide);
                    $data['logo_img'] = 'logo'.'.'.$ext;
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Image uploading error!'));
                }
            }
			// post data save --------------------------------------------------
            $this->web_settings_model->update_general_settings($data);
            $this->session->set_flashdata('succ_msg', get_phrase('update_general_settings_successfully'));
            redirect('web_settings/general_settings');
        }else{

            // default ---------------------------------------------------------   
            $page_data['gen_settings'] = $this->web_settings_model->get_general_settings();
            $page_data['page_name'] = 'web_settings/general_settings';
			$page_data['page_title'] = get_phrase('General Settings');
			$this->load->view('backend/index', $page_data);
        }
	}

// ##############################################################################################
//  Slider section
// ##############################################################################################

	// slider function ---------------------------------------------------------
    public function slider() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['cap_1'] = $this->input->post("cap_1", true);
            $data['cap_2'] = $this->input->post("cap_2", true);
            // slider image uploading ------------------------------------------
            if(empty($_FILES['slider_img']['name'])){
                // error on empty
               die("image file not found!");
            }else{
                // slider image upload -----------------------------------------
                $img_name = str_replace(' ', '_', $data['cap_1']);
                $avatar = strtolower($img_name);
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/slider/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('slider_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // post data save ------------------------------------------
                    $this->web_settings_model->add_slider($data);
                    $this->session->set_flashdata('succ_msg', get_phrase('add_slider_successfully'));
                    redirect('web_settings/slider');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            // default ---------------------------------------------------------   
            $page_data['lists'] = $this->web_settings_model->get_slider();
            $page_data['page_name'] = 'web_settings/slider';
            $page_data['page_title'] = get_phrase('Slider');
            $this->load->view('backend/index', $page_data);
        }
    }


    // update slider -----------------------------------------------------------
    public function update_slider() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $data = array();
            $data['cap_1'] = $this->input->post("cap_1", true);
            $data['cap_2'] = $this->input->post("cap_2", true);

            $old_img = $this->input->post("old_img", true);
            $id = $this->input->post("id", true);

            // renaming --------------------------------------------------------
            $divide = explode('.', $old_img);
            $ext = end($divide);
            $first_part = str_replace(' ', '_', $data['cap_1']);
            $first_part = strtolower($first_part);
            $data['img'] = $first_part.'.'.$ext;
            rename("./uploads/slider/" . $old_img, "./uploads/slider/" . $data['img']);
            
            // save and redirecting --------------------------------------------
            $this->web_settings_model->update_slider($data, $id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_slider_successfully'));
            redirect('web_settings/slider');
        } else {
            redirect('web_settings/slider');
        }
    }

    // replace slider image ----------------------------------------------------
    public function rep_slider_image() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);

            // get file name & delete ------------------------------------------
            unlink(FCPATH.'/uploads/slider/' . $img);

            // name process ----------------------------------------------------
            $divide = explode('.', $img);
            $img_name = current($divide);
            
            // replacing image -------------------------------------------------
            if(empty($_FILES['rep_slider_img']['name'])){
                die("image file not found!");
            }else{
                $avatar = strtolower($img_name);
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/slider/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('rep_slider_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // save & redirecting --------------------------------------
                    $this->web_settings_model->rep_slider_image($data, $id);
                    $this->session->set_flashdata('succ_msg', get_phrase('update_slider_image_successfully'));
                    redirect('web_settings/slider');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            redirect('web_settings/slider');
        }
    }

    // slider Delete -----------------------------------------------------------
    public function slider_delete() {
        if ($_POST) {
            // get file name ---------------------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);
            // delete from dir -------------------------------------------------
            unlink(FCPATH.'/uploads/slider/' . $img);
            // delete from db and redirecting ----------------------------------
            $this->web_settings_model->slider_delete($id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_slider_image_successfully'));
            redirect('web_settings/slider');
        } else {
            redirect('web_settings/slider');
        }
    }


// ##############################################################################################
//  About us section
// ##############################################################################################

    // about us function -------------------------------------------------------
    public function about_us() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            $data['desc'] = $this->input->post("desc", true);
            // about us img uploading ------------------------------------------
            if(!empty($_FILES['about_us_img']['name'])){
                
                // delete file -------------------------------------------------
                $img = $this->input->post("img", true);
                unlink(FCPATH.'/uploads/about_us/' . $img);

                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/about_us/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = "about_us";
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('about_us_img')){
                    // get file extension with name ----------------------------
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $divide = explode('.', $file_name);
                    $ext = end($divide);
                    $data['img'] = 'about_us'.'.'.$ext;
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
            // post data save --------------------------------------------------
            $this->web_settings_model->update_about_us($data);
            $this->session->set_flashdata('succ_msg', get_phrase('update_about_us_successfully'));
            redirect('web_settings/about_us');
        }else{
            // default ---------------------------------------------------------   
            $page_data['get_info'] = $this->web_settings_model->get_about_us();
            $page_data['page_name'] = 'web_settings/about_us';
            $page_data['page_title'] = get_phrase('About us');
            $this->load->view('backend/index', $page_data);
        }
    }
   

// ##############################################################################################
//  faq section
// ##############################################################################################  

    
    // faq function ---------------------------------------------------------
    public function faq() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['ques'] = $this->input->post("ques", true);
            $data['answ'] = $this->input->post("answ", true);
            // post data save & redirecting ------------------------------------
            $this->web_settings_model->add_faq($data);
            $this->session->set_flashdata('succ_msg', get_phrase('add_faq_successfully'));
            redirect('web_settings/faq');
        }else{
            // default ---------------------------------------------------------   
            $page_data['lists'] = $this->web_settings_model->get_faq();
            $page_data['page_name'] = 'web_settings/faq';
            $page_data['page_title'] = get_phrase('Faq');
            $this->load->view('backend/index', $page_data);
        }
    }

    // update faq --------------------------------------------------------------
    public function update_faq() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $data = array();
            $data['ques'] = $this->input->post("ques", true);
            $data['answ'] = $this->input->post("answ", true);
            $id = $this->input->post("id", true);   
            // update & redirecting --------------------------------------------
            $this->web_settings_model->update_faq($data, $id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_faq_successfully'));
            redirect('web_settings/faq');
        } else {
            redirect('web_settings/faq');
        }
    }

    // faq Delete --------------------------------------------------------------
    public function faq_delete() {
        if ($_POST) {
            // delete & redirecting --------------------------------------------
            $id = $this->input->post("id", true);
            $this->web_settings_model->faq_delete($id);
            $this->session->set_flashdata('succ_msg', get_phrase('delete_faq_successfully'));
            redirect('web_settings/faq');
        } else {
            redirect('web_settings/faq');
        }
    }

// ##############################################################################################
//  our farm section
// ##############################################################################################

    // our_farm function ---------------------------------------------------------
    public function our_farm() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            $data['desc'] = $this->input->post("desc", true);
            // slider image uploading ------------------------------------------
            if(empty($_FILES['farm_img']['name'])){
                // error on empty
               die("image file not found!");
            }else{
                // slider image upload -----------------------------------------
                $avatar = strtolower($data['title']);
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/farm/';
                $config['file_ext_tolower'] = TRUE;
                $config['overwrite']       = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('farm_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // post data save ------------------------------------------
                    $this->web_settings_model->add_farm($data);
                    $this->session->set_flashdata('succ_msg', get_phrase('add_farm_successfully'));
                    redirect('web_settings/our_farm');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            // default ---------------------------------------------------------   
            $page_data['lists'] = $this->web_settings_model->get_farm();
            $page_data['page_name'] = 'web_settings/our_farm';
            $page_data['page_title'] = get_phrase('Our farm');
            $this->load->view('backend/index', $page_data);
        }
    }


    // update slider -----------------------------------------------------------
    public function update_our_farm() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            $data['desc'] = $this->input->post("desc", true);
            
            $old_img = $this->input->post("old_img", true); 
            $id = $this->input->post("id", true);

            // renaming --------------------------------------------------------
            $divide = explode('.', $old_img);
            $ext = end($divide);
            $first_part = str_replace(' ', '_', $data['title']);
            $first_part = strtolower($first_part);
            $data['img'] = $first_part.'.'.$ext;
            rename("./uploads/farm/" . $old_img, "./uploads/farm/" . $data['img']);

            // save and redirecting --------------------------------------------
            $this->web_settings_model->update_farm($data, $id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_farm_successfully'));
            redirect('web_settings/our_farm');
        } else {
            redirect('web_settings/our_farm');
        }
    }

    // replace farm image ------------------------------------------------------
    public function rep_our_farm_image() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);

            // file delete -----------------------------------------------------
            unlink(FCPATH.'/uploads/farm/' . $img);

            // name process ----------------------------------------------------
            $divide = explode('.', $img);
            $avatar = current($divide);

            // replacing image -------------------------------------------------
            if(empty($_FILES['rep_farm_img']['name'])){
                die("image file not found!");
            }else{
                $avatar = strtolower($avatar);
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/farm/';
                $config['file_ext_tolower'] = TRUE;
                $config['overwrite']       = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('rep_farm_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // save & redirecting --------------------------------------
                    $this->web_settings_model->rep_farm_image($data, $id);
                    $this->session->set_flashdata('succ_msg', get_phrase('update_farm_image_successfully'));
                    redirect('web_settings/our_farm');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            redirect('web_settings/our_farm');
        }
    }

    // Delete ------------------------------------------------------------------
    public function our_farm_delete() {
        if ($_POST) {
            // get file name ---------------------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);
            // file delete from directory --------------------------------------
            unlink(FCPATH.'/uploads/farm/' . $img);
            // delete from db and redirecting ----------------------------------
            $this->web_settings_model->farm_delete($id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_farm_image_successfully'));
            redirect('web_settings/our_farm');
        } else {
            redirect('web_settings/our_farm');
        }
    }

// ##############################################################################################
// farm gallery section
// ##############################################################################################

    // farm gallery function ---------------------------------------------------------
    public function farm_gallery() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            // image uploading ------------------------------------------
            if(empty($_FILES['farm_img']['name'])){
                // error on empty
               die("image file not found!");
            }else{
                // image upload ------------------------------------------------
                $avatar = strtolower($data['title']);
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/farm_gallery/';
                $config['file_ext_tolower'] = TRUE;
                $config['overwrite']       = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('farm_img')){
                    $up_data = array('upload_data' => $this->upload->data());
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // post data save ------------------------------------------
                    $this->web_settings_model->add_farm_gallery($data);
                    $this->session->set_flashdata('succ_msg', get_phrase('add_farm_gallery_successfully'));
                    redirect('web_settings/farm_gallery');
                }else{
                    // show error on not upload
                }
            }
        }else{
            // default ---------------------------------------------------------   
            $page_data['lists'] = $this->web_settings_model->get_farm_gallery();
            $page_data['page_name'] = 'web_settings/farm_gallery';
            $page_data['page_title'] = get_phrase('Farm_gallery');
            $this->load->view('backend/index', $page_data);
        }
    }


    // update ------------------------------------------------------------------
    public function update_farm_gallery() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            
            $old_img = $this->input->post("old_img", true); 
            $id = $this->input->post("id", true);

            // renaming --------------------------------------------------------
            $divide = explode('.', $old_img);
            $ext = end($divide);
            $first_part = str_replace(' ', '_', $data['title']);
            $first_part = strtolower($first_part);
            $data['img'] = $first_part.'.'.$ext;
            rename("./uploads/farm_gallery/" . $old_img, "./uploads/farm_gallery/" . $data['img']);
            
            // save and redirecting --------------------------------------------
            $this->web_settings_model->update_farm_gallery($data, $id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_farm_gallery_successfully'));
            redirect('web_settings/farm_gallery');
        } else {
            redirect('web_settings/farm_gallery');
        }
    }

    // replace farm image ------------------------------------------------------
    public function rep_farm_gallery_image() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);

            // file delete ------------------------------------------
            unlink(FCPATH.'/uploads/farm_gallery/' . $img);

            // name process ----------------------------------------------------
            $divide = explode('.', $img);
            $avatar = current($divide);
            
            // replacing image -------------------------------------------------
            if(empty($_FILES['rep_farm_img']['name'])){
                die("image file not found!");
            }else{
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/farm_gallery/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('rep_farm_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // save & redirecting --------------------------------------
                    $this->web_settings_model->rep_farm_gallery_image($data, $id);
                    $this->session->set_flashdata('succ_msg', get_phrase('update_farm_gallery_image_successfully'));
                    redirect('web_settings/farm_gallery');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            redirect('web_settings/farm_gallery');
        }
    }

    // Delete ------------------------------------------------------------------
    public function our_farm_gallery_delete() {
        if ($_POST) {
            // get file name ---------------------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);
            // file delete from directory --------------------------------------
            unlink(FCPATH.'/uploads/farm_gallery/' . $img);
            // delete from db and redirecting ----------------------------------
            $this->web_settings_model->farm_gallery_delete($id);
            $this->session->set_flashdata('succ_msg', get_phrase('delete_farm_gallery_image_successfully'));
            redirect('web_settings/farm_gallery');
        } else {
            redirect('web_settings/farm_gallery');
        }
    }


// ##############################################################################################
//  Testimonial section
// ##############################################################################################

    // testimonial function ---------------------------------------------------------
    public function testimonial() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['cli_name'] = $this->input->post("cli_name", true);
            $data['cli_comment'] = $this->input->post("cli_comment", true);
            // testimonial image uploading ------------------------------------------
            if(empty($_FILES['cli_img']['name'])){
                // error on empty
               die("image file not found!");
            }else{
                // slider image upload -----------------------------------------
                $img_name = str_replace(' ', '_', $data['cli_name']);
                $avatar = strtolower($img_name);
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/testimonial/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('cli_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // post data save ------------------------------------------
                    $this->web_settings_model->add_testimonial($data);
                    $this->session->set_flashdata('succ_msg', get_phrase('add_testimonial_successfully'));
                    redirect('web_settings/testimonial');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            // default ---------------------------------------------------------   
            $page_data['lists'] = $this->web_settings_model->get_testimonial();
            $page_data['page_name'] = 'web_settings/testimonial';
            $page_data['page_title'] = get_phrase('Testimonial');
            $this->load->view('backend/index', $page_data);
        }
    }


    // update testimonial -----------------------------------------------------------
    public function update_testimonial() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $data = array();
            $data['cli_name'] = $this->input->post("cli_name", true);
            $data['cli_comment'] = $this->input->post("cli_comment", true);

            $old_img = $this->input->post("old_img", true); 
            $id = $this->input->post("id", true);

            // renaming --------------------------------------------------------
            $divide = explode('.', $old_img);
            $ext = end($divide);
            $first_part = str_replace(' ', '_', $data['cli_name']);
            $first_part = strtolower($first_part);

            $data['img'] = $first_part.'.'.$ext;
            rename("./uploads/testimonial/" . $old_img, "./uploads/testimonial/" . $data['img']);
            
            // save and redirecting --------------------------------------------
            $this->web_settings_model->update_testimonial($data, $id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_testimonial_successfully'));
            redirect('web_settings/testimonial');
        } else {
            redirect('web_settings/testimonial');
        }
    }

    // replace testimonial image ----------------------------------------------------
    public function rep_testimonial_image() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);

            // file delete -----------------------------------------------------
            unlink(FCPATH.'/uploads/testimonial/' . $img);

            // name process ----------------------------------------------------
            $divide = explode('.', $img);
            $avatar = current($divide);

            // replacing image -------------------------------------------------
            if(empty($_FILES['rep_cli_img']['name'])){
                die("image file not found!");
            }else{
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/testimonial/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('rep_cli_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // save & redirecting --------------------------------------
                    $this->web_settings_model->rep_testimonial_image($data, $id);
                    $this->session->set_flashdata('succ_msg', get_phrase('update_testimonial_image_successfully'));
                    redirect('web_settings/testimonial');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            redirect('web_settings/testimonial');
        }
    }

    // testimonial Delete ------------------------------------------------------
    public function testimonial_delete() {
        if ($_POST) {
            // get file name ---------------------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);
            // delete from directory -------------------------------------------
            unlink(FCPATH.'/uploads/testimonial/' . $img);
            // delete from db and redirecting ----------------------------------
            $this->web_settings_model->testimonial_delete($id);
            $this->session->set_flashdata('succ_msg', get_phrase('delete_testimonial_successfully'));
            redirect('web_settings/testimonial');
        } else {
            redirect('web_settings/testimonial');
        }
    }


// ##############################################################################################
//  Offers section
// ##############################################################################################

    // offers function ---------------------------------------------------------
    public function offers() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            $data['desc'] = $this->input->post("desc", true);


            // testimonial image uploading -------------------------------------
            if(empty($_FILES['offer_img']['name'])){
                // error on empty
               die("image file not found!");
            }else{
                // slider image upload -----------------------------------------
                $img_name = str_replace(' ', '_', $data['title']);
                $avatar = strtolower($img_name);
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/offers/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('offer_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // post data save ------------------------------------------
                    $this->web_settings_model->add_offer($data);
                    $this->session->set_flashdata('succ_msg', get_phrase('add_offer_successfully'));
                    redirect('web_settings/offers');
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            // default ---------------------------------------------------------   
            $page_data['lists'] = $this->web_settings_model->get_offer();
            $page_data['page_name'] = 'web_settings/offers';
            $page_data['page_title'] = get_phrase('offers');
            $this->load->view('backend/index', $page_data);
        }
    }


    // update offer ------------------------------------------------------------
    public function update_offer() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            $data['desc'] = $this->input->post("desc", true);

            $old_img = $this->input->post("old_img", true); 
            $id = $this->input->post("id", true);

            // renaming --------------------------------------------------------
            $divide = explode('.', $old_img);
            $ext = end($divide);
            $first_part = str_replace(' ', '_', $data['title']);
            $first_part = strtolower($first_part);
            $data['img'] = $first_part.'.'.$ext;
            rename("./uploads/offers/" . $old_img, "./uploads/offers/" . $data['img']);
            
            // save and redirecting --------------------------------------------
            $this->web_settings_model->update_offer($data, $id);
            $this->session->set_flashdata('succ_msg', get_phrase('update_offer_successfully'));
            redirect('web_settings/offers');
        } else {
            redirect('web_settings/offers');
        }
    }

    // replace offer image -----------------------------------------------------
    public function rep_offer_image() {
        if ($_POST) {
            // data receive from post array ------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);

            // file delete -----------------------------------------------------
            unlink(FCPATH.'/uploads/offers/' . $img);

            // name process ----------------------------------------------------
            $divide = explode('.', $img);
            $avatar = current($divide);

            // replacing image -------------------------------------------------
            if(empty($_FILES['rep_offer_img']['name'])){
                die("image file not found!");
            }else{
                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/offers/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = $avatar;
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('rep_offer_img')){
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $data['img'] = $file_name;
                    // save & redirecting --------------------------------------
                    $this->web_settings_model->rep_offer_image($data, $id);
                    $this->session->set_flashdata('succ_msg', get_phrase('update_offer_image_successfully'));
                    redirect('web_settings/offers');
                }else{
                   $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
        }else{
            redirect('web_settings/offers');
        }
    }

    // offer Delete ------------------------------------------------------------
    public function offer_delete() {
        if ($_POST) {
            // get file name ---------------------------------------------------
            $id = $this->input->post("id", true);
            $img = $this->input->post("img", true);
            // delete from directory -------------------------------------------
            unlink(FCPATH.'/uploads/offers/' . $img);
            // delete from db and redirecting ----------------------------------
            $this->web_settings_model->offer_delete($id);
            $this->session->set_flashdata('succ_msg', get_phrase('delete_offer_successfully'));
            redirect('web_settings/offers');
        } else {
            redirect('web_settings/offers');
        }
    }


// ##############################################################################################
//  welcome message on home section
// ##############################################################################################

    // wlc_msg_on_home function ------------------------------------------------
    public function wlc_msg_on_home() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['title1'] = $this->input->post("title1", true);
            $data['title2'] = $this->input->post("title2", true);
            $data['desc'] = $this->input->post("desc", true);
            // about us img uploading ------------------------------------------
            if(!empty($_FILES['wlc_img']['name'])){
                // delete file -------------------------------------------------
                $img = $this->input->post("img", true);
                unlink(FCPATH.'/uploads/wlc/' . $img);

                // load library and init ---------------------------------------
                $config['upload_path']     = './uploads/wlc/';
                $config['file_ext_tolower'] = TRUE;
                $config['file_name']       = "wlc";
                $config['remove_spaces']   = TRUE;
                $config['allowed_types']   = 'jpg|png';
                $config['xss_clean']       = TRUE;
                $this->load->library('upload', $config);
                // uploading ---------------------------------------------------
                if($this->upload->do_upload('wlc_img')){
                    // get file extension with name ----------------------------
                    $up_data = array('upload_data' => $this->upload->data()); 
                    $file_name = $up_data['upload_data']['file_name'];
                    $divide = explode('.', $file_name);
                    $ext = end($divide);
                    $data['img'] = 'wlc'.'.'.$ext;
                }else{
                    $this->session->set_flashdata('err_msg', get_phrase('Error!'));
                }
            }
            // post data save and redirecting ----------------------------------
            $this->web_settings_model->update_wlc($data);
            $this->session->set_flashdata('succ_msg', get_phrase('update_welcome_message_successfully'));
            redirect('web_settings/wlc_msg_on_home');
        }else{
            // default ---------------------------------------------------------   
            $page_data['get_info'] = $this->web_settings_model->get_wlc();
            $page_data['page_name'] = 'web_settings/wlc';
            $page_data['page_title'] = get_phrase('Welcome_message_on_home');
            $this->load->view('backend/index', $page_data);
        }
    }


// ##############################################################################################
//  privacy policy section
// ##############################################################################################

    // privacy policy function -------------------------------------------------
    public function privacy_policy() {
        if($_POST){
            // data receive from post array ------------------------------------
            $data = array();
            $data['title'] = $this->input->post("title", true);
            $data['desc'] = $this->input->post("desc", true);
            // post data save and redirecting ----------------------------------
            $this->web_settings_model->update_privacy($data);
            $this->session->set_flashdata('succ_msg', get_phrase('update_privay_policy_successfully'));
            redirect('web_settings/privacy_policy');
        }else{
            // default ---------------------------------------------------------   
            $page_data['get_info'] = $this->web_settings_model->get_privacy();
            $page_data['page_name'] = 'web_settings/privacy_policy';
            $page_data['page_title'] = get_phrase('Privacy_&_policy');
            $this->load->view('backend/index', $page_data);
        }
    }



}