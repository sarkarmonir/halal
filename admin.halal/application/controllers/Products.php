<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller used for product maintains
 * @package product
 * Use data model for data retrive and processing
 * @author Jahid Al Mamun
 */
class Products extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid al mamun
     */
    function __construct() {
        $this->table = "product";
        $this->table1 = "category";
        parent :: __construct();
        $this->load->model('product');
        $this->load->model('data');
        $this->load->model('category');
        $this->load->helper('common_helper');
    }

    function _example_output($output = null) {
        $this->load->view('example.php', $output);
    }

    /**
     * This method will display all product in this system and this view comming from product/index
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * 
     */
    public function index() {

        $data['all_products'] = $this->product->getall($this->table);


        $data['page_name'] = 'product/index';

        $data['page_title'] = 'Product';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for show feature product management
     * @author Jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function feature() {

        $data['feature_products'] = $this->product->feature($this->table);

        $data['page_name'] = 'product/feature';

        $data['page_title'] = 'Product';

        $this->load->view('backend/index', $data);
    }

    /**
     * this method use for get promo product
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function promo() {
        $data['promo_products'] = $this->product->promo($this->table);

        $data['page_name'] = 'product/promo';

        $data['page_title'] = 'Product';

        $this->load->view('backend/index', $data);
    }

    /**
     * This method has been used to upload file.
     * @author Jahid Al Mamun
     * @param type $field_name
     * @return boolean
     */
    public function do_upload($field_name) {
        $config['upload_path'] = './img/products';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['encrypt_name'] = true;
        $config['max_size'] = '5120';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
    }

    /**
     * This method add new product and view comming from product/add
     * @author Jahid al mamun
     */
    public function add() {

        $data['tax'] = $this->data->getall('tax');
        $data['allcategory'] = $this->category->all_by_option();
        //print_r($data['category']); die();
        $data['page_name'] = 'product/add';

        $data['page_title'] = 'Product';

        $this->load->view('backend/index', $data);
    }

    /**
     * this function use for add master product and view come from product/add
     * @author Jahid Al Mamun
     * 
     */
    public function doadd() {
        $validation = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger', 'Validation error');
        } else {
            if ($_FILES['image']['name'] != '') {
                $image_upload = $this->do_upload('image');

                if ($image_upload == FALSE) {
                    $this->session->set_flashdata('danger', 'Product Image Upload Failed');
                    redirect('products/add', $data);
                } else {
                    $image = $image_upload["file_name"];
                }
            }
            $id = $this->product->add($image);
            if ($id) {
                $this->session->set_flashdata('success', 'Product Added Successfully.');
                redirect('products');
            } else {
                $this->session->set_flashdata('danger', 'Product Not Added Successfully');
                redirect('products/add', $data);
            }
        }
//        echo json_encode($response);
    }

    /**
     * This method use for product edit or update and view comming from product/edit
     * @author Jahid al mamun
     * @package product
     * @return varchar product Description
     */
    public function edit($id) {

        $data['product'] = $this->data->getone($this->table, $id);


        $data['product_select_tax'] = $this->product->product_tax($id);

        $data['tax'] = $this->data->getall('tax');

        $data['category'] = $this->category->all_by_option();

        $data['product_category'] = $this->product->category($id);

        $data['id'] = $id;

        $data['page_name'] = 'product/edit';

        $data['page_title'] = 'Product';

        $this->load->view('backend/index', $data);
    }

    /**
     * this function use for update edit data process
     * @author rjs
     */
    public function doedit() {
        $validation = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            )
        );
        $id = $this->input->post('id');

        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger', 'Validation error');
        } else {
            if ($_FILES['image']['name'] != '') {
                $del_image = $this->input->post('img');
                $filename = "./img/products/" . $del_image;
                if (file_exists($filename)) {
                    unlink($filename);
                }
                $image_upload = $this->do_upload('image');

                if ($image_upload == FALSE) {
                    $this->session->set_flashdata('danger', 'Product Image Upload Failed');
                    redirect('products/add', $data);
                } else {
                    $image = $image_upload["file_name"];
                }
            } else {
                $image = $this->input->post('img');
            }

            if ($this->product->edit($id, $image)) {
                $this->session->set_flashdata('success', 'Product Update Successfully.');
                redirect('products', 'refresh');
            }
        }
//        echo json_encode($response);
    }

    /**
     * this method use for delete existing product and have no view
     * @author Jahid al mamun
     * @package product
     * @return status
     */
    public function delete($id) {
        
    }

    /**
     * This method use for product details in frontend site
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @return type Description
     */
    public function details($id, $name) {
        $this->load->library('common_library');
        $lang_id = $this->common_library->language_id();
        $this->load->model('category_views');
        $data['lot_details'] = '';
        /**
         * get product details with product id with specific language id
         */
        $data['product_details'] = $this->product->product_details($id, $lang_id);
//        echo "<pre>";
//        print_r($data['product_details']);
//        die();
        if ($data['product_details'][0]->lot_type == 1) {
            /**
             * if this product is lot type then get all lot details with product id
             */
            $data['lot_details'] = $this->product->lot_details($id);
        }

        //related product depend on backend related product management process
        $data['related_product'] = $this->product->product_related_product($id, $lang_id);
        if (empty($data['related_product'])) {
            //        get product category id
            $product_category = $this->product->product_category($id);
            //get all product with separate category id
            foreach ($product_category as $category) {
                $category_product[] = $this->category_views->category_product($category->category_id, $lang_id);
            }

            //get related brand product
            $product_brand_id = $data['product_details'][0]->brand;
            $brand_product = $this->product->brand_product($product_brand_id, $lang_id);

            //multidimentional array to one dimentional array//
            $products = call_user_func_array('array_merge', $category_product);
            $product = array_merge_recursive($products, $brand_product);

            //then get uniq product with product list on different category id 
            $data['related_product'] = array_map("unserialize", array_unique(array_map("serialize", $product)));
//        echo "<pre>";
//        print_r($data['related_product']);
//        die(); 
        }

        $rating = $this->product->rating_value($id);
        $data['rating'] = $rating[0]->rating;
        $data['category_menu'] = $this->data->categories();
        $data['content'] = "product_details";
        $this->load->view('site_template', $data);
    }

    /**
     * 
     * @param type $product_id
     * @param type $rating_val
     * @return boolean
     * this method use to give product rating from registration user from product details page
     * @author jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function add_rating($product_id, $rating_val) {
        $customer_id = $this->session->userdata('customer_id');
        $value = array(
            'product_id' => $product_id,
            'rating_value' => $rating_val,
            'customer_id' => $customer_id
        );
        if ($this->data->save('product_rating', $value)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * 
     * @param type $id
     * this method use to manage product gallery from backend 
     * @author jahid al mamun<rjs.jahid11@gmai.com>
     */
    public function gallery($id = NULL) {
        if (!$this->auth->is_admin()) {
            $this->session->set_userdata('back_url', 'products/gallery');
            redirect('admin');
        }
        $this->load->library('image_CRUD');
        $data['parent'] = 'product';
        $data['child'] = 'product';
        $image_crud = new image_CRUD();
        $image_crud->set_primary_key_field('id');
        $image_crud->set_url_field('url');
        $image_crud->set_title_field('title');
        $image_crud->set_table('product_gallery')
                ->set_ordering_field('order')
                ->set_image_path('img/products');

        $data['output'] = $image_crud->render();
//                $data['output'] = $this->load->view('example',$output);	


        $data['content'] = "product/gallery";
        $this->load->view('admin_template', $data);
    }

    public function check_availability($id = NULL, $lot_id = NULL) {
        $check = $this->product->check_availability($id);
        if ($lot_id != NULL) {
            $lot_quantity = $this->product->lot_product_quantity($lot_id);
            if ($check[0]->quantity > $lot_quantity) {
                echo '1';
            } else {
                echo lang('availability');
            }
        } else {
            if ($check[0]->quantity > $check[0]->minimum_quantity) {
                echo '1';
            } else {
                echo lang('availability');
            }
        }
    }

    public function image_tagg_save($id, $tagg) {
        $tagg = urldecode($tagg);

        $value = array(
            'image_tagg' => $tagg
        );
        $this->data->update('product', $id, $value);
        echo 1;
    }

    public function quantity_update($id, $quantity) {
        $val = array(
            'quantity' => $quantity
        );
        if ($this->data->update('product', $id, $val)) {
            echo 1;
        } else {
            echo 2;
        }
    }

}
