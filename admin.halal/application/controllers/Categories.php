<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
/**
 * This controller used for product category maintains
 * @package product
 * @author Jahid Al Mamun
 */
class Categories extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'category';
        parent :: __construct();
        $this->load->model('data');
        $this->load->model('category');
        
        if ($this->session->userdata('roule') != 'admin'){

            redirect(base_url() . 'login', 'refresh');
        }
    }

    /**
     * This method display all product category with tree based 
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @package product
     * 
     * 
     */
    public function index() {
        
        $data['allcategory'] = $this->category->category_list();
        
        $data['page_name'] = 'category/index';

        $data['page_title'] = 'Category';

        $this->load->view('backend/index', $data);
    }
    
   public function do_upload($field_name) {
        $config['upload_path'] = './img/category';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['encrypt_name'] = true;
        $config['max_size'] = '5120';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field_name)) {
            return false;
        } else {
            return $this->upload->data();
        }
    }
    /**
     * this method use for add new product category
     * @author Jahid al mamun
     * 
     */
    public function add() {

        $data['allcategory'] = $this->data->getall($this->table);
        
        $data['page_name'] = 'category/add';

        $data['page_title'] = 'Category';

        $this->load->view('backend/index', $data);
    }

    public function doadd() {

        $response = array();
        $response['error'] = 1;

        $validation = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            redirect('categories/add'.$data);
        } else {
          if($_FILES['image']['name'] !='')
            {    
             $image_upload  =   $this->do_upload('image');
             
              if ($image_upload == FALSE) {
                  $this->session->set_flashdata('danger','Product Image Upload Failed');
                  redirect('categories/add',$data);
              }  else {
                  $image = $image_upload["file_name"];
              }
            } else {
            $image = '';    
            }
            
                if ($this->category->add($image)) {
                    $this->session->set_flashdata('success','Category Added Successfully.');
                    redirect('categories');
                    
                }else
                {
                    
                }
            
        }
        
//        echo json_encode($response);
    }

    /**
     * This method use for edit category
     * @param int $id edit with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function edit($id) {
        
        $data['id'] = $id;
        $data['allcategory'] = $this->data->getall($this->table);
        $data['category'] = $this->data->getone($this->table,$id);
        
        $data['page_name'] = 'category/edit';

        $data['page_title'] = 'Category';

        $this->load->view('backend/index', $data);
        
    }

    /**
     * this function use for update edit data process
     * @author rjs
     */
    public function doedit()
    {
        //print_r($_POST); die();
        $validation = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
            )
        );
        $id = $this->input->post('id');

        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('danger','Validation error');
            echo "validation err";
        } else {           
           if($_FILES['image']['name'] !='')
            {    
             $image_upload  =   $this->do_upload('image');
             
            if ($image_upload == FALSE) {
                $this->session->set_flashdata('danger','category Image Upload Failed');
                redirect('categories/edit',$data);
            }  else {
                $image = $image_upload["file_name"];
            }
              
            } else {
                $image = $this->input->post('img');
            }
                $this->load->model('category');
                if ($this->category->edit($id,$image)) {
                    $this->session->set_flashdata('success','Category Update Successfully.');
                    redirect('categories',$data);
                }
            
        }
//        echo json_encode($response);
    }
    /**
     * use for delete this category with id
     * @param int $id delete category with id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function delete($id) {
        
    }

}
