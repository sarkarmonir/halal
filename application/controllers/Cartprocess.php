<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Home Class
*/
class Cartprocess extends CI_controller
{
	
	function __construct()
	{
		parent::__construct();
	}
        
        public function addtocart(){
            $product_id = $this->input->post('productId');
            $qty = 1;
            //first retrive all product details with discount and availability
            $product_info = $this->data->product_info($product_id);
            //print_r($product_info);
            if($product_info->quantity >= $qty ){
                $cartval = array(
                    'id'=>$product_info->id,
                    'name'=>$product_info->name,
                    'price'=>$product_info->price,
                    'sale_price'=>$product_info->sale_price,
                    'image'=>$product_info->image,
                    'qty'=>$qty,
                    'description'=>$product_info->short_description,
                );
                
                $this->load->library('halal_cart');
                $addtocart_return = $this->halal_cart->addtocart($cartval);
                
            }else{
               echo 2;
               return false;
            }
          
        }
        
        public function delete_from_cart(){
            $rowid = $this->input->post('rowid');
            $this->load->library('halal_cart');
            $removeitem = $this->halal_cart->delete_from_cart($rowid);
            $cartnumber = count($this->cart->contents());
           
            echo $cartnumber;
        }
        
        public function qtyplus(){
            $rowid = $this->input->post('rowid');
            $this->load->library('halal_cart');
            $removeitem = $this->halal_cart->qtyplus_from_cart($rowid);
            $cartnumber = count($this->cart->contents());
           
            echo $cartnumber;
        }
        
        public function qtyminus(){
            $rowid = $this->input->post('rowid');
            $this->load->library('halal_cart');
            $removeitem = $this->halal_cart->qtyminus_from_cart($rowid);
            $cartnumber = count($this->cart->contents());
           
            echo $cartnumber;
        }
        
        public function view_cart_content(){
            $data['title'] = "Your Cart";
        
        $data['cartitems'] = $this->cart->contents();
        
        $total = 0;
        $subtotal = 0;
        
        foreach($data['cartitems'] as $rowid=>$item){
         $total = $total + $item['price']*$item['qty'];
         $subtotal = $subtotal + $item['sale_price']*$item['qty'];
       
        }
        $total_save = $subtotal - $total;
        $data['total_save'] = number_format($total_save,2);
        $data['total'] = number_format($total,2);
        $data['subtotal'] = number_format($subtotal,2);
        
        return $this->load->view('pages/cart_update',$data);
        }
        
        public function mini_cart_content(){
            $data['title'] = "Your Cart";
        
        $data['cartitems'] = $this->cart->contents();
        
        $total = 0;
        $subtotal = 0;
        
        foreach($data['cartitems'] as $rowid=>$item){
         $total = $total + $item['price']*$item['qty'];
         $subtotal = $subtotal + $item['sale_price']*$item['qty'];
       
        }
        $total_save = $subtotal - $total;
        $data['total_save'] = number_format($total_save,2);
        $data['total'] = number_format($total,2);
        $data['subtotal'] = number_format($subtotal,2);
        
        return $this->load->view('pages/mini_cart_update',$data);
        }
        
        public function checkout_calculation(){
        
        $data['cartitems'] = $this->cart->contents();
        
        $total = 0;
        $subtotal = 0;
        
        foreach($data['cartitems'] as $rowid=>$item){
         $total = $total + $item['price']*$item['qty'];
         $subtotal = $subtotal + $item['sale_price']*$item['qty'];
       
        }
        $total_save = $subtotal - $total;
        $data['total_save'] = number_format($total_save,2);
        $data['total'] = number_format($total,2);
        $data['subtotal'] = number_format($subtotal,2);
        
        return $this->load->view('pages/checkout_calculation',$data);
        }
        
        public function update_cart_qty(){
            $qty = $this->input->post('qty');
            $rowid = $this->input->post('rowid');
            $this->load->library('halal_cart');
            $update_cart = $this->halal_cart->update_cart_qty($rowid,$qty);
            return true;
        }
        
        public function coupon_apply($code) {
        $coupon = $this->data->coupon_info($code);
        if ($coupon) {
            $data = array(
                'coupon_name' => $coupon[0]->name,
                'coupon_code' => $coupon[0]->code,
                'coupon_type' => $coupon[0]->type,
                'coupon_discount' => $coupon[0]->discount,
                'coupon_shipping' => $coupon[0]->shipping,
            );
            $this->session->set_userdata($data);
            echo "1";
        } else {
            $data = array(
                'coupon_name' => '',
                'coupon_code' => '',
                'coupon_type' => '',
                'coupon_discount' => '',
                'coupon_shipping' => '',
            );
            $this->session->set_userdata($data);
            echo "0";
        }
    }
        
}