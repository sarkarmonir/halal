<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Class
 */
class Home extends CI_controller {

    function __construct() {
        parent::__construct();
        $data = array();
        $this->load->model("web_settings_model");
    }

    public function index() {
        $data['title'] = "Hala Organic Meat";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['sliders'] = $this->web_settings_model->get_slider();
        $data['wlc'] = $this->web_settings_model->get_wlc();
        $data['last_offer'] = $this->web_settings_model->last_offer();
        $data['offers'] = $this->web_settings_model->get_offer();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['last_farm'] = $this->web_settings_model->last_farm();
        $data['farm_gallerys'] = $this->web_settings_model->get_farm_gallery();
        $data['allcategories'] = $this->data->getall_with_status('category');
        $data['feature_products'] = $this->data->feature_product();
        //print_r($data['feature_products']); die();



        $this->load->view('common/head', $data);
        $this->load->view('common/slider', $data);
        $this->load->view('common/home', $data);
        $this->load->view('common/footer', $data);
    }

    public function about() {

        $data['title'] = "About us";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['info'] = $this->web_settings_model->get_about_us();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/about', $data);
        $this->load->view('common/footer', $data);

        //$this->load->view('common/template', $data);
    }

    public function order_online($category = '') {
        $data['title'] = "Order online";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['allcategories'] = $this->data->getall_with_status('category');
        if ($category == '') {
            $selectCat = $data['allcategories'][0]->id;
        } else {
            $selectCat = (int) $category;
        }
        $category_details = $this->data->getone('category',$selectCat);
        
        $data['title'] = $category_details[0]->name;
        $data['currentCat'] = $selectCat;
        //now retrive selected category products
        $data['allProducts'] = $this->data->category_products($selectCat);
        //echo "<pre>"; print_r($data['allProducts']); die();

        $data['cartitems'] = $this->cart->contents();

        $total = 0;
        $subtotal = 0;

        foreach ($data['cartitems'] as $rowid => $item) {
            $total = $total + $item['price'] * $item['qty'];
            $subtotal = $subtotal + $item['sale_price'] * $item['qty'];
        }
        $total_save = $subtotal - $total;
        $data['total_save'] = number_format($total_save, 2);
        $data['total'] = number_format($total, 2);
        $data['subtotal'] = number_format($subtotal, 2);

        $this->load->view('common/head', $data);
        $this->load->view('common/header', $data);
        $this->load->view('pages/order_online', $data);
        $this->load->view('common/footer', $data);
    }

    public function item_details($item) {
        $item = (int) $item;

        
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['item_details'] = $this->data->item_details($item);
        $data['title'] = $data['item_details']->name;
        
        //print_r($data['item_details']); die();
        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/item_details');
        $this->load->view('common/footer', $data);
    }

    public function contact() {

        $data['title'] = "Contact us";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/contact');
        $this->load->view('common/footer', $data);
    }

    public function faq() {

        $data['title'] = "F . A . Q";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['infos'] = $this->web_settings_model->get_faq();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/faq');
        $this->load->view('common/footer', $data);
    }

    public function farm() {

        $data['title'] = "Farm";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/farm');
        $this->load->view('common/footer', $data);
    }

    public function farm_gallery() {

        $data['title'] = "Farm Gallery";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/farm_gallery');
        $this->load->view('common/footer', $data);
    }

    public function my_account() {
        $data['title'] = "My Account";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        
        
        $customer_id = $this->session->userdata('customer_id');
        $data['all_orders'] = $this->data->customer_orders($customer_id);
        $data['customer_info'] = $this->data->customer_details($customer_id);
        $data['customer_shipping_info'] = $this->data->customer_shipping_address($customer_id);
        //echo "<pre>"; print_r($data['customer_info']); die();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/my_account',$data);
        $this->load->view('common/footer', $data);
    }
    public function order_info() {

        $data['title'] = "My Account";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/my_account');
        $this->load->view('common/footer', $data);
    }
    public function personal_info() {

        $data['title'] = "My Account";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/personal_info');
        $this->load->view('common/footer', $data);
    }
    public function shipping_info() {

        $data['title'] = "My Account";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/shipping_info');
        $this->load->view('common/footer', $data);
    }
    public function payment_info() {

        $data['title'] = "My Account";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/payment_info');
        $this->load->view('common/footer', $data);
    }

    public function cart() {

        $data['title'] = "Your Cart";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['cartitems'] = $this->cart->contents();

        $total = 0;
        $subtotal = 0;

        foreach ($data['cartitems'] as $rowid => $item) {
            $total = $total + $item['price'] * $item['qty'];
            $subtotal = $subtotal + $item['sale_price'] * $item['qty'];
        }
        $total_save = $subtotal - $total;
        $data['total_save'] = number_format($total_save, 2);
        $data['total'] = number_format($total, 2);
        $data['subtotal'] = number_format($subtotal, 2);


        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/cart', $data);
        $this->load->view('common/footer', $data);
    }

    public function checkout() {

        $data['title'] = "Checkout";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['cartitems'] = $this->cart->contents();

        $total = 0;
        $subtotal = 0;

        foreach ($data['cartitems'] as $rowid => $item) {
            $total = $total + $item['price'] * $item['qty'];
            $subtotal = $subtotal + $item['sale_price'] * $item['qty'];
        }
        //now check coupon apply or not
        if ($this->session->userdata('coupon_name') != '') {
            //get coupon value
            if ($this->session->userdata('coupon_type') == 'p') {
                $coupon_value = $this->cart->total() * $this->session->userdata('coupon_discount') / 100;
            } elseif ($this->session->userdata('coupon_type') == 'f') {
                $coupon_value = $this->session->userdata('coupon_discount');
            } 
        } else {
            $coupon_value = 0;
        }

        $data['coupon_value'] = $coupon_value;

        if ($this->session->userdata('customer_id') != '') {
            $data['customer'] = $this->data->customer_details($this->session->userdata('customer_id'));
            //print_r($data['customer']); die();
        }

        $total_save = $coupon_value;
        $total = $total - $coupon_value;
        $data['total_save'] = number_format($total_save, 2);
        $data['total'] = number_format($total, 2);
        $data['subtotal'] = number_format($subtotal, 2);

        //retrive payment method
        $data['payments'] = $this->data->getall_with_status('payments');
        //retrive area
        $data['areas'] = $this->data->getall_with_status('area');
        //echo "<pre>"; print_r($data['payments']); die();

        $this->load->view('common/head', $data);
        $this->load->view('common/header', $data);
        $this->load->view('pages/checkout', $data);
        $this->load->view('common/footer', $data);
    }

    public function checkout_process() {
        if ($_POST) {

            //first get customer and set or new registration with add new shipping address
            //then prepare order data
            if ($this->session->userdata('customer_id') == '') {
                if ($this->input->post('checkout_as') != '') {
                    $name = $this->input->post('cust_name');
                    $email = $this->input->post('cust_email');
                    $phone = $this->input->post('cust_phone');
                    $password = $this->input->post('cust_password');
                    $city = $this->input->post('cust_city');
                    $place = $this->input->post('bill_user_area');
                    $address = $this->input->post('billing_address');
                    $agree = $this->input->post('agree');
                    $type = $this->input->post('checkout_as');


                    $signup_val = array(
                        'name' => $name,
                        'email' => $email,
                        'password' => md5($password),
                        'city' => $city,
                        'place' => $place,
                        'telephone' => $phone,
                        'address' => $address,
                        'agree' => 1,
                        'status' => 1,
                        'type' => $type
                    );

                    $this->data->save('customers', $signup_val);

                    $result = $this->data->check_valid_customer($email, $password);

                    if (count($result) > 0) {
                        $id = $result[0]['id'];
                        $name = $result[0]['name'];
                        $customer = array('customer_id' => $id, 'type' => 'customer', 'name' => $name, 'email' => $email);

                        $this->session->set_userdata($customer);

                        //now check same as delivery address or not
                        if ($this->input->post('same_as_billing') == 'same') {
                            $shipping_val = array(
                                'customer_id' => $id,
                                'place' => $place,
                                'telephone' => $phone,
                                'address' => $address
                            );

                            $shipping_id = $this->data->save('customer_shipping_address', $shipping_val);
                        } else {
                            $shipping_val = array(
                                'customer_id' => $id,
                                'place' => $this->input->post('ship_user_area'),
                                'telephone' => $phone,
                                'address' => $this->input->post('shipping_address')
                            );

                            $shipping_id = $this->data->save('customer_shipping_address', $shipping_val);
                        }
                    }
                }
                $customer_email =  $this->input->post('cust_email');
            } else {
                $customer_details = $this->data->customer_details($this->session->userdata('customer_id'));

//                we can set here past selected shipping address for this customer 
                //now check same as delivery address or not
                if ($this->input->post('same_as_billing') == 'same') {

                    //check if exist or not this shipping address
                    $shipping_address_exist = $this->data->shipping_exist_check($customer_details->id, $customer_details->place, $customer_details->address);
                    if ($shipping_address_exist) {
                        $shipping_id = $$shipping_address_exist;
                    } else {
                        $shipping_val = array(
                            'customer_id' => $customer_details->id,
                            'place' => $customer_details->place,
                            'telephone' => $customer_details->telephone,
                            'address' => $customer_details->address
                        );



                        $shipping_id = $this->data->save('customer_shipping_address', $shipping_val);
                    }
                } else {
                    $shipping_val = array(
                        'customer_id' => $customer_details->id,
                        'place' => $this->input->post('ship_user_area'),
                        'telephone' => $customer_details->telephone,
                        'address' => $this->input->post('shipping_address')
                    );

                    $shipping_id = $this->data->save('customer_shipping_address', $shipping_val);
                }
                $customer_email = $customer_details->email;
            }


            //then prepare and save order item data 
            $data['cartitems'] = $this->cart->contents();

            $total = 0;
            $subtotal = 0;

            foreach ($data['cartitems'] as $rowid => $item) {
                $total = $total + $item['price'] * $item['qty'];
                $subtotal = $subtotal + $item['sale_price'] * $item['qty'];
            }
            //now check coupon apply or not
            if ($this->session->userdata('coupon_name') != '') {
                //get coupon value
                if ($this->session->userdata('coupon_type') == 'p') {
                    $coupon_value = $this->cart->total() * $this->session->userdata('coupon_discount') / 100;
                } elseif ($this->session->userdata('coupon_type') == 'f') {
                    $coupon_value = $this->session->userdata('coupon_discount');
                } else {
                    $coupon_value = 0;
                }
                $data['coupon_value'] = $coupon_value;
            } else {
                $coupon_value = 0;
            }

            if ($this->session->userdata('customer_id') != '') {
                $customer_details = $this->data->customer_details($this->session->userdata('customer_id'));
                //print_r($data['customer']); die();
            }

            $total_save = $coupon_value;
            $total = $total - $coupon_value;
            $on_date = date('h:i:sa d/m/y');
            $order_info = array(
                'customer_id' => $customer_details->id,
                'status' => '1',
                'ordered_on' => $on_date,
                'tax' => '',
                'item_total' => $subtotal,
                'total' => $total,
                'subtotal' => $subtotal,
                'notes' => $this->input->post('order_note'),
                'payment_type' => $this->input->post('payment'),
                'payment_status' => 'not_paid',
                'order_type' => $this->input->post('order_type'),
                'coupon_code' => $this->session->userdata('coupon_code'),
                'coupon_value' => $coupon_value,
                'shipping_cost' => 0,
                'shipping_address' => $shipping_id,
                'tax' => 0
            );

            $order_id = $this->data->save('orders', $order_info);

            //Then prepare order item data to save data in order_item table with details
            foreach ($data['cartitems'] as $item) {
                $order_item = array(
                    'rowid' => $item['rowid'],
                    'order_id' => $order_id,
                    'product_id' => $item['id'],
                    'quantity' => $item['qty'],
                    'discount' => '',
                    'subtotal' => $item['price'],
                    'total' => $item['subtotal'],
                    'product_name' => $item['name'],
                    'product_image' => $item['image']
                );

                //then prepare for trasaction
            }
            $this->data->save('order_items', $order_item);

            //now process for payment 
            $payment_method = $this->input->post('payment');
            $payment_details = $this->data->payment_details($payment_method);
            
            //now send email
            $this->load->library('common_library');
            $to = $customer_email;
            $subject = "Halal | Thank you For your Order";
            $message = '<h3>Your Order Number is'.$order_id.'</h3><p>Thank you for your order.</p>';
            $sss = $this->common_library->send_mail($to, $subject, $message);

            if ($payment_details->type == 'direct') {
                //update first as COD payment then show success page
                $order_update_info = array(
                    'payment_status' => 'COD',
                );
                $this->data->update('orders', $order_id, $order_update_info);

                redirect("home/order_success/$order_id");
            } elseif ($payment_details->type == 'paypal') {
                //else if it is payment payment process then have need to prepare submit post data for payment getway 
                $data['paypal_config'] = json_decode($payment_details->configs);
                //print_r($data['paypal_config']); die();
                
                if($data['paypal_config']->sandbox ==1){
                   $data['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; 
                }else{
                   $data['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr'; 
                }
                
                $data['shipping_cost'] = 0;
                $data['order_id'] = $order_id;
                $data['coupon_value'] = $total_save;
                $data['business_email'] = $data['paypal_config']->email;
                
                $data['title'] = "Payment with paypal";
                $data['web_set'] = $this->web_settings_model->get_general_settings();
                $data['testimonials'] = $this->web_settings_model->get_testimonial();
                
                $this->load->view('common/head', $data);
                $this->load->view('common/header');
                $this->load->view('pages/paypal_form', $data);
                $this->load->view('common/footer', $data);
            }
        }
    }
    
    public function cancel_order($order_id)
   {
        $update_order = array(
                    'payment_status'=>'cancel'
                );
                
       $this->data->update('orders',$order_id,$update_order);
       
       $this->session->set_flashdata('danger','Your Transection Process is cancel! Again You Can Continue Your Shopping with your existing cart Items.');
       redirect('checkout');
   }
   
    public function ipn() {
        $payment_details = $this->data->paypal_payment_details();
        
        $paypal_config = json_decode($payment_details->configs);
        
        if($paypal_config->sandbox ==1){
                   $paypalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; 
                }else{
                   $paypalUrl = 'https://www.paypal.com/cgi-bin/webscr'; 
                }
                
        $paypalBusinessEmail = $paypal_config->email;
        //now insert the transaction record to balance record
        $transactionCode = explode('#', $_POST['custom']);
        $transactionId = $transactionCode[0];
        $customerId = $transactionCode[1];
        $pay_amt = $_POST['mc_gross'];
        $pay_currency = $_POST['mc_currency'];
        
        $req = 'cmd=_notify-validate';
        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }
 
        $ch = curl_init($paypalUrl);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        $res = curl_exec($ch);
        curl_close($ch);
 
        //$this->ipn_data = $_POST;
 
        if (strcmp ($res, "VERIFIED") == 0) {
            //here update the balance feild with userId and balanceId
            if($_POST['txn_type'] == "web_accept" && $_POST['payment_status'] == "Completed" && $_POST['business'] == $paypalBusinessEmail) {
                
                //update system data and destroy cart data//jahid al mamun <rjs.jahid11@gmail.com>
                $update_order = array(
                    'payment_status'=>'paid'
                );
                
                $this->data->update('orders',$transactionId,$update_order);
               
            }
        } else if (strcmp ($res, "INVALID") == 0) {
            
        } else {
           
        }
        
    }

    public function order_success($order_id) {
        //first destroy cart history
        $this->cart->destroy();
        $data['order_id'] = $order_id;
        $data['title'] = "Order Success Page";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/order_success', $data);
        $this->load->view('common/footer', $data);
    }

    public function login() {
        if ($this->session->userdata('customer_id') != '' && $this->session->userdata('name') != '') {
            redirect();
        } else {
            $data['title'] = "User login";
            $data['web_set'] = $this->web_settings_model->get_general_settings();
            $data['testimonials'] = $this->web_settings_model->get_testimonial();

            $this->load->view('common/head', $data);
            $this->load->view('common/header');
            $this->load->view('pages/login');
            $this->load->view('common/footer', $data);
        }
    }

    public function signup() {

        if ($this->input->post()) {
            if ($this->crud_model->signup()) {
                redirect(base_url('success'));
            }
        }

        $data['title'] = "User registration";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        //retrive area
        $data['areas'] = $this->data->getall_with_status('area');

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/signup');
        $this->load->view('common/footer', $data);
    }

    public function signup_success() {

        echo "Signup Success <a href=" . base_url() . ">Back to home</a>";
    }

    public function privacy() {

        $data['title'] = "Privacy & Policy";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['info'] = $this->web_settings_model->get_privacy();

        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/privacy', $data);
        $this->load->view('common/footer', $data);
    }

    public function dologin() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $result = $this->data->check_valid_customer($email, $password);

        if (count($result) > 0) {
            $id = $result[0]['id'];
            $name = $result[0]['name'];
            $customer = array('customer_id' => $id, 'type' => 'customer', 'name' => $name, 'email' => $email);

            $this->session->set_userdata($customer);

            echo 1;
        } else {
            echo 0;
        }
    }

    public function signup_pro() {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $password = $this->input->post('password');
        $con_password = $this->input->post('confirm_password');
        $city = $this->input->post('city');
        $address = $this->input->post('address');
        $place = $this->input->post('user_area');
        $agree = $this->input->post('agree');

        //first check email is exist or not
        $check_email = $this->data->email_check($email);
        if ($check_email != '') {
            echo 2;
            return false;
        }
        $signup_val = array(
            'name' => $name,
            'email' => $email,
            'password' => md5($password),
            'city' => $city,
            'place' => $place,
            'telephone' => $phone,
            'address' => $address,
            'agree' => 1,
            'status' => 1,
        );
        $this->data->save('customers', $signup_val);

        $result = $this->data->check_valid_customer($email, $password);

        if (count($result) > 0) {
            $id = $result[0]['id'];
            $name = $result[0]['name'];
            $customer = array('customer_id' => $id, 'type' => 'customer', 'name' => $name, 'email' => $email);

            $this->session->set_userdata($customer);

            echo 1;
        } else {
            echo 0;
        }
    }

    public function logout() {
        $customer = array('customer_id' => '', 'type' => '', 'name' => '', 'email' => '');
        $this->session->unset_userdata($customer);
        $this->session->sess_destroy();
        redirect();
    }

    public function search_result() {
        $search_val = $this->input->get('search_val');
        $data['title'] = "Search Result";
        $data['web_set'] = $this->web_settings_model->get_general_settings();
        $data['testimonials'] = $this->web_settings_model->get_testimonial();
        $data['info'] = $this->web_settings_model->get_privacy();
        $data['search_result'] = $this->data->search($search_val);
//echo '<pre>'; print_r($data['search_result']); die();
        $this->load->view('common/head', $data);
        $this->load->view('common/header');
        $this->load->view('pages/search_result', $data);
        $this->load->view('common/footer', $data);
    }
    
    public function order_modal($id){
        $this->load->model('customer');
        $data['order_details'] = $this->data->getone('orders',$id);
        $data['order_items'] = $this->customer->order_items($id);
        $shipping_id = $data['order_details'][0]->shipping_address;
        $customer_id = $data['order_details'][0]->customer_id;
        $data['customer_details'] = $this->data->getone('customers',$customer_id);
        
        
        
        if($shipping_id == 0)
        {
         $data['shipping_address'] = '';  
        }else
        {
        $data['shipping_address'] = $this->customer->shipping_address($shipping_id);
        }
        
        $this->load->view('pages/order_modal',$data,1);
    }
    
    public function personal_info_modal($customer_id){
        
        $data['customer_details'] = $this->data->getone('customers',$customer_id);
        
        $this->load->view('pages/personal_info_modal',$data,1);
    }
    
    public function shipping_info_modal($shipping_id){
        
        $data['shipping_details'] = $this->data->shipping_address_details($shipping_id);
      
        $this->load->view('pages/shipping_info_modal',$data,1);
    }
    
    public function update_personal_info(){
        if($_POST){
            $customer_id = $this->input->post('customer_id');
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $address = $this->input->post('address');
            
            $update_val = array(
                'name'=>$name,
                'email'=>$email,
                'telephone'=>$phone,
                'address'=>$address
            );
            
            $this->data->update('customers',$customer_id,$update_val);
            redirect('my_account');
        }
    }
    
    public function update_shipping_info(){
        if($_POST){
            $ship_id = $this->input->post('customer_id');
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $address = $this->input->post('address');
            
            $update_val = array(
                'name'=>$name,
                'email'=>$email,
                'telephone'=>$phone,
                'address'=>$address
            );
            
            $this->data->shipping_update($ship_id,$update_val);
             redirect('my_account');
        }
    }

}