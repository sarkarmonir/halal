<?php

/**
 * @package helper functions
 */

/**
 * Helper function to generate Disabled/Enabled/Pending labels based on 0/1/2 status input
 * @param int $st
 * @param bool $label
 * @return string
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function get_status_label($st, $label = TRUE) {
    if ($label) {
        switch ($st) {
            case 0:
                return "<span class='label label-danger'>Disabled</span>";
                break;
            case 1:
                return "<span class='label label-success'>Enabled</span>";
                break;
            case 2:
                return "<span class='label label-success'>Pending</span>";
                break;
            default:
                return $st;
        }
    } else {
        switch ($st) {
            case 0:
                return "Disabled";
                break;
            case 1:
                return "Enabled";
                break;
            case 2:
                return "Pending";
                break;
            default:
                return $st;
        }
    }
}

/**
 * Helper function to generate no/yes labels based on 0/1 input
 * @param int $st
 * @param bool $label
 * @return string
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function get_yesno($st, $label = TRUE) {
    if ($label) {
        switch ($st) {
            case 0:
                return "<span class='label label-danger'>" . lang('no') . "</span>";
                break;
            case 1:
                return "<span class='label label-success'>" . lang('yes') . "</span>";
                break;
            default:
                return $st;
        }
    } else {
        switch ($st) {
            case 0:
                return "No";
                break;
            case 1:
                return "Yes";
                break;
            default:
                return $st;
        }
    }
}

/**
 * Wrap and text with bootstrap label
 * @param string $text
 * @param string $label
 * @return string
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function wrap_label($text, $label = 'success') {
    return "<span class='label label-$label'>$text</span>";
}

/**
 * Helper function to get clean string to be able to use in url
 * @param string $title
 * @return string
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function make_alias($title) {
    return str_replace(array(" ", ",", "'", "\"", "&#39;"), "-", strtolower(trim(html_entity_decode($title))));
}

function non_alias($title) {
    return str_replace(array("-", ",", "'", "\"", "&#39;"), " ", strtolower(trim(html_entity_decode($title))));
}

function make_comma($title) {
    return str_replace(array("_", ",", "'",), ",", strtolower(trim(html_entity_decode($title))));
}

function comma_to_dash($title) {
    return str_replace(array(",", "_", "'",), "_", strtolower(trim(html_entity_decode($title))));
}

/**
 * Return multilingual url depending on the current language
 * @param string $url
 * @return string
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function base_url_tr($url = "") {
    $CI = & get_instance();
    $lang = $CI->session->userdata('lang_code');
    if (!$lang || ($lang == "en")) {
        return base_url($url);
    } else {
        $base = base_url($lang . '/' . $url) . '/';
        return $base;
    }
}

/**
 * Return translated current url based on language passed
 * @param string $lang_code
 * @return string
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function current_url_tr($lang_code = NULL) {
    $CI = &get_instance();
    if ($CI->config->item('lang_code_default') == $lang_code) {
//        $lang_code = "";
    }
    $uri_string = uri_string();
    $current_uri = "";
    $current_uri_seg = explode("/", $uri_string);
    if (strlen($current_uri_seg[0]) == 2) {
        $current_uri_seg[0] = $lang_code;
        $current_uri = implode("/", $current_uri_seg);
    } else {
        $current_uri = $lang_code . '/' . $uri_string;
    }
    if ($_SERVER['QUERY_STRING']) {
        $current_uri .= "?" . $_SERVER['QUERY_STRING'];
    }
    $current_uri = base_url($current_uri);
    return $current_uri;
}

/**
 * Modified redirect() function to direct user to url with proper language
 * @param string $url
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function redirect_tr($url = "") {
    redirect(base_url_tr($url), 'refresh');
}

/**
 * Helper function to get an array of avalable language stored in db
 * @return array eg: array("en"=>array("lang_code"=>"en","lang_name"=>"english","lang_flag"=>"gb"))
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function language_array() {

    $ci = & get_instance();
    $ci->load->database();

    $ci->db->select('lang_code, lang_name, lang_flag');
    $ci->db->where('status', 1);
    $query = $ci->db->get('language');

    $languages = array();

    foreach ($query->result_array() as $row) {
        $languages[$row['lang_code']] = $row;
    }

    //$_SESSION["languages"] = $languages;

    return $languages;
}

/**
 * helper function to generate language select selector dropdown html (bootstrap compatible)
 * @return string html codes
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function language_selector() {
    $CI = & get_instance();

    $selector = "";
    $current_lang = ucfirst($CI->session->userdata('lang_name'));

    $selector = '<div class="btn-group"><button type="button"  data-toggle="dropdown"  class="btn dropdown-toggle artdata_curla transparent1"><i class="flag-' . $CI->session->userdata('lang_flag') . '"></i> ' . $current_lang . '<span class="caret"></span></button>';
    $selector .= '<ul class="dropdown-menu artdata_curla_drop" role="menu">';

    $languages = language_array();

    foreach ($languages as $code => $lang) {
        $selector .= '<li style="display:block; width:100%;"><a href="' . current_url_tr($lang['lang_code']) . '"><i class="flag-' . $lang['lang_flag'] . '"></i> ' . ucfirst($lang['lang_name']) . '</a></li>';
    }

    $selector .= '</ul> </div>';
    //$selector .= '</select>'."\n";
    echo $selector;
}

/**
 * helper function to generate language selector dropdown html (bootstrap compatible). This is the smaller version used in logged in top nav
 * @return string html codes
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function language_selector_small() {
    $CI = & get_instance();

    $selector = "";

    $selector = '<button type="button"  data-toggle="dropdown"  class="btn btn-danger dropdown-toggle artdata_curla artdata_curla_top"><i class="flag-' . $CI->session->userdata('lang_flag') . '"></i> <span class="caret"></span></button>';
    $selector .= '<ul class="dropdown-menu artdata_curla_drop languageClass" role="menu">';

    $languages = language_array();
    foreach ($languages as $code => $lang) {
        $selector .= '<li><a href="' . current_url_tr($lang['lang_code']) . '"><i class="flag-' . $lang['lang_flag'] . '"></i> ' . ucfirst($lang['lang_name']) . '</a></li>';
    }

    $selector .= '</ul>';
    //$selector .= '</select>'."\n";
    return $selector;
}

/**
 * Helper function to get sql formatted date
 * @param string $date input date that needs to be formatted
 * @param string $seperator default seperator is "-". It is provided to identify date seperator properly
 * @param string $format default format is d-m-Y
 * @return string date in SQL format
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function sqldate($date, $seperator = "-", $format = "d-m-Y") {
    if ($date) {
        $d = explode($seperator, $date);
        $f = explode($seperator, $format);

        $dd[$f[2]] = $d[2];
        $dd[$f[1]] = $d[1];
        $dd[$f[0]] = $d[0];

        //$finaldate = $m;
        return $dd['Y'] . "-" . $dd['m'] . "-" . $dd['d'];
    } else {
        return false;
    }
}

/**
 * Get d-m-Y formatted date from sql formatted date
 * @param string $d sql formatted date
 * @return string d-m-Y formatted date
 * @author Jamiul Hasan
 */
function mydate($d, $seperator = "-") {
    $d = explode("-", $d);
    $year = $d[0];
    $month = $d[1];
    $day = $d[2];

    $finaldate = $day . $seperator . $month . $seperator . $year;
    return $finaldate;
}

/**
 * This function is used to generate password hash
 * @param string $user_password
 * @return string hashed password. Hash string is set in configuration page
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function adPWCrypt($user_password) {
    $CI = & get_instance();
    $pwsalt = $CI->config->item('pwsalt');
    return crypt($user_password, $pwsalt);
}

/**
 * This is a helper function to set an alert (by setting up flash session) and redirect to the desired url. So after redirection, alert will be displayed
 * @param string $url
 * @param string $alert alert message that needs to be displayed
 * @param string $alertType 
 * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
 */
function redirectAlert($url, $alert, $alertType = "success") {
    $CI = & get_instance();
    $CI->session->set_flashdata('alertmsg', $alert);
    $CI->session->set_flashdata('alertType', $alertType);
    redirect_tr($url);
}

/**
 * @ignore
 */
function showAlert($msg) {
    $CI = & get_instance();
    $CI->output->append_output("<script>show_alert('$msg');</script>");
}

function chkDefaultLang($lang_code) {
    $CI = & get_instance();
    if ($lang_code && ($lang_code == $CI->config->item('lang_code_default'))) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function uploadImage($field_name = "userfile", $file_name = "", $folder = "artdatatemp/", $max_width = "4000", $max_height = "4000") {
    $CI = & get_instance();
    $config['upload_path'] = './uploads/' . $folder;
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '4096';
    $config['max_width'] = $max_width;
    $config['max_height'] = $max_height;
    if ($file_name) {
        $config['file_name'] = $file_name;
    } else {
        $config['encrypt_name'] = TRUE;
    }


    $CI->load->library('upload', $config);

    if (!$CI->upload->do_upload($field_name)) {
        return array('error' => $CI->upload->display_errors());
    } else {
        return array('upload_data' => $CI->upload->data());
    }
}

function resizeImage($imgsrc, $width, $height, $newimgsrc = "", $maintainratio = TRUE) {
    $CI = & get_instance();

    $config['image_library'] = 'gd2';
    $config['source_image'] = $imgsrc;
    if ($newimgsrc) {
        $config['new_image'] = $newimgsrc;
    }
    $config['maintain_ratio'] = $maintainratio;
    $config['width'] = $width;
    $config['height'] = $height;
    $CI->load->library('image_lib', $config);
    $CI->image_lib->resize();
    $CI->image_lib->clear();
}

function saveCroppedImage($x1, $x2, $y1, $y2, $imgsrc, $newimgsrc, $cropwidth = 100, $cropheight = 100) {
    $CI = & get_instance();
    $data = array();
    $filename = $imgsrc;
    $image_info = getimagesize($filename);
    if ($image_info['mime'] == 'image/png') {
        $image = imagecreatefrompng($filename);
    } else if ($image_info['mime'] == 'image/gif') {
        $image = imagecreatefromgif($filename);
    } else {
        $image = imagecreatefromjpeg($filename);
    }

    $width = imagesx($image);
    $height = imagesy($image);
    if (($x1 == "") && ($y1 == "") && ($x2 == "") && ($y2 == "")) {
        $x1 = $width / 2 - $cropwidth / 2;
        $x2 = $width / 2 + $cropwidth / 2;
        $y1 = $height / 2 - $cropheight / 2;
        $y2 = $height / 2 + $cropheight / 2;
    }
    $resized_width = ((int) $x2) - ((int) $x1);
    $resized_height = ((int) $y2) - ((int) $y1);
    //$resized_width = 340;  //We are maintaining the ratio in clientside. Now lets resize to our required size
    // $resized_height = 230;
    $resized_image = imagecreatetruecolor($resized_width, $resized_height);
    //$resized_image = imagecreatetruecolor(340, 230);
    imagecopyresampled($resized_image, $image, 0, 0, (int) $x1, (int) $y1, $width, $height, $width, $height);
    $new_file_name = $newimgsrc;
    imagejpeg($resized_image, $new_file_name);
    //$data['cropped_image'] = $img_name;
    //$data['cropped_image_axis'] = (int)$x1.",".(int)$y1.",".(int)$x2.",".(int)$y2;
    imagedestroy($resized_image);
    return true;
}

function afterSubmitProcess($pagename, $url, $msg, $type) {
    $CI = & get_instance();
    if (!$CI->input->is_ajax_request()) {
        redirectAlert($url, $msg, $type);
    } else {
        $url = $url ? base_url_tr($url) : "";
        showAlertGrowl($msg, $type, $url, $pagename);
    }
}

function showAlertGrowl($msg, $type, $url = "", $pagename = "") {
    echo json_encode(array("msg" => $msg, "type" => $type, "url" => $url, "pagename" => $pagename));
}

/**
 * Get user detail of current user (from session)
 * @param type $key Provide if only particular field should be returned (email,type,displayName,user_id,is_admin)
 * @return mixed
 */
function getUserdata($key = "") {
    $CI = & get_instance();
    if ($CI->session->userdata('userdata')) {
        $userdata = $CI->session->userdata('userdata');
        if ($key) {
            return @$userdata[$key];
        } else {
            return $userdata;
        }
    } else {
        return FALSE;
    }
}

/**
 * Helper function to get user detail
 * @param type $user_id
 * @param type $field if blank, all fields are returned as object. If NOT blank, only provided field is returned.
 * @return mixed
 */
function getUser($user_id, $field = "") {
    $CI = & get_instance();
    $CI->db->where("user_id", $user_id);
    $result = $CI->db->get("user");
    if ($result->num_rows()) {
        $row = $result->row();
        if ($field) {
            $row->$field;
        } else {
            return $row;
        }
    } else {
        return FALSE;
    }
}

function code_integer($code) {
    $x = substr($code, 1, 5);
    return (int) $x;
}

function code_alnum($code, $first_char = "B") {
    return $first_char . str_pad($code, 5, "0", STR_PAD_LEFT);
}

/**
 * 
 * @param type $id
 * This method use in category view for get product lot details with product id
 * @author Jahid al mamun<rjs.jahid11@gmail.com>
 */
function lot_details($id) {
    $CI = & get_instance();
    $CI->load->model('data');

    $result = $CI->data->getallbyid('lot_management', $id);
    return $result;
}

/**
 * This method use in header view top menu options
 * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
 * 
 */
function top_category_menu() {
    $CI = & get_instance();
    $CI->load->model('data');

    return $to_menu_category = $CI->data->top_category_menu();
//       print_r($to_menu_category);
}

/**
 * 
 * @param type $id
 * @return type
 * this method use for get category product in top menu
 * @author Jahid AL MAMUN <RJS.JAHID11@GMAIL.COM>
 */
function top_menu_product($id) {
    $CI = & get_instance();
    $CI->load->model('data');

    $lang_id = $CI->common_library->language_id();
    $top_category_product = $CI->data->menu_category_product($id, $lang_id);
    return $top_category_product;
}

/**
 * 
 * @param type $position
 * this method use for show banner in right postion
 * @author Jahid al mamun <rjs.jahid11@gmail.com>
 */
function banner_show($position) {
    $CI = & get_instance();
    $CI->load->model('data');
    $banner = $CI->data->banner_info($position);
    $banner_content = '';
    if ($banner != '') {
        foreach ($banner as $banner) {
            $banner_content .= '<a href="' . $banner->link . '" target="_blank"><img src="' . base_url() . 'img/banner/' . $banner->image . '" alt="" width="100%"></a>';
        }
    }

    echo $banner_content;
}

/**
 * this method use to show best offer in left sidebar best offer section 
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 */
function best_offer() {
    $CI = & get_instance();
    $CI->load->model('product');
    $best = $CI->product->promo();
    $best_offer = array_map("unserialize", array_unique(array_map("serialize", $best)));        

    $offer_content = '';
    if ($best_offer != '') {
        $count = 1;
        foreach ($best_offer as $best_offer) {
            if ($count <= 3) {
                if ($best_offer->type == 'p') {
                    $discount_value = $best_offer->price * $best_offer->discount / 100;
                    $price = $best_offer->price - $discount_value;
                } else {
                    $price = $best_offer->price - $best_offer->discount;
                }

                if ($best_offer->name != '') {
                    $offer_content .='<div class="offerPro" >
                    <div class="proImage">
                        <img src="' . base_url() . 'img/products/' . $best_offer->image . '" alt="" >
                    </div>
                    <div class="offer_content">
                    <div class="proName">
                        <a href="' . base_url_tr('products/details') . '/' . $best_offer->id . '/' . make_alias($best_offer->name) . '">' . $best_offer->name . '</a>
                    </div>
                    <div class="price">
                        <span class="price-new price product-price">' . '€' . $CI->cart->format_number($price) . '</span>
                        <span class="price-old old-price product-price">' . '€' . $CI->cart->format_number($best_offer->price) . '</span>
                    </div>
                    <a href="' . base_url_tr('products/details') . '/' . $best_offer->id . '/' . make_alias($best_offer->name) . '"><i class="fa fa-plus-square"></i>' . lang("details") . '</a>
                    
                    </div>
                    
                    </div>';
                }
                $count++;
            }
        }
    }
    echo $offer_content;
}

/**
 * 
 * @param type $product_id
 * @return type
 * this method use for get product discount from every view page with product id
 * @author jahid al mamun <rjs.jahid11@gmai.com>
 */
function show_discount($product_id) {
    $CI = & get_instance();
    $CI->load->model('product');
    $product_discount = $CI->product->product_discount($product_id);
    $discount = '';
    if ($product_discount) {
        $discount = array(
            'type' => $product_discount[0]->type,
            'discount' => $product_discount[0]->discount
        );
    }
    return $discount;
}

/**
 * 
 * @param type $id
 * @return type
 * this method use for show product vat in checkout index page and other page and call from every view page with product id
 * @author Jahid al mamun <rjs.jahid11@gmail.com>
 */
function show_product_vat($id) {
    $CI = & get_instance();
    $CI->load->model('product');
    return $CI->product->product_vat($id);
}

/**
 * 
 * @return type
 * this method use to get unread msg from user backend and call from user profile section
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 */
function unread_msg($id) {
    $CI = & get_instance();
    $CI->load->model('order_model');
    $count = $CI->order_model->unread_order_msg($id);
    echo $count[0]->unread;
}

/**
 * 
 * @param type $product_id
 * this method returns product description if there have no any language description is blank
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 */
function product_blank_desc($product_id) {
    $CI = & get_instance();
    $CI->load->model('product');
    $CI->db->select('product_lng.description,product_lng.meta_tag_title,product_lng.meta_tag_description,product_lng.meta_tag_keyword');
    $CI->db->where('id', $product_id);
    $CI->db->where('lng_id', 1);
    $CI->db->from('product_lng');
    $desc = $CI->db->get()->result();
    echo $desc[0]->description;
}

function cookie_bar() {
    $CI = & get_instance();
    $CI->load->helper('cookie');
    //get cookie value//rjs
    $social = get_cookie('social');
    $analytics = get_cookie('analytics');
    if ($social != '' && $analytics != '') {
        return 1;
    }
}



function product_discount($id) {
    $CI = & get_instance();
    $CI->load->model('product');
    return $discount_value = $CI->product->product_discount($id);
}

/*==================================
Replaces special characters with non-special equivalents
==================================*/
function normalize_special_characters($str)
{

//    # Quotes cleanup
//    $str = preg_replace( '/chr(ord("`"))/', "'", $str );        # `
//    $str = preg_replace( '/chr(ord("´"))/', "'", $str );        # ´
//    $str = preg_replace( '/chr(ord("„"))/', ",", $str );        # „
//    $str = preg_replace( '/chr(ord("`"))/', "'", $str );        # `
//    $str = preg_replace( '/chr(ord("´"))/', "'", $str );        # ´
//    $str = preg_replace( '/chr(ord("“"))/', "\"", $str );        # “
//    $str = preg_replace( '/chr(ord("”"))/', "\"", $str );        # ”
//    $str = preg_replace( '/chr(ord("´"))/', "'", $str );        # ´

//$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
//                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
//                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
//                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
//                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
//$str = strtr( $str, $unwanted_array );

# Bullets, dashes, and trademarks
//$str = preg_replace( '/chr(149)/', "&#8226;", $str );    # bullet •
//$str = preg_replace( '/chr(150)/', "&ndash;", $str );    # en dash
//$str = preg_replace( '/chr(151)/', "&mdash;", $str );    # em dash
//$str = preg_replace( '/chr(153)/', "&#8482;", $str );    # trademark
//$str = preg_replace( '/chr(169)/', "&copy;", $str );    # copyright mark
//$str = preg_replace( '/chr(174)/', "&reg;", $str );        # registration mark

    return $str;
}
function wishlist()
{
    $CI = & get_instance();
    $customer_id = $CI->session->userdata('customer_id');
    $wish = $CI->data->wishlist_count($customer_id);
    if(!empty($wish))
    {
    echo $wish[0]->total;
    }else{echo 0;}
}

function min_product_price($category_id){
    $CI = & get_instance();
    $min_price = $CI->db->select('MIN(sale_price) as avgprice')->join('product',"product.id = product_to_category.id",'left')->where('product_to_category.category_id',"$category_id")->get('product_to_category')->row();
    echo number_format($min_price->avgprice,2);
}

function show_place($id){
   $CI = & get_instance();
   $name = $CI->db->select('name')->where('id',$id)->get('area')->row();
   return $name->name;
}


?>
