<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_library {

    /**
     * 
     * @param type $to
     * @param type $subject
     * @param type $message
     * @return boolean
     * This method use for send email and call from different types of controller with message/subject/to information
     * @author Jahid al mamun <rjs.jahid11@gmail.com>
     * 
     */
    function send_mail($to,$subject,$message,$from_email='')
    {
        $CI = & get_instance();
        $CI->load->model('data');
        $email_setting = $CI->data->email_setting();
        
        //get email setting information//rjs
        $protocol = $email_setting[0]->protocol;
        $smtp_host = $email_setting[0]->smtp_host;
        $smtp_port = $email_setting[0]->smtp_port;
        $smtp_user = $email_setting[0]->smtp_user;
        $smtp_pass = $email_setting[0]->smtp_pass;
        $mailtype = $email_setting[0]->mailtype;
        $charset = $email_setting[0]->charset;
        $wordwrap = $email_setting[0]->wordwrap;
        $from_name = $email_setting[0]->from_name;
        if($from_email=='')
        {
            $from_email = $email_setting[0]->from_email;
        }
        
        $config = Array(
        'protocol' => $protocol,
        'smtp_host' => $smtp_host,
        'smtp_port' => $smtp_port,
        'smtp_user' => $smtp_user, 
        'smtp_pass' => $smtp_pass, 
        'mailtype' => $mailtype,
        'charset' => $charset,
        'wordwrap' => $wordwrap
    );

       
      $CI->load->library('email', $config);
      $CI->email->set_newline("\r\n");
      $CI->email->from("$from_email"); 
      $CI->email->to($to);
      $CI->email->subject($subject);
      $CI->email->message($message);
      if($CI->email->send())
     {
          return TRUE;
     }
     else
    {
         return show_error($CI->email->print_debugger());
    }

}


}