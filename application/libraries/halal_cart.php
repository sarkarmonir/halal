<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of artdata_cart
 */
class Halal_cart {
    
    function __construct() {
//        $ci =& get_instance();
//        $ci->load->library('cart');
    }
    /**
     * This is the main function to add items in cart
     * @param array $options
     */
    
    public function addtocart($options){
       
        $ci =& get_instance();
        $ci->load->library('cart');
        $itemExist = false;
        $this->check_product_validity($options);
        $rowid = $this->is_item_exist_in_cart($options);
        //$ci->cart->destroy();
        
        if($rowid){
            $options['rowid'] = $rowid[0]; //adding rowid and 
            $options['qty'] = $rowid[1];   //update quantity
         
            $ci->cart->update($options);  //updating this item in cart
        }else{
            $ci->cart->insert($options);  //inserting item in cart
        }
        $cart = $ci->cart->contents();
        $total = 0;
        $subtotal = 0;
        $cart_itemnumber = 0;
        
        foreach($cart as $rowid=>$item){
         $total = $total + $item['price'];
         $subtotal = $subtotal + $item['sale_price'];
         $cart_itemnumber++;
        }
        $total = number_format($total,2);
        $subtotal = number_format($subtotal,2);
        
        $add_cart_product = '<table cellspacing="0" class="table">
                                        <tbody>
                                            <tr class="cart-subtotal">
                                                <th>Subtotal</th>
                                                <td><span class="amount">$'.$subtotal.'</span></td>
                                            </tr>
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td><strong><span class="amount">$'.$total.'</span></strong> </td>
                                            </tr>
                                        </tbody>
                                    </table>';
//        foreach($cart as $rowid=>$item){
//        }
        
        //echo $add_cart_product;
        $response['mini_html'] = $add_cart_product;
        $response['cart_number'] = $cart_itemnumber;
        echo json_encode($response);
//        echo $this->get_mini_cart();
    }
    
    /**
     * This function checks if all required fields are available
     * @param type $options
     */
    public function check_product_validity($options){
        $req_fields = array("id","qty","name"); //Required fields. Price will be retreived from database
        foreach($req_fields as $field){
            if(array_key_exists($field,$options)){
                if($options[$field] != ""){
                    continue;
                }else{
                    echo "product $field is missing";
                    exit();
                }
            }else{
                //echo "Invalid Product";
                exit();
            }
        }
    }
    
    /**
     * return rowid if exist. Otherwise false
     * @param array $options
     * @return mixed
     */
    public function is_item_exist_in_cart($options){
        $ci =& get_instance();
        $ci->load->library('cart');
        $cart = $ci->cart->contents();
        foreach($cart as $rowid=>$item){
            
            if($item['id'] == $options['id'] ){
                $quantity = $item['qty']+$options['qty'];
                return $rowid =array('0'=>$rowid,'1'=>$quantity);
            }
           
        }
        return FALSE;
    }
    
    public function get_mini_cart(){
        $ci =& get_instance();
        
        $cart = $ci->cart->contents();
        $data['cart_freetrade'] = array();
        $data['cart_service'] = array();
        foreach($cart as $rowid=>$item){
            if($item['type'] == "freetrade"){
                $data['cart_freetrade'][] = $item;
            }else{
                $data['cart_service'][] = $item;
            }
        }
 
        $data['cart_count'] = count($data['cart_freetrade']) + count($data['cart_service']);
        return $ci->load->view('mini_cart',$data,TRUE);
    }
    
    public function delete_from_cart($id){
        
        $ci =& get_instance();
        $cart = $ci->cart->contents();
        $rowid1 = "";
        foreach($cart as $rowid=>$item){
            if($item['rowid'] == $id){
                $cart[$rowid]['qty'] = 0;
                $rowid1 = $rowid;
                break;
            }
        }      
        $ci->cart->update($cart);
    }
    
    public function qtyplus_from_cart($id){
        
        $ci =& get_instance();
        $cart = $ci->cart->contents();
        $rowid1 = "";
        foreach($cart as $rowid=>$item){
            if($item['rowid'] == $id){
                $cart[$rowid]['qty'] = $item['qty']+1;
                $rowid1 = $rowid;
                break;
            }
        }      
        $ci->cart->update($cart);
    }
    
    public function qtyminus_from_cart($id){
        
        $ci =& get_instance();
        $cart = $ci->cart->contents();
        $rowid1 = "";
        foreach($cart as $rowid=>$item){
            if($item['rowid'] == $id){
                
                $cart[$rowid]['qty'] = $item['qty']-1;
                $rowid1 = $rowid;
                break;
            }
        }      
        $ci->cart->update($cart);
        
    }

    public function update_cart_qty($id, $qty){
        
        $ci =& get_instance();
        $cart = $ci->cart->contents();
        $rowid1 = "";
        foreach($cart as $rowid=>$item){
            if($item['rowid'] == $id){
                $cart[$rowid]['qty'] = $qty;
                $rowid1 = $rowid;
                break;
            }
        }      
        $ci->cart->update($cart);
    }
 
   
   /**
    * 
    * @return boolean
    * This method use for destroy all cart item and empty
    * @author Jahid Al mamun <rjs.jahid11@gmail.com>
    */
   public function clear_cart(){
    if($this->cart->destroy()){
        // Destroy all cart data
        return TRUE;
    }else
    {
        return FALSE; 
    }
    }
   

    
}
