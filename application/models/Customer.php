<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * 
 * @copyright  Copyright (C) 2017 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
class Customer extends CI_Model
{
        /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        $this->table = 'customers';
        parent :: __construct();
    }
    /**
     * Add New customer in database table customers
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function add()
    {
        $password = md5($this->input->post('password'));
        $value = array(
            'f_name'=>         $this->input->post('f_name'),
            'email'=>         $this->input->post('email'),
            'telephone'=>     $this->input->post('telephone'),
            'password'=>      $password,
            'fax'=>           $this->input->post('fax'),
            'ip'=>            'Add by Admin',
            'newsletter'=>    $this->input->post('newsletter'),
            'status'=>        $this->input->post('status')
        );
        
        /**
         * call data model for save basic data in customer table and return customer
         * @author rjs
         */
         $customer = $this->data->save($this->table,$value);
         if($customer)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
    
    /**
     * Add New customer in database table customers
     *@author Jahid Al Mamun
     * @param bool tru/false
     */
    public function registration()
    {
        $password = md5($this->input->post('password'));
        $ip = $_SERVER['REMOTE_ADDR'];

        $value = array(
            'title'=>         $this->input->post('title'),
            'f_name'=>        $this->input->post('f_name'),
            'l_name'=>        $this->input->post('l_name'),
            'group'=>         $this->input->post('type'),
            'email'=>         $this->input->post('email'),
            'telephone'=>     $this->input->post('telephone'),
            'address_1'=>     $this->input->post('address_1'),
            'address_2'=>     $this->input->post('address_2'),
            'password'=>      $password,
            'fax'=>           $this->input->post('fax'),
            'city'=>          $this->input->post('city'),
            'place'=>          $this->input->post('place'),
            'postcode'=>      $this->input->post('postcode'),
            'agree'=>         $this->input->post('agree'),
            'ip'=>            $ip,
            'newsletter'=>    $this->input->post('newsletter'),
            'status'=>        1
        );
        
        /**
         * call data model for save basic data in customer table and return customer
         * @author rjs
         */
         $customer = $this->data->save($this->table,$value);
         //update newsletter data in newsletter table
         $newsletter = array(
             'email'=>$this->input->post('email'),
             'status'=>$this->input->post('newsletter')
         );
         $newsletter = $this->data->save('newsletter',$newsletter);
         if($customer)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }
    /**
     * 
     * @param type $id
     * @return boolean
     */
    public function edit($id)
    { 
        if($this->input->post('password') !='')
            {
            $password = md5($this->input->post('password'));
             $value = array(
            'f_name'=>        $this->input->post('f_name'),
            'email'=>         $this->input->post('email'),
            'telephone'=>     $this->input->post('telephone'),
            'city'=>          $this->input->post('city'),
            'place'=>          $this->input->post('place'),
            'password'=>      $password,
            'fax'=>           $this->input->post('fax'),
            'ip'=>            'Add by Admin',
            'newsletter'=>    $this->input->post('newsletter'),
            'status'=>        $this->input->post('status')
        );
            }  else {
            $value = array(
            'f_name'=>         $this->input->post('f_name'),
            'email'=>         $this->input->post('email'),
            'telephone'=>     $this->input->post('telephone'),
            'city'=>          $this->input->post('city'),
            'place'=>          $this->input->post('place'),
            'fax'=>           $this->input->post('fax'),
            'ip'=>            'Add by Admin',
            'newsletter'=>    $this->input->post('newsletter'),
            'status'=>        $this->input->post('status')
        );  
            }
        
        /**
         * call data model for save basic data in customer table and return customer
         * @author rjs
         */
         $customer = $this->data->update($this->table,$id,$value);
         if($customer)
         {
             return TRUE;
         }  else {
             return FALSE;    
         }
        
    }


    /**
     * 
     * @param type $id
     * @return type
     * Tthis method use for get order list with customer id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function orders($id)
    {
        $this->db->select('orders.*,customers.f_name,customers.l_name,customers.m_name');

        $this->db->join('customers', 'orders.user_id = customers.id');
        $this->db->where('pay_status !=','wait');
        $this->db->order_by("id", "DESC");
        
        $this->db->where('orders.user_id',$id);
        $this->db->from('orders');
        
        return $this->db->get()->result();
    }
   /**
    * 
    * @param type $id
    * @return type
    * This method use for get total item on single order
    * @author Jahid Al mamun <rjs.jahid11@gmail.com>
    */
    public function order_items($id)
    {
        $this->db->select('*');
        $this->db->where('order_id',$id);
        $this->db->from('order_items');
        return $this->db->get()->result();
    }
    /**
     * 
     * @param type $shipping_id
     * @return type
     * this method use for get order customer shipping address
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
    public function shipping_address($shipping_id)
    {
        $this->db->select('customer_shipping_address.*');
        $this->db->where('shipping_id',$shipping_id);
        $this->db->from('customer_shipping_address');
        return $this->db->get()->result();
    }

}
