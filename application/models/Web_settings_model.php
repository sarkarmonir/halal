<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Web_settings_model extends CI_Model {

    function __construct() {
        parent::__construct();

    }

    function clear_cache() {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

// ##############################################################################################
//  front end
// ##############################################################################################

    // get_general_settings --------------+-------------------------------------
    public function get_general_settings() {
        $r = $this->db->select('*')->from('web_general_settings')->where('id', '1')->get();
        return $result = $r->row();
    }

    // get slider --------------------------------------------------------------
    public function get_slider() {
        $r = $this->db->select('*')->from('web_slider')->get();
        return  $result = $r->result();
    }

    // get wlc -----------------------------------------------------------------
    public function get_wlc() {
        $r = $this->db->select('*')->from('web_wlc')->where('id', '1')->get();
        return $result = $r->row();
    }  

    // get offer ---------------------------------------------------------------
    public function get_offer() {
        $r = $this->db->select('*')->from('web_offers')->get();
        return  $result = $r->result();
    }
    // get last offer  ---------------------------------------------------------
    public function last_offer() {
        //$r = $this->db->select('id, title, desc, img')->from('web_offers')->order_by('id, title, desc, img', 'desc')->limit(1)->get();
        $r = $this->db->select('*')->from('web_offers')->limit(1)->get();
        return  $result = $r->row();
    }

    // get testimonial ---------------------------------------------------------
    public function get_testimonial() {
        $r = $this->db->select('*')->from('web_testimonial')->get();
        return  $result = $r->result();
    }

    // get last farm -----------------------------------------------------------
    public function last_farm() {
        $r = $this->db->select('*')->from('web_our_farm')->limit(1)->get();
        return  $result = $r->row();
    }

    // get farm gallery --------------------------------------------------------
    public function get_farm_gallery() {
        $r = $this->db->select('*')->from('web_farm_gallery')->get();
        return  $result = $r->result();
    }

    // get_about_us_ -----------------------------------------------------------
    public function get_about_us() {
        $r = $this->db->select('*')->from('web_about_us')->where('id', '1')->get();
        return $result = $r->row();
    }


    // get faq -----------------------------------------------------------------
    public function get_faq() {
        $r = $this->db->select('*')->from('web_faq')->get();
        return  $result = $r->result();
    }

    // get privacy -------------------------------------------------------------
    public function get_privacy() {
        $r = $this->db->select('*')->from('web_privacy_policy')->where('id', '1')->get();
        return $result = $r->row();
    }


}