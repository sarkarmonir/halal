<?php
/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * @date: 2015-9-10
 * 
 * @copyright  Copyright (C) 2015 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Model used in product models for category/brand/product type/product/
 * @package product
 * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
 */
class Data extends CI_Model {

    /**
     * this method use for all data retrive with table name
     * @author Jahid All Mamun
     */
    public function getall($table) {
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    /**
     * This function use for get all data with check status = 1
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $table
     * @return type
     */
    public function getall_with_status($table) {
        $this->db->select('*');
        $this->db->where('status','1');
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    /**
     * this method use for one data or rows retrive with table name and id
     * @author Jahid All Mamun
     */
    public function getone($table, $id) {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    
     public function getcat($table, $id) {
        $this->db->where('id', $id);
        $this->db->where('type', 'clipart');
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
        /**
     * this method use for one data or rows retrive with table name and id and status = 1
     * @author Jahid All Mamun
     */
    public function getone_with_status($table, $id) {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

    /**
     * delete row with table name and id
     * @author Jahid All Mamun
     */
    public function delete($table, $id) {
        $this->db->where('id', $id);
        $resul = $this->db->delete($table);
        if ($resul) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This method use for save single row with table name and id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @param array value save with table name provided
     */
    public function save($table, $data) {
 
        $result = $this->db->insert($table, $data);
        if ($result) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * update table value with table name / id / and array value
     * @author Jahid All Mamun
     */
    function update($table, $id, $data) {
        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;   
         }
    }
    
    /**
     * 
     * @param type $table
     * @param type $id
     * @param type $data
     * @return boolean
     * this function use to update page update
     * @author JAHID AL MAMUN
     */
        function page_update($table, $id,$lang_id, $data) {
        $this->db->where('id', $id);
        $this->db->where('lng_id', $lang_id);
        $result = $this->db->update($table, $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;   
         }
    }
    
        /**
     * update table value with table name / id / and array value
     * @author Jahid All Mamun
     */
    function art_update($table, $id, $data) {
        $this->db->where('clipart_id', $id);
        $result = $this->db->update($table, $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;   
         }
    }
    /**
     * 
     * @param type $id
     * @param type $data
     * @return boolean
     * this method use for active customer registration
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     */
     function active_acount($id, $data) {
        $this->db->where('password', $id);
        $result = $this->db->update('customers', $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;   
         }
    }
    
        /**
     * update table value with table name / id / and array value
     * @author Jahid All Mamun
     */
    function updateall($table, $data) {
//        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;   
         }
    }
    /**
     * get all by id
     * @author jahid al mamun
     */
    public function getallbyid($table, $id) {
        $this->db->where('id', $id);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    /**
     * 
     * This method use for get all data with id with status = 1
     * @author Jahid Al mamun <rjs.jahid11@gmail.com>
     * @param type $table
     * @param type $id
     * @return type
     */
    public function getallbyid_with_status($table, $id) {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    /**
     * check language exist with id and language id
     * @author Jahid Al Mamun
     * @param bool 
     */
    public function lang_exist($table,$id,$lng_id)
    {
        $status = $this->db->where('id',$id)
                ->where('lng_id',$lng_id)
                ->from($table)
                ->get();
        if($status->num_rows())
		return TRUE;
		else
		return FALSE;
    }
    
    function blank_replace_all($text) { 
    $text = strtolower(htmlentities($text)); 
    $text = str_replace(get_html_translation_table(), "_", $text);
    $text = str_replace(" ", "_", $text);
    $text = preg_replace("/[-]+/i", "_", $text);
    return $text;
}
    /**
     * this method retrive region data with country id
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $country_id
     * @return type
     * 
     */
    public function get_region($country_id)
    {
        $query = $this->db->query("select *from region where country_id = $country_id");
        return $query->result();

    }
    /**
     * This method retrive city data with country id
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $country_id
     * @return type
     */
    public function get_city($country_id)
    {
        $query = $this->db->query("select *from city where country_id = $country_id");
        return $query->result();

    }
    
     /**
     * 
     * @param type $region_id
     * @return type
     * @author jahid al mamun
     */
        public function get_region_city($region_id)
    {
        $query = $this->db->query("select *from city where region_id = $region_id order by name asc");
        return $query->result();

    }
    /**
     * This method use for check valid email or not for different purpose as like forget password reset
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @param type $table
     * @param type $email
     * @return type
     */
    public function valid_email($table,$email)
    { 
        $this->db->where('email',$email);
        $this->db->from($table);
        return $this->db->get()->result();
       
      
    }
    

    public function child($id)
    {
        $this->table = 'category';
        $this->db->where('parent_id',$id);
        $this->db->from($this->table);
        return $child = $this->db->get()->result();
        
    }

    public function categories()
    {
       
        $this->table = 'dg_categories';
        $this->db->where('parent_id',0);
        $this->db->where('type','clipart');
        $this->db->or_where('top',1);
        $this->db->order_by('order','asc');
        $this->db->from($this->table);
        $parent_categories = $this->db->get()->result();
        
        $serial = 1;
        //get root parent id // sub parent id // and active category id //
        $active_id = $this->session->userdata('category_id');
        
        if($active_id==NULL){$active_id =1;}
        $p_a = $this->getcat('dg_categories', $active_id);
        $sub_paid = NULL;
        $paid = NULL;
        if($p_a !=NULL){
        
        if($p_a[0]->parent_id ==0){$paid =$p_a[0]->id; }else{
            $sub_paid = $p_a[0]->parent_id;
            $p_a = $this->getcat('dg_categories', $p_a[0]->parent_id );
            if($p_a[0]->parent_id ==0){$paid =$p_a[0]->id; }else{
                $p_a = $this->getcat('dg_categories', $p_a[0]->parent_id );
                if($p_a[0]->parent_id ==0){$paid =$p_a[0]->id; }else{
                    $paid =0;
                }
            }
        }
        }
//        echo $paid."-".$sub_paid."-".$active_id;
        $menu = '<nav class="sidebarMenu-nav">';
        $menu .= '<ul id="menu2">';
        foreach ($parent_categories as $parent)
        {
            //parent loop
            if($paid == $parent->id){$menu .='<li class="active">';}else{$menu .='<li >';}
            $menu .='<a href="'.  base_url_tr("category/".$parent->slug).'">'.$parent->title.'<span class="glyphicon arrow"></span></a>';
            $this->db->where('parent_id',$parent->id);
            $this->db->from($this->table);
            $child_categories = $this->db->get()->result();
            $child = sizeof($child_categories);
            if(!empty($child_categories))
            {
               //check child
                if($paid == $parent->id){$menu .=' <ul class="collapse" style="height: 0px;">';}else{$menu .=' <ul class="collapse" style="height: 0px;">';}
                $serial++;
                foreach ($child_categories as $child)
                 {  
                    // child loop
                    if($active_id == $child->id){$menu .='<li class="nav-submenu-item active">';}else{$menu .='<li class="nav-submenu-item">';}
                    
                    $menu .='<a href="'.  base_url_tr("category/".$child->slug).'">'.$child->title.'<span class="glyphicon arrow"></span><span class="fa arrow"></span></a>';
                    $this->db->where('parent_id',$child->id);
                    $this->db->from($this->table);
                    $child_sub_category = $this->db->get()->result();
                    $sub_size = sizeof($child_sub_category);
                    if(!empty($child_sub_category))
                    {
                        //check sub child
                        if($sub_paid == $child->id){$menu .=' <ul class="collapse" style="display: block;" >';}else{$menu .=' <ul class="collapse">';}
//                        $menu .='<ul class="nav-submenu">';
                        foreach ($child_sub_category as $child_sub)
                        {
                            //sub child loop
                            if($active_id == $child_sub->id){$menu .='<li class="nav-submenu-item active">';}else{$menu .='<li class="nav-submenu-item">';}
                            
                            $menu .='<a href="'.  base_url_tr("category/".$child_sub->alias).'">'.$child_sub->title.'<span class="fa plus-minus"></span></a></li>';
    
                        }
                        $menu .='</ul></li>';
                      
                    }else
                    {
                       $menu .='</li>'; 
                    }
                }
                $menu .='</ul>';
            
            }else{$menu .='</li>';}
            $menu .='</li>'; 
        }
        $menu .='</ul></nav>';
        $this->session->set_userdata('category_id',0);
//        print_r($menu);
//        die();
        return $menu;
    }
    
//    public function categories()
//    {
//        $olx = 0;
//        $this->table = 'dg_categories';
//        $this->db->where('parent_id',0);
//        $this->db->or_where('top',1);
//        $this->db->where('type','clipart');
//        $this->db->order_by('order','asc');
//        $this->db->from($this->table);
//        $parent_categories = $this->db->get()->result();
//        $serial = 1;
//        //get root parent id // sub parent id // and active category id //
//        $active_id = $this->session->userdata('category_id');
//        if($active_id==NULL){$active_id =81;}
//        $p_a = $this->getone('dg_categories', $active_id);
//        $sub_paid = NULL;
//        $paid = NULL;
//        if($p_a !=NULL){
//        
//        if($p_a[0]->parent_id ==0){$paid =$p_a[0]->id; }else{
//            $sub_paid = $p_a[0]->parent_id;
//            $p_a = $this->getone('dg_categories', $p_a[0]->parent_id );
//            if($p_a[0]->parent_id ==0){$paid =$p_a[0]->id; }else{
//                $p_a = $this->getone('dg_categories', $p_a[0]->parent_id );
//                if($p_a[0]->parent_id ==0){$paid =$p_a[0]->id; }else{
//                    $paid =0;
//                }
//            }
//        }
//        }
////        echo $paid."-".$sub_paid."-".$active_id;
//        $menu = '<nav class="sidebarMenu-nav">';
//        $menu .= '<ul id="menu2">';
//        foreach ($parent_categories as $parent)
//        {
//            //parent loop
//            $menu .='<li class="nav-item ">';
//            $menu .='<a href="'.  base_url_tr("category/".$parent->slug).'">'.$parent->title.'</a>';
//            $this->db->where('parent_id',$parent->id);
//            $this->db->where('type','clipart');
//            $this->db->order_by('order','asc');
//            $this->db->from($this->table);
//            $child_categories = $this->db->get()->result();
//            $child = sizeof($child_categories);
//            if(!empty($child_categories))
//            {
//               //check child
//                if($paid == $parent->id){$menu .=' <ul class="collapse" style="height: 0px;">';}else{$menu .=' <ul class="collapse" style="height: 0px;">';}
////                if($paid == $parent->id){$menu .=' <ul class="nav-submenu" style="display: block;" >';}else{$menu .=' <ul class="nav-submenu" >';}
//                $serial++;
//                foreach ($child_categories as $child)
//                 {  
//                    // child loop
//                    if($active_id == $child->id){$menu .='<li class="nav-submenu-item active">'; $olx = 1;}else{$menu .='<li class="nav-submenu-item">';}
//                    
//                    $menu .='<a href="'.  base_url_tr("category/".$child->slug).'">'.$child->title.'</a>';
//                    $this->db->where('parent_id',$child->id);
//                    $this->db->where('type','clipart');
//                    $this->db->order_by('order','asc');
//                    $this->db->from($this->table);
//                    $child_sub_category = $this->db->get()->result();
//                    $sub_size = sizeof($child_sub_category);
//                    if(!empty($child_sub_category))
//                    {
//                        //check sub child
////                        if($sub_paid == $child->id){$menu .=' <ul class="collapse" style="display: block;" >';}else{$menu .=' <ul class="collapse">';}
//                        if($sub_paid == $child->id){$menu .='<ul class="nav-submenu" style="display: block;" >';}elseif($olx ==1){$menu .='<ul class="nav-submenu" style="display: block;" >';$olx =0;}else{$menu .=' <ul class="nav-submenu" >';}
////                        $menu .='<ul class="nav-submenu">';
//                        foreach ($child_sub_category as $child_sub)
//                        {
//                            //sub child loop
//                            if($active_id == $child_sub->id){$menu .='<li class="nav-submenu-item active">';}else{$menu .='<li class="nav-submenu-item">';}
//                            
//                            $menu .='<a href="'.  base_url_tr("category/".$child_sub->alias).'">'.$child_sub->name.'</a></li>';
//    
//                        }
//                        $menu .='</ul></li>';
//                      
//                    }else
//                    {
//                       $menu .='</li>'; 
//                    }
//                }
//                $menu .='</ul>';
//            
//            }else{$menu .='</li>';}
//            $menu .='</li>'; 
//        }
//        $menu .='</ul></nav>';
//        $this->session->set_userdata('category_id',0);
//        return $menu;
//    }
    /**
     * 
     * @param type $code
     * @return type
     * Tthis method use for get coupon information with coupon code
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     */
   public function coupon_info($code)
   {
        $this->db->select('*');
        $this->db->where('code',$code);
        $this->db->from('coupon');
        $coupon = $this->db->get()->result();
        return $coupon;
   }
   /**
    * 
    * @return type
    * This method use for get top category meny root
    * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
    */
   public function top_category_menu()
   {
       $this->db->select('id,name');
       $this->db->limit(3);
       $this->db->where('top',1);
       $this->db->from('category');
       return $this->db->get()->result();
       
   }
   
   public function menu_category_product($id,$lang_id)
   {
       $query = $this->db->query("select product.id,product.name,product.product_type,product.category_id,product.lot_type,"
                . "product.feature_product,product.brand,product.model,product.sku,product.ean,product.quantity,"
                . "product.price,product_lng.meta_tag_title,product_lng.description,product_lng.meta_tag_description,"
                . "product_lng.meta_tag_keyword,product.image,count(order_items.product_id) as best_sell from product"
                . " left join product_to_category  ON product_to_category.id = product.id "
                . " left join product_lng  ON product_lng.id = product.id AND product_lng.lng_id = $lang_id"
                . " left join order_items ON order_items.product_id = product.id"
                . " where product_to_category.category_id = $id group by order_items.product_id order by best_sell desc");
       
        return $query->result();
   }
   /**
    * 
    * @param type $position
    * This method use for get banner information from common helper banner_show function
    * @author Jahid Al mamun <rjs.jahid11@gmail.com>
    */
   public function banner_info($position)
   {
       $this->db->select('*');
       $this->db->where('position',$position);
       $this->db->where('status',1);
       $this->db->from('banner');
       $this->db->order_by('id');
       return $this->db->get()->result();
   }
   
   public function email_setting()
   {
       $this->db->select('*');
       $this->db->from('email_setting');
       return $this->db->get()->result();
   }
   //retrun site email
   public function site_email()
   {
       $this->db->select('site_email');
       $this->db->from('site_setting');
       $result = $this->db->get()->result();
       $email = $result[0]->site_email;
       return $email;
   }
   
   public function quantity_marge($product_id,$item_quantity)
   {
       $this->db->select('quantity');
       $this->db->where('id',$product_id);
       $this->db->from('product');
       $result = $this->db->get()->result();
       $quantity = $result[0]->quantity;
       $marg_quantity = $quantity - $item_quantity;
       $value = array(
           'quantity' => $marg_quantity
       );
       $this->db->where('id',$product_id);
       $this->db->update('product',$value);
       return TRUE;
   }
   
   public function art_product($search_val)
   {
       $this->db->select('*');
       $this->db->like('name',"$search_val");
       $this->db->from('product');
       return $this->db->get()->result();
       
   }

   public function clipart_product_color($product_id)
   {
       $this->db->select('*');
       $this->db->join('products_design',"products_design.product_id=product.id",'left');
       $this->db->where('product.id',"$product_id");
       $this->db->from('product');
       return $this->db->get()->row();
       
   }
   
   /**
    * this method use to check exist clipart product before entry time
    * @author jahid al mamun
    */
   public function check_clip_product($product_id,$clipart_id)
   {
       $this->db->select('*');
       $this->db->where('clipart_id',$clipart_id);
       $this->db->where('product_id',$product_id);
       $this->db->from('clipart_product');
       $result  = $this->db->get()->result();
       if($result)
       {
           return true;
       }else{
           return false;
       }
   }
   public function delete_art_product_color($product_id,$clipart_id,$color)
   {
       $this->db->where('product_id',$product_id);
       $this->db->where('clipart_id',$clipart_id);
       $this->db->where('color',$color);
       $result = $this->db->delete('clipart_product_color');
       if($result)
       {
           return true;
       }  else {
       return false;    
       }
   }
   
   public function remove_art_product($product_id,$clipart_id)
   {
       //clipart product delete
       $this->db->where('product_id',$product_id);
       $this->db->where('clipart_id',$clipart_id);
       $product_del = $this->db->delete('clipart_product');
       
       //clipart product color delete
       $this->db->where('product_id',$product_id);
       $this->db->where('clipart_id',$clipart_id);
       $product_color_del = $this->db->delete('clipart_product_color');
       return true;
   }
   
   public function clipart_product_list($id)
   {
       $this->db->select('*');
       $this->db->where('clipart_id',$id);
       $this->db->from('clipart_product');
       return $this->db->get()->result();
       
   }
   
   public function clipart_product_selected_color($p_id,$id)
   {
       $this->db->select('*');
       $this->db->where('product_id',$p_id);
       $this->db->where('clipart_id',$id);
       $this->db->from('clipart_product_color');
       return $this->db->get()->result();
       
   }
   
   public function set_default_product($product_id,$clipart_id)
   {
       $set = array(
           'product_status'=>1
       );
       $unset = array(
           'product_status'=>0
       );
       $this->db->update('clipart_product',$unset);
       
       $this->db->where('product_id',$product_id);
       $this->db->where('clipart_id',$clipart_id);
       $this->db->update('clipart_product',$set);
   }
  public function get_first($table)
  {
      $this->db->select('*');
      $this->db->limit(1);
      $this->db->from($table);
      return $this->db->get()->row();
  }
  
  public function get_default_product($table)
  {
      $this->db->select('*');
      $this->db->where('default',1);
      $this->db->limit(1);
      $this->db->from($table);
      return $this->db->get()->row();
  }
  
  public function all_country()
    {
        $this->db->select('*');
        $this->db->order_by('country_shortName','asc');
        $this->db->from('country');
        return $this->db->get()->result();
    }
    
    public function art_tag_update($art_id,$art_cat_id,$value)
    {
        $this->db->where('clipart_id',$art_id);
        $check = $this->db->update('dg_cliparts',$value);
        if($check){            redirect('art');}  else {
echo "no"; die();    
}
        
        
        
    }
    
    public function clipart_tags($id)
    {
        $this->db->select('tags');
        $this->db->where('clipart_id',$id);
        $this->db->from('dg_cliparts');
        return $this->db->get()->row();
    }
    /**
     * this function use to set default product
     * @author JAHID AL MAMUN <rjs.jahid11@gmail.com>
     */
    
     
   public function privacy($lang_id)
   {
       $this->db->select('page.*, page_lng.description');
       $this->db->where('page.id',2);
       $this->db->join('page_lng',"page_lng.id=page.id and page_lng.lng_id = $lang_id",'left');
       $this->db->from('page');
       return $this->db->get()->row();
   }
   
     /**
     * Duplicate of above function to replace it in forget password 
     * @author Ashikur Rahman
     * @param type $table
     * @param type $email
     * @return type
     */
    public function valid_email_1($table,$email)
    { 
        $this->db->where('email',$email);
        $this->db->from($table);
        return $this->db->get();
    }
    /**
    * 
    * @param type $id
    * @param type $lng_id
    * @return type
    * this method use to get page information 
    * @author jahid al mamun <rjs.jahid11@gmal.com>
    */
   public function page_info($id,$lng_id)
   {
       $this->db->select('*');
       $this->db->join('page_lng',"page_lng.id=page.id and page_lng.lng_id = $lng_id",'left');
       $this->db->where('page.id',$id);
       $this->db->from('page');
       return $this->db->get()->result();
   } 
    public function home_page($lang_id)
    {
        $this->db->select()->where('id',1)->where('lng_id')->get()->result();
    }
    
    public function feature_product(){
        $allItem = $this->db->select('*')->where('feature_product',1)->where('status',1)->get('product')->result();
        $modifPrice = [];

        //now calculation discount of all items
        foreach ($allItem as $item) {

            $discount = $this->check_product_availability_and_discount($item->id, 0, $item->sale_price);
            if ($discount) {
                $item->sale_price = ceil($item->sale_price);
                $item->price = $discount['product_discount_price'];
                $item->save_amount = $discount['save_amount'];
                $item->discount_apply = 1;
                $modifPrice[] = $item;
            } else {
                $modifPrice[] = $item;
                $item->discount_apply = 0;
                $item->price = ceil($item->sale_price);
                $item->save_amount = 0;
            }
        }


        return $modifPrice;
        }
    
    public function category_products($category_id){
        //echo $category_id; die();
        $this->db->select('*');
        $this->db->from('product_to_category');
        $this->db->join('product', 'product.id = product_to_category.id','left');
        $this->db->where('product_to_category.category_id', $category_id);
        $allItem = $this->db->get()->result();
        //echo "<pre>"; print_r($allItem); die();
        $modifPrice = [];

        //now calculation discount of all items
        foreach ($allItem as $item) {

            $discount = $this->check_product_availability_and_discount($item->id, 0, $item->sale_price);
            if ($discount) {
                $item->sale_price = ceil($item->sale_price);
                $item->price = $discount['product_discount_price'];
                $item->save_amount = $discount['save_amount'];
                $item->discount_apply = 1;
                $modifPrice[] = $item;
            } else {
                $modifPrice[] = $item;
                $item->discount_apply = 0;
                $item->price = ceil($item->sale_price);
                $item->save_amount = 0;
            }
        }


        return $modifPrice;
        
    }
    
    function check_product_availability_and_discount($product_id, $product_qty, $price) {

        //  now check all apply discount on this product
        $today = date('Y-m-d');
        $discount = $this->db->select('discount.id, discount.discount,discount.type')->order_by('discount.id', 'desc')->limit(1)->where('date_start <=', $today)->where('date_end >=', $today)->join('discount_product', "discount_product.id = discount.id AND discount_product.product_id = $product_id")->where('discount.status', 1)->get('discount')->row();
        $response = [];
        if (!empty($discount)) {
            if ($discount->type == 'p') {
                $product_price = $price;
                $discount_price = $product_price * $discount->discount / 100;
                $product_price = $product_price - $discount_price;
                $product_discount_price = ceil($product_price);
                $response['product_sale_price'] = ceil($price);
                $response['product_discount_price'] = ceil($product_discount_price);
                $response['save_amount'] = $response['product_sale_price'] - $response['product_discount_price'];
            } else {
                $product_price = $price;
                $product_price = $product_price - $discount->discount;
                $product_discount_price = ceil($product_price);
                $response['product_sale_price'] = ceil($price);
                $response['product_discount_price'] = ceil($product_discount_price);
                $response['save_amount'] = $response['product_sale_price'] - $response['product_discount_price'];
            }

            return $response;
        } else {
            return false;
        }
    }
    
    public function check_valid_customer($email, $password)
        {
            $password = md5($password);
            $this->db->where('email',$email);
            $this->db->where('status',1);
            $this->db->where('password',$password);
            $query = $this->db->get('customers');
            return $query->result_array();
            
        }
        
        public function email_check($email){
            $this->db->where('email',$email);
            return $this->db->get('customers')->row();
            
        }
        
        public function product_info($product_id){
            $allItem = $this->db->select('*')->where('id',$product_id)->where('status',1)->get('product')->result();
            $modifPrice = [];

        //now calculation discount of all items
        foreach ($allItem as $item) {

            $discount = $this->check_product_availability_and_discount($item->id, 0, $item->sale_price);
            if ($discount) {
                $item->sale_price = ceil($item->sale_price);
                $item->price = $discount['product_discount_price'];
                $item->save_amount = $discount['save_amount'];
                $item->discount_apply = 1;
                $modifPrice[] = $item;
            } else {
                $modifPrice[] = $item;
                $item->discount_apply = 0;
                $item->price = ceil($item->sale_price);
                $item->save_amount = 0;
            }
        }


        return  $modifPrice[0];
        }
        
        public function search($search_val){
          $allItem = array();
//        First check category
            $search_category = $this->db->select('*')->limit(10)->like('name', $search_val)->get('category')->result();
        foreach ($search_category as $category) {
            $category_id = $category->id;
            $this->db->select('*');
            $this->db->from('product')->limit(10);
            $this->db->join('product_to_category', 'product.id = product_to_category.id');
            $this->db->where('product_to_category.category_id', $category_id);
            $this->db->where('product.status', 1);
            $result = $this->db->get()->result();
            $allItem = array_merge($allItem, $result);
        }
        
        
        
            $singleItem = $this->db->select('*')->like('name',$search_val)->order_by('sort_order')->get('product')->result();

        $allItem = array_merge($allItem, $singleItem);
        $allItem = array_map("unserialize", array_unique(array_map("serialize", $allItem)));
        $modifPrice = [];

        //now calculation discount of all items
        foreach ($allItem as $item) {

            $discount = $this->check_product_availability_and_discount($item->id, 0, $item->sale_price);
            if ($discount) {
                $item->sale_price = ceil($item->sale_price);
                $item->price = $discount['product_discount_price'];
                $item->save_amount = $discount['save_amount'];
                $item->discount_apply = 1;
                $modifPrice[] = $item;
            } else {
                $modifPrice[] = $item;
                $item->discount_apply = 0;
                $item->price = ceil($item->sale_price);
                $item->save_amount = 0;
            }
        }


        return $modifPrice;
        }
        
        public function item_details($item){
           $allItem = $this->db->select('*')->where('id',$item)->get('product')->result();
           $modifPrice = [];

        //now calculation discount of all items
        foreach ($allItem as $item) {

            $discount = $this->check_product_availability_and_discount($item->id, 0, $item->sale_price);
            if ($discount) {
                $item->sale_price = ceil($item->sale_price);
                $item->price = $discount['product_discount_price'];
                $item->save_amount = $discount['save_amount'];
                $item->discount_apply = 1;
                $modifPrice[] = $item;
            } else {
                $modifPrice[] = $item;
                $item->discount_apply = 0;
                $item->price = ceil($item->sale_price);
                $item->save_amount = 0;
            }
        }


        return (object) $modifPrice[0];
        }
        
        public function customer_details($customer_id){
            return $this->db->select('*')->where('id',$customer_id)->get('customers')->row();
        }
        
        public function payment_details($payment_method){
            return $this->db->select('*')->where('id',$payment_method)->get('payments')->row();
        }
        
        public function shipping_exist_check($id,$place,$address){
            $result = $this->db->select('shipping_id')->where('customer_id',$id)->where('place',$place)->where('address',$address)->get('customer_shipping_address')->row();
            
            if(isset($result->id) && $result->id !=''){
                return $result->id;
            }else{
                return false;
            }
        }
        
        public function paypal_payment_details(){
            return $this->db->select('*')->where('type','paypal')->get('payments')->row();
        }
        
        /**
     * 
     * @return type
     * this method use to show all orders in order list
     * @author Jahid al mamun <rjs.jahid11@gmail.com>
     */
    public function all_orders()
    {
        $this->db->select('orders.*,orders.id as oid,orders.status as ostatus,customers.*,customers.id as cid');
        $this->db->join('customers','orders.customer_id=customers.id','left')->order_by('orders.id','desc');
        $this->db->from('orders');
        
        return $this->db->get()->result();
        
    }
    
    public function customer_orders($id)
    {
        $this->db->select('orders.*,orders.id as oid,orders.status as ostatus,customers.*,customers.id as cid');
        $this->db->join('customers','orders.customer_id=customers.id','left')->order_by('orders.id','desc');
        $this->db->from('orders')->where('orders.customer_id',$id);
        
        return $this->db->get()->result();
        
    }
    
    public function customer_shipping_address($customer_id){
        return $this->db->select('*')->where('customer_id',$customer_id)->group_by('address')->get('customer_shipping_address')->result();
    }
    
    public function shipping_address_details($ship_id){
        
        return $this->db->select('*')->where('shipping_id',$ship_id)->from('customer_shipping_address')->get()->row();
    }
    
    function shipping_update($id, $data) {
        $this->db->where('shipping_id', $id);
        $result = $this->db->update('customer_shipping_address', $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;   
         }
    }
       
}
