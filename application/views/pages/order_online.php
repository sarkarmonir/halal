        <!-- ***** Shop ***** -->
        <div class="shop" id="shop">

            <div class="container">

                <div class="col-md-3 stk_1">

                    <!-- nav-side-menu -->
                    <div class="nav-side-menu">

                        <h3 class="headline"><span>Categories</span></h3>

                        <div class="menu-list">

                            <ul class="menu-content">
                                <?php  foreach ($allcategories as $category ){ ?>
                                <li <?php if($currentCat == $category->id){echo "class='active'"; } ?> ><a href="<?php echo base_url() ?>category/<?php echo $category->id."-".make_alias($category->name);  ?>"><i class="fa fa-bars"></i> <?php echo $category->name; ?></a></li>
                                <?php $i=''; } ?>
                               
                            </ul>
                        </div>
                    </div><!-- /.nav-side-menu -->


                </div>

                <div class="col-md-6 stk_2">

                    <div class="row products-items-list">
                    <h3 class="headline"><span>Category Items</span></h3>

                        <ul class="shop-items">
                            <?php if(!empty($allProducts)){ foreach ($allProducts as $catproduct){ ?>

                            <li class="col-lg-4 col-md-4 col-sm-6 item product hvr-float-shadow">

                                <div class="container-inner clearfix">

                                    <figure>
                                        <img src="<?php echo cdn(); ?>products/<?php echo $catproduct->image; ?>" class="img-responsive" alt="<?php echo $catproduct->name; ?>">
                                    </figure>

                                    <?php if($catproduct->product_status !=''){ ?><span class="ribbon ribbon-danger"><?php echo $catproduct->product_status; ?></span><?php } ?>

                                    <div class="text-center product-detail">

                                        <h2 class="product-name"><a href="<?php echo base_url(); ?>details/<?php echo $catproduct->id.'-'.make_alias($catproduct->name); ?>"><?php echo $catproduct->name; ?></a></h2>

                                        <div class="price-box"><span class="product-price">$<?php echo $catproduct->price; ?></span><?php if($catproduct->discount_apply == 1){ ?><del><?php echo $catproduct->sale_price; ?></del><?php } ?></div>

                                        <a href="#" class="btn btn-primary hvr-shutter-out-horizontal" onclick="halal.addToCart(<?php echo $catproduct->id; ?>)"><i class="fa fa-shopping-cart"></i><img class="wait_cart" id="wait_<?php echo $catproduct->id; ?>" style="display:none;" src="<?php echo base_url(); ?>/assets/front/img/loading.gif" /> Add to cart</a>

                                    </div>

                                </div>

                            </li><!-- /.item -->
                            
                            <?php } }else{ ?>
                             <div class="row-fluid">
                                 <div class="col-md-12">
                                    <div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> No Product Available in This Category! Please try again.</div>
                                 </div>
                             </div>
                           <?php }?>


                        </ul>

                    </div><!-- /.row -->

<!--                    <div class="center-block text-center">

                        <nav>
                            <ul class="pagination">
                                <li><a href="#">«</a></li>
                                <li><span class="current">1</span></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                        </nav>

                    </div>-->

                </div><!-- /.product-item-list -->

                <div class="col-md-3 stk_3">

                    <h3 class="headline"><span>Your Cart</span></h3>
                    <div class="page-quick-cart">
                        <div class="cart-collaterals">
                            <div class="cart_totals">
                                <!-- <h2>Cart Totals</h2> -->
                                <div class="table-responsive minimart_content">
                                    <table cellspacing="0" class="table">
                                        <thead>
                                            <tr class="cart-subtotal">
                                                <th>Name</th>
                                                <th>QTY</th>
                                                <th align="right">Price</th>
                                                <th><i class="fa fa-close"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($cartitems as $item){ ?>
                                            <tr class="cart-subtotal" class="row_<?php echo $item['rowid']; ?>">
                                                <td><?php echo $item['name']; ?></td>
                                                <td>
                                                    <div class="input-group top-cart-input">
                                                        <input type="text" class="form-control" value="<?php echo $item['qty']; ?>">
                                                        <div class="input-group-btn-vertical">
                                                            <button class="btn btn-primary btn-inc" onclick="halal.qtyplus('<?php echo $item['rowid']; ?>')">
                                                                <i class="fa fa-caret-up"></i>
                                                            </button>
                                                            <button class="btn btn-primary btn-dec" onclick="halal.qtyminus('<?php echo $item['rowid']; ?>')">
                                                                <i class="fa fa-caret-down"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td align="right"><span class="amount">$<?php echo $item['subtotal']; ?></span></td>
                                                <td><a href="javascript:void()" onclick="halal.remove_cart(`<?php echo $item['rowid']; ?>`)"><i class="fa fa-close"></i></a></td>
                                            </tr>
                                            <?php } ?>                                      
                                        </tbody>
                                        <tfoot>
                                            <tr class="cart-subtotal">
                                                <!-- <th>Subtotal</th>
                                                <td><span class="amount">$3,360.00</span></td> -->
                                                <td>Total</td>
                                                <td colspan="2" align="right"><strong><span class="amount">$<?php echo $total; ?></span></strong> </td>
                                                <td><!-- <a href="javascript:void()"><i class="fa fa-close"></i></a> --></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="wc-proceed-to-checkout pull-right">
                                    <a href="<?php echo base_url('cart'); ?>" class="btn btn-info">View cart</a>
                                    <a href="<?php echo base_url('checkout'); ?>" class="btn btn-success">Checkout</a>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>

                </div>

            </div><!-- /.container -->

        </div><!-- /#shop -->
