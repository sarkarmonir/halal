<!-- ***** About us ***** -->
<section class="about-us page-post" id="aboutUs">
    <div class="container">

        <div class="row">

            <div class="col-md-6 col-md-offset-3">
                <h1 class="text-center">Privacy <span class="extra-bold">& Policy</span></h1>
                <p class="text-center">
                    <?=$info->title?><br><br><i>20. November 2015</i>
                </p>
            </div>

        </div>

        <div class="zigzag"><i class="fa fa-home"></i></div>

        <div class="row">

            <div class="col-md-10">
                <?=$info->desc?>
            </div>
            <div class="col-md-2">
                <img src="<?php echo base_url(); ?>assets/front/img/content/thumb2.jpg" class="img-thumbnail img-responsive" alt="Pistacia Restaurant And Food Template">
            </div>

        </div>

    </div>
</section>