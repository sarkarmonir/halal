
<!-- ***** Shop ***** -->
<div class="shop-cart-section" id="shop-cart-section">

    <div class="container">

        <div class="blog-item">
            <div class="shop-cart">
                <div class="col-md-3">

                    <!-- nav-side-menu -->
                    <div class="nav-side-menu">

                        <div class="menu-list">
                            <ul class="menu-content">
                                <li><a href="<?php echo base_url(); ?>/home/farm"><i class="flaticon flaticon-burger8"></i> Our Farm</a></li>
                                <li><a href="<?php echo base_url(); ?>/home/farm_gallery"><i class="flaticon flaticon-burger8"></i> Farm Gallery</a></li>
                            </ul>
                        </div>
                    </div><!-- /.nav-side-menu -->


                </div>
                <div class="col-md-9">
                        <h1 class="title">Our<span class="extra-bold">Halal organic farm</span></h1>

                        <p class="">
                            We are certified HALAL facility.
                            All Our animals are humanely treated and naturally fed and raised in a spacious feeding lot.
                        </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <img src="<?php echo base_url(); ?>assets/front/img/farm/10.jpg" class="img-responsive" alt="Pistacia Restaurant And Food Template">
                </div>

            </div>
        </div>

    </div><!-- /.container -->

        </div><!-- /#shop -->