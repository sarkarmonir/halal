
<!-- ***** Shop ***** -->
<div class="shop-cart-section" id="shop-cart-section">

    <div class="container">

        <div class="blog-item">
            <div class="shop-cart">
                <div class="col-md-3">
                    <!-- nav-side-menu -->
                    <div class="nav-side-menu">

                        <div class="menu-list">
                            <ul class="menu-content">
                                <li><a href="<?php echo base_url(); ?>/home/farm"><i class="flaticon flaticon-burger8"></i> Our Farm</a></li>
                                <li><a href="<?php echo base_url(); ?>/home/farm_gallery"><i class="flaticon flaticon-burger8"></i> Farm Gallery</a></li>
                            </ul>
                        </div>
                    </div><!-- /.nav-side-menu -->
                </div>
                <div class="col-md-9">
                        <div class="farm-img">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/1.jpg">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/2.jpg">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/3.jpg">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/4.jpg">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/1.jpg">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/2.jpg">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/3.jpg">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/front/img/farm/4.jpg">
                        </div>
                </div>

            </div>
        </div>

    </div><!-- /.container -->

        </div><!-- /#shop -->