        <!-- ***** Shop ***** -->
        <div class="shop" id="shop">

            <div class="container">


                <div class="col-md-9 products-items-list">

                    <div class="row">

                        <ul class="shop-items">
                            <?php foreach ($search_result as $fproduct){ ?>

                            <li class="col-lg-3 col-md-4 col-sm-6 item product hvr-float-shadow">

                                <div class="container-inner clearfix">

                                    <figure>
                                        <img src="<?php echo cdn(); ?>products/<?php echo $fproduct->image; ?>" class="img-responsive" alt="<?php echo $fproduct->name; ?>">
                                    </figure>

                                    <?php if($fproduct->product_status !=''){ ?><span class="ribbon ribbon-danger"><?php echo $fproduct->product_status; ?></span><?php } ?>

                                    <div class="text-center product-detail">

                                        <h2 class="product-name"><a href="<?php echo base_url(); ?>index.php/home/item_details"><?php echo $fproduct->name; ?></a></h2>

                                        <div class="price-box"><span class="product-price">$<?php echo $fproduct->price; ?></span><?php if($fproduct->discount_apply == 1){ ?><del>$<?php echo $fproduct->sale_price; ?></del><?php  } ?></div>

                                        <a href="javascript:void(0)" class="btn btn-primary hvr-shutter-out-horizontal" onclick="halal.addToCart(<?php echo $fproduct->id; ?>)"><i class="fa fa-shopping-cart"></i> Add to cart</a>

                                    </div>

                                </div>

                            </li><!-- /.item -->
                            <?php  } ?>

                           

                        </ul>

                    </div><!-- /.row -->

<!--                 <div class="center-block text-center">

                        <nav>
                            <ul class="pagination">
                                <li><a href="#">«</a></li>
                                <li><span class="current">1</span></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                        </nav>

                    </div>-->

                </div><!-- /.product-item-list -->

            </div><!-- /.container -->

        </div><!-- /#shop -->
