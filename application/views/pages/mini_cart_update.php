<table cellspacing="0" class="table" style="font-family: 'Merriweather';
    color: #575A5C;">
    <thead>
        <tr class="cart-subtotal">
            <th>Item Name</th>
            <th>QTY</th>
            <th align="right">Price</th>
            <th><i class="fa fa-close"></i></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($cartitems as $item){ ?>
        <tr class="cart-subtotal" class="row_<?php echo $item['rowid']; ?>">
            <td><?php echo $item['name']; ?></td>
            <td>
                <div class="input-group top-cart-input">
                    <input type="text" class="form-control" value="<?php echo $item['qty']; ?>">
                    <div class="input-group-btn-vertical">
                        <button class="btn btn-primary btn-inc" type="button" onclick="halal.qtyplus('<?php echo $item['rowid']; ?>')">
                            <i class="fa fa-caret-up"></i>
                        </button>
                        <button class="btn btn-primary btn-dec" type="button" onclick="halal.qtyminus('<?php echo $item['rowid']; ?>')">
                            <i class="fa fa-caret-down"></i>
                        </button>
                    </div>
                </div>
            </td>
            <td align="right"><span class="amount">$<?php echo $item['subtotal']; ?></span></td>
            <td><a href="javascript:void()" onclick="halal.remove_cart(`<?php echo $item['rowid']; ?>`)"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php } ?>

    </tbody>
    <tfoot>
        <tr class="cart-subtotal">
            <!-- <th>Subtotal</th>
            <td><span class="amount">$<?php echo $subtotal; ?></span></td> -->
            <td>Total</td>
            <td colspan="2" align="right"><strong><span class="amount">$<?php echo $total; ?></span></strong> </td>
            <td></td>
        </tr>
    </tfoot>
</table>