
        <!-- ***** Shop ***** -->
        <div class="shop-cart-section" id="shop-cart-section">

            <div class="container">

                <div class="col-md-12">

                    <div class="blog-item">
                        <div class="shop-cart">
                            <div class="row">

                                <div class="col-md-8">

                                    <form action="#" method="post">

                                        <div class="table-responsive">
                                            <table class="shop_table cart table" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th class="product-remove">&nbsp;</th>
                                                        <th class="product-thumbnail">&nbsp;</th>
                                                        <th class="product-name">Product</th>
                                                        <th class="product-price">Price</th>
                                                        <th class="product-quantity">Quantity</th>
                                                        <th class="product-subtotal">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    foreach ($cartitems as $item){
                                                    ?>
                                                    <tr class="cart_item" class="row_<?php echo $item['rowid']; ?>">
                                                        <td class="product-remove"><a href="javascript:void(0)" class="remove" title="Remove this item" data-product_id="238" data-product_sku="" onclick="halal.remove_cart(`<?php echo $item['rowid']; ?>`)"><i class="fa fa-close"></i></a></td>
                                                        <td class="product-thumbnail"><a href="#"><img src="<?php echo cdn(); ?>products/<?php echo $item['image']; ?>" class="attachment-shop_thumbnail wp-post-image img-responsive" alt="product02"></a></td>
                                                        <td class="product-name"><a href="#"><?php echo $item['name']; ?> </a></td><td class="product-price"><span class="amount">$<?php echo $item['price']; ?></span><?php if($item['price'] !=$item['sale_price']){ ?> <del><?php echo $item['sale_price'];  } ?></del></td>
                                                        <td class="product-quantity"><div class="quantity"><input type="number" step="1" min="0" max="12" name="" value="<?php echo $item['qty']; ?>" onchange="halal.qty_update(this,`<?php echo $item['rowid']; ?>`)" title="Qty" class="input-text qty text" size="4"></div></td>
                                                        <td class="product-subtotal"><span class="amount">$<?php echo $item['subtotal']; ?></span></td>
                                                    </tr>
                                                    
                                                    <?php } ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>
                                </div>

                                <div class="col-md-4">
                                    <div class="cart-collaterals">
                                        <div class="cart_totals">
                                            <h2>Cart Totals</h2>
                                            <div class="table-responsive">
                                                <table cellspacing="0" class="table">
                                                    <tbody>
                                                        <tr class="cart-subtotal">
                                                            <th>Subtotal</th>
                                                            <td align="right"><span class="amount">$<?php echo $subtotal; ?></span></td>
                                                        </tr>
                                                        <tr class="order-total">
                                                            <th>Discount</th>
                                                            <td align="right"><strong><span class="amount">- $<?php echo $total_save; ?></span></strong> </td>
                                                        </tr>
                                                        <tr class="order-total">
                                                            <th>Total</th>
                                                            <td align="right"><strong><span class="amount">$<?php echo $total; ?></span></strong> </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="wc-proceed-to-checkout">
                                                <a href="<?php echo base_url(); ?>index.php/home/checkout" class="checkout-button btn alt wc-forward">Proceed to Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
