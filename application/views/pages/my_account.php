
<!-- ***** Shop ***** -->
<!-- <div class="shop-cart-section" id="shop-cart-section"> -->
<div class="shop" id="shop">
    <div class="container">

        <div class="blog-item">
            <div class="shop-cart">
                <div class="col-md-3 stk_1">

                    <!-- nav-side-menu -->
                    <div class="nav-side-menu">

                        <h3 class="headline"><span>My Account</span></h3>

                        <div class="menu-list">

                            <ul class="menu-content" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                                <li role="presentation"><a href="#payment_info" aria-controls="payment_info" role="tab" data-toggle="tab"><i class="fa fa-credit-card" aria-hidden="true"></i> Billing info</a></a></li>
                                <li role="presentation"><a href="#shipping_address" aria-controls="shipping_address" role="tab" data-toggle="tab"><i class="fa fa-truck" aria-hidden="true"></i> Shipping address</a></a></li>
                                <li role="presentation"><a href="#my_orders" aria-controls="my_orders" role="tab" data-toggle="tab"><i class="fa fa-shopping-cart" aria-hidden="true"></i> My orders</a></li>
                            </ul>

                        </div>
                    </div><!-- /.nav-side-menu -->


                </div>
                <div class="col-md-9 stk_2">

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="home">
                            <h3 class="headline"><span>My Account</span></h3>

                            <div class="row">
                                <div class="col-md-3"><img src="http://lorempixel.com/200/200/"></div>
                                <div class="col-md-9">
                                    <table>
                                        <tr>
                                            <td>Name:</td>
                                            <td><?php echo $customer_info->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>User id:</td>
                                            <td><?php echo $customer_info->email; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Phone:</td>
                                            <td><?php echo $customer_info->telephone; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Address:</td>
                                            <td><?php echo $customer_info->address; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <h3>My letest order</h3>
                            <div class="table-responsive">
                                <table class="shop_table cart table" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="product-name">Order no</th>
                                            <th class="product-name">Name</th>
                                            <th class="product-price">Date</th>
                                            <th class="product-price">Amount</th>
                                            <th class="product-subtotal">Payment Status</th>
                                            <th class="product-subtotal">Order Type</th>
                                            <th class="product-subtotal">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($all_orders as $sorder) {
                                            if ($i < 2) {
                                                ?>
                                                <tr id="row_<?php echo $sorder->oid; ?>">
                                                    <td><a href="#" onclick="order_details_modal(<?php echo $sorder->oid; ?>)">H<?php echo $sorder->oid; ?></a></td>
                                                    <td><?php echo $sorder->name; ?></td>
                                                    <td><?php echo $sorder->ordered_on; ?></td>
                                                    <td><?php echo $sorder->total; ?></td>
                                                    <td><?php echo $sorder->payment_status; ?></td>
                                                    <td><?php echo $sorder->order_type; ?></td>

                                                    <td>
                                                        <?php
                                                        if ($sorder->ostatus == '') {
                                                            echo "Pending";
                                                        } else {
                                                            echo $sorder->ostatus;
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>

                        </div>



                        <div role="tabpanel" class="tab-pane" id="payment_info">
                            <h3 class="headline"><span>Billing info</span></h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td>Name:</td>
                                            <td><?php echo $customer_info->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email:</td>
                                            <td><?php echo $customer_info->email; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Phone:</td>
                                            <td><?php echo $customer_info->telephone; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Address:</td>
                                            <td><?php echo $customer_info->address; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <a href="#" class="btn btn-success" onclick="personal_edit_modal(<?php echo $customer_info->id; ?>)">Modify</a>


                        </div>

                        <div role="tabpanel" class="tab-pane" id="shipping_address">
                            <h3 class="headline"><span>Shipping address</span></h3>
                            <div class="row">
                                <?php
                                if (isset($customer_shipping_info)) {
                                    foreach ($customer_shipping_info as $shipping) {
                                        ?>


                                        <div class="col-md-12">
                                            <table>
                                                <tr>
                                                    <td>Name:</td>
                                                    <td><?php echo $shipping->name; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Email:</td>
                                                    <td><?php echo $shipping->email; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Phone:</td>
                                                    <td><?php echo $shipping->telephone; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Address:</td>
                                                    <td><?php echo $shipping->address; ?></td>
                                                </tr>
                                            </table>
                                        </div>
<a href="#" class="btn btn-success" onclick="shipping_edit_modal(<?php echo $shipping->shipping_id; ?>)">Modify</a>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            


                        </div>

                        <div role="tabpanel" class="tab-pane" id="my_orders">
                            <h3 class="headline"><span>My Orders</span></h3>


                            <?php if (isset($all_orders) && $all_orders != '') { ?>

                                <form action="#" method="post">

                                    <div class="table-responsive">
                                        <table class="shop_table cart table" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th class="product-name">Order no</th>
                                                    <th class="product-name">Name</th>
                                                    <th class="product-price">Date</th>
                                                    <th class="product-price">Amount</th>
                                                    <th class="product-subtotal">Payment Status</th>
                                                    <th class="product-subtotal">Order Type</th>
                                                    <th class="product-subtotal">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($all_orders as $order) { ?>
                                                    <tr id="row_<?php echo $order->oid; ?>">
                                                        <td><a href="#" onclick="order_details_modal(<?php echo $order->oid; ?>)">H<?php echo $sorder->oid; ?></a>

                                                        </td>
                                                        <td><?php echo $order->name; ?></td>
                                                        <td><?php echo $order->ordered_on; ?></td>
                                                        <td><?php echo $order->total; ?></td>
                                                        <td><?php echo $order->payment_status; ?></td>
                                                        <td><?php echo $order->order_type; ?></td>
                                                        <!--<td><a href="<?php echo base_url_tr('order/order_view') . "/" . $order->oid; ?>" ><i class="fa fa-eye"></i></a></td>-->

                                                        <td>
                                                            <?php
                                                            if ($order->ostatus == '') {
                                                                echo "Pending";
                                                            } else {
                                                                echo $order->ostatus;
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>


                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                                <?php
                            } else {
                                echo "Your Have no order";
                            }
                            ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div><!-- /.container -->

</div><!-- /#shop -->
<!-- Button trigger modal -->

<script type="text/javascript">
                                                        function order_details_modal(order_id) {
                                                            $.ajax({
                                                                type: "POST",
                                                                url: BASE_URL + "home/order_modal/" + order_id,
                                                                dataType: "text",
                                                                success: function(response)
                                                                {
                                                                    $('#order_modal_content').html(response);
                                                                    $('#order_modal').modal('show');
                                                                },
                                                                error: function(a, b, c)
                                                                {
                                                                    // alert(b);
                                                                }
                                                            });
                                                        }
                                                        
                                                        function personal_edit_modal(cust_id) {
                                                            $.ajax({
                                                                type: "POST",
                                                                url: BASE_URL + "home/personal_info_modal/" + cust_id,
                                                                dataType: "text",
                                                                success: function(response)
                                                                {
                                                                    $('#personal_info_content').html(response);
                                                                    $('#personal_info_modal').modal('show');
                                                                },
                                                                error: function(a, b, c)
                                                                {
                                                                    // alert(b);
                                                                }
                                                            });
                                                        }
                                                        
                                                         function shipping_edit_modal(ship_id) {
                                                            $.ajax({
                                                                type: "POST",
                                                                url: BASE_URL + "home/shipping_info_modal/" + ship_id,
                                                                dataType: "text",
                                                                success: function(response)
                                                                {
                                                                    $('#shipping_info_content').html(response);
                                                                    $('#shipping_info_modal').modal('show');
                                                                },
                                                                error: function(a, b, c)
                                                                {
                                                                    // alert(b);
                                                                }
                                                            });
                                                        }

</script>
<div id="order_modal_content"></div>
<div id="personal_info_content"></div>
<div id="shipping_info_content"></div>