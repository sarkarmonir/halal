        <!-- ***** Shop ***** -->
        <div class="shop-checkout-section" id="shop-checkout-section">

            <div class="container">

                <div class="col-md-12">

                    <div class="blog-item">
                        <div class="shop-cart">
                            <div class="row">

                                <div class="col-md-offset-2 col-md-8">

                                    <div class="checkout-form">

                                        <h3>My Information</h3>

                                        <form id="signup-form" onsubmit="return halal.signup()" action="<?php echo base_url() ?>home/signup_pro" method="post" >

                                            <input type="text" name="name" id="signame" class="form-control" placeholder="*Name">
                                            <input type="text" name="email" id="sigemail" class="form-control" placeholder="*E-mail">
                                            <span id="existemail" style="color: red; display: none;">Email Address Already Exist</span>
                                            <input type="text" name="phone" id="sigphone" class="form-control" placeholder="*Phone">
                                            
                                            <input type="text" name="password" id="sigpassword" class="form-control" placeholder="*Password">
                                            <input type="text" name="confirm_password" id="sigconpass" class="form-control" placeholder="Confirm Password">
                                            <span id="sigconpassalert" style="color: red; display: none;">Password and Confirm Password is not match</span>
                                            <input type="text" name="city" id="sigcity" class="form-control" placeholder="City">
                                            <label>Area</label>
                                    <select name="user_area" class="form-control" >
                                        <?php foreach ($areas as $area){ ?>
                                        <option <?php if($area->default == 1){echo 'selected';} ?> value="<?php echo $area->id; ?>" ><?php echo $area->name; ?></option>
                                        <?php } ?>
                                    </select>
                                            <textarea name="address" id="sigaddress" class="form-control" placeholder="*Address"></textarea>
                                           e.g. CHRIS NISWANDEE, SMALLSYS INC, 795 E DRAGRAM, TUCSON AZ 85705 USA
                                            <br>
                                            <input type="checkbox" name="agree" value="1" /> <span><a href="#">Private Policy</a></span>

                                            <!-- <a href="#" class="btn">Send</a> -->
                                            <button type="submit" class="btn">Signup</button>

                                        </form>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
