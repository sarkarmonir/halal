
        <!-- ***** Contact form ***** -->
        <section class="contact-form" id="contactForm">

            <div class="google-maps" id="map-canvas"></div>

            <div class="container">

                <div class="row">

                    <div class="col-md-4">

                        <h1><span class="light">Our</span> <span class="extra-bold">Restaurants</span></h1>

                        <p class="lead">Aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>

                        <div class="contacts-wrapper">
                            <ul class="contacts">
                                <li><i class="fa fa-map-marker"></i>753 A 4th St, NY, USA</li>
                                <li><i class="fa fa-phone"></i>+5 432-234-1234</li>
                                <li><i class="fa fa-envelope"></i><a href="#">me@medomain.com</a></li>
                            </ul>
                        </div>
                        <div class="socialbar socialbar-inner socialbar-radius colorizesocial">
                            <ul>
                                <li><a class="socicon socicon-facebook hvr-buzz-out" href="#" title="Facebook"></a></li>
                                <li><a class="socicon socicon-youtube hvr-buzz-out" href="#" title="YouTube"></a></li>
                                <li><a class="socicon socicon-pinterest hvr-buzz-out" href="#" title="Pinterest"></a></li>
                                <li><a class="socicon socicon-twitter hvr-buzz-out" href="#" title="Twitter"></a></li>
                                <li><a class="socicon socicon-google hvr-buzz-out" href="#" title="Google"></a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-8">

                        <h1><span class="light">Contact</span> <span class="extra-bold">us</span></h1>

                        <form name="contactForm" id="cntForm" method="post">

                            <input type="text" name="cntCode" id="cntCode" class="form-control hidden" placeholder="" value="">

                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name *" required="">
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" name="email" id="email" class="form-control" placeholder="E-mail *" required="">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea name="cntMessage" id="cntMessage" class="form-control contact-message" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="message">
                                <span class="label label-lg label-success">Reservation has been sent</span>
                            </div>

                            <a class="btn btn-primary hvr-shutter-out-horizontal btn-send" href="#">Send reservation</a>

                        </form>

                    </div>

                </div>

            </div>

        </section>
        