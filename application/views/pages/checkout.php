<!-- ***** Shop ***** -->
<div class="shop-checkout-section" id="shop-checkout-section">

    <div class="container">

        <div class="col-md-12">

            <div class="blog-item">
                <div class="shop-cart">
                    <div class="row">

                        <div class="col-md-12">
                            <?php if ($this->session->userdata('customer_id') != '' && $this->session->userdata('name') != '') { ?>

                            <?php } else { ?>
                                <div class="checkout-form co-login">

                                    <div class="login-box-inner">
                                        <form id="login-form" onsubmit="return halal.checkout_login()" action="<?php echo base_url() ?>home/dologin" method="post" >
                                            <div class="col-md-12"><span id="login_error" style="color:red; display: none;" >Your Email or Password is invalid!</span></div>
                                            <div class="col-md-12"><span id="login_success" style="color:green; display: none;" >Login Successfull!</span></div>
                                            <div class="col-md-1">
                                                <h3>Login</h3>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="checkemail" name="email" class="form-control" placeholder="E-mail">
                                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="password" id="checkpass" name="password" class="form-control" placeholder="Password">
                                                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="clearfix">
                                                    <div class="col-md-2">
                                                        <input type="submit" class="btn" value="Login" />
                                                    </div>
                                                    <!--                                                        <div class="col-md-5 columns">
                                                                                                                <a href="#" class="small">Forgot password?</a>
                                                                                                            </div>
                                                                                                            <div class="col-md-5 columns">
                                                                                                                <a href="#" class="small">Create an account</a>
                                                                                                            </div>-->
                                                </div>
                                            </div>
                                            <br>
                                        </form>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <form name="checkout_form" action="<?php echo base_url('home/checkout_process'); ?>" method="post" >
                        <div class="row">
                            <div class="col-md-8">

                                <div class="checkout-form">
                                    <?php if ($this->session->userdata('customer_id') == '') { ?>
                                        <h3>Checkout as </h3>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="checkout_as"  value="registration" checked>
                                                Create New Account
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="checkout_as" value="guest">
                                                Guest User
                                            </label>
                                        </div>

                                        <h3>Please Enter Your Information</h3>

                                        <input type="text" required name="cust_name" class="form-control" placeholder="Name">
                                        <input type="text" required name="cust_phone" class="form-control" placeholder="Phone">
                                        <input type="text" required name="cust_email" class="form-control" placeholder="E-mail">
                                        <input type="password" required name="cust_password" class="form-control" placeholder='Password'>
                                        <input type="text" required name="cust_city" class="form-control" placeholder="City">
                                        <select required name="bill_user_area" class="form-control" >
                                            <?php foreach ($areas as $area) { ?>
                                                <option <?php
                                                if ($area->default == 1) {
                                                    echo 'selected';
                                                }
                                                ?> value="<?php echo $area->id; ?>" ><?php echo $area->name; ?></option>
                                        <?php } ?>
                                        </select>
                                        <textarea class="form-control" placeholder="Billing Address" rows="6" name="billing_address"></textarea>
<?php } else { ?>
                                        <h4><?php echo $customer->name; ?></h4>
                                        <h6><?php echo $customer->email; ?></h6>
                                        <h6><?php echo $customer->telephone; ?></h6>
                                        <h6><?php echo $customer->address; ?></h6>

<?php } ?>
                                    <br>
                                    <input type="checkbox" checked value="same" id="same_as" name="same_as_billing" /> Same As Billing Address
                                    <div style="display:none;" id="shipping_address">
                                        <label>Shipping Area</label>
                                        <select name="ship_user_area" class="form-control" >
                                            <?php foreach ($areas as $area) { ?>
                                                <option <?php
                                                    if ($area->default == 1) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="<?php echo $area->id; ?>" ><?php echo $area->name; ?></option>
<?php } ?>
                                        </select>
                                        <textarea class="form-control" placeholder="Shipping Address" rows="6" name="shipping_address"></textarea>
                                        (e.g. 500 South Main Street Bishop, CA 93514)
                                    </div>
                                    <br>

                                    <br>

                                    <textarea class="form-control" name="order_note" placeholder="Order Note" rows="6"></textarea>



                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="checkout-form">
                                    <h3>Order type</h3>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="order_type" value="delivery" checked />
                                            Dalivery
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="order_type" value="collection" />
                                            Collection
                                        </label>
                                    </div>
                                    <h3>Select Payment</h3>
                                            <?php foreach ($payments as $payment) { ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="payment"  value="<?php echo $payment->id; ?>"  <?php
                                            if ($payment->default == 1) {
                                                echo "checked";
                                            }
                                            ?> >
    <?php echo $payment->title; ?>
                                            </label>
                                        </div>
<?php } ?>

                                    <h3>Your Cart</h3>
                                    <div class="page-quick-cart">
                                        <div class="cart-collaterals">
                                            <div class="cart_totals">
                                                <!-- <h2>Cart Totals</h2> -->
                                                <div class="table-responsive minimart_content" >
                                                    <table cellspacing="0" class="table">
                                                        <thead>
                                                            <tr class="cart-subtotal">
                                                                <th>Name</th>
                                                                <th>QTY</th>
                                                                <th>Price</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
<?php foreach ($cartitems as $item) { ?>
                                                                <tr class="cart-subtotal" class="row_<?php echo $item['rowid']; ?>">
                                                                    <td><?php echo $item['name']; ?></td>
                                                                    <td>
                                                                        <div class="input-group top-cart-input">
                                                                            <input type="text" class="form-control" value="<?php echo $item['qty']; ?>">
                                                                            <div class="input-group-btn-vertical">
                                                                                <button class="btn btn-primary btn-inc" type="button" onclick="halal.qtyplus('<?php echo $item['rowid']; ?>')">
                                                                                    <i class="fa fa-caret-up"></i>
                                                                                </button>
                                                                                <button class="btn btn-primary btn-dec" type="button" onclick="halal.qtyminus('<?php echo $item['rowid']; ?>')">
                                                                                    <i class="fa fa-caret-down"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td><span class="amount">$<?php echo $item['subtotal']; ?></span></td>
                                                                    <td><a href="javascript:void()" onclick="halal.remove_cart(`<?php echo $item['rowid']; ?>`)"><i class="fa fa-close"></i></a></td>
                                                                </tr>
<?php } ?>   
                                                        </tbody>
                                                        <tfoot>
                                                            <tr class="cart-subtotal">

                                                                <td>Total</td>
                                                                <td colspan="2" align="right"><strong><span class="amount">$<?php echo $total; ?></span></strong> </td>
                                                                <td><a href="javascript:void()"></a></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                <?php if ($this->session->userdata('coupon_name') == '') { ?>
                                                    <input type="password" class="form-control" id="coupon_val" placeholder="Enter Coupon Code">
                                                    <span style="color:red; display: none;" id="invalid_coupon">Invalid Coupon Code</span>
                                                    <button class="btn btn-success" type="button" onclick="halal.coupon_apply()">Apply Coupon</button>
                                                    <img id="coupon_wait" style="display: none;" src="<?php echo base_url('assets/front/img/loading.gif'); ?>" />
<?php } else { ?>
                                                    <h4>You Get <?php echo $this->session->userdata('coupon_name'); ?></h4>
<?php } ?>
                                                <table width="100%" class="table checkout_calculation">
                                                    <tr class="cart-subtotal">
                                                        <td width="65%">Discount</td>
                                                        <td><strong><span class="amount">-$<?php echo $total_save; ?></span></strong> </td>
                                                        <td width="10%"><a></a></td>
                                                    </tr>
                                                    <tr class="cart-subtotal">
                                                        <td width="65%">Final Price</td>
                                                        <td><strong><span class="amount">$<?php echo $total; ?></span></strong> </td>
                                                        <td width="10%"><a></a></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="wc-proceed-to-checkout pull-right">
                                                <button type="submit" class="btn btn-success"> Checkout</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
