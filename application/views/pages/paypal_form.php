<?php $rand = rand(); ?>
<form action="<?php echo $paypal_url; ?>" method="post" name="frmPayPal1" class="jNice" id="paypal_form">
    
    <fieldset>
        <?php
        $i = 1;
        $cart = $this->cart->contents();
     
        foreach ($cart as $rowid => $item) {            
            ?>
            <input type="hidden" name="amount_<?php echo $i; ?>" value="<?php echo number_format((float) $item['price'], 2, '.', ''); ?>" />
            <input type="hidden" name="item_name_<?php echo $i; ?>" value="<?php echo $item['name']; ?>" />
            <input type="hidden" name="item_number_<?php echo $i; ?>" value="<?php echo $item['id']; ?>" />
            <input type="hidden" name="quantity_<?php echo $i; ?>" value="<?php echo $item['qty']; ?>" />
            
                <input type="hidden" name="shipping_<?php echo $i; ?>" value="<?php echo $shipping_cost; ?>">
                <input type="hidden" name="discount_amount_cart" value="<?php echo $coupon_value; ?>">
               
            <?php
            $i++;
        }
        ?>
        <input type="hidden" name="tax_cart" value="<?php echo 0; ?>">
        <input type='hidden' name='business' value='<?php echo $business_email; ?>' />
        <input type="hidden" name="cmd" value="_cart">
        <input type="hidden" name="upload" value="1">
        <input type="hidden" name="currency_code" value="US">
        <input type="hidden" name="first_name" VALUE="<?php echo $this->session->userdata('name'); ?>">

        <input type="hidden" name="email" VALUE="<?php echo $this->session->userdata('email'); ?>">
        <input type="hidden" name="night_phone_a" VALUE="610">
        <input type="hidden" name="night_phone_b" VALUE="555">
        <input type="hidden" name="night_phone_c" VALUE="1234">
        <input type="hidden" name="cbt" value="Return to Coffing">
        <input type='hidden' name='return' value='<?php echo base_url('home/verify_order/' . $order_id); ?>' />
        <input type='hidden' name='cancel_return' value='<?php echo base_url('home/cancel_order/'.$order_id); ?>' />
        <input type="hidden" name="notify_url" value="<?php echo base_url('home/ipn/'.$order_id); ?>" />
        <input type="hidden" name="custom" value='<?php echo $order_id . "#" . $customer_id; ?>' /><!-- custom field -->


        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="" aria-valuemin="100" aria-valuemax="100" style="width:100%">
You will be redirect soon in paypal ......
            </div>
        </div>

    </fieldset>
</form>
<script>
    window.onload = function (e) {
        document.getElementById("paypal_form").submit();
    }
</script>