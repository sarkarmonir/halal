
<!-- ***** About us ***** -->
<section class="about-us page-post" id="aboutUs">

  <div class="container">

    <div class="row">

      <div class="col-md-6 col-md-offset-3">

        <h1 class="text-center">F.A.Q</h1>

        <p class="text-center">
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet
        </p>

      </div>

    </div>

    <div class="zigzag"><i class="fa fa-home"></i></div>

    <div class="row">

      <div class="col-md-10">
       <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        
      <?php $x=0; foreach($infos as $info): $x++; ?>
        <div class="panel panel-default">

          <div class="panel-heading" role="tab" id="heading<?=$x?>">
            <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$x?>" aria-expanded="true" aria-controls="collapse<?=$x?>">
                <?=$info->ques?>
              </a>
            </h4>
          </div>

          <div id="collapse<?=$x?>" class="panel-collapse collapse <?php if($x == 1){ echo 'in';} ?>" role="tabpanel" aria-labelledby="heading<?=$x?>">
            <div class="panel-body">
              <?=$info->answ?>
            </div>
          </div>

        </div>
        <?php endforeach; ?>

      </div>
    </div>

    <div class="col-md-2">
      <img src="<?php echo base_url(); ?>assets/front/img/content/thumb2.jpg" class="img-thumbnail img-responsive" alt="Pistacia Restaurant And Food Template">
    </div>

  </div>

</div>

</section>
