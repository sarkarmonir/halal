<!-- Modal -->
                            <div class="modal fade" id="shipping_info_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Shipping information Update</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form name="personal_edit_form" id="personal_info_update" method="post" action="<?php echo base_url('home/update_shipping_info'); ?>">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Name:</label>
                                                    <input type="text" required class="form-control" id="recipient-name" name="name" value="<?php echo $shipping_details->name; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Email:</label>
                                                    <input type="text" required class="form-control" id="recipient-name" name="email" value="<?php echo $shipping_details->email; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Phone:</label>
                                                    <input type="text" required class="form-control" id="recipient-name" name="phone" value="<?php echo $shipping_details->telephone; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Address:</label>
                                                    <input type="text" required class="form-control" id="recipient-name" name="address" value="<?php echo $shipping_details->address; ?>">
                                                </div>
                                                <input type="hidden" required class="form-control" id="recipient-name" name="customer_id" value="<?php echo $shipping_details->shipping_id; ?>">
                                                <button type="submit" class="btn btn-primary">Update changes</button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>