
        <!-- ***** About us ***** -->
        <section class="about-us page-post" id="aboutUs">

            <div class="container">

                <div class="row">

                    <div class="col-md-6 col-md-offset-3">

                        <h1 class="text-center">About <span class="extra-bold">Us</span></h1>

                        <p class="text-center">
                            <?=$info->title?><br><br><i>20. November 2015</i>
                        </p>

                    </div>

                </div>

                <div class="zigzag"><i class="fa fa-home"></i></div>

                <div class="row">

                    <div class="col-md-10">
                        <?=$info->desc?>
                    </div>

                    <div class="col-md-2">
                        <img src="<?=base_url()?>admin.halal/uploads/about_us/<?=$info->img?>" class="img-thumbnail img-responsive" alt="">
                    </div>
                </div>

            </div>

        </section>
