<section class="shop-item" id="shopItem">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <div class="image-zoom">

                    <img src="<?php echo cdn(); ?>products/<?php echo $item_details->image; ?>" class="img-responsive" alt="<?php echo $item_details->name; ?>">
                    <span class="zoom-icon triangle-button"></span>

                </div><!-- /.image-zoom -->


            </div><!-- /.col-md-6 -->

            <div class="col-md-6">

                <h1><span class="extra-bold"><?php echo $item_details->name; ?></span></h1>

                <p class="lead"><?php echo $item_details->short_description; ?></p>
                

                <!--<a href="#" class="add-to-favourite"><i class="fa fa-heart"></i> Add to favourites</a>-->

                <div class="socialbar socialbar-inner socialbar-radius colorizesocial">
                    <div class="sharethis-inline-share-buttons"></div>
                </div>

                <hr>

                <div class="clearfix">

                    <span class="price" id="itemPrice">$<?php echo $item_details->price; ?></span><?php if($item_details->discount_apply == 1){?> <del><?php echo $item_details->sale_price ?></del> <?php } ?>

<!--                    <div class="input-group spinner">

                        <input type="text" class="form-control" value="1">

                        <div class="input-group-btn-vertical">
                            <button class="btn btn-primary btn-inc">
                                <i class="fa fa-caret-up"></i>
                            </button>
                            <button class="btn btn-primary btn-dec">
                                <i class="fa fa-caret-down"></i>
                            </button>
                        </div>

                    </div>-->

                    <a class="btn btn-primary btn-buy hvr-shutter-out-horizontal" onclick="halal.addToCart(<?php echo $item_details->id; ?>)"><i class="fa fa-shopping-cart"></i> Add to cart</a>

                </div>

                <hr>

            </div><!-- /.col-md-6 -->

        </div><!-- /.row -->


        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div role="tabpanel" class="tabpanel">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab" aria-expanded="false">Description</a></li>
                        <!--<li role="presentation" class=""><a href="#additional" aria-controls="additional" role="tab" data-toggle="tab" aria-expanded="false">Additional info</a></li>-->
                    </ul><!-- /.nav-tabs -->

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="description">
                            <p class="small"><?php echo $item_details->long_description; ?></p>

                        </div><!-- /.tab-pane -->

                        

                    </div><!-- /.tab-content -->

                </div><!-- /.tabpanel -->

            </div><!-- /.col-md-6 -->

        </div><!-- /.row -->

    </div><!-- /.container -->

</section>