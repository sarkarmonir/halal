        <!-- ***** Shop ***** -->
        <div class="shop-checkout-section" id="shop-checkout-section">

            <div class="container">

                <div class="col-md-12">

                    <div class="blog-item">
                        <div class="shop-cart">
                            <div class="row">
                                
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="login-box">
                                        <img alt="" src="<?php echo base_url(); ?>assets/front/img/content/thumb9.jpg" class="thumb">
                                        <div class="login-box-inner">
                                            <h3>Login Information</h3>
                                                <form id="login-form" onsubmit="return halal.dologin()" action="<?php echo base_url() ?>home/dologin" method="post" >
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input name="email" id="log_email" type="text" class="form-control" placeholder="E-mail">
                                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input id="log_pass" name="password" type="password" class="form-control" placeholder="Password">
                                                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                    </div>
                                                </div>

                                                <div class="clearfix">
                                                    <div class="col-md-12"><span id="login_error" style="color:red; display: none;" >Your Email or Password is invalid!</span></div>
                                                    <div class="col-md-12"><span id="login_success" style="color:green; display: none;" >Login Successfull!</span></div>
                                                    <div class="col-md-6 columns">
                                                        <a href="#" class="small">Forgot password?</a>
                                                    </div>

                                                    <div class="col-md-6 text-right">
                                                        
                                                        <a href="#" ><input class="btn"  type="submit" value="login" /></a>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>


                                                <div class="clearfix">
                                                    <div class="col-md-12 columns">
                                                    <br>
                                                        <span class="small"> Don't have an account? <a href="<?php echo base_url(); ?>index.php/home/signup">Signup</a></span>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
