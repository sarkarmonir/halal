       <!--++  ***** Chef text ***** ++-->
        <section class="chef" id="chef">

            <div class="container">

                <div class="row">

                    <div class="col-md-4">
                        <h1 class=""><?=$wlc->title1?> <span class="extra-bold"><?=$wlc->title2?></span></h1>
                        <p class="">
                            <?=$wlc->desc?>
                        </p>
                        <img src="<?=cdn()?>uploads/wlc/<?=$wlc->img?>" class="img-responsive" alt="">
                        <br>
                        <a class="btn btn-primary hvr-shutter-out-horizontal" href="#">Read more...</a>
                        <br>
                    </div>

                    <div class="col-md-8">
                        <div class="sec-meat">
                            <br>
                            <div class="row">
                                <?php foreach ($allcategories as $category ){ ?>
                                <div class="col-md-6">
                                    <div class="meats">
                                        <a href="<?php echo base_url() ?>/category/<?php echo $category->id.'-'.$category->name; ?>">
                                        <img src="<?php echo cdn(); ?>category/<?php echo $category->image; ?>" alt="">
                                        <h3><?php echo $category->name; ?> <span class="text-right">Started-$<?php  echo  min_product_price($category->id); ?></span></h3>
                                        </a>
                                        <a class="hide-btn btn btn-primary hvr-shutter-out-horizontal" href="<?php echo base_url(); ?>/category/<?php echo $category->id.'-'.$category->name; ?>">Order now</a>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                            
                            
                        </div>
                    </div><!-- /.chef-text -->

                </div><!-- /.row -->

            </div><!-- /.container -->

        </section><!-- /.chef -->

        <div class="shop" id="shop">

            <div class="container">


                <div class="col-md-12 products-items-list">

                    <div class="row">
                        <h2>Special Product</h2>
                    </div>

                    <div class="row">

                        <ul class="shop-items">
                            <?php foreach ($feature_products as $fproduct){ ?>

                            <li class="col-lg-3 col-md-4 col-sm-6 item product hvr-float-shadow">

                                <div class="container-inner clearfix">

                                    <figure>
                                        <img src="<?php echo cdn(); ?>products/<?php echo $fproduct->image; ?>" class="img-responsive" alt="<?php echo $fproduct->name; ?>">
                                    </figure>

                                    <?php if($fproduct->product_status !=''){ ?><span class="ribbon ribbon-danger"><?php echo $fproduct->product_status; ?></span><?php } ?>

                                    <div class="text-center product-detail">

                                        <h2 class="product-name"><a href="<?php echo base_url(); ?>details/<?php echo $fproduct->id.'-'.make_alias($fproduct->name); ?>"><?php echo $fproduct->name; ?></a></h2>

                                        <div class="price-box"><span class="product-price">$<?php echo $fproduct->price; ?></span><?php if($fproduct->discount_apply == 1){ ?><del>$<?php echo $fproduct->sale_price; ?></del><?php  } ?></div>

                                        <a href="javascript:void(0)" class="btn btn-primary hvr-shutter-out-horizontal" onclick="halal.addToCart(<?php echo $fproduct->id; ?>)"><i class="fa fa-shopping-cart"></i><img class="wait_cart" id="wait_<?php echo $fproduct->id; ?>" style="display:none;" src="<?php echo base_url(); ?>/assets/front/img/loading.gif" /> Add to cart</a>

                                    </div>

                                </div>

                            </li><!-- /.item -->
                            <?php  } ?>

                           

                        </ul>

                    </div><!-- /.row -->


                </div><!-- /.product-item-list -->

            </div><!-- /.container -->

        </div>


        <!-- ***** Reservation ***** -->
        <section id="reservation" class="farm-sec">

            <div class="overlay overlay-black-60"></div>

            <div class="container container-inner">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="">Our<span class="extra-bold"><?=$last_farm->title?></span></h1>
                        <p class="light-gray-txt"><?=$last_farm->desc?></p>
                        <img src="<?=base_url()?>admin.halal/uploads/farm/<?=$last_farm->img?>" class="img-responsive" alt="Pistacia Restaurant And Food Template">
                        <br>
                        <a class="btn btn-primary hvr-shutter-out-horizontal" href="<?=base_url()?>home/farm">Read more...</a>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-6">
                        <div class="farm-img">
                        <?php foreach($farm_gallerys as $farm_gallery):  ?>
                            <img class="img-responsive" src="<?=base_url()?>admin.halal/uploads/farm_gallery/<?=$farm_gallery->img?>" >
                        <?php endforeach; ?>   
                        </div>
                    </div>
                </div>
            </div><!-- /.container -->

        </section><!-- /#reservation -->

        <!-- ***** Offer owl ***** -->
        <section class="we-can-offer" id="weCanOffer">

            <div class="container">
 
                <div class="row">

                    <div class="col-md-6">

                        <h1 class="text-center"><span class="light">We can </span> <span class="extra-bold"><?=$last_offer->title?></span></h1>
                        <div class="zigzag"><i class="fa fa-bell"></i></div>
                        <p class="lead"><?=$last_offer->desc?></p>

                    </div>

                    <div class="col-md-6">

                        <div class="offer-owl-carousel">

                         <?php foreach($offers as $offer):  ?>
                            <div class="item">
                                <h3><i class="flaticon-circle flaticon-dark flaticon-covered16"></i> <?=$offer->title?></h3>
                                <p><?=$offer->desc?></p>
                                <a href="#" class="btn-more hvr-icon-forward">More</a>
                            </div><!-- /.item -->
                        <?php endforeach; ?>

                        </div><!-- /.offer-owl-carousel -->

                    </div>

                </div><!-- /.row -->

            </div><!-- /.container -->

        </section><!-- /#weCanOffer -->