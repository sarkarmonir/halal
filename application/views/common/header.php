<!-- ***** Header ***** -->
<header class="header shop-header">

    <div class="header-img">
        <div class="overlay overlay-black-70"></div>
        <div class="container container-inner">
            <h1><?=$title?></h1>
        </div>
    </div>

</header>

<!-- ***** Breadcrumb ***** -->
<div class="container-fluid breadcrumb-container">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>">Home</a></li>
            <li class="active"><?=$title?></li>
        </ol>
    </div>
</div>