
        <!-- ***** Footer ***** -->
        <footer id="footer" class="footer">

            <div class="overlay overlay-black-60"></div>

            <div class="container container-inner">

                <div class="row">

                    <div class="col-md-3">
                        <h2><span class="extra-bold">Other</span> <span class="light">Said</span></h2>
                        
                        <div id="carouselOthersaid" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner" role="listbox">
                                
                                <?php $x = 0; foreach($testimonials as $testimonial): $x++; ?>
                                <div class="item <?php if($x == 1){echo 'active';} ?>">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <figure class="figure-hubba figure-circle">
                                                <img class="img-responsive" src="<?=base_url()?>assets/front/img/content/thumb10.jpg" alt="">
                                                <div class="overlay overlay-black-70"></div>
                                                <figcaption class="figcaption">
                                                    <a class="icon"><i class="fa fa-search"></i></a>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="col-sm-8">
                                            <i class="fa fa-quote-left"></i>
                                            <?=$testimonial->cli_comment?><i class="fa fa-quote-right"></i>
                                            <br><small><?=$testimonial->cli_name?></small>
                                        </div>
                                    </div>
                                </div><!-- /.item -->
                                <?php endforeach; ?> 

                            </div><!-- /.carousel-inner -->

                        </div><!-- /.carousel-othersaid -->

                    </div><!-- /.-col-md-3 -->

                    <div class="col-md-3">

                        <h2><span class="extra-bold">Operation</span> <span class="light">Time</span></h2>

                        <ul class="contacts">
                            <li><i class="fa fa-clock-o"></i> <?=$web_set->opera_desc?></span></li>
                        </ul>

                    </div><!-- /.col-md-3 -->

                    <div class="col-md-3">

                        <h2><span class="extra-bold">Contact</span> <span class="light">Us</span></h2>

                        <ul class="contacts">
                            <li><i class="fa fa-map-marker"></i>00 St, NY, USA</li>
                            <li><i class="fa fa-phone"></i>+5 123-456-7980</li>
                            <li><i class="fa fa-envelope"></i><a href="#">contact@domain.com</a></li>
                        </ul>

                    </div><!-- /.col-md-3 -->

                    <div class="col-md-3 about-us">

                        <h2><span class="extra-bold">About</span> <span class="light">us</span></h2>

                        <div class="">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                        </div>

                    </div><!-- /.about-us -->

                </div><!-- /.row -->

            </div><!-- /.container-inner -->

        </footer>


        <!-- ***** Bottom bar ***** -->
        <section class="bottom-bar" id="bottomBar">

            <a href="#pageTop" class="btn btn-white scroll-to"><i class="fa fa-chevron-up"></i></a>

            <div class="container">

                <div class="pull-left">
                    <div class="padding-top-20"><span class="small">Copyright &copy; 2017</span></div>
                </div>

                <div class="pull-right">
                    <ul class="clear bottom-menu">
                        <li><a href="<?=base_url()?>">Home</a></li>
                        <li><a href="<?=base_url()?>index.php/home/about">About</a></li>
                        <li><a href="<?=base_url()?>index.php/home/privacy">Privacy & Policy</a></li>
                        <li><a href="<?=base_url()?>index.php/home/faq">F.A.Q</a></li>
                    </ul>
                </div>

            </div><!-- /.container -->

        </section><!-- /.bottom-bar -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?=base_url()?>assets/front/js/html5shiv.min.js"></script>
        <script src="<?=base_url()?>assets/front/js/respond.min.js"></script>
        <![endif]-->

        <!-- Scripts -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAO7Mg2Cs1qzo_3jkKkZAKY6jtwIlm41-I"></script>
        <script src="<?=base_url()?>assets/front/js/jquery.js"></script>
        <script src="<?=base_url()?>assets/front/js/jquery.cookie.js"></script>
        <script src="<?=base_url()?>assets/front/js/jquery-ui.js"></script>
        <script src="<?=base_url()?>assets/front/js/bootstrap.js"></script>
        <script src="<?=base_url()?>assets/front/js/smoothscroll.js"></script>
        <script src="<?=base_url()?>assets/front/js/html5shiv.js"></script>
        <script src="<?=base_url()?>assets/front/js/imagesloaded.js"></script>
        <script src="<?=base_url()?>assets/front/js/flexslider.js"></script>
        <script src="<?=base_url()?>assets/front/js/owlcarousel.js"></script>
        <!--<script src="<?=base_url()?>assets/front/js/easing.js"></script>-->
        <script src="<?=base_url()?>assets/front/js/validate.js"></script>
        <script src="<?=base_url()?>assets/front/js/masonry.js"></script>
        <script src="<?=base_url()?>assets/front/js/scrollto.js"></script>
        <script src="<?=base_url()?>assets/front/js/parallax.js"></script>
        <script src="<?=base_url()?>assets/front/js/countup.js"></script>
        <script src="<?=base_url()?>assets/front/js/google-mapster.js"></script>
        <script src="<?=base_url()?>assets/front/js/isotope.js"></script>
        <script src="<?=base_url()?>assets/front/js/qwertyuiop.js"></script>

        <!-- Sticky Sidebar -->
        <script src="<?=base_url()?>assets/front/js/theia-sticky-sidebar.js"></script>
        
        <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=590ed0aac79ff50011470c5f&product=inline-share-buttons"></script>
        <script>
          $(document).ready(function() {
            $('.stk_1, .stk_2, .stk_3')
              .theiaStickySidebar({
                additionalMarginTop: 75
              });
          });
        </script>

        <!-- Application -->
        <script src="<?=base_url()?>assets/front/js/application.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                $(".quick-cart").hide();
                $(".q-cart").click(function(){
                    $(".quick-cart").slideToggle('fast');
                });
            });
        </script>

        <!-- Scripts only this page -->
        <script type="text/javascript">
            APP.init();
            APP.stickynav();
            APP.flexSlider();
            APP.owlCarousel();
            APP.reservationForm();
            APP.createGoogleMap();
            APP.masonry();
            APP.parallax();
            APP.counterNumbers();
            APP.portfolioIsotope();
        </script>

    </body>
</html>