        <!-- ***** Header ***** -->
        <header class="header">

            <!-- ***** Welcome ***** -->
            <div class="welcome-header" id="welcomeHeader">

                <div class="overlay overlay-black-40"></div>

                <div class="welcome-img-slider">
                    <ul class="slides">
                        <li style="background-image:url('<?=base_url()?>assets/front/img/inner-banner-img-7.jpg');"></li>
                        <li style="background-image:url('<?=base_url()?>assets/front/img/inner-banner-img-1.jpg');"></li>
                        <li style="background-image:url('<?=base_url()?>assets/front/img/inner-banner-img-3.jpg');"></li>
                        <li style="background-image:url('<?=base_url()?>assets/front/img/inner-banner-img-6.jpg');"></li>
                        <li style="background-image:url('<?=base_url()?>assets/front/img/inner-banner-img-8.jpg');"></li>
                    </ul>
                </div>

                <div class="welcome-inner">

                    <div class="container">

                        <div class="text-slider welcome-flexslider">

                            <ul class="slides">
                            <?php foreach($sliders as $slider):  ?>
                                <li class="text-center"><span class="slider-txt"><?=$slider->cap_1?></span><span class="extra-bold"><?=$slider->cap_2?></span></li>
                            <?php endforeach; ?>
                            </ul>

                        </div>

                        <div class="text-center">
                            <a href="<?=$web_set->more_link?>" class="btn btn-transparent hvr-shutter-out-horizontal scroll-to center-block">More</a>
                        </div>

                    </div><!-- /.container -->

                </div><!-- /.welcome-inner -->

            </div><!-- /.welcome-header -->

        </header>