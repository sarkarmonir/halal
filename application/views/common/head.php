<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title><?=$title?></title>

        <meta name="keywords" content="halalic,beef,cow,usa beef">
        <meta name="author" content="CloudNext">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/bootstrap.css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/font-awesome.css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/flexslider.css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/socicons.css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/hover.css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/animate.css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/owlcarousel.css" />
        <!-- <link rel="stylesheet" href="<?=base_url()?>assets/front/css/style-red.css" /> -->
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/style-green.css" />
        <link rel="stylesheet" href="<?=base_url()?>assets/front/css/custom.css" />

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,700,400italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,800,900' rel='stylesheet' type='text/css'>

        <!-- Favicons -->
        <link rel="shortcut icon" href="<?=base_url()?>assets/front/img/logo/logo-orange.png" type="image/x-icon"/>
        <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url()?>assets/front/img/logo/logo-orange.png"/>
        <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url()?>assets/front/img/logo/logo-orange.png"/>
        <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url()?>assets/front/img/logo/logo-orange.png"/>
        <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url()?>assets/front/img/logo/logo-orange.png"/>
        <script type="text/javascript">
        var BASE_URL = '<?php echo base_url(); ?>';
        </script>
    </head>
    <body id="pageTop">
        <?php 
$cart = $this->cart->contents();
        $total = 0;
        $subtotal = 0;
        $cart_itemnumber = 0;
        
        foreach($cart as $rowid=>$item){
         $total = $total + $item['price'];
         $subtotal = $subtotal + $item['sale_price'];
         $cart_itemnumber++;
        }
        $total = number_format($total,2);
        $subtotal = number_format($subtotal,2);
        $cart_item_number = $cart_itemnumber;
?>
        <!-- ***** Page loader ***** -->
        <div class="page-loader">
            <div class="loading">
                <div class="loading-spin"></div>
                <span>Loading...</span>
            </div>
        </div><!-- /.page-loader -->


        <!-- ***** Top Navigation ***** -->
        <section id="topNavigation" class="top-navigation">
            <div class="top-nav">
                <div class="container">
                        <span class="Welcome-txt"><?=$web_set->wlc_text?> &nbsp; | &nbsp;</span>
                        <?php if($this->session->userdata('customer_id') != '' && $this->session->userdata('name') != ''){ ?><a href="<?=base_url()?>logout">Logout</a> &nbsp; | &nbsp;
                        <a href="<?=base_url()?>my_account">My Account</a>
                        <?php }else{ ?> <a href="<?=base_url()?>login">Login</a>&nbsp; | &nbsp;<a href="<?=base_url()?>signup">Sign up</a><?php  } ?>
                </div>
            </div>

            <nav class="navbar navbar-default" role="navigation">

                <div class="container container-inner">

                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#top-menu-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="q-cart btn-cart-xs visible-xs" href="#">
                            <i class="fa fa-shopping-cart primary-color"></i>
                            <span class="cart-number"><?php echo $cart_item_number; ?></span>
                        </a>

                        <a class="navbar-brand" href="<?=base_url()?>">
                            <img src="<?=  cdn()?>uploads/logo/<?=$web_set->logo_img?>" class="img-responsive" alt=""></a>

                    </div>

                    <div class="quick-cart">
                        <div class="cart-collaterals">
                            <div class="cart_totals">
                                <!-- <h2>Cart Totals</h2> -->
                                <div class="table-responsive minimart_content">
                                    <table cellspacing="0" class="table">
                                        <thead>
                                            <tr class="cart-subtotal">
                                                <th>Item Name</th>
                                                <th>QTY</th>
                                                <th align="right">Price</th>
                                                <th><i class="fa fa-close"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($cart as $item){ ?>
                                            <tr class="cart-subtotal" class="row_<?php echo $item['rowid']; ?>">
                                                <td><?php echo $item['name']; ?></td>
                                                <td>
                                                    <div class="input-group top-cart-input">
                                                        <input type="text" class="form-control" value="<?php echo $item['qty']; ?>">
                                                        <div class="input-group-btn-vertical">
                                                            <button class="btn btn-primary btn-inc" onclick="halal.qtyplus('<?php echo $item['rowid']; ?>')">
                                                                <i class="fa fa-caret-up"></i>
                                                            </button>
                                                            <button class="btn btn-primary btn-dec" onclick="halal.qtyminus('<?php echo $item['rowid']; ?>')">
                                                                <i class="fa fa-caret-down"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td align="right"><span class="amount">$<?php echo $item['subtotal']; ?></span></td>
                                                <td><a href="javascript:void()" onclick="halal.remove_cart(`<?php echo $item['rowid']; ?>`)"><i class="fa fa-close"></i></a></td>
                                            </tr>
                                            <?php } ?>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="cart-subtotal">
                                                <!-- <th>Subtotal</th>
                                                <td><span class="amount">$<?php echo $subtotal; ?></span></td> -->
                                                <td >Total</td>
                                                <td colspan="2" align="right"><strong><span class="amount">$<?php echo $total; ?></span></strong> </td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="wc-proceed-to-checkout">
                                    <a href="<?=base_url()?>cart" class="btn btn-info alt wc-forward">View all cart</a>
                                    <a href="<?=base_url()?>checkout" class="btn btn-success pull-right alt wc-forward">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="top-menu-collapse">

                        <ul id="top-menu" class="nav navbar-nav navbar-left">
                            <li class="active"><a href="<?= base_url()?>">Home</a></li>
                            <li><a href="<?=base_url('home/about')?>">About us</a></li>
                            <li><a href="<?=base_url('home/order_online')?>">Order online</a></li></li>
                            <li><a href="<?=base_url('home/faq')?>">F.A.Q</a></li>
                            <li><a href="<?=base_url('home/contact')?>">Contact</a></li>
                        </ul><!-- /#top-menu -->

                        <ul id="topIcons" class="nav navbar-nav navbar-right hidden-xs">
                            <li><a href="#">Call to order: <span class="phone-number"><?=$web_set->order_no?></span></a></li>
                            <li>
                                <a href="#" id="desktopSearchButton"><i class="fa fa-search"></i></a>
                            </li>
                            <li>
                                <a href="#" class="q-cart btn-shopping-cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="cart-number"><?php echo $cart_item_number; ?></span>
                                </a>
                            </li>
                        </ul><!-- /#topIcons -->

                        <div class="visible-xs mobile-search">
                            <form action="<?php echo base_url(); ?>search" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search_val" placeholder="Search for product e.g. meet, beef, leg">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            </form>
                        </div><!-- /.mobile-search -->

                    </div><!-- /.top-menu-collapse -->

                    <div class="desktop-search" id="desktopSearch">
                        <form action="<?php echo base_url(); ?>search" method="get">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="desktopSearchClose" type="button"><i class="fa fa-close"></i></button>
                            </span>
                            <input type="text" class="form-control searchField" name="search_val" placeholder="Search for product e.g. meet, beef, leg">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                        </form>
                    </div><!-- /.desktop-search -->

                </div>

            </nav>

        </section><!-- /.top-navigation -->